package com.liaoyin.travel.business.util;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.moble.task.TimedTaskBackView;
import com.liaoyin.travel.entity.task.Task;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 任务相关工具类
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-25 16:01
 */
public class TaskUtil {



    /**
     * @方法名：IspubWeekValueRange
     * @描述： 判断传入的【星期】的参数有效性
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/25 17:41
     */
    public static boolean IspubWeekValueRange(String pubWeek){
        String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        boolean flag = Arrays.asList(weekDays).contains(pubWeek);
        return flag;
    }

    /**
     * @方法名：getForWeeks
     * @描述： 得到时间是【星期X】
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/26 17:31
     */
    public static String getForWeeks(LocalDateTime localDateTime){
        int weekDay = localDateTime.getDayOfWeek().getValue();
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"星期一");
        map.put(2,"星期二");
        map.put(3,"星期三");
        map.put(4,"星期四");
        map.put(5,"星期五");
        map.put(6,"星期六");
        map.put(7,"星期天");
        if(map.containsKey(weekDay)){
            return map.get(weekDay);
        }
        return null;
    }

    public static String getForWeeks(String dates){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date d=null;
        try {
            d=f.parse(dates);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        cal.setTime(d);
        int w=cal.get(Calendar.DAY_OF_WEEK)-1;
        if(w==0){
            w=7;
        }
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"星期一");
        map.put(2,"星期二");
        map.put(3,"星期三");
        map.put(4,"星期四");
        map.put(5,"星期五");
        map.put(6,"星期六");
        map.put(7,"星期天");
        if(map.containsKey(w)){
            return map.get(w);
        }
        return null;
    }

    /**
     * @方法名：weekSortFormat
     * @描述： 传入星期的字符串list，把字符串list转为星期的排序显示
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/29 14:23
     */
    public static String weekSortFormat(List<String> weekList){
        String[] destArrs = {"一","二","三","四","五","六","天"};
        String[] originArrs = new String[weekList.size()];
        weekList.toArray(originArrs);
        String[] tempArrs = new String[originArrs.length];

        int size = 0;
        for(int i=0;i<destArrs.length;i++){
            for(int j=0;j<originArrs.length;j++){
                if(originArrs[j].indexOf(destArrs[i])>-1){
                    tempArrs[size]=originArrs[j];
                    size++;
                    break;
                }
            }
        }
        if(tempArrs.length==7){
            return "周一至周日";
        }
        String result = "周";
        for(String temp:tempArrs){
            System.err.println("tempArrs="+tempArrs);
            System.err.println("temp="+temp);
            result += temp.substring(2,temp.length())+",";
        }
        if(result.indexOf("天")!=-1){
            result = result.replace("天", "日");
        }
        return result.substring(0,result.length()-1);
    }

    /**
     * @方法名：removeDuplicate
     * @描述： list去重
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/30 18:51
     */
    public static List removeDuplicate(List list){
        List listTemp = new ArrayList();
        for(int i=0;i<list.size();i++){
            if(!listTemp.contains(list.get(i))){
                listTemp.add(list.get(i));
            }
        }
        return listTemp;
    }

    /**
     * @方法名：strGetTomorrow
     * @描述： 得到明天的日期字符串
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/30 22:53
     */
    public static String strGetTomorrow(String date) {
        Date today = strToDate(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天
        Date tomorrow = calendar.getTime();
        String result = dateFormat(tomorrow);
        return result;
    }

    /**
     * @方法名：strToDate
     * @描述： 字符串转日期
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/30 22:53
     */
    public static Date strToDate(String str) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(str);
        } catch (ParseException e) {
        }
        return date;
    }

    /**
     * @方法名：dateFormat
     * @描述： 将日期格式化为 yyyy-MM-dd
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/30 22:54
     */
    public static String dateFormat(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    /**
     * @方法名：isNotThereAnyData
     * @描述： 判断是否不能能请求到数据
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/31 11:42
     */
    public static boolean isNotThereAnyData(Integer num, PageInfo<Task> p) {
        Double result = Double.valueOf(p.getTotal())/p.getPageSize()+1;
        System.err.println("总数据量/页大小="+result);
        if(num<result){
            return false;
        }
        return true;
    }

    /**
     * @方法名：isNotThereAnyTimedTaskBackView
     * @描述： 判断能不能够请求到数据
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/12/5 20:56
     */
    public static boolean isNotThereAnyTimedTaskBackView(Integer num, PageInfo<TimedTaskBackView> p) {
        Double result = Double.valueOf(p.getTotal())/p.getPageSize()+1;
        System.err.println("总数据量/页大小="+result);
        if(num<result){
            return false;
        }
        return true;
    }
}
