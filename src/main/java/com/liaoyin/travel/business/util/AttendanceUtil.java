package com.liaoyin.travel.business.util;

import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.service.UsersService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 考勤相关工具类
 *
 * @author kuang.jiazhuo
 * @date 2019-11-24 17:09
 */
@Component
public class AttendanceUtil {

    @Resource
    private UsersService usersService;

    public static AttendanceUtil attendanceUtil;

    @PostConstruct
    public void init() {
        attendanceUtil = this;
    }

    /**
     * @方法名：judgeAskForLeaveIsAllDay
     * @描述： 传入请假时长判断请假是不是整天
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/24 17:10
     */
    public static boolean judgeAskForLeaveIsAllDay(double duration){
        int halfDayCount = new Double(duration/0.5).intValue();
        if (halfDayCount % 2 != 0) {
            return false;
        }
        return true;
    }

    /**
     * @方法名：parseDateStr
     * @描述： 把日期字符串格式化成 yyyy-MM-DD【比如:2019-8-5 格式化为 2019-08-05】
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/12/2 11:32
     */
    public static String parseDateStr(String str){
        String result = "";
        String[] strArray = str.split("-");
        String month = strArray[1];
        String day = strArray[2];

        if(month.length()<2){
            month = addLetter('0',0,month);
        }
        if(day.length()<2){
            day = addLetter('0',0,day);
        }
        result = strArray[0]+"-"+month+"-"+day;
        return result;
    }

    /**
     * @方法名：addLetter
     * @描述： 往字符串的指定位置插入字符
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/12/2 11:41
     */
    public static String addLetter(char letter, int position, String word){
        char[] wordCharArray = word.toCharArray();
        char[]newWord = new char[wordCharArray.length+1];

        if(position == 0){
            for(int i = position+1; i<wordCharArray.length+1; i++){
                newWord[i] = wordCharArray[i-1];
            }
            newWord[position] = letter;
        }else{

        }
        return new String(newWord);
    }


    /**
     * @方法名：isNotAdministrators
     * @描述： 判断是否 非管理端用户
     * @作者： kjz
     * @日期： Created in 2020/4/7 19:29
     */
    public static boolean isNotAdministrators(String approvedBy) {
        Users users = attendanceUtil.usersService.selectUsersById(approvedBy);
        if(users.getAssumeOffice()==2){
            return true;
        }
        return false;
    }
}
