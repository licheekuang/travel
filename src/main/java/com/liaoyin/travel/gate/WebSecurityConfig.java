package com.liaoyin.travel.gate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private GateUserDetailsServicempl userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/swagger").permitAll()
                .antMatchers("/v2").permitAll()
                .antMatchers("/api").permitAll()
                .antMatchers("/webjars").permitAll()
                .antMatchers("/file").permitAll()              
                .antMatchers("/dictBasic/**").permitAll()
                .antMatchers("/sUsers/**").permitAll()
                .antMatchers("/menu/**").permitAll()
                .antMatchers("/role/**").permitAll()
                .antMatchers("/websocket").permitAll()
                .antMatchers("/moble/**").permitAll()
                .antMatchers("/back/**").permitAll()
                .antMatchers("/usersManagement").permitAll()
                .antMatchers("/externalApi/**").permitAll()
                .antMatchers("/all/**").permitAll()
            .and()
            .authorizeRequests()
            .antMatchers("/**").permitAll() 
           ; 
//                .anyRequest().authenticated()
                

        //由于使用的是JWT，我们这里不需要csrf
        http.csrf().disable();
        // 基于token，所以不需要session
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.headers().frameOptions().disable();
        http.httpBasic();
//         添加JWT filter
//        http.addFilter(new JWTAuthenticationFilter());
        http.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
        // 禁用缓存
        http.headers().cacheControl();
        http.headers().contentTypeOptions().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }
    @Bean
    public JWTAuthenticationFilter authenticationTokenFilterBean() throws Exception {
        return new JWTAuthenticationFilter();
    }
}
