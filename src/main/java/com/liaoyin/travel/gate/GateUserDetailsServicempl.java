package com.liaoyin.travel.gate;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * 实现Spring Security的接口UserDetailsService 提供通过用户名查询用户的实现
 *
 */
@Service
public class GateUserDetailsServicempl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

		//通过user对象封装成security需要的userDetails对象
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

		return new org.springframework.security.core.userdetails.User(userName.split(",")[0], userName.split(",")[1], true, // 是否可用
				true, // 是否过期
				true, // 证书不过期为true
				true, // 账户未锁定为true
				authorities);
//		return  null;
	}
}
