package com.liaoyin.travel.scheduling;

import com.liaoyin.travel.util.ExcelUtil;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 删除文件定时任务
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-04 03:21
 */
@Component
@EnableScheduling
@PropertySource("classpath:com/liaoyin/travel/scheduling/scheduling.properties")
public class FileDeleteSchedule {


    /**
     * @方法名：deleteExcelByAll
     * @描述： 删除excel文件夹下所有的文件
     * @作者： kjz
     * @日期： Created in 2020/3/4 3:56
     */
    @Scheduled(cron = "${scheduling.oneday}")
    public void deleteExcelByAll(){
        ExcelUtil.deleteExcelByAll();
    }
}
