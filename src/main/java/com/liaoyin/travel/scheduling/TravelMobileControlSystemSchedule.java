package com.liaoyin.travel.scheduling;

import com.liaoyin.travel.service.TravelMobileControlSystemService;
import com.liaoyin.travel.service.task.TimingTaskParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 旅游移动管控系统定时任务
 *
 * @author Kuang.JiaZhuo
 * @date 2020-04-02 10:15
 */
@Component
@EnableScheduling
@PropertySource("classpath:com/liaoyin/travel/scheduling/scheduling.properties")
public class TravelMobileControlSystemSchedule {
    
    @Autowired
    TravelMobileControlSystemService travelMobileControlSystemService;

    /**
     * @方法名：callTravelMobileControl
     * @描述： 旅游移动数据管控数据对接(每10分钟发一次)
     * @作者： kjz
     * @日期： Created in 2020/4/2 10:26
     */
    @Scheduled(cron = "${travel.mobile}")
    public void callTravelMobileControl() {
      travelMobileControlSystemService.callStaffInfoModify();
      travelMobileControlSystemService.callAttendanceAdd();
      travelMobileControlSystemService.callTaskAdd();
    }
}
