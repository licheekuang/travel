package com.liaoyin.travel.socket;

import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/websocket/{userId}")
@Component
public class WebSocketServer {
    //concurrent包的线程安全Set，用来存放每个客户端对应的WebSocket对象。
    private static ConcurrentHashMap<String, WebSocketServer> webSocketSetHash = new ConcurrentHashMap<String, WebSocketServer>();
    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    private String userId;
    /**
     * 连接建立成功调用的方法*/
    @OnOpen
    public void onOpen(Session session,@PathParam("userId")String userId) {
        this.session = session;
        this.userId = userId;
        webSocketSetHash.put(userId, this);//加入map中
        System.out.println(this);
    }
    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        if(!userId.isEmpty()){
            webSocketSetHash.remove(userId);  //从set中删除
        }
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("来自于客户端的消息:"+message);
        //群发消息
       /* for (WebSocketServer item : webSocketSet) {
            try {
                item.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

    /**
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }


    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }
    /**
     * 群发自定义消息
     * */
    public static void sendInfo(String message){
        for (String key : webSocketSetHash.keySet()) {
            try {
                webSocketSetHash.get(key).sendMessage(message);
            } catch (IOException e) {
                continue;
            }
        }
    }
    /**
     * 发送指定消息给用户
     * */
    public static void sendInfoByOrderId(String message,String userId){
            try {
                if(webSocketSetHash.get(userId) != null){
                    webSocketSetHash.get(userId).sendMessage(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
