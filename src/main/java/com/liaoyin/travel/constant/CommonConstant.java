package com.liaoyin.travel.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommonConstant {
	public final static int ROOT = -1;
	public final static int DEFAULT_GROUP_TYPE = 0;
	/**
	 * 权限关联类型
	 */
	public final static String AUTHORITY_TYPE_GROUP = "group";
	/**
	 * 权限资源类型
	 */
	public final static String RESOURCE_TYPE_MENU = "menu";
	public final static String RESOURCE_TYPE_FUN = "fun";
	//AES加密密钥
	public static final String AES_KEY = "ae2f88c2031f470385a4edb16be8eda5";

	public final static String RESOURCE_REQUEST_METHOD_GET = "GET";
	public final static String RESOURCE_REQUEST_METHOD_PUT = "PUT";
	public final static String RESOURCE_REQUEST_METHOD_DELETE = "DELETE";
	public final static String RESOURCE_REQUEST_METHOD_POST = "POST";

	public final static String RESOURCE_ACTION_VISIT = "访问";

	public final static String BOOLEAN_NUMBER_FALSE = "0";

	public final static String BOOLEAN_NUMBER_TRUE = "1";

	//获取文件服务器地址(前缀)
	//@Value("${commonConstant.file.server}")
	public static String FILE_SERVER="";

	//服务器地址
	public final static String SERVER="http://39.104.99.107/www";
	//前端页面地址
	//@Value("${commonConstant.FRONT_ADDRESS}")
	public static String FRONT_ADDRESS;
	
	//socket连接地址ip
	//public static String SOCKET_URL;
	//支付回调地址 TODO url通用
	//public static String NOTIFYURL= "http://120.77.83.200:8088/pay/ali/payBack.json";

	@Value("${gate.file.server}")
	public void setFileServer(String fileServer){
		CommonConstant.FILE_SERVER=fileServer;
	}
	
	/*@Value("${prize.file.frontAddress}")
	public void setFrontAddress(String fileServer){
		CommonConstant.FRONT_ADDRESS=fileServer;
	}*/

	/*@Value("${ali.notify.server}")
	public void setNotifyUrl(String aliNotifyServer){
		CommonConstant.NOTIFYURL="http://"+aliNotifyServer+"/pay/ali/payBack.json";
	}*/

}
