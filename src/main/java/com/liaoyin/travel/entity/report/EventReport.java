package com.liaoyin.travel.entity.report;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.liaoyin.travel.entity.FileUpload;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@ApiModel("事件上报")
@Table(name = "t_event_report")
@Data
public class EventReport {
	
	@Id
    private String id;

    /**
     * 上报人
     */
    @Column(name = "user_id")
    @ApiModelProperty("上报人")
    private String userId;

    /**
     * 事件纬度
     */
    @Column(name = "event_log")
    @ApiModelProperty("事件经度")
    private String eventLog;

    /**
     * 事件经度
     */
    @Column(name = "event_lat")
    @ApiModelProperty("事件纬度")
    private String eventLat;

    /**
     * 设备id
     */
    @Column(name = "equipment_id")
    @ApiModelProperty("设备id")
    private String equipmentId;

    /**
     * 事件描述
     */
    @Column(name = "envet_context")
    @ApiModelProperty("事件描述")
    private String envetContext;

    /**
     * 事件图片id
     */
    @Column(name = "event_pic_id")
    @ApiModelProperty("事件图片id")
    private String eventPicId;

    /**
     * 事件音频信息id
     */
    @Column(name = "event_auto_id")
    @ApiModelProperty("事件音频信息id")
    private String eventAutoId;

    @Column(name = "event_video_id")
    @ApiModelProperty("事件视频信息id")
    private String eventVideoId;

    /**
     * 事件类型【字典 eventType】
     */
    @Column(name = "event_type")
    @ApiModelProperty("事件类型【字典 eventType】")
    private Short eventType;

    /**
     * 上报时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    @ApiModelProperty("上报时间")
    private Date creatTime;

    /**
     * 是否删除【0：未删除 1：删除】
     */
    @Column(name = "is_delete")
    @ApiModelProperty("是否删除【0：未删除 1：删除】")
    private Short isDelete;

    /**
     * 上报时间进度【字典 eventProgress】
     */
    @Column(name = "event_progress")
    @ApiModelProperty("上报时间进度【字典 eventProgress】")
    private Short eventProgress;

    @Column(name = "scenic_id")
    @ApiModelProperty("景区id")
    private String scenicId;

    public String getEventProgressDisplay() {
        return this.eventProgress !=null ? DictUtil.getDisplay("eventProgress", this.eventProgress+""):null;
    }
    public String getEventTypeDisplay() {
        return this.eventType !=null ? DictUtil.getDisplay("eventType", this.eventType+""):null;
    }

    @Transient
    @ApiModelProperty("上报人昵称")
    private String userNikeName;

    @Transient
    @ApiModelProperty("上报人头像")
    private String userPicPath;
    
    @Transient
    @ApiModelProperty("图片实体")
    private List<FileUpload> picFileUploadList;
    
    @Transient
    @ApiModelProperty("图片列表")
    private List<String> picList;
    
    
    @Transient
    @ApiModelProperty("音频实体")
    private List<FileUpload> audioFileUploadList;

    @Transient
    @ApiModelProperty("音频列表")
    private List<String> audioList;

    @Transient
    @ApiModelProperty("视频实体")
    private List<FileUpload> videoFileUploadList;

    @Transient
    @ApiModelProperty("视频列表")
    private List<String> videoList;

    @Transient
    @ApiModelProperty("事件上报接收人")
    private List<EventUser> eventUsers;

    @Transient
    @ApiModelProperty("距离当前位置的距离")
    private double disparity;
    
}