package com.liaoyin.travel.entity.report;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.entity.FileUpload;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@ApiModel("员工简报")
@Data
@Table(name = "t_user_briefing")
public class UserBriefing {
	
	@Id
    private String id;

    /**
     * 员工id
     */
    @Column(name = "user_id")
    @ApiModelProperty("员工id")
    private String userId;

    /**
     * 简报名称
     */
    @Column(name = "briefing_name")
    @ApiModelProperty("简报名称")
    private String briefingName;

    /**
     * 简报内容
     */
    @Column(name = "briefing_context")
    @ApiModelProperty("简报内容")
    private String briefingContext;

    /**
     * 简报图片信息
     */
    @Column(name = "briefing_pic_id")
    @ApiModelProperty("简报图片信息")
    private String briefingPicId;

    /**
     * 简报音频信息
     */
    @Column(name = "briefing_audio_id")
    @ApiModelProperty("简报音频信息")
    private String briefingAudioId;

    @Column(name = "briefing_video_id")
    @ApiModelProperty("简报视频信息")
    private String briefingVideoId;

    /**
     * 创建时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    @ApiModelProperty("创建时间")
    private Date creatTime;

    /**
     * 是否删除【0：未删除 1：已删除】
     */
    @Column(name = "is_delete")
    @ApiModelProperty("是否删除【0：未删除 1：已删除】")
    private Short isDelete;

    /**
     * 关联任务id
     */
    @Column(name = "task_id")
    @ApiModelProperty("关联任务id")
    private String taskId;

    /**
     * 景区id，员工登陆后后台获得
     */
    @Column(name = "scenic_id")
    private String scenicId;

    @Transient
    @ApiModelProperty("上报人昵称")
    private String userNikeName;

   @Transient
   @ApiModelProperty("图片实体")
   private List<FileUpload> picFileUploadList;
   
   @Transient
   @ApiModelProperty("图片列表")
   private List<String> picList;
   
   
   @Transient
   @ApiModelProperty("音频实体")
   private List<FileUpload> audioFileUploadList;


   @Transient
   @ApiModelProperty("音频列表")
   private List<String> audioList;

    @Transient
    @ApiModelProperty("视频实体")
    private List<FileUpload> videoFileUploadList;


    @Transient
    @ApiModelProperty("视频列表")
    private List<String> videoList;
   
   
   @Transient
   @ApiModelProperty("用户头像")
   private String userHeadPicUrl;

    /**
     * @方法名：getUserHeadPicUrlDisplay
     * @描述： 返回用户带ip的全路径头像地址
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/5 11:36
     */
   public String getUserHeadPicUrlDisplay() {
	   return this.userHeadPicUrl!=null?CommonConstant.FILE_SERVER+this.userHeadPicUrl:null;
   }
   
}