package com.liaoyin.travel.entity.report;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@ApiModel("事件上报接收人")
@Table(name = "t_event_user")
@Data
public class EventUser {
	
	@Id
    private String id;

    /**
     * 事件id
     */
    @Column(name = "event_id")
    @ApiModelProperty("事件id")
    private String eventId;

    
    @Column(name="report_type")
    @ApiModelProperty("上报类型【1:上报部门经理  2：上报管理员】")
    private Short reportType;
    
    /**
     * 事件接受人
     */
    @Column(name = "user_id")
    @ApiModelProperty("事件接受人")
    private String userId;

    /**
     * 创建时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    @ApiModelProperty(" 创建时间")
    private Date creatTime;

    @Column(name = "scenic_id")
    private String scenicId;

	@Transient
    @ApiModelProperty("上报接收人名称")
	private String userName;
}