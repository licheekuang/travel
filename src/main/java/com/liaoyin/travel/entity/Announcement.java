package com.liaoyin.travel.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Accessors(chain = true)
@Table(name = "t_announcement")
@Data
public class Announcement {
    /**
     * 公告表id主键
     */
    @Id
    private String id;

    /**
     * 公告标题
     */
    private String title;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 发布人id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 公告发布时间
     */
    @Column(name = "create_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date createTime;

    /**
     * 1为删除，0为未删除
     */
    @Column(name = "del_flag")
    private Short delFlag;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;


    @Transient
    @ApiModelProperty("发布人昵称")
    private String userName;


}