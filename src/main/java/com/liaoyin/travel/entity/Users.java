package com.liaoyin.travel.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.entity.task.Task;
import com.liaoyin.travel.entity.team.Worker;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@ApiModel(value = "用户实体")
@Table(name = "s_users")
@Data
@Accessors(chain = true)
public class Users {
	
    @Id
    @GeneratedValue(generator = "UUID")
    @ApiModelProperty(value = "id")
    private String id;

    /**
     * 账户
     */
    @ApiModelProperty(value = "账户")
    private String account;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "手机号码")
    private String phone;
    
    /**
     * 盐值
     */
    @ApiModelProperty(value = "盐值")
    private String salt;

    /**
     * 删除标识
     */
    @ApiModelProperty(value = "删除标识")
    @Column(name = "del_flag")
    private Short delFlag;

    /**
     * 用户类型：用户类型（用户类型（1-员工端；2-管理端））
     */
    @ApiModelProperty(value = "用户类型（1-员工端；2-管理端）")
    @Column(name = "user_type")
    private String userType;

    public String getUserTypeDisplay() {
        if(userTypeDisplay==null){
            return this.userType != null ? DictUtil.getDisplay("userType", this.userType+""):null;
        }
       return userTypeDisplay;
    }

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "昵称")
    @Column(name = "nick_name")
    private String nickName;
    
    @ApiModelProperty("部门id")
    @Column(name="team_id")
    private String teamId;
    
    @Column(name="assume_office")
    @ApiModelProperty("担任职务【1：部门经理 2：员工】")
    private Short assumeOffice;

    public String getAssumeOfficeDisplay(){
        return this.assumeOffice != null ?DictUtil.getDisplay("assumeOffice", this.assumeOffice+""):null;
    }

    @ApiModelProperty("工种id")
    @Column(name="work_id")
    private String workId;
    
    @ApiModelProperty(value = "是否启用")
    @Column(name = "is_using")
    private Short isUsing;

    public String getIsUsingDisplay() {
        if(isUsingDisplay==null){
            return this.isUsing!= null && this.isUsing.intValue()==1?"启用":"禁用";
        }
        return isUsingDisplay;
    }

    @ApiModelProperty(value = "用户头像id")
    @Column(name = "head_pic_id")
    private String headPicId;
    
    @ApiModelProperty("性别(1男2女)")
    private Short sex;

    public String getSexDisplay(){return this.sex != null && this.sex.intValue() ==1? "男":"女";}

    @ApiModelProperty("邮箱地址")
    @Column(name = "email_address")
    private String emailAddress;

    @ApiModelProperty("工号")
    @Column(name = "worker_num")
    private String workerNum;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 身份证号
     */
    @Column(name = "identity_card")
    private String identityCard;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 员工类型；关联数据字典【emptype】
     */
    private String emptype;

    @ApiModelProperty("机构类型【关联数据库organType字段】")
    @Column(name = "organ_type")
    private String organType;

    @ApiModelProperty("机构类型Display")
    public String getOrganTypeDisplay(){
        if(organType!=null && !organType.equals("")){
            return DictUtil.getDisplay("organType",organType);
        }
        return null;
    }

    public String getEmptypeDisplay(){
        return this.emptype != null ?DictUtil.getDisplay("emptype", this.emptype+""):null;
    }

    @Transient
    private FileUpload fileUpload;
    
    @Transient
    private String headUrl;

    @Transient
    @ApiModelProperty("纬度")
    private String lat;

    @Transient
    @ApiModelProperty("经度")
    private String log;

    /*@Transient
    @ApiModelProperty("部门信息")
    private Team team;*/

    @Transient
    @ApiModelProperty("工种信息")
    private Worker worker;

    @Transient
    @ApiModelProperty("任务信息")
    private Task task;
    
    @Transient
    @ApiModelProperty("部门信息")
    private List<UserTeam> userTeamList;

    @Transient
    @ApiModelProperty("原来的部门经理的id")
    private String originalDirectorId;


    /**  做excel导入需要  **/
    @Transient
    private String userTypeDisplay;
    @Transient
    private String isUsingDisplay;
    @Transient
    private String sexDisplay;
    @Transient
    @ApiModelProperty("部门名称")
    private String industryTitle;
    @Transient
    @ApiModelProperty("工种名字")
    private String workName;

}