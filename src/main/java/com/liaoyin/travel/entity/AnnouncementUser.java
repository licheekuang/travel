package com.liaoyin.travel.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @author 王海洋
 * @className: AnnouncementUser
 * @description:
 * @create 2019/11/29 10:31
 **/
@Table(name = "t_announcement_user")
@Data
@Accessors(chain = true)
public class AnnouncementUser {

    @ApiModelProperty("id")
    @Column(name = "id")
    private String id;

    @ApiModelProperty("用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty("公告id")
    @Column(name = "announcement_id")
    private String announcementId;

    @ApiModelProperty("添加时间")
    @Column(name = "create_time")
    private String createTime;
}
