package com.liaoyin.travel.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;

@ApiModel(value = "角色 —— 功能 关联表")
@Table(name = "s_role_func")
public class RoleFunc {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT REPLACE(UUID(),'-','')")
    private String id;

    /*
     * 角色ID
     */
    @Column(name = "ROLE_ID")
    @ApiModelProperty(value = "角色ID")
    private String roleId;

    /*
     * 角色的功能ID
     */
    @Column(name = "FUNC_ID")
    @ApiModelProperty(value = "角色的功能ID")
    private String funcId;

    /**
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取角色ID
     *
     * @return ROLE_ID - 角色ID
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * 设置角色ID
     *
     * @param roleId 角色ID
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     * 获取角色的功能ID
     *
     * @return FUNC_ID - 角色的功能ID
     */
    public String getFuncId() {
        return funcId;
    }

    /**
     * 设置角色的功能ID
     *
     * @param funcId 角色的功能ID
     */
    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }
}