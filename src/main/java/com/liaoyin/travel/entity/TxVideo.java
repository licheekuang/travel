package com.liaoyin.travel.entity;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import javax.persistence.*;

import com.alibaba.fastjson.annotation.JSONField;

@Table(name = "tx_video")
public class TxVideo {
    /**
     * 主键
     */
    @Id
    @ApiModelProperty("主键")
    private String id;

    /**
     * 直播名称
     */
    @ApiModelProperty("名称")
    @Column(name = "video_name")
    private String videoName;

    /**
     * 娃娃机对应腾讯服务视频ID
     */
    @ApiModelProperty("对应腾讯服务视频ID")
    @Column(name = "video_id")
    private String videoId;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Short status;

    /**
     * 是否使用水印
     */
    @ApiModelProperty("是否使用水印")
    private Boolean watemark;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 视频URL
     */
    @ApiModelProperty("视频URL")
    private String url;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取直播名称
     *
     * @return video_name - 直播名称
     */
    public String getVideoName() {
        return videoName;
    }

    /**
     * 设置直播名称
     *
     * @param videoName 直播名称
     */
    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    /**
     * 获取娃娃机对应腾讯服务视频ID
     *
     * @return video_id - 娃娃机对应腾讯服务视频ID
     */
    public String getVideoId() {
        return videoId;
    }

    /**
     * 设置娃娃机对应腾讯服务视频ID
     *
     * @param videoId 娃娃机对应腾讯服务视频ID
     */
    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    /**
     * 获取状态
     *
     * @return status - 状态
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 获取是否使用水印
     *
     * @return watemark - 是否使用水印
     */
    public Boolean getWatemark() {
        return watemark;
    }

    /**
     * 设置是否使用水印
     *
     * @param watemark 是否使用水印
     */
    public void setWatemark(Boolean watemark) {
        this.watemark = watemark;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取视频URL
     *
     * @return url - 视频URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置视频URL
     *
     * @param url 视频URL
     */
    public void setUrl(String url) {
        this.url = url;
    }
}