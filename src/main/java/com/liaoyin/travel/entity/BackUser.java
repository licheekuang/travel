package com.liaoyin.travel.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.view.moble.back.MenuView;
import com.liaoyin.travel.base.entity.Page;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@ApiModel(value = "后台用户实体")
@Table(name = "t_back_user")
@Data
@Accessors(chain = true)
public class BackUser extends Page {

    @Id
    @GeneratedValue(generator = "UUID")
    @ApiModelProperty(value = "id")
    private String id;

    /**
     * 账户
     */
    @ApiModelProperty(value = "账户")
    private String account;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 删除标识
     */
    @ApiModelProperty(value = "删除标识")
    @Column(name = "is_delete")
    private Short isDelete;

    /**
     * 用户类型：用户类型（1-管理员  2:部门经理）
     */
    @ApiModelProperty(value = "用户类型【1:景区员工端;2:景区管理端;3:超级管理员;4:文旅委管理员】")
    @Column(name = "user_type")
    private String userType;

    @ApiModelProperty("绑定的经理Id")
    @Column(name = "business_id")
    private String businessId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "昵称")
    @Column(name = "nick_name")
    private String nickName;
    
    @ApiModelProperty(value = "角色id")
    @Column(name = "role_id")
    private String roleId;

    @ApiModelProperty(value = "是否启用")
    @Column(name = "is_using")
    private Short isUsing;

    @ApiModelProperty(value = "身份证")
    @Column(name = "identity_card")
    private String identityCard;

    @ApiModelProperty(value = "联系电话")
    private String phone;

    @ApiModelProperty(value = "性别【1:男;2:女】")
    private String gender;

    @ApiModelProperty(value = "用于显示在页面的密码")
    private String cipher;

    @ApiModelProperty(value = "景区id")
    @Column(name = "scenic_id")
    private String scenicId;

    @ApiModelProperty(value = "账号类型(1:景区;2:机构)")
    @Column(name = "account_type")
    private String accountType;

    @ApiModelProperty("账号类型Display")
    public String getAccountTypeDisplay(){
        if(accountType!=null && !accountType.equals("")){
            return DictUtil.getDisplay("accountType",accountType);
        }
        return null;
    }

    @ApiModelProperty("机构类型【关联数据库organType字段】")
    @Column(name = "organ_type")
    private String organType;

    @ApiModelProperty("机构类型Display")
    public String getOrganTypeDisplay(){
        if(organType!=null && !organType.equals("")){
            return DictUtil.getDisplay("organType",organType);
        }
        return null;
    }

    @ApiModelProperty("角色名")
    @Transient
    private String roleName;
    
    public String getUserTypeDisplay() {
    	if(this.userType !=null) {
    		switch (this.userType) {
			case "1":
				return "管理员";
			case "2":
				return "部门经理";			
			}
    	}
    	
    	return null;
    }
    
    public String getIsUsingDisplay() {
    	return this.isUsing!= null && this.isUsing.intValue()==1?"启用":"禁用";
    }
    
    @ApiModelProperty("头像")
    @Transient
    private String userHeadPicUrl;
  
    @Transient
    private FileUpload fileUpload;

    @Transient
    private String token;

    @Transient
    private List<MenuView> menuList;
}