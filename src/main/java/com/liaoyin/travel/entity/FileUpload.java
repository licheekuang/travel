package com.liaoyin.travel.entity;

import java.util.Date;
import javax.persistence.*;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Table(name = "s_file_upload")
public class FileUpload {
	
    @Id
    @ApiModelProperty("id")
    private String id;

    /**
     * 业务ID
     */
    @ApiModelProperty("业务ID")
    @Column(name = "business_id")
    private String businessId;

    /**
     * 文件名
     */
    @ApiModelProperty("文件名")
    @Column(name = "file_name")
    private String fileName;

    /**
     * 原文件名
     */
    @ApiModelProperty("原文件名")
    @Column(name = "original_file_name")
    private String originalFileName;

    /**
     * 文件路径
     */
    @ApiModelProperty("文件路径")
    @Column(name = "file_path")
    private String filePath;

    /**
     * 文件类型
     */
    @ApiModelProperty("文件类型")
    @Column(name = "file_type")
    private String fileType;

    /**
     * 文件大小(KB)
     */
    @ApiModelProperty("文件大小(KB)")
    @Column(name = "file_size")
    private String fileSize;

    /**
     * 上传时间
     */
    @ApiModelProperty("上传时间")
    @Column(name = "create_time")
    private Date createTime;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取业务ID
     *
     * @return business_id - 业务ID
     */
    public String getBusinessId() {
        return businessId;
    }

    /**
     * 设置业务ID
     *
     * @param businessId 业务ID
     */
    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    /**
     * 获取文件名
     *
     * @return file_name - 文件名
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置文件名
     *
     * @param fileName 文件名
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 获取原文件名
     *
     * @return original_file_name - 原文件名
     */
    public String getOriginalFileName() {
        return originalFileName;
    }

    /**
     * 设置原文件名
     *
     * @param originalFileName 原文件名
     */
    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    /**
     * 获取文件路径
     *
     * @return file_path - 文件路径
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * 设置文件路径
     *
     * @param filePath 文件路径
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * 获取文件类型
     *
     * @return file_type - 文件类型
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * 设置文件类型
     *
     * @param fileType 文件类型
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * 获取文件大小(KB)
     *
     * @return file_size - 文件大小(KB)
     */
    public String getFileSize() {
        return fileSize;
    }

    /**
     * 设置文件大小(KB)
     *
     * @param fileSize 文件大小(KB)
     */
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * 获取上传时间
     *
     * @return create_time - 上传时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置上传时间
     *
     * @param createTime 上传时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    /**
     * 图片绝对路径
     */
    @Transient
    @Getter
    @Setter
    private String url;
}