package com.liaoyin.travel.entity.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.view.user.UserView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;


@Table(name = "t_attendance_management")
@Data
@Accessors(chain = true)
@ApiModel("考勤管理设置")
public class AttendanceManagement {

    private String id;

    @Column(name = "checking_in_name")
    @ApiModelProperty("考勤名称")
    private String checkingInName;

    @ApiModelProperty("考勤开始时间")
    @JSONField(format="HH:mm")
    @Column(name = "attendance_start_time")
    private LocalTime attendanceStartTime;

    @ApiModelProperty("考勤结束时间")
    @JSONField(format="HH:mm")
    @Column(name = "attendance_end_time")
    private LocalTime attendanceEndTime;

    @ApiModelProperty("设备表的id")
    @Column(name = "equipment_id")
    private String equipmentId;

    @ApiModelProperty("考勤范围(允许偏差的距离)")
    @Column(name = "attendance_range")
    private Double attendanceRange;

    @Column(name = "cteate_by_id")
    @ApiModelProperty("创建人id")
    private String cteateById;

    @Column(name = "create_time")
    @ApiModelProperty("创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("最新更新人id")
    @Column(name = "update_by_id")
    private String updateById;

    @ApiModelProperty("最新更新时间")
    @Column(name = "update_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @ApiModelProperty("是否启用(0:否;1:是)")
    @Column(name = "is_using")
    private String isUsing;

    @ApiModelProperty("是否删除(0:否;1:是)")
    @Column(name = "is_delete")
    private String isDelete;

    @ApiModelProperty("班次名(没有这个概念,但是为了考虑以后的扩展,多加了这个字段)")
    @Column(name = "classes")
    private String classes;

    @ApiModelProperty("考勤工作日字符串集")
    @Column(name = "attendance_days")
    private String attendanceDays;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId ;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /*******************  非数据库字段 ********************/

    @Transient
    @ApiModelProperty("参与考勤人员的id字符串集")
    private String participantId;

    @Transient
    @ApiModelProperty("参与考勤人员")
    private List<UserView> attendancePersonnel;

}