package com.liaoyin.travel.entity.attendance;

import java.util.Date;
import javax.persistence.*;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.liaoyin.travel.util.DictUtil;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("考勤设置")
@Table(name = "t_attendance_settings")
public class AttendanceSettings {
	
	@Id
    private String id;

    /**
     * 考勤开始时间
     */
    @Column(name = "check_start_time")
    @JSONField(format="HH:mm:ss")
    //@JsonFormat(pattern="HH:mm:ss",timezone="GMT+8")
    private Date checkStartTime;

    /**
     * 考勤结束时间
     */
    @Column(name = "check_end_time")
    @JSONField(format="HH:mm:ss")
    //@JsonFormat(pattern="HH:mm:ss",timezone="GMT+8")
    private Date checkEndTime;

    /**
     * 考勤设备id
     */
    @Column(name = "equipment_id")
    private String equipmentId;

    /**
     * 距离【允许偏差的距离】
     */
    private Integer distance;

    /**
     * 名称
     */
    @Column(name = "check_name")
    private String checkName;

    /**
     * 设置人id
     */
    @Column(name = "creat_user_id")
    private String creatUserId;

    /**
     * 设置时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date creatTime;

    /**
     * 修改人id
     */
    @Column(name = "update_user_id")
    private String updateUserId;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date updateTime;

    /**
     * 删除人id
     */
    @Column(name = "delete_user_id")
    private String deleteUserId;

    /**
     * 删除时间
     */
    @Column(name = "delete_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date deleteTime;

    /**
     * 是否删除【0：未删除 2：已删除】
     */
    @Column(name = "is_delete")
    private Short isDelete;

    /**
     * 是否启用【0：禁用 1：启用】
     */
    @Column(name = "is_using")
    private Short isUsing;

    /**
     * 打卡班次[字典 【checkShifts】1,2,3,4]
     */
    @Column(name = "check_shifts")
    private Short checkShifts;
    
    @Column(name="team_id")
    @ApiModelProperty("班组id")
    private String teamId;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取考勤开始时间
     *
     * @return check_start_time - 考勤开始时间
     */
    public Date getCheckStartTime() {
        return checkStartTime;
    }

    /**
     * 设置考勤开始时间
     *
     * @param checkStartTime 考勤开始时间
     */
    public void setCheckStartTime(Date checkStartTime) {
        this.checkStartTime = checkStartTime;
    }

    /**
     * 获取考勤结束时间
     *
     * @return check_end_time - 考勤结束时间
     */
    public Date getCheckEndTime() {
        return checkEndTime;
    }

    /**
     * 设置考勤结束时间
     *
     * @param checkEndTime 考勤结束时间
     */
    public void setCheckEndTime(Date checkEndTime) {
        this.checkEndTime = checkEndTime;
    }

    /**
     * 获取考勤设备id
     *
     * @return equipment_id - 考勤设备id
     */
    public String getEquipmentId() {
        return equipmentId;
    }

    /**
     * 设置考勤设备id
     *
     * @param equipmentId 考勤设备id
     */
    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    /**
     * 获取距离【允许偏差的距离】
     *
     * @return distance - 距离【允许偏差的距离】
     */
    public Integer getDistance() {
        return distance;
    }

    /**
     * 设置距离【允许偏差的距离】
     *
     * @param distance 距离【允许偏差的距离】
     */
    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    /**
     * 获取名称
     *
     * @return check_name - 名称
     */
    public String getCheckName() {
        return checkName;
    }

    /**
     * 设置名称
     *
     * @param checkName 名称
     */
    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    /**
     * 获取设置人id
     *
     * @return creat_user_id - 设置人id
     */
    public String getCreatUserId() {
        return creatUserId;
    }

    /**
     * 设置设置人id
     *
     * @param creatUserId 设置人id
     */
    public void setCreatUserId(String creatUserId) {
        this.creatUserId = creatUserId;
    }

    /**
     * 获取设置时间
     *
     * @return creat_time - 设置时间
     */
    public Date getCreatTime() {
        return creatTime;
    }

    /**
     * 设置设置时间
     *
     * @param creatTime 设置时间
     */
    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    /**
     * 获取修改人id
     *
     * @return update_user_id - 修改人id
     */
    public String getUpdateUserId() {
        return updateUserId;
    }

    /**
     * 设置修改人id
     *
     * @param updateUserId 修改人id
     */
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取删除人id
     *
     * @return delete_user_id - 删除人id
     */
    public String getDeleteUserId() {
        return deleteUserId;
    }

    /**
     * 设置删除人id
     *
     * @param deleteUserId 删除人id
     */
    public void setDeleteUserId(String deleteUserId) {
        this.deleteUserId = deleteUserId;
    }

    /**
     * 获取删除时间
     *
     * @return delete_time - 删除时间
     */
    public Date getDeleteTime() {
        return deleteTime;
    }

    /**
     * 设置删除时间
     *
     * @param deleteTime 删除时间
     */
    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    /**
     * 获取是否删除【0：未删除 2：已删除】
     *
     * @return is_delete - 是否删除【0：未删除 2：已删除】
     */
    public Short getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除【0：未删除 2：已删除】
     *
     * @param isDelete 是否删除【0：未删除 2：已删除】
     */
    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取是否启用【0：禁用 1：启用】
     *
     * @return is_using - 是否启用【0：禁用 1：启用】
     */
    public Short getIsUsing() {
        return isUsing;
    }

    /**
     * 设置是否启用【0：禁用 1：启用】
     *
     * @param isUsing 是否启用【0：禁用 1：启用】
     */
    public void setIsUsing(Short isUsing) {
        this.isUsing = isUsing;
    }

    /**
     * 获取打卡班次[1,2,3,4]
     *
     * @return check_shifts - 打卡班次[1,2,3,4]
     */
    public Short getCheckShifts() {
        return checkShifts;
    }

    /**
     * 设置打卡班次[1,2,3,4]
     *
     * @param checkShifts 打卡班次[1,2,3,4]
     */
    public void setCheckShifts(Short checkShifts) {
        this.checkShifts = checkShifts;
    }
    
    
    public String getCheckShiftsDisplay() {
    	return this.checkShifts!=null?DictUtil.getDisplay("checkShifts",this.checkShifts+""):null;
    }

    public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}



	@Transient
    @Getter
    @Setter
    @ApiModelProperty("设备名称")
    private String equipmentName;
	
	@Transient
    @Getter
    @Setter
	private String lat;
	
	@Transient
    @Getter
    @Setter
	private String log;
    
}