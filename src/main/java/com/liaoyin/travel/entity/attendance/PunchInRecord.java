package com.liaoyin.travel.entity.attendance;

import java.util.Date;
import javax.persistence.*;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("打卡记录")
@Table(name = "t_punch_in_record")
public class PunchInRecord {
	
	@Id
    private String id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 打卡时间
     */
    @Column(name = "check_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date checkTime;

    /**
     * 打卡经度
     */
    private String lat;

    /**
     * 打卡纬度
     */
    private String log;

    /**
     * 偏离的位置距离
     */
    private Integer distance;

    /**
     * 打卡设备id
     */
    @Column(name = "equipment_id")
    private String equipmentId;

    @Column(name="attendance_settings_id")
    @ApiModelProperty("考勤设置id")
    private String attendanceSettingsId;
    
    /**
     * 打卡班次[1,2,3,4]
     */
    @Column(name = "check_shifts")
    private Short checkShifts;

    /**
     * 打卡ip
     */
    @Column(name = "check_ip")
    private String checkIp;

    /**
     * 打卡设备编号【用户手机编号】
     */
    @Column(name = "check_code")
    private String checkCode;

    /**
     * 打卡连接的wifi名称
     */
    @Column(name = "check_wifi_name")
    private String checkWifiName;

    /**
     * 打卡连接的WiFi ip
     */
    @Column(name = "check_wifi_ip")
    private String checkWifiIp;

    /**
     * 打卡连接的wifi mac码
     */
    @Column(name = "check_wifi_mac")
    private String checkWifiMac;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取打卡时间
     *
     * @return check_time - 打卡时间
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * 设置打卡时间
     *
     * @param checkTime 打卡时间
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * 获取打开经度
     *
     * @return lat - 打开经度
     */
    public String getLat() {
        return lat;
    }

    /**
     * 设置打开经度
     *
     * @param lat 打开经度
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * 获取打卡纬度
     *
     * @return log - 打卡纬度
     */
    public String getLog() {
        return log;
    }

    /**
     * 设置打卡纬度
     *
     * @param log 打卡纬度
     */
    public void setLog(String log) {
        this.log = log;
    }

    /**
     * 获取偏离的位置距离
     *
     * @return distance - 偏离的位置距离
     */
    public Integer getDistance() {
        return distance;
    }

    /**
     * 设置偏离的位置距离
     *
     * @param distance 偏离的位置距离
     */
    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    /**
     * 获取打卡设备id
     *
     * @return equipment_id - 打卡设备id
     */
    public String getEquipmentId() {
        return equipmentId;
    }

    /**
     * 设置打卡设备id
     *
     * @param equipmentId 打卡设备id
     */
    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    /**
     * 获取打卡班次[1,2,3,4]
     *
     * @return check_shifts - 打卡班次[1,2,3,4]
     */
    public Short getCheckShifts() {
        return checkShifts;
    }

    /**
     * 设置打卡班次[1,2,3,4]
     *
     * @param checkShifts 打卡班次[1,2,3,4]
     */
    public void setCheckShifts(Short checkShifts) {
        this.checkShifts = checkShifts;
    }

    /**
     * 获取打卡ip
     *
     * @return check_ip - 打卡ip
     */
    public String getCheckIp() {
        return checkIp;
    }

    /**
     * 设置打卡ip
     *
     * @param checkIp 打卡ip
     */
    public void setCheckIp(String checkIp) {
        this.checkIp = checkIp;
    }

    /**
     * 获取打卡设备编号【用户手机编号】
     *
     * @return check_code - 打卡设备编号【用户手机编号】
     */
    public String getCheckCode() {
        return checkCode;
    }

    /**
     * 设置打卡设备编号【用户手机编号】
     *
     * @param checkCode 打卡设备编号【用户手机编号】
     */
    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    /**
     * 获取打卡连接的wifi名称
     *
     * @return check_wifi_name - 打卡连接的wifi名称
     */
    public String getCheckWifiName() {
        return checkWifiName;
    }

    /**
     * 设置打卡连接的wifi名称
     *
     * @param checkWifiName 打卡连接的wifi名称
     */
    public void setCheckWifiName(String checkWifiName) {
        this.checkWifiName = checkWifiName;
    }

    /**
     * 获取打卡连接的WiFi ip
     *
     * @return check_wifi_ip - 打卡连接的WiFi ip
     */
    public String getCheckWifiIp() {
        return checkWifiIp;
    }

    /**
     * 设置打卡连接的WiFi ip
     *
     * @param checkWifiIp 打卡连接的WiFi ip
     */
    public void setCheckWifiIp(String checkWifiIp) {
        this.checkWifiIp = checkWifiIp;
    }

    /**
     * 获取打卡连接的wifi mac码
     *
     * @return check_wifi_mac - 打卡连接的wifi mac码
     */
    public String getCheckWifiMac() {
        return checkWifiMac;
    }

    /**
     * 设置打卡连接的wifi mac码
     *
     * @param checkWifiMac 打卡连接的wifi mac码
     */
    public void setCheckWifiMac(String checkWifiMac) {
        this.checkWifiMac = checkWifiMac;
    }

	public String getAttendanceSettingsId() {
		return attendanceSettingsId;
	}

	public void setAttendanceSettingsId(String attendanceSettingsId) {
		this.attendanceSettingsId = attendanceSettingsId;
	}

    public String getCheckShiftsDisplay() {
        return this.checkShifts != null ? DictUtil.getDisplay("checkShifts", this.checkShifts+""):null;
    }
    
	@Transient
	@Getter
	@Setter
	@ApiModelProperty("考勤名称")
	private String checkName;
	
	@Transient
	@Getter
	@Setter
	@ApiModelProperty("设备名称")
	private String equipmentName;

	@Transient
    @Getter
    @Setter
    @ApiModelProperty("用户名称")
    private String userNikeName;
}