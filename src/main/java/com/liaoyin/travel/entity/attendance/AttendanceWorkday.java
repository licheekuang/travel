package com.liaoyin.travel.entity.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_attendance_workday")
@Data
@Accessors(chain = true)
@ApiModel("考勤管理-工作日关联")
public class AttendanceWorkday {
    /**
     * ID
     */
    private String id;

    /**
     * 考勤id
     */
    @Column(name = "attendance_id")
    private String attendanceId;

    /**
     * 星期X
     */
    @Column(name = "week_day")
    private Integer weekDay;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 是否删除(0:未删除;1:删除)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;



}