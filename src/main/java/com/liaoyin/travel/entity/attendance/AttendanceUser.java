package com.liaoyin.travel.entity.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_attendance_user")
@ApiModel("考勤用户关联")
@Accessors(chain = true)
@Data
public class AttendanceUser {

    @Id
    private String id;

    @Column(name = "attendance_id")
    @ApiModelProperty("考勤管理表id")
    private String attendanceId;

    @Column(name = "user_id")
    @ApiModelProperty("用户表id")
    private String userId;

    @Column(name = "creat_time")
    @ApiModelProperty("创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creatTime;

    @Column(name = "is_delete")
    @ApiModelProperty("是否删除:0,否;1,是")
    private String isDelete;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;




}