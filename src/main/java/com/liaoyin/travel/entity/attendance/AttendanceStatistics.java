package com.liaoyin.travel.entity.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@ApiModel("出勤统计")
@Table(name = "t_attendance_statistics")
@Data
public class AttendanceStatistics {
	
	@Id
    private String id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 工作时间
     */
    @Column(name = "work_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date workTime;

    /**
     * 类型【1：出勤 2：休息 3：迟到 4：缺卡 5：旷工】
     */
    @Column(name = "work_type")
    private Short workType;
    
    /**
     * 打卡班次[字典 【checkShifts】1,2,3,4]
     */
    @Column(name = "check_shifts")
    private Short checkShifts;
    
    
    @Column(name = "check_time")
    @JSONField(format="HH:mm:ss")
    @ApiModelProperty("考勤时间")
    private Date checkTime;

    /**
     * 创建时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date creatTime;


	@Transient
    @ApiModelProperty("displayName")
    private String displayName;

    @Transient
    @ApiModelProperty("workType")
    private String dworkType;

    @Transient
    @ApiModelProperty("次数")
    private Integer num;

    
}