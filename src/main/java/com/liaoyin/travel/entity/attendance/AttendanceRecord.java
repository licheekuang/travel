package com.liaoyin.travel.entity.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Table(name = "t_attendance_record")
@Data
@Accessors(chain = true)
@ApiModel("考勤记录实体")
@JsonInclude(value= JsonInclude.Include.NON_NULL)
public class AttendanceRecord {

    @Id
    private String id;

    /**
     * 被考勤的用户id
     */
    @Column(name = "user_id")
    @ApiModelProperty("被考勤的用户id")
    private String userId;

    /**
     * 考勤当天的日期
     */
    @Column(name = "date_of_attendance")
    //@JSONField(format="yyyy-MM-dd")
    @ApiModelProperty("考勤当天的日期")
    private LocalDate dateOfAttendance;

    /**
     * 上班打卡的时间
     */
    @Column(name = "clock_in_time")
    //@JSONField(format="yyyy-MM-dd")
    @ApiModelProperty("上班打卡的时间")
    private LocalTime clockInTime;

    /**
     * 上班打卡的考勤状态【数据字典 attendanceStatus】
     */
    @Column(name = "clock_in_state")
    @ApiModelProperty("上班打卡的考勤状态")
    private String clockInState;

    @ApiModelProperty("上班打卡的考勤状态Display")
    public String getClockInStateDisplay(){
        if(clockInState!=null){
            return DictUtil.getDisplay("attendanceStatus",clockInState);
        }
        return null;
    }

    /**
     * 下班打卡的时间
     */
    @Column(name = "clock_out_time")
    @JSONField(format="HH:mm")
    @ApiModelProperty("下班打卡的时间")
    private LocalTime clockOutTime;

    /**
     * 下班打卡的考勤状态【数据字典 attendanceStatus】
     */
    @Column(name = "clock_out_state")
    @ApiModelProperty("下班打卡的考勤状态")
    private String clockOutState;

    @ApiModelProperty("下班打卡的考勤状态Display")
    public String getClockOutStateDisplay(){
        if(clockOutState!=null){
            return DictUtil.getDisplay("attendanceStatus",clockOutState);
        }
        return null;
    }

    /**
     * 当天的最终考勤状态
     */
    @Column(name = "final_attendance_status")
    @ApiModelProperty("当天的最终考勤状态")
    private String finalAttendanceStatus;

    @ApiModelProperty("当天的最终考勤状态Display")
    public String getFinalAttendanceStatusDisplay(){
        if(finalAttendanceStatus!=null){
            return DictUtil.getDisplay("attendanceStatus",finalAttendanceStatus);
        }
        return null;
    }

    /**
     * 考勤管理表的id
     */
    @Column(name = "attendance_id")
    @ApiModelProperty("考勤管理表的id")
    private String attendanceId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 工时
     */
    @Column(name = "man_hour")
    @ApiModelProperty("工时")
    private String manHour;

    /**
     * 打卡次数
     */
    @Column(name = "clock_frequency")
    @ApiModelProperty("打卡次数")
    private Integer clockFrequency;

    /**
     * 上班打卡的经度
     */
    @Column(name = "chock_in_longitude")
    @ApiModelProperty("上班打卡的经度")
    private String chockInLongitude;

    /**
     * 上班打卡的纬度
     */
    @Column(name = "chock_in_latitude")
    @ApiModelProperty("上班打卡的纬度")
    private String chockInLatitude;

    /**
     * 下班打卡的经度
     */
    @Column(name = "chock_out_longitude")
    @ApiModelProperty("下班打卡的经度")
    private String chockOutLongitude;

    /**
     * 下班打卡的纬度
     */
    @Column(name = "chock_out_latitude")
    @ApiModelProperty("下班打卡的纬度")
    private String chockOutLatitude;

    /**
     * 星期几
     */
    @Column(name = "what_day")
    @ApiModelProperty("星期几")
    private Integer whatDay;

    @ApiModelProperty("外出日期显示")
    @Column(name = "go_out_date_display")
    private String goOutDateDisplay;

    @ApiModelProperty("是否处于外出状态(0:否;1:是)")
    @Column(name = "is_go_out")
    private String isGoOut;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;



}