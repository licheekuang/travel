package com.liaoyin.travel.entity.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel("设备列表")
@Table(name = "t_equipment")
@Data
public class Equipment {
	
	@Id
    private String id;

    /**
     * 设备名称
     */
    @Column(name = "equipment_name")
    @Size(min = 2, max = 25, message = "设备名称长度必须在2到25之间")
    private String equipmentName;

    /**
     * 设备类型【字典【equipmentType】{1：考勤机}】
     */
    @Column(name = "equipment_type")
    private Short equipmentType;

    public String getEquipmentTypeDisplay() {
        return this.equipmentType!=null?DictUtil.getDisplay("equipmentType", this.equipmentType+""):null;
    }

    /**
     * 纬度
     */
    @Column(name = "lat")
    private String lat;

    /**
     * 经度
     */
    @Column(name = "log")
    private String log;
    
    @ApiModelProperty("详细地址")
    @Column(name="equipment_address")
    private String equipmentAddress;
    

    /**
     * 是否删除【0：未删除 1：已删除】
     */
    @Column(name = "is_delete")
    private Short isDelete;

    /**
     * 是否启用【0：禁用 1：启用】
     */
    @Column(name = "is_using")
    private Short isUsing;

    public String getIsUsingDisplay() {
        return this.isUsing!=null?DictUtil.getDisplay("isUsing", this.isUsing+""):null;
    }

    /**
     * 设置人id
     */
    @Column(name = "creat_user_id")
    private String creatUserId;

    /**
     * 设置时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date creatTime;

    @Column(name = "scenic_id")
    private String scenicId;


/*    @Transient
    @Getter
    @Setter
    @ApiModelProperty("图片路径")
    private String picPath;*/

    @Transient
    @ApiModelProperty("距离前端当前位置的距离")
    private double disparity;

}