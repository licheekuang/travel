package com.liaoyin.travel.entity.team;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Date;

@ApiModel("工种列表")
@Table(name = "t_worker")
@Data
@Accessors(chain = true)
public class Worker {
	
	@Id
    private String id;

    /**
     * 工种名称
     */
    @Column(name = "worker_name")
    @ApiModelProperty("工种名称")
    private String workerName;

    /**
     * 工种编码
     */
    @Column(name = "worker_code")
    @ApiModelProperty("工种编码")
    private String workerCode;

    /**
     * 工种简介
     */
    @Column(name = "worker_context")
    @ApiModelProperty("工种简介")
    private String workerContext;

    /**
     * 是否删除【0：未删除  1：删除】
     */
    @Column(name = "is_delete")
    @ApiModelProperty("是否删除【0：未删除  1：删除】")
    private Short isDelete;

    /**
     * 是否启用【0：未启用 1：启用】
     */
    @Column(name = "is_using")
    @ApiModelProperty("是否启用【0：未启用 1：启用】")
    private Short isUsing;

    public String getIsUsingDisplay() {
        return this.isUsing !=null ? DictUtil.getDisplay("isUsing", this.isUsing+""):null;
    }

    /**
     * 创建时间
     */
    @Column(name = "creat_time")
    @ApiModelProperty("创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creatTime;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;
    

}