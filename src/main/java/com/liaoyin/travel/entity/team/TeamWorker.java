package com.liaoyin.travel.entity.team;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Table(name = "t_team_worker")
@Data
public class TeamWorker {
	
	@Id
    private String id;

    /**
     * 部门id
     */
    @Column(name = " team_id")
    private String teamId;
    
    @Column(name="assume_office")
    @ApiModelProperty("担任职务【1：部门经理 2：员工】")
    private Short assumeOffice;

    /**
     * 工种id
     */
    @Column(name = "worker_id")
    private String workerId;

    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date creatTime;


	@Transient
    @ApiModelProperty("工种名称")
    private String workerName;
    
    @Transient
    @ApiModelProperty("部门名称")
    private String teamName;
}