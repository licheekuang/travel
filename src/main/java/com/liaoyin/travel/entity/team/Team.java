package com.liaoyin.travel.entity.team;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@ApiModel("部门列表")
@Table(name = "t_team")
@Data
public class Team {
	
	@Id
    private String id;

    /**
     * 部门名称
     */
    @Column(name = "team_name")
    @ApiModelProperty("部门名称")
    private String teamName;

    /**
     * 部门简介
     */
    @Column(name = "team_context")
    @ApiModelProperty("部门简介")
    private String teamContext;

    /**
     * 部门负责人id
     */
    @Column(name = "user_id")
    @ApiModelProperty("部门负责人id")
    private String userId;

    /**
     * 部门成立日期
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("部门成立日期")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date creatTime;

    /**
     * 部门级别
     */
    @Column(name = "team_level")
    @ApiModelProperty("部门级别")
    private Short teamLevel;

    public String getTeamLevelDisplay() {
        return this.teamLevel !=null ?DictUtil.getDisplay("teamLevel", this.teamLevel+""):null;
    }

    /**
     * 上级部门id
     */
    @Column(name = "parent_id")
    @ApiModelProperty("上级部门id")
    private String parentId;

    /**
     * 部门编码
     */
    @Column(name = "team_code")
    @ApiModelProperty("部门编码")
    private String teamCode;

    /**
     * 是否删除【0：未删除 1：删除】
     */
    @Column(name = "is_delete")
    @ApiModelProperty(" 是否删除【0：未删除 1：删除】")
    private Short isDelete;

    /**
     * 是否启用【0：未启用 1：启用】
     */
    @Column(name = "is_using")
    @ApiModelProperty("是否启用【0：未启用 1：启用】")
    private Short isUsing;

    public String getIsUsingDisplay() {
        return this.isUsing !=null ?DictUtil.getDisplay("isUsing", this.isUsing+""):null;
    }

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    @Transient
    @ApiModelProperty("工种列表")
    private List<TeamWorker> workerList;
    
    @Transient
    @ApiModelProperty("部门负责人姓名")
    private String nickName;
}