package com.liaoyin.travel.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import com.liaoyin.travel.base.entity.Page;

import java.util.Date;

@Table(name = "global_system_param")
public class GlobalSystemParam {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT REPLACE(UUID(),'-','')")
    private String id;

    /**
     * 参数code
     */
    @Column(name = "param_code")
    private String paramCode;

    /**
     * 参数名称
     */
    @Column(name = "param_name")
    private String paramName;

    /**
     * 参数默认值
     */
    @Column(name = "param_value")
    private String paramValue;

    @Column(name = "create_time")
    @Getter
    @Setter
    private Date createTime;

    /*******
     * 条件
     * *******/
    @Column(name = "condition_param")
    private String conditionParam;
    
    /**
     * 描述
     */
    @Column(name = "param_desc")
    private String paramDesc;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取参数code
     *
     * @return param_code - 参数code
     */
    public String getParamCode() {
        return paramCode;
    }

    /**
     * 设置参数code
     *
     * @param paramCode 参数code
     */
    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    /**
     * 获取参数名称
     *
     * @return param_name - 参数名称
     */
    public String getParamName() {
        return paramName;
    }

    /**
     * 设置参数名称
     *
     * @param paramName 参数名称
     */
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    /**
     * 获取参数默认值
     *
     * @return param_value - 参数默认值
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * 设置参数默认值
     *
     * @param paramValue 参数默认值
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    /**
     * 获取描述
     *
     * @return param_desc - 描述
     */
    public String getParamDesc() {
        return paramDesc;
    }

    /**
     * 设置描述
     *
     * @param paramDesc 描述
     */
    public void setParamDesc(String paramDesc) {
        this.paramDesc = paramDesc;
    }

	public String getConditionParam() {
		return conditionParam;
	}

	public void setConditionParam(String conditionParam) {
		this.conditionParam = conditionParam;
	}




	@Transient
    @Getter
    @Setter
    private String conpouId;
}