package com.liaoyin.travel.entity;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.liaoyin.travel.util.DictUtil;

@ApiModel(value = "角色表")
@Table(name = "s_role")
public class Role {
    /*
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT REPLACE(UUID(),'-','')")
    @ApiModelProperty(value = "主键")
    private String id;

    /*
     * 角色名称
     */
    @Column(name = "ROLE_NAME")
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    /*
     * 继承角色ID
     */
    @Column(name = "extend_role_id")
    @ApiModelProperty(value = "继承角色ID")
    private String extendRoleId;
    
    /*
     * 角色归属
     */
    @Column(name = "role_affiliate")
    @ApiModelProperty(value = "角色归属【字典表:roleAffiliate】")
    private short roleAffiliate;
    
    /*
     * 角色图标
     */
    @Column(name = "role_icon")
    @ApiModelProperty(value = "角色图标")
    private String roleIcon;
    
    
    /*
     * 角色描述
     */
    @Column(name = "ROLE_DESC")
    @ApiModelProperty(value = "角色描述")
    private String roleDesc;

    /*
     * 创建人ID
     */
    @Column(name = "create_user_id")
    @ApiModelProperty(value = "创建人ID")
    private String createUserId;

    /*
     * 更新人ID
     */
    @Column(name = "update_user_id")
    @ApiModelProperty(value = "更新人ID")
    private String updateUserId;

    /*
     * 删除人ID
     */
    @Column(name = "delete_user_id")
    @ApiModelProperty(value = "删除人ID")
    private String deleteUserId;

    /*
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(value = "创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd" , timezone = "GMT+8")
    private Date createTime;

    /*
     * 更新时间
     */
    @Column(name = "update_time")
    @ApiModelProperty(value = "更新时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd" , timezone = "GMT+8")
    private Date updateTime;

    /*
     * 删除时间
     */
    @Column(name = "delete_time")
    @ApiModelProperty(value = "删除时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd" , timezone = "GMT+8")
    private Date deleteTime;

    /*
     * 是否删除
     */
    @Column(name = "is_delete")
    @ApiModelProperty(value = "是否删除")
    private Boolean isDelete;

    /*
     * 扩展字段1
     */
    @ApiModelProperty(value = "扩展字段1--当前状态")
    private String attr1;

    /*
     * 扩展字段2
     */
    @ApiModelProperty(value = "扩展字段2--角色归属")
    private String attr2;

    /*
     * 扩展字段3
     */
    @ApiModelProperty(value = "扩展字段3--继承角色")
    private String attr3;

    /*
     * 扩展字段4
     */
    @ApiModelProperty(value = "扩展字段4--角色图标")
    private String attr4;

    /*
     * 扩展字段5
     */
    @ApiModelProperty(value = "扩展字段5")
    private String attr5;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取角色名称
     *
     * @return ROLE_NAME - 角色名称
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置角色名称
     *
     * @param roleName 角色名称
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 获取角色描述
     *
     * @return ROLE_DESC - 角色描述
     */
    public String getRoleDesc() {
        return roleDesc;
    }

    /**
     * 设置角色描述
     *
     * @param roleDesc 角色描述
     */
    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    /**
     * 获取创建人ID
     *
     * @return create_user_id - 创建人ID
     */
    public String getCreateUserId() {
        return createUserId;
    }

    /**
     * 设置创建人ID
     *
     * @param createUserId 创建人ID
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 获取更新人ID
     *
     * @return update_user_id - 更新人ID
     */
    public String getUpdateUserId() {
        return updateUserId;
    }

    /**
     * 设置更新人ID
     *
     * @param updateUserId 更新人ID
     */
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    /**
     * 获取删除人ID
     *
     * @return delete_user_id - 删除人ID
     */
    public String getDeleteUserId() {
        return deleteUserId;
    }

    /**
     * 设置删除人ID
     *
     * @param deleteUserId 删除人ID
     */
    public void setDeleteUserId(String deleteUserId) {
        this.deleteUserId = deleteUserId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取删除时间
     *
     * @return delete_time - 删除时间
     */
    public Date getDeleteTime() {
        return deleteTime;
    }

    /**
     * 设置删除时间
     *
     * @param deleteTime 删除时间
     */
    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取扩展字段1
     *
     * @return attr1 - 扩展字段1
     */
    public String getAttr1() {
        return attr1;
    }

    /**
     * 设置扩展字段1
     *
     * @param attr1 扩展字段1
     */
    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    /**
     * 获取扩展字段2
     *
     * @return attr2 - 扩展字段2
     */
    public String getAttr2() {
        return attr2;
    }

    /**
     * 设置扩展字段2
     *
     * @param attr2 扩展字段2
     */
    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    /**
     * 获取扩展字段3
     *
     * @return attr3 - 扩展字段3
     */
    public String getAttr3() {
        return attr3;
    }

    /**
     * 设置扩展字段3
     *
     * @param attr3 扩展字段3
     */
    public void setAttr3(String attr3) {
        this.attr3 = attr3;
    }

    /**
     * 获取扩展字段4
     *
     * @return attr4 - 扩展字段4
     */
    public String getAttr4() {
        return attr4;
    }

    /**
     * 设置扩展字段4
     *
     * @param attr4 扩展字段4
     */
    public void setAttr4(String attr4) {
        this.attr4 = attr4;
    }

    /**
     * 获取扩展字段5
     *
     * @return attr5 - 扩展字段5
     */
    public String getAttr5() {
        return attr5;
    }

    /**
     * 设置扩展字段5
     *
     * @param attr5 扩展字段5
     */
    public void setAttr5(String attr5) {
        this.attr5 = attr5;
    }
    
    
    public String getExtendRoleId() {
		return extendRoleId;
	}

	public void setExtendRoleId(String extendRoleId) {
		this.extendRoleId = extendRoleId;
	}

	public short getRoleAffiliate() {
		return roleAffiliate;
	}

	public void setRoleAffiliate(short roleAffiliate) {
		this.roleAffiliate = roleAffiliate;
	}

	public String getRoleIcon() {
		return roleIcon;
	}

	public void setRoleIcon(String roleIcon) {
		this.roleIcon = roleIcon;
	}


	/**
     * 角色图标
     */
    @Transient
    @Getter
    @Setter
    private FileUpload fileupload;
    @Transient
    @Getter
    @Setter
    private String fullFilePath;
    
    
    @JsonProperty("roleAffiliateDisplay")
	public String getroleAffiliateDisplay() {
    	return DictUtil.getDisplay("roleAffiliate", this.roleAffiliate+"");
	} 
    @JsonProperty("attrStr")
   	public String getAttrStr() {
       	return this.attr1 != null &&!"".equals(this.attr1)&&"1".equals(this.attr1)?"启用":"禁用";
   	} 
}