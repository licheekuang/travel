package com.liaoyin.travel.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "t_motion_track")
@Data
@ApiModel(value = "用户路径的实体类")
@Accessors(chain = true)
public class MotionTrack {
    /**
     * 主键id
     */
    @Id
    private String id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 纬度
     */
    @Column(name = "u_lat")
    @ApiModelProperty(value = "记录纬度")
    private String uLat;

    /**
     * 经度
     */
    @Column(name = "u_log")
    @ApiModelProperty(value = "记录经度")
    private String uLog;

    /**
     * 上传时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(value = "上传时间")
   /* @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")*/
    private Date createTime;

    @Column(name = "del_flag")
    @ApiModelProperty(value = "是否删除 0未删除 1已删除")
    private Short delFlag;

    @ApiModelProperty(value = "景区id")
    @Column(name = "scenic_id")
    private String scenicId;


    /**
     * 获取纬度
     *
     * @return u_lat - 纬度
     */
    public String getuLat() {
        return uLat;
    }

    /**
     * 设置纬度
     *
     * @param uLat 纬度
     */
    public void setuLat(String uLat) {
        this.uLat = uLat;
    }

    /**
     * 获取经度
     *
     * @return u_log - 经度
     */
    public String getuLog() {
        return uLog;
    }

    /**
     * 设置经度
     *
     * @param uLog 经度
     */
    public void setuLog(String uLog) {
        this.uLog = uLog;
    }

}