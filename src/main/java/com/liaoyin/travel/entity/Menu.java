package com.liaoyin.travel.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.liaoyin.travel.base.entity.Page;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@ApiModel(value = "菜单表")
@Table(name = "s_menu")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@Data
public class Menu extends Page{

    /**
     * 菜单编码，与页面DOM元素的MENUMODEL属性进行匹配
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT REPLACE(UUID(),'-','')")
    @ApiModelProperty(value = "菜单编码，与页面DOM元素的MENUMODEL属性进行匹配")
    private String id;

    /**
     * 菜单编码，与页面DOM元素的MENUMODEL属性进行匹配
     */
    @Column(name = "MENU_CODE")
    @ApiModelProperty(value = "菜单编码，与页面DOM元素的MENUMODEL属性进行匹配")
    private String menuCode;

    /**
     * 菜单名称
     */
    @Column(name = "MENU_NAME")
    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    /**
     * 父级菜单编码
     */
    @Column(name = "PARENT_ID")
    @ApiModelProperty(value = "父级菜单ID")
    private String parentId;

    /**
     * 菜单全编码【包含所有上级菜单编码，格式为：AA.BB.CC.DD】
     */
    @Column(name = "MENU_FULL_CODE")
    @ApiModelProperty(value = "菜单全编码【包含所有上级菜单编码，格式为：AA.BB.CC.DD】")
    private String menuFullCode;

    /**
     * 菜单节点顺序
     */
    @Column(name = "NODE_ORDER")
    @ApiModelProperty(value = "菜单节点顺序")
    private Integer nodeOrder;

    /**
     * 菜单路径
     */
    @Column(name = "URL")
    @ApiModelProperty(value = "菜单路径")
    private String url;

    /**
     * 菜单ICON
     */
    @Column(name = "ICON")
    @ApiModelProperty(value = "菜单ICON")
    private String icon;

    /**
     * 是否叶子
     */
    @Column(name = "IS_LEAF")
    @ApiModelProperty(value = "是否叶子")
    private Short isLeaf;

    /**
     * 菜单层级
     */
    @Column(name = "LEVEL")
    @ApiModelProperty(value = "菜单层级")
    private Integer level;

    /**
     * 创建人ID
     */
    @Column(name = "create_user_id")
    @ApiModelProperty(value = "创建人ID")
    private String createUserId;

    /**
     * 更新人ID
     */
    @Column(name = "update_user_id")
    @ApiModelProperty(value = "更新人ID")
    private String updateUserId;

    /**
     * 删除人ID
     */
    @Column(name = "delete_user_id")
    @ApiModelProperty(value = "删除人ID")
    private String deleteUserId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 删除时间
     */
    @Column(name = "delete_time")
    @ApiModelProperty(value = "删除时间")
    private Date deleteTime;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    @ApiModelProperty(value = "是否删除")
    private Short isDelete;

    /**
     * 背景色
     */
    @Column(name = "background")
    @ApiModelProperty(value="背景色")
    private String background;

    /**
     * 状态
     */
    @Column(name = "is_using")
    @ApiModelProperty(value = "状态[0-启用；1-禁用]")
    private String isUsing;
    
    /**
     * 客户端类型（字典表：clientType）
     */
    @Column(name = "client_type")
    @ApiModelProperty(value = "客户端类型（字典表：clientType）")
    private short clientType;

    /**
     * 是否和单位子管理员关联(0:否;1:是)
     */
    @ApiModelProperty(value = "是否和单位子管理员关联")
    @Column(name = "is_scenic_subordinate")
    private String isScenicSubordinate;

    /**
     * 扩展字段1
     */
    @ApiModelProperty(value = "扩展字段1")
    private String attr1;

    /**
     * 扩展字段2
     */
    @ApiModelProperty(value = "扩展字段2")
    private String attr2;

    /**
     * 扩展字段3
     */
    @ApiModelProperty(value = "扩展字段3")
    private String attr3;

    /**
     * 扩展字段4
     */
    @ApiModelProperty(value = "扩展字段4")
    private String attr4;


    /**
     * 下级数量
     * */
    @Transient
    private Integer parentNum;
    
    @Transient
    private String fullFilePath;
    
    /**
     * 菜单ICON
     */
    @Transient
    private FileUpload fileupload;
    
    /**
     * 所属菜单
     */
    @Transient
    @ApiModelProperty(value = "上级菜单名称")
    private String menuNameNext;
    

    
	 /***
	   *状态
	 * ***/
	 @JsonProperty("isUsingStr")
	 public String getIsUsingStr() {
		 return this.isUsing !=null && !"".equals(this.isUsing)&& "1".equals( this.isUsing) ? "开启" : "禁用";
	 }
   /***
	  *客户端类型
	* ***/
	@JsonProperty("clientTypeDisplay")
	public String getClientTypeDisplay() {
		 return DictUtil.getDisplay("clientType", this.clientType+"");
	}
	
	@Transient
	private String name;


}