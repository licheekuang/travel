package com.liaoyin.travel.entity.scenicInfo;

import lombok.Data;

import javax.persistence.*;

@Table(name = "t_area_district")
@Data
public class AreaDistrict {

    @Id
    private String id;

    /**
     * 市_id
     */
    @Column(name = "city_id")
    private String cityId;

    /**
     * 区
     */
    private String name;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;


}