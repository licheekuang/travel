package com.liaoyin.travel.entity.scenicInfo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 景区(单位)下级管理员和菜单关联
 */
@Accessors(chain = true)
@Data
@Table(name = "t_scenic_lower_level_admin_menu")
public class ScenicLowerLevelAdminMenu {

    @Id
    private String id;

    /**
     * 后台用户id
     */
    @Column(name = "back_user_id")
    private String backUserId;

    /**
     * 菜单id
     */
    @Column(name = "menu_id")
    private String menuId;

    /**
     * 景区(单位)id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 备用字段1
     */
    private String attr1;

    /**
     * 备用字段2
     */
    private String attr2;

    /**
     * 备用字段3
     */
    private String attr3;


}