package com.liaoyin.travel.entity.scenicInfo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_scenic_admin")
@Data
@Accessors(chain = true)
public class ScenicAdmin {

    @Id
    private String id;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 景区管理员姓名
     */
    private String name;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 身份证号
     */
    @Column(name = "identity_card")
    private String identityCard;

    /**
     * 性别【1:男;2:女】
     */
    private String gender;

    /**
     * 管理员账号
     */
    private String account;

    /**
     * 管理员密码
     */
    private String password;

    @Column(name = "create_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;

    /**
     * 备用字段3
     */
    @Column(name = "alternate_field_3")
    private String alternateField3;

}