package com.liaoyin.travel.entity.scenicInfo;

import lombok.Data;

import javax.persistence.*;

@Table(name = "t_area_province")
@Data
public class AreaProvince {

    @Id
    private String id;

    /**
     * 省(直辖市)
     */
    private String name;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;


}