package com.liaoyin.travel.entity.scenicInfo;

import lombok.Data;

import javax.persistence.*;

@Table(name = "t_area_city")
@Data
public class AreaCity {

    @Id
    private String id;

    /**
     * 省_id
     */
    @Column(name = "province_id")
    private String provinceId;

    /**
     * 市
     */
    private String name;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;


}