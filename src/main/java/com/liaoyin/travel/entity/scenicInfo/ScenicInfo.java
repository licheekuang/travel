package com.liaoyin.travel.entity.scenicInfo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Table(name = "t_scenic_info")
@Data
@Accessors(chain = true)
public class ScenicInfo {

    @Id
    private String id;

    /**
     * 区域(暂定精确到区)id
     */
    @Column(name = "district_id")
    private String districtId;

    /**
     * 单位类型【关联数据字典unitType字段】
     */
    @Column(name = "unit_type")
    private String unitType;

    /**
     * 景区名字
     */
    @Column(name = "scenic_name")
    private String scenicName;

    /**
     * 公司名字
     */
    @Column(name = "corp_name")
    private String corpName;

    /**
     * 管理员id
     */
    @Column(name = "back_user_id")
    private String backUserId;

    /**
     * 审核状态【0:未申请；1:通过；2:拒绝】
     */
    @Column(name = "audit_status")
    private String auditStatus;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 机构类型,只有机构才有此值(关联数据库organType字段)
     */
    @Column(name = "organ_type")
    private String organType;

    /**
     * 是否之前通过审核
     */
    @Column(name = "is_passed")
    private String isPassed;

    /**
     * 当日最大承载量
     */
    @Column(name = "maximum_capacity_for_the_day")
    private BigInteger maximumCapacityForTheDay;

    /**
     * 是否为是文旅委
     */
    @Column(name = "is_whlyw")
    private Integer whlyw;
    /**
     * 设备编号
     */
    @Column(name = "equipment_number")
    private String equipmentNumber;

    /**
     * 是否为应急文旅资源
     */
    @Column(name = "is_emergency")
    private Integer emergency;

    /**
     * 应急文旅资源ID
     */
    @Column(name = "emergency_id")
    private String emergencyId;

}