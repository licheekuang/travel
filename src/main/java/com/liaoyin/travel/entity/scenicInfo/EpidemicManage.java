package com.liaoyin.travel.entity.scenicInfo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_epidemic_manage")
@Data
@Accessors(chain = true)
public class EpidemicManage {

    @Id
    private String id;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 疫情XX的链接
     */
    @Column(name = "skip_url")
    private String skipUrl;

    /**
     * banner图片的url
     */
    @Column(name = "img_url")
    private String imgUrl;

    /**
     * 0:禁用;1:启用
     */
    @Column(name = "is_using")
    private String isUsing;

    public String getIsUsingDislpay(){
        if(isUsing!=null){
            return isUsing.equals("0")?"禁用":"启用";
        }
        return null;
    }

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private LocalDateTime updateTime;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;

    /**
     * 备用字段3
     */
    @Column(name = "alternate_field_3")
    private String alternateField3;




}