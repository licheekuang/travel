package com.liaoyin.travel.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.liaoyin.travel.base.entity.Page;

@ApiModel(value = "功能表")
@Table(name = "s_function")
public class Function extends Page{
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT REPLACE(UUID(),'-','')")
    private String id;

    /*
     * 功能编码，与页面DOM元素的MODEL属性进行匹配
     */
    @Column(name = "FUNC_CODE")
    @ApiModelProperty(value = "功能编码，与页面DOM元素的MODEL属性进行匹配")
    private String funcCode;

    /*
     * 功能名称
     */
    @Column(name = "FUNC_NAME")
    @ApiModelProperty(value = "功能名称")
    private String funcName;

    /*
     * 功能类型
     */
    @Column(name = "FUNC_TYPE")
    @ApiModelProperty(value = "功能类型")
    private String funcType;

    /*
     * URI
     */
    @Column(name = "URI")
    @ApiModelProperty(value = "URI")
    private String uri;

    /*
     * 所属菜单ID
     */
    @Column(name = "MENU_ID")
    @ApiModelProperty(value = "所属菜单ID")
    private String menuId;

    /*
     * 调用方式
     */
    @Column(name = "METHOD")
    @ApiModelProperty(value = "调用方式")
    private String method;

    /*
     * 功能编码备注说明
     */
    @Column(name = "FUN_MARK")
    @ApiModelProperty(value = "功能编码备注说明")
    private String funMark;

    /*
     * 创建日期
     */
    @Column(name = "CRDATE")
    @ApiModelProperty(value = "创建日期")
    private Date crdate;

    /*
     * 是否删除
     */
    @Column(name = "is_delete")
    @ApiModelProperty(value = "是否删除")
    private Boolean isDelete;

    /*
     * 状态
     */
    @Column(name = "is_using")
    @ApiModelProperty(value = "状态[1-启用；0-禁用]")
    private String isUsing;
    
    /**
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取功能编码，与页面DOM元素的MODEL属性进行匹配
     *
     * @return FUNC_CODE - 功能编码，与页面DOM元素的MODEL属性进行匹配
     */
    public String getFuncCode() {
        return funcCode;
    }

    /**
     * 设置功能编码，与页面DOM元素的MODEL属性进行匹配
     *
     * @param funcCode 功能编码，与页面DOM元素的MODEL属性进行匹配
     */
    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    /**
     * 获取功能名称
     *
     * @return FUNC_NAME - 功能名称
     */
    public String getFuncName() {
        return funcName;
    }

    /**
     * 设置功能名称
     *
     * @param funcName 功能名称
     */
    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    /**
     * 获取功能类型
     *
     * @return FUNC_TYPE - 功能类型
     */
    public String getFuncType() {
        return funcType;
    }

    /**
     * 设置功能类型
     *
     * @param funcType 功能类型
     */
    public void setFuncType(String funcType) {
        this.funcType = funcType;
    }

    /**
     * 获取URI
     *
     * @return URI - URI
     */
    public String getUri() {
        return uri;
    }

    /**
     * 设置URI
     *
     * @param uri URI
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * 获取所属菜单ID
     *
     * @return MENU_ID - 所属菜单ID
     */
    public String getMenuId() {
        return menuId;
    }

    /**
     * 设置所属菜单ID
     *
     * @param menuId 所属菜单ID
     */
    public void setMenuCode(String menuId) {
        this.menuId = menuId;
    }

    /**
     * 获取调用方式
     *
     * @return METHOD - 调用方式
     */
    public String getMethod() {
        return method;
    }

    /**
     * 设置调用方式
     *
     * @param method 调用方式
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * 获取功能编码备注说明
     *
     * @return FUN_MARK - 功能编码备注说明
     */
    public String getFunMark() {
        return funMark;
    }

    /**
     * 设置功能编码备注说明
     *
     * @param funMark 功能编码备注说明
     */
    public void setFunMark(String funMark) {
        this.funMark = funMark;
    }

    /**
     * 获取创建日期
     *
     * @return CRDATE - 创建日期
     */
    public Date getCrdate() {
        return crdate;
    }

    /**
     * 设置创建日期
     *
     * @param crdate 创建日期
     */
    public void setCrdate(Date crdate) {
        this.crdate = crdate;
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }
    
    
    public String getIsUsing() {
		return isUsing;
	}

	public void setIsUsing(String isUsing) {
		this.isUsing = isUsing;
	}

	/***
     *（1是0否）
     * ***/
  @JsonProperty("isUsingStr")
  public String getIsUsingStr() {
	  return this.isUsing!=null && "1".equals(this.isUsing) ? "启用" : "禁用";
  } 
  /**
   * 所属菜单
   */
  @Transient
  @ApiModelProperty(value = "所属菜单")
  @Getter
  @Setter
  private String menuName;
}