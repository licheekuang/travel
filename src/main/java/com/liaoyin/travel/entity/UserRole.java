package com.liaoyin.travel.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户角色表（用户包括人员、客户端、分组、班级等）")
@Table(name = "s_user_role")
public class UserRole {
    /*
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT REPLACE(UUID(),'-','')")
    @ApiModelProperty(value = "主键")
    private String id;

    /*
     * 党员ID
     */
    @Column(name = "user_id")
    @ApiModelProperty(value = "党员ID")
    private String userId;

    /*
     * 角色ID
     */
    @Column(name = "role_id")
    @ApiModelProperty(value = "角色ID")
    private String roleId;

    /*
     * 用户类型【字典表：userType】
     */
    @Column(name = "user_type")
    @ApiModelProperty(value = "用户类型【字典表：userType】")
    private Short userType;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取党员ID
     *
     * @return user_id - 党员ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置党员ID
     *
     * @param userId 党员ID
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取角色ID
     *
     * @return role_id - 角色ID
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * 设置角色ID
     *
     * @param roleId 角色ID
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     * 获取用户类型【字典表：userType】
     *
     * @return user_type - 用户类型【字典表：userType】
     */
    public Short getUserType() {
        return userType;
    }

    /**
     * 设置用户类型【字典表：userType】
     *
     * @param userType 用户类型【字典表：userType】
     */
    public void setUserType(Short userType) {
        this.userType = userType;
    }
}