package com.liaoyin.travel.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;

@Table(name = "t_user_team")
@Data
@Accessors(chain = true)
public class UserTeam {
	
	@Id
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "team_id")
    private String teamId;

    /**
     * 担任职务【1：部门经理 2：员工】
     */
    @Column(name = "assume_office")
    private Short assumeOffice;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Column(name = "update_time")
    private LocalDateTime updateTime;

    @Transient
    @ApiModelProperty("部门名称")
    private String teamName;
    
}