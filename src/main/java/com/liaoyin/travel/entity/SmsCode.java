package com.liaoyin.travel.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@ApiModel(value = "短信验证")
@Table(name = "s_sms_code")
@Data
public class SmsCode {
	
    @Id
    @GeneratedValue(generator = "UUID")
    @ApiModelProperty(value = "id")
    private String id;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    @Column(name = "mobile_phone")
    private String mobilePhone;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码")
    @Column(name = "sms_code")
    private String smsCode;

    /**
     * 过期时间
     */
    @ApiModelProperty(value = "过期时间")
    @Column(name = "expire_time")
    private Date expireTime;

    /**
     * 功能编码
     */
    @ApiModelProperty(value = "功能编码")
    @Column(name = "function_code")
    private String functionCode;

    /***
     * 总发送数
     * **/
    @Column(name="count_num")
    private Integer countNum;

}