package com.liaoyin.travel.entity.approval;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_approval_outgoing_status_display")
@Data
@Accessors(chain = true)
public class OutgoingStatusDisplay {
    private String id;

    /**
     * 考勤记录表id
     */
    @Column(name = "attendance_record_id")
    private String attendanceRecordId;

    /**
     * 外出开始时间
     */
    @Column(name = "departure_time")
    private LocalDateTime departureTime;

    /**
     * 外出结束时间
     */
    @Column(name = "end_of_outing")
    private LocalDateTime endOfOuting;

    /**
     * 上班时间
     */
    @Column(name = "clock_in_time")
    private String clockInTime;

    /**
     * 下班时间
     */
    @Column(name = "clock_out_time")
    private String clockOutTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;


}