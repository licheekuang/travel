package com.liaoyin.travel.entity.approval;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_approval_ask_for_leave_packet_record")
@Data
@Accessors(chain = true)
public class AskForLeavePacketRecord {
    private String id;

    /**
     * 分组名称
     */
    @Column(name = "packet_name")
    private String packetName;

    /**
     * 审批人id
     */
    @Column(name = "approved_by")
    private String approvedBy;

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新人id
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;


}