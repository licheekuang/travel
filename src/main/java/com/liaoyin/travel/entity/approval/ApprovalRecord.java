package com.liaoyin.travel.entity.approval;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_approval_record")
@Data
@Accessors(chain = true)
public class ApprovalRecord {

    private String id;

    /**
     * 审批编号
     */
    @Column(name = "approval_number")
    private String approvalNumber;

    /**
     * 审批标题
     */
    @Column(name = "approval_title")
    private String approvalTitle;

    /**
     * 审批类型【数据字典approvalType】
     */
    @Column(name = "approval_type")
    private String approvalType;

    /**
     * 审批发起人的id
     */
    @Column(name = "initiator_id")
    private String initiatorId;

    /**
     * 审批的发起时间
     */
    @Column(name = "initiation_time")
    private LocalDateTime initiationTime;

    /**
     * 审批的完成时间
     */
    @Column(name = "finish_time")
    private LocalDateTime finishTime;

    /**
     * 拒绝通过审批的原因
     */
    @Column(name = "refusal_cause")
    private String refusalCause;

    /**
     * 审批的结果(状态)【数据字典approvalStatus】
     */
    @Column(name = "approval_status")
    private String approvalStatus;

    /**
     * 审批耗时
     */
    @Column(name = "approval_consuming")
    private String approvalConsuming;

    /**
     * 审批人的id
     */
    @Column(name = "approver_id")
    private String approverId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;






}