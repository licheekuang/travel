package com.liaoyin.travel.entity.approval;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_approval_ask_for_leave_packet_applicant")
@Data
@Accessors(chain = true)
public class AskForLeavePacketApplicant {
    private String id;

    /**
     * 请假申请-审批分组-id
     */
    @Column(name = "packet_id")
    private String packetId;

    /**
     * 申请人id
     */
    @Column(name = "applicant_id")
    private String applicantId;

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;

}