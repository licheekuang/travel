package com.liaoyin.travel.entity.approval;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Table(name = "t_approval_go_out_record")
@Data
@Accessors(chain = true)
public class GoOutRecord {
    private String id;

    /**
     * 审批记录表id
     */
    @Column(name = "approval_record_id")
    private String approvalRecordId;

    /**
     * 外出开始时间
     */
    @Column(name = "departure_time")
    private LocalDateTime departureTime;

    /**
     * 外出结束时间
     */
    @Column(name = "end_of_outing")
    private LocalDateTime endOfOuting;

    /**
     * 外出时长
     */
    @Column(name = "go_out_time")
    private String goOutTime;

    /**
     * 外出事由
     */
    @Column(name = "go_out_for")
    private String goOutFor;

    /**
     * 交通费金额
     */
    @Column(name = "car_fare")
    private BigDecimal carFare;

    /**
     * 图片的地址
     */
    @Column(name = "img_url")
    private String imgUrl;

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

}