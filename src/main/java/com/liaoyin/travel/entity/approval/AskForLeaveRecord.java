package com.liaoyin.travel.entity.approval;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_approval_ask_for_leave_record")
@Data
@Accessors(chain = true)
public class AskForLeaveRecord {

    @Id
    private String id;

    /**
     * 审批记录表id
     */
    @Column(name = "approval_record_id")
    private String approvalRecordId;

    /**
     * 请假开始时间
     */
    @Column(name = "leave_start_time")
    private LocalDateTime leaveStartTime;

    /**
     * 请假结束时间
     */
    @Column(name = "leave_end_time")
    private LocalDateTime leaveEndTime;

    /**
     * 请假时长
     */
    @Column(name = "leave_time")
    private String leaveTime;

    /**
     * 请假事由
     */
    @Column(name = "reason_for_a_leave")
    private String reasonForALeave;

    /**
     * 图片的地址
     */
    @Column(name = "img_url")
    private String imgUrl;

    /**
     * 附件的地址
     */
    @Column(name = "accessory_url")
    private String accessoryUrl;

    /**
     * 考勤管理的id
     */
    @Column(name = "attendance_settings_id")
    private String attendanceSettingsId;

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 请假开始时间(陈鹏建议保存的格式)
     */
    @Column(name = "leave_start_time_android")
    private String leaveStartTimeAndroid;

    /**
     * 请假结束时间(陈鹏建议保存的格式)
     */
    @Column(name = "leave_end_time_android")
    private String leaveEndTimeAndroid;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;





}