package com.liaoyin.travel.entity.approval;

import com.liaoyin.travel.util.DictUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "t_approval_visual_range_setting")
@Data
@Accessors(chain = true)
public class ApprovalVisualRangeSetting {

    @Id
    private String id;

    /**
     * 审批类型【数据字典approvalType】
     */
    @Column(name = "approval_type")
    private String approvalType;

    public String getApprovalTypeDisplay(){
        if(approvalType!=null){
            return DictUtil.getDisplay("approvalType",approvalType);
        }
        return null;
    }

    /**
     * 可见范围【数据字典visualRange】
     */
    @Column(name = "visual_range")
    private String visualRange;

    public String getVisualRangeDisplay(){
        if(visualRange!=null){
            return DictUtil.getDisplay("visualRange",visualRange);
        }
        return null;
    }

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 是否启用(0:否;1:是)
     */
    @Column(name = "is_using")
    private String isUsing;


}