package com.liaoyin.travel.entity.approval;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@Data
@Table(name = "t_approval_card_reissue_record")
@Accessors(chain = true)
public class CardReissueRecord {

    @Id
    private String id;

    /**
     * 审批记录表id
     */
    @Column(name = "approval_record_id")
    private String approvalRecordId;

    /**
     * 补卡时间点
     */
    @Column(name = "time_point_of_card_renewal")
    private Date timePointOfCardRenewal;

    /**
     * 补卡那天是星期几
     */
    @Column(name = "what_day")
    private Integer whatDay;

    /**
     * 补卡事由
     */
    @Column(name = "supplementary_card_cause")
    private String supplementaryCardCause;

    /**
     * 缺卡班次标识(1:上班;2:下班)
     */
    private String shift;

    /**
     * 缺卡id(考勤记录表id)
     */
    @Column(name = "lack_of_card_id")
    private String lackOfCardId;

    /**
     * 缺卡班次显示
     */
    @Column(name = "lack_of_card_divisions")
    private String lackOfCardDivisions;

    /**
     * 缺卡时间(需要打卡的时间)
     */
    @Column(name = "clock_time")
    private LocalTime clockTime;

    /**
     * 缺卡时间显示
     */
    @Column(name = "clock_time_display")
    private String  clockTimeDisplay;

    /**
     * 图片的地址
     */
    @Column(name = "img_url")
    private String imgUrl;

    /**
     * 是否删除(0:否;1:是)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field_1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field_2")
    private String alternateField2;


}