package com.liaoyin.travel.entity;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.util.DictUtil;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Table(name = "t_banner")
public class Banner {
    @Id
    @ApiModelProperty("主键")
    private String id;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    @Column(name = "sort_order")
    private Integer sortOrder;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    @Column(name = "banner_desc")
    private String bannerDesc;

    /**
     * 链接
     */
    @ApiModelProperty("链接")
    @Column(name = "banner_url")
    private String bannerUrl;
    
    @ApiModelProperty("链接类型【】")
    @Column(name = "banner_url_type")
    private Short bannerUrlType;

    @ApiModelProperty("关联的业务id")
    @Column(name = "business_id")
    private String businessId;
    /***
     * banner类型【字典表 bannerType】
     * ***/
    @ApiModelProperty("banner类型【字典表 bannerType】")
    @Column(name = "banner_type")
    private String bannerType;
    
    
    
    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取排序
     *
     * @return sort_order - 排序
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * 设置排序
     *
     * @param sortOrder 排序
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 获取描述
     *
     * @return banner_desc - 描述
     */
    public String getBannerDesc() {
        return bannerDesc;
    }

    /**
     * 设置描述
     *
     * @param bannerDesc 描述
     */
    public void setBannerDesc(String bannerDesc) {
        this.bannerDesc = bannerDesc;
    }

    /**
     * 获取链接
     *
     * @return banner_url - 链接
     */
    public String getBannerUrl() {
        return bannerUrl;
    }

    /**
     * 设置链接
     *
     * @param bannerUrl 链接
     */
    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }
    
    public String getBannerType() {
		return bannerType;
	}

	public void setBannerType(String bannerType) {
		this.bannerType = bannerType;
	}
	
	public Short getBannerUrlType() {
		return bannerUrlType;
	}

	public void setBannerUrlType(Short bannerUrlType) {
		this.bannerUrlType = bannerUrlType;
	}
	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}





	@Transient
    @Getter
    @Setter
    private String pic;
    
    @Transient
    @Getter
    @Setter
    private List<FileUpload> listFileupload;
    
    public String getPicDisplay() {
    	if(this.pic != null && !"".equals(this.pic)) {
    		return CommonConstant.FILE_SERVER+this.pic;
    	}
    	return null;
    }
    @JsonProperty("bannerTypeDisplay")
    public String getBannerTypeDisplay() {
    	return this.bannerType!=null&&!"".equals(this.bannerType)?DictUtil.getDisplay("bannerType", this.bannerType):null;
    }
}