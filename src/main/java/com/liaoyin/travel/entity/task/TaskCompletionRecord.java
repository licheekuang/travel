package com.liaoyin.travel.entity.task;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@ApiModel("任务完成记录")
@Table(name = "t_task_completion_record")
@Data
public class TaskCompletionRecord {
	
	@Id
    private String id;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private String taskId;

    /**
     * 任务详情id
     */
    @Column(name = "task_details_id")
    private String taskDetailsId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private String userId;
    
    /**
     * 当前任务流程【1,2,3,4】
     */
    @Column(name = "task_flow")
    private Short taskFlow;
    

    /**
     * 用户领取任务记录id
     */
    @Column(name = "task_receiving_records_id")
    private String taskReceivingRecordsId;
    
    
    @ApiModelProperty("签到纬度")
    private String lat;

	@ApiModelProperty("签到经度")
    private String log;
	
	@ApiModelProperty("签到偏差的距离")
	private Double disparity;

    /**
     * 反馈内容
     */
    @Column(name = "feed_back_content")
    private String feedBackContent;

    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date creatTime;

    @Column(name = "scenic_id")
    private String scenicId;

    @Transient
    @ApiParam("设备ID")
	private String equipmentId;

    @Transient
    @ApiParam("任务类型")
    private Short taskType;

    @Transient
    @ApiParam("任务线路id")
    private String lineId;
}