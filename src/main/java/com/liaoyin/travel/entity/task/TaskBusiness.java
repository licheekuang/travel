package com.liaoyin.travel.entity.task;

import java.util.Date;
import javax.persistence.*;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.experimental.Accessors;

@ApiModel("任务指定的领取人")
@Table(name = "t_task_business")
@Accessors(chain = true)
public class TaskBusiness {
	@Id
    private String id;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    @ApiModelProperty("任务id")
    private String taskId;

    /**
     * 是否指定人员【0：所有人  1：指定人员 2：指定部门 3：指定工种】
     */
    @Column(name = "is_specify")
    @ApiModelProperty("是否指定人员【 1：指定人员 2：指定部门 3：指定工种】")
    private Short isSpecify;
    
    /**
     * 业务id【可关联 用户 部门 工种 的id】
     */
    @Column(name = "business_id")
    @ApiModelProperty("业务id【可关联 用户 部门 工种 的id】")
    private String businessId;

    /**
     * 创建时间
     */
    @Column(name = "creat_time")
    @ApiModelProperty("创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date creatTime;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取业务id【可关联 用户 部门 工种 的id】
     *
     * @return business_id - 业务id【可关联 用户 部门 工种 的id】
     */
    public String getBusinessId() {
        return businessId;
    }

    /**
     * 设置业务id【可关联 用户 部门 工种 的id】
     *
     * @param businessId 业务id【可关联 用户 部门 工种 的id】
     */
    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    /**
     * 获取创建时间
     *
     * @return creat_time - 创建时间
     */
    public Date getCreatTime() {
        return creatTime;
    }

    /**
     * 设置创建时间
     *
     * @param creatTime 创建时间
     */
    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

	public Short getIsSpecify() {
		return isSpecify;
	}

	public void setIsSpecify(Short isSpecify) {
		this.isSpecify = isSpecify;
	}
    
    
}