package com.liaoyin.travel.entity.task;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@ApiModel("任务详情")
@Table(name = "t_task_details")
@Data
public class TaskDetails {
	
	@Id
    private String id;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private String taskId;

    /**
     * 任务流程【1,2,3,4】
     */
    @Column(name = "task_flow")
    private Short taskFlow;

    /**
     * 任务操作【字典 taskOperation】
     */
    @Column(name = "task_operation")
    private Short taskOperation;

    /**
     * 任务操作 Display
     */
    public String getTaskOperationDisplay(){
        if(taskOperation==null){
            return null;
        }
        return DictUtil.getDisplay("taskOperation",taskOperation.toString());
    }

    /**
     * 设备id【当任务操作为签到时候，必传】
     */
    @Column(name = "equipment_id")
    private String equipmentId;

    /**
     * 任务说明
     */
    @Column(name = "details_explain")
    private String detailsExplain;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 经度
     */
    private String log;
    
    /**
     * 是否删除【0：未删除 1：删除】
     */
    @Column(name = "is_delete")
    private Short isDelete;

    /**
     * 创建时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date creatTime;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

	@Transient
    @ApiModelProperty("单项任务完成情况")
    private TaskCompletionRecord taskCompletionRecord;
    
    @Transient
    @ApiModelProperty("设备名称")
    private String equipmentName;

    @Transient
    @ApiModelProperty("任务路线详情")
    public TaskLine taskLine;

    @Transient
    @ApiModelProperty("任务信息")
    public Task task;

    @Transient
    @ApiModelProperty("纬度")
    public String taskLat;

    @Transient
    @ApiModelProperty("经度")
    public String taskLog;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskDetails{");
        sb.append("id='").append(id).append('\'');
        sb.append(", taskId='").append(taskId).append('\'');
        sb.append(", taskFlow=").append(taskFlow);
        sb.append(", taskOperation=").append(taskOperation);
        sb.append(", equipmentId='").append(equipmentId).append('\'');
        sb.append(", detailsExplain='").append(detailsExplain).append('\'');
        sb.append(", lat='").append(lat).append('\'');
        sb.append(", log='").append(log).append('\'');
        sb.append(", isDelete=").append(isDelete);
        sb.append(", creatTime=").append(creatTime);
        sb.append(", scenicId='").append(scenicId).append('\'');
        sb.append(", taskCompletionRecord=").append(taskCompletionRecord);
        sb.append(", equipmentName='").append(equipmentName).append('\'');
        sb.append(", taskLine=").append(taskLine);
        sb.append(", task=").append(task);
        sb.append(", taskLat='").append(taskLat).append('\'');
        sb.append(", taskLog='").append(taskLog).append('\'');
        sb.append('}');
        return sb.toString();
    }
}