package com.liaoyin.travel.entity.task;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "t_timing_task_designated_receiver")
@Data
@Accessors(chain = true)
public class TimingTaskDesignatedReceiver {
    private String id;

    /**
     * 定时任务参数表id
     */
    @Column(name = "timed_task_id")
    private String timedTaskId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 删除标识(0:未删除;1:删除)
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field2")
    private String alternateField2;




}