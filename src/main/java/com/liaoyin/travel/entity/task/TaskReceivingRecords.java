package com.liaoyin.travel.entity.task;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@ApiModel("任务领取记录")
@Table(name = "t_task_receiving_records")
@Data
@Accessors(chain = true)
public class TaskReceivingRecords {
	
	@Id
    private String id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private String taskId;

    /**
     * 任务状态【字典 tastStatus】
     */
    @Column(name = "tast_status")
    private Short tastStatus;

    public String getTaskStatusDisplay() {
        return this.tastStatus!=null?DictUtil.getDisplay("tastStatus", this.tastStatus+""):null;
    }

    /**
     * 领取时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date creatTime;
    
    @Column(name="task_start_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("任务开始时间")
    private Date taskStartTime;
    
    @Column(name="task_end_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("任务结束时间")
    private Date taskEndTime;

    @ApiModelProperty("用于临时任务管理的关联(定时任务不传此参数)")
    @Column(name = "task_manager_id")
    private String taskManagerId;

    @ApiModelProperty("发布方式(1:定时发布;2:临时发布)")
    @Column(name = "release_way")
    private String releaseWay;

    @Column(name = "scenic_id")
    private String scenicId;

    @Transient
	@ApiModelProperty("任务名称")
    private String taskName;
	
	@Transient
	@ApiModelProperty("任务类型【字典 taskType】")
	private Short taskType;

    public String getTaskTypeDisplay() {
        return this.taskType!=null?DictUtil.getDisplay("taskType", this.taskType+""):null;
    }

    @Transient
    @ApiModelProperty("任务发放人ID")
    private String tempId;

	@Transient
	@ApiModelProperty("任务发放人")
	private String nickName;
	
	@Transient
	@ApiModelProperty("执行人")
	private String executorName;
	
	@Transient
	@ApiModelProperty("工种名称")
	private String workName;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskReceivingRecords{");
        sb.append("id='").append(id).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", taskId='").append(taskId).append('\'');
        sb.append(", tastStatus=").append(tastStatus);
        sb.append(", creatTime=").append(creatTime);
        sb.append(", taskStartTime=").append(taskStartTime);
        sb.append(", taskEndTime=").append(taskEndTime);
        sb.append(", taskManagerId='").append(taskManagerId).append('\'');
        sb.append(", releaseWay='").append(releaseWay).append('\'');
        sb.append(", scenicId='").append(scenicId).append('\'');
        sb.append(", taskName='").append(taskName).append('\'');
        sb.append(", taskType=").append(taskType);
        sb.append(", tempId='").append(tempId).append('\'');
        sb.append(", nickName='").append(nickName).append('\'');
        sb.append(", executorName='").append(executorName).append('\'');
        sb.append(", workName='").append(workName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}