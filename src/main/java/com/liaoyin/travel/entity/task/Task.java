package com.liaoyin.travel.entity.task;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.reactivestreams.Publisher;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@ApiModel("任务列表")
@Data
@Table(name = "t_task")
@Accessors(chain = true)
public class Task{
	
	@Id
    private String id;

    /**
     * 任务名称
     */
    @Column(name = "task_name")
    @ApiModelProperty("任务名称")
    @NotBlank(message = "任务名称不能为空")
    private String taskName;

    /**
     * 任务类型【字典 taskType】
     */
    @Column(name = "task_type")
    @NotBlank(message = "任务类型不能为空")
    @ApiModelProperty("任务类型【字典 taskType】")
    private Short taskType;

    /**
     * 任务级别【字典 taskLevel】
     */
    @Column(name = "task_level")
    @NotBlank(message = "任务级别不能为空")
    @ApiModelProperty("任务级别【字典 taskLevel】")
    private Short taskLevel;

    /**
     * 任务类别【字典 taskCategories】
     */
    @Column(name = "task_categories")
    @NotBlank(message = "任务类别不能为空")
    @ApiModelProperty("任务类别【字典 taskCategories】")
    private Short taskCategories;

    /**
     * 任务说明
     */
    @Column(name = "task_statement")
    @ApiModelProperty("任务说明")
    private String taskStatement;

    /**
     * 任务发布人id
     */
    @Column(name = "user_id")
    @NotBlank(message = "任务名称不能为空")
    @ApiModelProperty("任务发布人id")
    private String userId;

    @Transient
    @ApiModelProperty("任务发布人类型(1.APP用户;2.后台管理员)")
    private String publisherType;

    /**
     * 发布时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("发布时间")
    private Date creatTime;

    /**
     * 是否删除【0：未删除 1：删除】
     */
    @Column(name = "is_delete")
    @ApiModelProperty("是否删除【0：未删除 1：删除】")
    private Short isDelete;

    /**
     * 是否指定人员【0：所有人  1：指定人员 2：指定部门 3：指定工种】
     */
    @Column(name = "is_specify")
    @ApiModelProperty("是否指定人员【 0：所有人;1：指定人员;2：指定部门;3：指定工种】")
    private Short isSpecify;

    @Column(name="line_id")
    @ApiModelProperty("线路id")
    private String lineId;

    /**
     * 普通任务的纬度
     */
    @Column(name = "task_lat")
    @ApiModelProperty("普通任务的纬度")
    private String taskLat;

    /**
     * 普通任务的纬度
     */
    @Column(name = "task_log")
    @ApiModelProperty("普通任务的经度")
    private String taskLog;

    @Column(name="task_end_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("任务截止时间")
    private Date taskEndTime;

    @ApiModelProperty("对应事件ID，针对灾害发布事件任务时必填")
    @Column(name = "event_id")
    private String eventId;

    @ApiModelProperty("用于临时任务管理的关联(定时任务不传此参数)")
    @Column(name = "task_manager_id")
    private String taskManagerId;

    @ApiModelProperty("发布方式(1:定时发布;2:临时发布)")
    @Column(name = "release_way")
    private String releaseWay;

    /**
     * 景区id
     */
    @Column(name = "scenic_id ")
    private String scenicId;

    @Transient
    @ApiModelProperty("任务详情列表")
    private List<TaskDetails> taskDetailsList;
    
    @Transient
    @ApiModelProperty("任务状态【字典 taskStatus】")
    private String taskStatus;
    
    @Transient
    @ApiModelProperty("发布人昵称")
    private String nickName;
    
    @Transient
    @ApiModelProperty("任务领取id")
    private String receivingRecordsId;
    
    @Transient
    @ApiModelProperty("任务领取人id集合")
    private String businessId;

    @Transient
    @ApiModelProperty("任务领取信息")
    private List<TaskReceivingRecords> taskReceivingRecords;

    @Transient
    @ApiModelProperty("简报是否上传 0 未上传 1已上传")
    private Integer userBriefing;

    @Transient
    @ApiModelProperty("简报提交时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date userBriefingTime;

    @Transient
    @ApiModelProperty("头像的地址")
    private String headPicUrl;

    @Transient
    @ApiModelProperty("领取人")
    private String receiver;

    @Transient
    @ApiModelProperty("领取人id")
    private String receiverId;

    @ApiModelProperty("临时发布类型【字典：publishType，1即时，2定时】")
    @Column(name = "publish_type")
    private Short publishType;

    @ApiModelProperty("发布时间")
    @Column(name = "publish_time")
    private String publishTime;

    public String getTaskStatusDisplay() {
        return this.taskStatus!=null?DictUtil.getDisplay("tastStatus", this.taskStatus):null;
    }
    public String getTaskTypeDisplay() {
        return this.taskType!=null?DictUtil.getDisplay("taskType", this.taskType+""):null;
    }
    public String getTaskLevelDisplay() {
        return this.taskLevel!=null?DictUtil.getDisplay("taskLevel", this.taskLevel+""):null;
    }
    public String getTaskCategoriesDisplay() {
        return this.taskCategories!=null?DictUtil.getDisplay("taskCategories", this.taskCategories+""):null;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Task{");
        sb.append("id='").append(id).append('\'');
        sb.append(", taskName='").append(taskName).append('\'');
        sb.append(", taskType=").append(taskType);
        sb.append(", taskLevel=").append(taskLevel);
        sb.append(", taskCategories=").append(taskCategories);
        sb.append(", taskStatement='").append(taskStatement).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", creatTime=").append(creatTime);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", isSpecify=").append(isSpecify);
        sb.append(", lineId='").append(lineId).append('\'');
        sb.append(", taskLat='").append(taskLat).append('\'');
        sb.append(", taskLog='").append(taskLog).append('\'');
        sb.append(", taskEndTime=").append(taskEndTime);
        sb.append(", eventId='").append(eventId).append('\'');
        sb.append(", taskManagerId='").append(taskManagerId).append('\'');
        sb.append(", releaseWay='").append(releaseWay).append('\'');
        sb.append(", scenicId='").append(scenicId).append('\'');
        sb.append(", taskDetailsList=").append(taskDetailsList);
        sb.append(", taskStatus='").append(taskStatus).append('\'');
        sb.append(", nickName='").append(nickName).append('\'');
        sb.append(", receivingRecordsId='").append(receivingRecordsId).append('\'');
        sb.append(", businessId='").append(businessId).append('\'');
        sb.append(", taskReceivingRecords=").append(taskReceivingRecords);
        sb.append(", userBriefing=").append(userBriefing);
        sb.append(", userBriefingTime=").append(userBriefingTime);
        sb.append(", headPicUrl='").append(headPicUrl).append('\'');
        sb.append(", receiver='").append(receiver).append('\'');
        sb.append(", receiverId='").append(receiverId).append('\'');
        sb.append(", publishType=").append(publishType);
        sb.append(", publishTime='").append(publishTime).append('\'');
        sb.append('}');
        return sb.toString();
    }
}