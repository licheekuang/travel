package com.liaoyin.travel.entity.task;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@ApiModel("任务线路")
@Table(name = "t_task_line")
@Data
@Accessors(chain = true)
public class TaskLine {
	
	@Id
    private String id;

    /**
     * 线路名称
     */
    @Column(name = "line_name")
    @ApiModelProperty("线路名称")
    @Size(min = 2, max = 25, message = "线路名称请保持在2到25之间")
    private String lineName;

    @Column(name = "is_delete")
    private Short isDelete;

    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creatTime;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    @Transient
    @ApiModelProperty("任务详情列表")
    private List<TaskDetails> taskDetailsList;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskLine{");
        sb.append("id='").append(id).append('\'');
        sb.append(", lineName='").append(lineName).append('\'');
        sb.append(", isDelete=").append(isDelete);
        sb.append(", creatTime=").append(creatTime);
        sb.append(", scenicId='").append(scenicId).append('\'');
        sb.append(", taskDetailsList=").append(taskDetailsList);
        sb.append('}');
        return sb.toString();
    }
}