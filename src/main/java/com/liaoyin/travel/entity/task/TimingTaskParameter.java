package com.liaoyin.travel.entity.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;
/**
 * 定时任务参数
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-26 13:12
 */
@Table(name = "t_timing_task_parameter")
@Data
@Accessors(chain = true)
public class TimingTaskParameter {

    @Id
    private String id;

    @ApiModelProperty("业务id【可关联 用户 部门 工种 的id】")
    private String businessId;

    @Column(name = "task_name")
    @ApiModelProperty("任务名称")
    private String taskName;

    @Column(name = "task_type")
    @ApiModelProperty("任务类型【字典 taskType】")
    private Short taskType;

    @ApiModelProperty(value = "任务类型Display")
    public String getTaskTypeDisplay() {
        if (this.taskType != null) {
            return DictUtil.getDisplay("taskType", this.taskType.toString());
        }
        return null;
    }

    @ApiModelProperty("任务级别【字典 taskLevel】")
    @Column(name = "task_level")
    private Short taskLevel;

    @ApiModelProperty(value = "任务级别Display")
    public String getTaskLevelDisplay() {
        if (this.taskType != null) {
            return DictUtil.getDisplay("taskLevel", this.taskLevel.toString());
        }
        return null;
    }

    @ApiModelProperty("任务类别【字典 taskCategories】")
    @Column(name = "task_categories")
    private Short taskCategories;

    @ApiModelProperty(value = "任务类别Display")
    public String getTaskCategoriesDisplay() {
        if (this.taskCategories != null) {
            return DictUtil.getDisplay("taskCategories", this.taskCategories.toString());
        }
        return null;
    }

    @ApiModelProperty("任务说明")
    @Column(name = "task_statement")
    private String taskStatement;

    @ApiModelProperty("任务发布人id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty("任务发布人")
    @Transient
    private String publisher;

    @ApiModelProperty("定时任务创建时间")
    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty("是否指定人员【0：所有人  1：指定人员 2：指定部门 3：指定工种】")
    @Column(name = "is_specify")
    private Short isSpecify;

    @ApiModelProperty("线路id")
    @Column(name = "line_id")
    private String lineId;

    @Column(name = "task_lat")
    private String taskLat;

    @Column(name = "task_log")
    private String taskLog;

    @Column(name = "task_end_time")
    private Date taskEndTime;

    @ApiModelProperty("0:未删除;1:删除")
    @Column(name = "is_delete")
    private String isDelete;

    @ApiModelProperty("启用状态(0:不启用;1:启用)")
    @Column(name = "enabled")
    private String enabled;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

    /**
     * 执行任务的地理信息(旅游移动管控系统需要的字段)
     */
    private String location;

    /**
     * 备用字段1
     */
    @Column(name = "alternate_field1")
    private String alternateField1;

    /**
     * 备用字段2
     */
    @Column(name = "alternate_field2")
    private String alternateField2;

    /**
     * 备用字段3
     */
    @Column(name = "alternate_field3")
    private String alternateField3;


    @ApiModelProperty("定时任务执行的时间参数")
    @Transient
    private List<TaskTiming> taskTimingList;

    @ApiModelProperty("临时发布类型【字典：publishType，1即时，2定时】")
    @Column(name = "publish_type")
    private Short publishType;


}