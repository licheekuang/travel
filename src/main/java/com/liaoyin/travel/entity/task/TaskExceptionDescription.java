package com.liaoyin.travel.entity.task;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@ApiModel("任务异常说明")
@Table(name = "t_task_exception_description")
@Data
@Accessors(chain = true)
public class TaskExceptionDescription {
	
	@Id
    private String id;

    @Column(name = "t_task_id")
    private String taskId;

    /**
     * 当前任务状态【字典 tastStatus】
     */
    @Column(name = "tast_status")
    private Short tastStatus;
    
    @Column(name="reasons_revocation")
    @ApiModelProperty("撤销理由【字典 reasonsRevocation】")
    private Short reasonsRevocation;

    public String getReasonsRevocationDisplay() {
        return this.reasonsRevocation !=null ? DictUtil.getDisplay("reasonsRevocation", this.reasonsRevocation+""):null;
    }

    /**
     * 申请人id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 任务异常说明
     */
    @Column(name = "exception_context")
    private String exceptionContext;

    /**
     * 任务异常时间
     */
    @Column(name = "creat_time")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date creatTime;
    
    @Column(name="handler_user_id")
    @ApiModelProperty("处理人id")
    private String handlerUserId;
    
    @Column(name="handler_status")
    @ApiModelProperty("处理结果【0:等待处理 1：同意,任务完成  2：拒绝,任务继续】")
    private Short handlerStatus;
    
    @Column(name="handler_context")
    @ApiModelProperty("处理意见备注")
    private String handlerContext;

    @Column(name = "is_delete")
    @ApiModelProperty("是否删除 1删除 0未删除")
    private Short isDelete;

    /**
     * 景区id
     */
    @Column(name = "scenic_id")
    private String scenicId;

	@Transient
    @ApiModelProperty("任务名称")
	private String taskName;
    
	@Transient
    @ApiModelProperty("申请人名称")
	private String nickName;
	
}