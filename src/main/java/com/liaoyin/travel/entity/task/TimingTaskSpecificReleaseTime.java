package com.liaoyin.travel.entity.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时任务具体的发布时间记录
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-25 15:20
 */
@Table(name = "t_timing_task_specific_release_time")
@Data
@Accessors(chain = true)
public class TimingTaskSpecificReleaseTime {
    private String id;

    @Column(name = "timing_task_parameter_id")
    @ApiModelProperty("定时任务参数id")
    private String timingTaskParameterId;

    @Column(name = "wee_num")
    private Integer weeNum;

    @ApiModelProperty("星期几(发布当天是星期几)")
    public String getWeekDay(){
        if(weeNum!=null) {
            if (weeNum == 1) {
                return "星期一";
            } else if (weeNum == 2) {
                return "星期二";
            } else if (weeNum == 3) {
                return "星期三";
            } else if (weeNum == 4) {
                return "星期四";
            } else if (weeNum == 5) {
                return "星期五";
            } else if (weeNum == 6) {
                return "星期六";
            }else if (weeNum == 7) {
                return "星期天";
            }
        }
        return null;
    }

    @Column(name = "specific_release_time")
    @ApiModelProperty("具体发布时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date specificReleaseTime;

    @ApiModelProperty("具体发布时间 Display")
    public String getSpecificReleaseTimeDisplay(){
        if(specificReleaseTime!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
           return sdf.format(specificReleaseTime);
        }
        return null;
    }

    @Column(name = "is_delete")
    @ApiModelProperty("0:未删除;1:删除")
    private String isDelete;

    @Column(name = "post_status")
    @ApiModelProperty("发布状态(0:未发;1:已发)")
    private String postStatus;

    @Column(name = "create_time")
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    @Column(name = "scenic_id")
    @ApiModelProperty("景点id")
    private String scenicId;

    @Column(name = "alternate_field1")
    @ApiModelProperty("备用字段1")
    private String alternateField1;

    @Column(name = "alternate_field2")
    @ApiModelProperty("备用字段2")
    private String alternateField2;




}