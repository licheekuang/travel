package com.liaoyin.travel.entity.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 定时任务保存的发布时间
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-25 15:20
 */
@Data
@Accessors(chain = true)
@Table(name = "t_task_timing")
public class TaskTiming {

    @Id
    private String id;

    @Column(name = "task_id")
    @ApiModelProperty("定时任务参数id")
    private String taskId;

    @ApiModelProperty("发布的星期")
    @Column(name = "pub_week")
    private String pubWeek;

    @ApiModelProperty("发布的星期字符串")
    @Transient
    private String pubWeeks;

    @Column(name = "pub_time")
    @ApiModelProperty("发布的时间")
    private String pubTime;

    @ApiModelProperty("发布状态【0:未发布;1:发布】")
    @Column(name = "is_delete")
    private String isDelete;

    @ApiModelProperty("创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "scenic_id")
    private String scenicId;

}