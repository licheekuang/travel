package com.liaoyin.travel.entity;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

import javax.persistence.*;

import com.liaoyin.travel.base.entity.Page;
import lombok.Getter;
import lombok.Setter;


@Table(name = "dict_basic")
public class DictBasic extends Page {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT REPLACE(UUID(),'-','')")
    @ApiModelProperty("主键")
    private String id;

    /**
     * 字典类型编码
     */
    @ApiModelProperty("字典类型编码")
    private String code;

    /**
     * 字典值【key】
     */
    @ApiModelProperty("字典值【key】")
    private Integer value;
   

    /**
     * 字典显示内容【value】
     */
    @ApiModelProperty("字典显示内容【value】")
    private String display;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    @Column(name = "dict_order")
    private Integer dictOrder;

    /**
     * 是否禁用
     */
    @ApiModelProperty("是否禁用")
    @Column(name = "is_using")
    private Short isUsing;

    /**
     * 分组code
     */
    @ApiModelProperty("分组code")
    @Column(name = "group_code")
    private String groupCode;

    /**
     * 父ID
     */
    @ApiModelProperty("父级id")
    @Column(name = "parent_id")
    private String parentId;

    @ApiModelProperty("图标关联ID")
    @Column(name = "pic_id")
    private String picId;

    public String getPicId() {
        return picId;
    }

    public void setPicId(String picId) {
        this.picId = picId;
    }

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取字典类型编码
     *
     * @return code - 字典类型编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置字典类型编码
     *
     * @param code 字典类型编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取字典值【key】
     *
     * @return value - 字典值【key】
     */
    public Integer getValue() {
        return value;
    }

    /**
     * 设置字典值【key】
     *
     * @param value 字典值【key】
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * 获取字典显示内容【value】
     *
     * @return display - 字典显示内容【value】
     */
    public String getDisplay() {
        return display;
    }

    /**
     * 设置字典显示内容【value】
     *
     * @param display 字典显示内容【value】
     */
    public void setDisplay(String display) {
        this.display = display;
    }

    /**
     * 获取排序
     *
     * @return dict_order - 排序
     */
    public Integer getDictOrder() {
        return dictOrder;
    }

    /**
     * 设置排序
     *
     * @param dictOrder 排序
     */
    public void setDictOrder(Integer dictOrder) {
        this.dictOrder = dictOrder;
    }

    /**
     * 获取是否禁用
     *
     * @return is_using - 是否禁用
     */
    public Short getIsUsing() {
        return isUsing;
    }

    /**
     * 设置是否禁用
     *
     * @param isUsing 是否禁用
     */
    public void setIsUsing(Short isUsing) {
        this.isUsing = isUsing;
    }

    /**
     * 获取分组code
     *
     * @return group_code - 分组code
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * 设置分组code
     *
     * @param groupCode 分组code
     */
    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    /**
     * 获取父ID
     *
     * @return parent_id - 父ID
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 设置父ID
     *
     * @param parentId 父ID
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    
    public String getIsUsingDisplay() {
    	return this.isUsing!=null && this.isUsing.intValue()==1?"启用":"禁用";
    }

    @Transient
    @Getter
    @Setter
    @ApiModelProperty("图标")
    public String picUrl;
    
    @Transient
    @Getter
    @Setter
    public List<FileUpload> fileUploadList;
}