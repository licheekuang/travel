package com.liaoyin.travel.entity;

import javax.persistence.*;

@Table(name = "s_dict_value")
public class DictValue {
    @Id
    private String id;

    /**
     * 字典code
     */
    @Column(name = "dict_code")
    private String dictCode;

    /**
     * 字典真实值
     */
    @Column(name = "dict_value")
    private String dictValue;

    /**
     * 字典页面值
     */
    @Column(name = "dict_display")
    private String dictDisplay;

    /**
     * 字典描述
     */
    @Column(name = "dict_desc")
    private String dictDesc;

    /**
     * 父ID
     */
    @Column(name = "parent_id")
    private String parentId;

    /**
     * 排序
     */
    @Column(name = "sort_order")
    private Integer sortOrder;

    /**
     * 分组code
     */
    @Column(name = "group_code")
    private String groupCode;

    /**
     * 删除标识（1是0否）
     */
    @Column(name = "del_flag")
    private String delFlag;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取字典code
     *
     * @return dict_code - 字典code
     */
    public String getDictCode() {
        return dictCode;
    }

    /**
     * 设置字典code
     *
     * @param dictCode 字典code
     */
    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    /**
     * 获取字典真实值
     *
     * @return dict_value - 字典真实值
     */
    public String getDictValue() {
        return dictValue;
    }

    /**
     * 设置字典真实值
     *
     * @param dictValue 字典真实值
     */
    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    /**
     * 获取字典页面值
     *
     * @return dict_display - 字典页面值
     */
    public String getDictDisplay() {
        return dictDisplay;
    }

    /**
     * 设置字典页面值
     *
     * @param dictDisplay 字典页面值
     */
    public void setDictDisplay(String dictDisplay) {
        this.dictDisplay = dictDisplay;
    }

    /**
     * 获取字典描述
     *
     * @return dict_desc - 字典描述
     */
    public String getDictDesc() {
        return dictDesc;
    }

    /**
     * 设置字典描述
     *
     * @param dictDesc 字典描述
     */
    public void setDictDesc(String dictDesc) {
        this.dictDesc = dictDesc;
    }

    /**
     * 获取父ID
     *
     * @return parent_id - 父ID
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 设置父ID
     *
     * @param parentId 父ID
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取排序
     *
     * @return sort_order - 排序
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * 设置排序
     *
     * @param sortOrder 排序
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 获取分组code
     *
     * @return group_code - 分组code
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * 设置分组code
     *
     * @param groupCode 分组code
     */
    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    /**
     * 获取删除标识（1是0否）
     *
     * @return del_flag - 删除标识（1是0否）
     */
    public String getDelFlag() {
        return delFlag;
    }

    /**
     * 设置删除标识（1是0否）
     *
     * @param delFlag 删除标识（1是0否）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }
}