package com.liaoyin.travel.exception;

import java.util.Arrays;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：全局异常处理类
 * @日期：Created in 2018/6/8 14:49
 */
public class BusinessException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3830256277582181780L;
	
	public BusinessException(String code) {
		super(code);
		this.code = code;
	}

	public BusinessException(String code, String... args) {
		super(code);
		this.args=args;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("BusinessException{");
		sb.append("code='").append(code).append('\'');
		sb.append(", desc='").append(desc).append('\'');
		sb.append(", args=").append(args == null ? "null" : Arrays.asList(args).toString());
		sb.append('}');
		return sb.toString();
	}
}
