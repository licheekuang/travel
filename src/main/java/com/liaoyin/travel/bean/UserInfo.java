package com.liaoyin.travel.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.liaoyin.travel.view.moble.back.MenuView;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.entity.FileUpload;
import com.liaoyin.travel.entity.UserTeam;
import com.liaoyin.travel.util.DictUtil;
import com.liaoyin.travel.util.FileUploadUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	//用户id
	private String id;
	//备用用户id
	private String userId;
	//登录名
	private String account;
	//用户名字
	private String nickName;
	//电话
	private String phone;
	//用户头像URL
	private String headPicUrl;
	//密码
    private String password;
    //人员类型(1.员工;2.经理)
    private String userType;
	//token
	private String token;
	//是否启用
	@ApiModelProperty("是否启用 0禁用 1 启用")
	private String isUsing;

	@ApiModelProperty("景区id")
	private String scenicId;

	@ApiModelProperty("是否为文旅委管理员(0:否；1:是)")
	private String isCommittee;

	@ApiModelProperty("员工类型；关联数据字典【emptype】)")
	private String emptype;

	@ApiModelProperty("员工类型Display")
	public String getEmptypeDisplay(){
		return this.emptype != null ?DictUtil.getDisplay("emptype", this.emptype+""):null;
	}

	@ApiModelProperty(value = "账号类型(1:景区;2:机构)")
	private String accountType;

	@ApiModelProperty("账号类型Display")
	public String getAccountTypeDisplay(){
		if(accountType!=null){
			return accountType.equals("1")?"景区":"机构";
		}
		return null;
	}

	@ApiModelProperty("机构类型【关联数据库organType字段】")
	@Column(name = "organ_type")
	private String organType;

	@ApiModelProperty("机构类型Display")
	public String getOrganTypeDisplay(){
		if(organType!=null && !organType.equals("")){
			return DictUtil.getDisplay("organType",organType);
		}
		return null;
	}

	/*@ApiModelProperty("班组id")
	private String teamId;*/
	
	/*@ApiModelProperty("部门名称")
	private String teamName;*/
	
	@ApiModelProperty("工种名称")
	private String workerName;
	
	@ApiModelProperty("工种id")
    private String workId;
	
	@ApiModelProperty("担任职务【1：部门经理 2：员工】")
	private Short assumeOffice;

	@ApiModelProperty("部门信息")
	private List<UserTeam> userTeamList;

	@ApiModelProperty("用户角色Id")
	private String roleId;

	@ApiModelProperty("角色名称")
	private String roleName;

	//注册时间
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@ApiModelProperty(value = "性别")
	private String sex;

	//菜单权限列表
	private List<MenuView> menuList;

	private Serializable userHeadPicUrl;

	 public String getUserTypeDisplay() {
    	return this.userType != null ?DictUtil.getDisplay("userType", this.userType+""):null;
    }
	
	public String getHeadPicUrlDisplay(){
		return this.headPicUrl != null && !"".equals(headPicUrl)?CommonConstant.FILE_SERVER+this.headPicUrl:null;
	}
	public FileUpload getHeadPicDisplay(){
		return this.headPicUrl != null && !"".equals(headPicUrl)? FileUploadUtil.getFileUpload(headPicUrl):null;
	}


}
