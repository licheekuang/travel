package com.liaoyin.travel.base.service;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.entity.Page;
import com.liaoyin.travel.entity.scenicInfo.*;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：公共业务类
 * @日期：Created in 2018/6/8 11:30
 */
public abstract class BaseService<M extends Mapper<T>, T> {

    public static Map<String, String> gpsDataMap = new HashMap<>();

    public static Map<String, String> tempTaskMap = new HashMap<>();

    @Autowired
    protected M mapper;

    public void setMapper(M mapper) {
        this.mapper = mapper;
    }

    public T selectOne(T entity) {
        return mapper.selectOne(entity);
    }


    public T selectById(Object id) {
        T t = mapper.selectByPrimaryKey(id);
        return t;
    }


//    public List<T> selectListByIds(List<Object> ids) {
//        return mapper.selectByIds(ids);
//    }


    public List<T> selectList(T entity) {
        return mapper.select(entity);
    }


    public List<T> selectListAll() {
        return mapper.selectAll();
    }


//    public Long selectCountAll() {
//        return mapper.selectCount(null);
//    }


    public Long selectCount(T entity) {
        return new Long(mapper.selectCount(entity));
    }


    public void insert(T entity) {
        mapper.insert(entity);
    }


    public void insertSelective(T entity) {
        mapper.insertSelective(entity);
    }


    public void delete(T entity) {
        mapper.delete(entity);
    }


    public void deleteById(Object id) {
        mapper.deleteByPrimaryKey(id);
    }


    public void updateById(T entity) {
        mapper.updateByPrimaryKey(entity);
    }


    public void updateSelectiveById(T entity) {
        mapper.updateByPrimaryKeySelective(entity);

    }

    public List<T> selectByExample(Object example) {
        return mapper.selectByExample(example);
    }

    public int selectCountByExample(Object example) {
        return mapper.selectCountByExample(example);
    }

    // 过滤特殊字符
    public String StringFilter(String   str)   throws PatternSyntaxException {
        // 只允许字母和数字
        // String   regEx  =  "[^a-zA-Z0-9]";

        // 清除掉所有特殊字符
        String regEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Pattern p   =   Pattern.compile(regEx);
        Matcher m   =   p.matcher(str);
        return   m.replaceAll("").trim();
    }
    
    public void executePage(Page page) {
        page.setIsParam(true);
        if (page.getNum() != null && page.getSize() != null) {
            PageHelper.startPage(page.getNum(), page.getSize());
            /*PageHelper.orderBy(this.getOrderby(page.getOrderby()));*/
        }
    }
    
    /**
     * 方   法  名:orderBy
     * 创 建 日 期:2018/1/10/010 9:57
     * 创  建   者:zhang.zhipeng
     * 描       述:一般排序
     * 参       数:[page]
     * 返  回   值:void
     */
    public void orderBy(Page page) {
        if (page.getOrderby() != null) {
            PageHelper.orderBy(getOrderby(page.getOrderby()));
        }
    }
    
    /**
     * 方   法  名：getOrderby
     * 创建日期：2017年8月31日 下午7:34:37
     * 创   建  者：zhou.ning
     * 描          述：字段排序中的 驼峰命名转为下划线字段，例如：userName Asc -> user_name Asc
     *
     * @param orderby
     * @return
     */
    public String getOrderby(String orderby) {
        orderby = StringFilter(orderby);
        String reOrderby = "";
        if (orderby != null && !orderby.isEmpty()) {
            //按空格分割
            String[] temp1 = orderby.split(" ");
            //按字母大写分割
            String[] temp2 = temp1[0].split("(?<!^)(?=[A-Z])");
            for (String obj : temp2) {
                reOrderby += obj.toLowerCase() + "_";
            }
            reOrderby = reOrderby.substring(0, reOrderby.length() - 1) + " " + temp1[1];
        }
        return reOrderby;

    }

}
