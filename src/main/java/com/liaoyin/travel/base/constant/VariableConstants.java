/**
 * Copyright(c)2013-2016 by liaoyin
 * All rights reserved
 */
package com.liaoyin.travel.base.constant;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：常量类
 * @日期：Created in 2018/6/8 11:10
 */
public class VariableConstants {
	//"1"
	public static final String STRING_CONSTANT_1 = "1";
	//"0"
	public static final String STRING_CONSTANT_0 = "0";
	//"2"
	public static final String STRING_CONSTANT_2 = "2";
	//"3"
	public static final String STRING_CONSTANT_3 = "3";
	//"4"
	public static final String STRING_CONSTANT_4 = "4";
	//"5"
	public static final String STRING_CONSTANT_5 = "5";
	//"6"
	public static final String STRING_CONSTANT_6 = "6";
	//"7"
	public static final String STRING_CONSTANT_7 = "7";
	//"8"
	public static final String STRING_CONSTANT_8 = "8";
	//"9"
	public static final String STRING_CONSTANT_9= "9";
	//"10"
	public static final String STRING_CONSTANT_10 = "10";
	//"11"
	public static final String STRING_CONSTANT_11 = "11";
	//"12"
	public static final String STRING_CONSTANT_12 = "12";
	//"13"
	public static final String STRING_CONSTANT_13 = "13";
	//"14"
	public static final String STRING_CONSTANT_14 = "14";
	//"15"
	public static final String STRING_CONSTANT_15 = "15";
	
	
	//功能编码 —— 用户注册
	public static final String FUNCTION_CODE_YHZC = "yhzc";
	//功能编码 —— 用户登录
	public static final String FUNCTION_CODE_YHDL = "yhdl";
	//功能编码 —— 忘记密码
	public static final String FUNCTION_CODE_WJMM = "wjmm";
}
