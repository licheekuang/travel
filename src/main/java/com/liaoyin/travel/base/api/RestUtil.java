package com.liaoyin.travel.base.api;


public class RestUtil {

	public static JsonRestResponse createResponse(Object obj) {
		JsonRestResponse result = new JsonRestResponse();
		result.setCode("rest.success");
		result.setDesc("成功");
		result.setResult(obj);
		return result;
	}
	
	public static JsonRestResponse createResponse() {
		return createResponse(null);
	}
}
