package com.liaoyin.travel.base.dao;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：demo持久层接口
 * @日期：Created in 2018/6/8 17:30
 */
public interface BaseMapper<T> extends  Mapper<T> , InsertListMapper<T> {

}