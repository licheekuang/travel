package com.liaoyin.travel.base.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：分页类
 * @日期：Created in 2018/6/8 11:19
 */
@Getter
@Setter
public class Page implements Serializable{

    private static final long serialVersionUID = -5206444485441643679L;
    /**
     * 第几页
     */
	@ApiModelProperty(value = "第几页")
    @Transient
    Integer num;
    /**
     * 每页显示多少行
     */
	@ApiModelProperty(value = "每页显示多少行")
    @Transient
    Integer size;
    /**
     * 排序字段 例如：orgType desc/asc
     */
	@ApiModelProperty(value = "排序字段 例如：orgType desc/asc")
    @Transient
    String orderby;
	
	/**
     * 是否是传参，作为传参时，不应该设置display
     */
    @Transient
    Boolean isParam = false;
}
