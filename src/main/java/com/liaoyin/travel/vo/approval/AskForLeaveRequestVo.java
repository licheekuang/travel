package com.liaoyin.travel.vo.approval;

import com.liaoyin.travel.common.validate.Insert;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 发起请假审批的请求参数
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 17:16
 */
@Data
public class AskForLeaveRequestVo {

    @ApiModelProperty("审批记录公共参数")
    private ApprovalRecordVo approvalRecordVo;

    @ApiModelProperty("请假开始时间")
    @NotBlank(groups={Insert.class},message = "请假开始时间不能为空")
    private String leaveStartTime;

    @ApiModelProperty("请假结束时间")
    @NotBlank(groups={Insert.class},message = "请假结束时间不能为空")
    private String leaveEndTime;

    @ApiModelProperty("请假时长")
    @NotBlank(groups={Insert.class},message = "请假时长不能为空")
    private String leaveTime;

    @ApiModelProperty("请假事由")
    @NotBlank(groups={Insert.class},message = "请假事由不能为空")
    private String reasonForALeave;

    @ApiModelProperty("附件的url")
    private String accessoryUrl;







}
