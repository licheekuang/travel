package com.liaoyin.travel.vo.approval;

import com.liaoyin.travel.common.validate.Insert;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * 新增【外出申请】请求参数
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 20:22
 */
@Data
public class GoOutRecordVo {

    @ApiModelProperty("审批记录公共参数")
    private ApprovalRecordVo approvalRecordVo;

    @ApiModelProperty("外出开始时间")
    @NotBlank(groups={Insert.class},message = "外出开始时间不能为空")
    private String departureTime;

    @ApiModelProperty("外出结束时间")
    @NotBlank(groups={Insert.class},message = "外出结束时间不能为空")
    private String endOfOuting;

    @ApiModelProperty("外出时长")
    private String goOutTime;

    @ApiModelProperty("外出事由")
    @NotBlank(groups={Insert.class},message = "外出事由不能为空")
    private String goOutFor;

    @ApiModelProperty("交通费金额")
    private BigDecimal carFare;


}
