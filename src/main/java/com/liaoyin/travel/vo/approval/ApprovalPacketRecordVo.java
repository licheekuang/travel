package com.liaoyin.travel.vo.approval;

import com.liaoyin.travel.base.entity.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 审批列表查询 vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 20:32
 */
@Data
public class ApprovalPacketRecordVo extends Page {

    @ApiModelProperty("分组名")
    private String packetName;

    /**
     * 景区id
     */
    private String scenicId;
}
