package com.liaoyin.travel.vo.approval;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 新增或修改可见范围设置
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 09:55
 */
@Data
public class InsertOrUpdateApprovalVisualRangeVo {

    @ApiModelProperty("审批-可见范围设置的id")
    private String approvalVisualRangeId;

    @ApiModelProperty("审批类型【数据字典approvalType】")
    private String approvalType;

    @ApiModelProperty("可见范围【数据字典visualRange】")
    private String visualRange;

    @ApiModelProperty("可见人id字符集")
    private String everyMenIds;
}
