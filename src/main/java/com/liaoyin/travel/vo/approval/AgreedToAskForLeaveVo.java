package com.liaoyin.travel.vo.approval;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

/**
 * 同意请假之后执行操作需要的参数
 *
 * @author kuang.jiazhuo
 * @date 2019-11-24 14:50
 */
@Data
@Accessors(chain = true)
public class AgreedToAskForLeaveVo {

    @ApiModelProperty("审批发起人的id")
    private String initiatorId;

    @ApiModelProperty("请假的开始日期")
    private LocalDate startLeaveDate;

    @ApiModelProperty("开始请假是上午还是下午")
    private String startLeaveTime;

    @ApiModelProperty("请假的结束日期")
    private LocalDate leaveEndDate;

    @ApiModelProperty("结束请假是上午还是下午")
    private String leaveEndTime;

    @ApiModelProperty("请假时长(单位:小时;最小时长为0.5)")
    private double duration;
}
