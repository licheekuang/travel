package com.liaoyin.travel.vo.approval;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 处理请假审批的 vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-24 14:33
 */
@Data
public class AskForLeaveApprovalVo {

    @ApiModelProperty("是否同意(0:否;1:是)")
    private String isConsent;

    @ApiModelProperty("审批发起人的id")
    private String initiatorId;

    @ApiModelProperty("审批处理人的id")
    private String approverId;

    @ApiModelProperty("事由")
    private String reasons;

    @ApiModelProperty("请假开始时间String")
    private String leaveStartTimeAndroid;

    @ApiModelProperty("请假结束时间String")
    private String leaveEndTimeAndroid;

    @ApiModelProperty("请假共计时长")
    private String duration;

    @ApiModelProperty("审批记录ID")
    private String approvalRecordId;

    @ApiModelProperty("拒绝原因")
    private String  refusalCause;

    @ApiModelProperty("审批的发起时间 【yyyy-MM-dd HH:mm:ss】")
    private String initiationTime;
}
