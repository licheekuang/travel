package com.liaoyin.travel.vo.approval;

import com.liaoyin.travel.base.entity.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 审批记录后台查询 vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-26 09:47
 */
@Data
public class ApprovalRecordBackVo extends Page {

    @ApiModelProperty("审批类型(1.补卡;2.请假;3.外出)")
    private String approvalType;

    @ApiModelProperty("发起审批的开始搜索时间")
    private String initiationTime;

    @ApiModelProperty("发起审批的结束搜索时间")
    private String launchEndTime;

    @ApiModelProperty("完成审批的开始搜索时间")
    private String completionStartTime;

    @ApiModelProperty("完成审批的结束搜索时间")
    private String completionTime;

    /**
     * 景区id
     */
    private String scenicId;
}
