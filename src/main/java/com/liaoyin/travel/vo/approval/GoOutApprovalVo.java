package com.liaoyin.travel.vo.approval;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 外出审批的Vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-24 18:42
 */
@Data
public class GoOutApprovalVo {

    @ApiModelProperty("是否同意(0:否;1:是)")
    private String isConsent;

    @ApiModelProperty("审批发起人的id")
    private String initiatorId;

    @ApiModelProperty("审批处理人的id")
    private String approverId;

    @ApiModelProperty("事由")
    private String reasons;

    @ApiModelProperty("外出开始时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;

    @ApiModelProperty("外出结束时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endOfOuting;

    @ApiModelProperty("外出共计时长")
    private String duration;

    @ApiModelProperty("审批记录ID")
    private String approvalRecordId;

    @ApiModelProperty("拒绝原因")
    private String  refusalCause;

    @ApiModelProperty("审批的发起时间 【yyyy-MM-dd HH:mm:ss】")
    private String initiationTime;
}
