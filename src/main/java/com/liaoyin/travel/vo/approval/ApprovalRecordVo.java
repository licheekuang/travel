package com.liaoyin.travel.vo.approval;

import com.liaoyin.travel.common.validate.Insert;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 新增审批记录 vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 19:17
 */
@Data
public class ApprovalRecordVo {

    @ApiModelProperty("图片的地址")
    private String imgUrl;

    @ApiModelProperty("审批发起人的id")
    @NotBlank(groups={Insert.class},message = "审批发起人的id不能为空")
    private String initiatorId;

    @ApiModelProperty("审批发起人的昵称")
    @NotBlank(groups={Insert.class},message = "必须传发起审批人的昵称")
    private String nickname;

    @ApiModelProperty("审批人的id")
    @NotBlank(groups={Insert.class},message = "审批人的id不能为空")
    private String approverId;
}
