package com.liaoyin.travel.vo.approval;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 发起补卡审批的请求参数
 *
 * @author kuang.jiazhuo
 * @date 2019-11-23 09:58
 */
@Data
public class CardReissueRecordVo {

    @ApiModelProperty("审批记录公共参数")
    private ApprovalRecordVo approvalRecordVo;

    @ApiModelProperty("缺卡班次显示")
    private String lackOfCardDivisions;

    @ApiModelProperty(" (考勤记录表id)")
    private String lackOfCardId;

    @ApiModelProperty("缺卡班次标识(1:上班;2:下班)")
    private String shift;

    @ApiModelProperty("补卡时间")
    private String clockTime;

    @ApiModelProperty("补卡事由")
    private String supplementaryCardCause;

    @ApiModelProperty("缺卡时间显示")
    private String clockTimeDisplay;


}
