package com.liaoyin.travel.vo.approval;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 新增或更新【补卡/请假/外出 申请-审批分组】vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 15:58
 */
@Data
public class InsertOrUpdateApprovalRecordVo {

    @ApiModelProperty("补卡申请-审批分组-id")
    private String packetId;

    @ApiModelProperty("分组名称")
    private String packetName;

    @ApiModelProperty("审批人id")
    private String approvedBy;

    @ApiModelProperty("申请人id字符串集")
    private String applicantIdS;

    @ApiModelProperty("是否删除(0:否;1:是)")
    private String isDelete;

}
