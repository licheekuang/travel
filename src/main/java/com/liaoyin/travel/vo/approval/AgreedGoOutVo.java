package com.liaoyin.travel.vo.approval;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 同意外出 vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-24 19:49
 */
@Data
@Accessors(chain = true)
public class AgreedGoOutVo {

    @ApiModelProperty("审批发起人的id")
    private String initiatorId;

    @ApiModelProperty("外出开始时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;

    @ApiModelProperty("外出结束时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endOfOuting;

    @ApiModelProperty("外出共计时长")
    private String duration;

}
