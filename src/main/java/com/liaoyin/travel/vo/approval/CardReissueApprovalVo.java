package com.liaoyin.travel.vo.approval;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 处理补卡审批 vo
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-24 00:51
 */
@Data
public class CardReissueApprovalVo {

    @ApiModelProperty("是否同意(0:否;1:是)")
    private String isConsent;

    @ApiModelProperty("审批发起人的id")
    private String initiatorId;

    @ApiModelProperty("审批处理人的id")
    private String approverId;

    @ApiModelProperty("补卡班次显示")
    private String lackOfCardDivisions;

    @ApiModelProperty("缺卡时间显示")
    private String  clockTimeDisplay;

    @ApiModelProperty("缺卡班次标识(1:上班;2:下班)")
    private String shift;

    @ApiModelProperty("缺卡id(考勤记录表id)")
    private String attendanceRecordId;

    @ApiModelProperty("事由")
    private String reasons;

    @ApiModelProperty("审批记录ID")
    private String approvalRecordId;

    @ApiModelProperty("拒绝原因")
    private String  refusalCause;

    @ApiModelProperty("审批的发起时间 【yyyy-MM-dd HH:mm:ss】")
    private String initiationTime;
}
