package com.liaoyin.travel.vo.scenic;

import com.liaoyin.travel.entity.FileUpload;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 新增或修改景区子管理员 VO
 *
 * @author kjz
 * @date 2020-04-28 14:48
 */
@Data
public class ScenicBackUserVo {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("账户")
    private String account;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("头像的文件实体")
    private FileUpload headPortraitFile;

    @ApiModelProperty(value = "是否启用")
    private Short isUsing;
}
