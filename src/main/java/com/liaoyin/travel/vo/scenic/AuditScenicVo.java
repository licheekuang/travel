package com.liaoyin.travel.vo.scenic;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 对景区进行审核操作的参数
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-22 10:22
 */
@Data
public class AuditScenicVo {

    @ApiModelProperty("单位ID")
    @NotBlank(message = "单位ID是必传参数")
    private String scenicId;

    @ApiModelProperty("审核结果【1:通过；2:拒绝】")
    @NotBlank(message = "审核结果是必传参数")
    private String auditStatus;

    @ApiModelProperty("单位类型【1:景区; 2:机构】")
    @NotBlank(message = "单位类型是必传参数")
    private String unitType;

    @ApiModelProperty("机构类型(单位类型为机构则必传)【1.集团;2.文旅委】")
    private String organType;

    @ApiModelProperty("景区管理员")
    private String adminName;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("身份证号")
    private String identityCard;

    @ApiModelProperty("性别【1:男;2:女】")
    private String gender;

    @ApiModelProperty("管理员账号")
    private String account;

    @ApiModelProperty("管理员密码")
    private String password;


}
