package com.liaoyin.travel.vo.scenic;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 手动组装pageinfo的参数
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-02 23:04
 */
@Data
@Accessors(chain = true)
public class PageInfoParameter {

    //当前页
    private int pageNum;
    //每页的数量
    private int pageSize;
    //当前页的数量
    private int size;

    //总记录数
    private long total;
    //总页数
    private int pages;

    //前一页
    private int prePage;
    //下一页
    private int nextPage;

    //是否为第一页
    private boolean isFirstPage = false;
    //是否为最后一页
    private boolean isLastPage = false;
    //是否有前一页
    private boolean hasPreviousPage = false;
    //是否有下一页
    private boolean hasNextPage = false;

    //所有导航页号
    private int[] navigatepageNums;
    //导航条上的第一页
    private int navigateFirstPage;
    //导航条上的最后一页
    private int navigateLastPage;

    //当前页面第一个元素在数据库中的行号
//    private int startRow;

    //当前页面最后一个元素在数据库中的行号
//    private int endRow;

    //导航页码数
//    private int navigatePages;

}
