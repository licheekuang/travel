package com.liaoyin.travel.vo.scenic;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询景区员工信息参数
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-03 10:09
 */
@Data
public class StaffHealthCheckInfoVo {

    @ApiModelProperty("当前页(不传默认为1)")
    private Integer num;

    @ApiModelProperty("页大小(不传默认为10)")
    private Integer size;

    @ApiModelProperty("景区id(前端不用传,管理员登录之后自动获取)")
    private String scenicId;

    @ApiModelProperty("健康状态(002001:正常;002002:异常)")
    private String healthState;
}
