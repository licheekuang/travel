package com.liaoyin.travel.vo.scenic;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.math.BigInteger;

/**
 * 新增或更改景区信息
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-21 12:56
 */
@Data
@Accessors(chain = true)
public class InsertOrUpdateScenicVo {

    @ApiModelProperty("景区id")
    private String scenicId;

    @ApiModelProperty("区域(暂定精确到区)id")
    @NotBlank(message = "区域id为空")
    private String districtId;

    @ApiModelProperty("景区名字")
    @NotBlank(message = "景区名字不能为空")
    private String scenicName;

    @ApiModelProperty("公司名字")
    @NotBlank(message = "公司名字不能为空")
    private String corpName;

    @ApiModelProperty("景区管理员姓名")
//    @NotBlank(message = "景区管理员姓名不能为空")
    private String adminName;

    @ApiModelProperty("联系电话")
//    @NotBlank(message = "联系电话不能为空")
    private String phone;

    @ApiModelProperty("身份证号")
//    @NotBlank(message = "身份证号不能为空")
    private String identityCard;

    @ApiModelProperty("性别【1:男;2:女】")
//    @NotBlank(message = "性别不能为空")
    private String gender;

    @ApiModelProperty("管理员账号")
    @NotBlank(message = "管理员账号不能为空")
    private String account;

    @ApiModelProperty("管理员密码")
    @NotBlank(message = "管理员密码不能为空")
    private String password;

    @ApiModelProperty("管理员Id")
    private String backUserId;

    @ApiModelProperty("单位类型(1:景区；2:非文旅委)")
    private String unitType;

    @ApiModelProperty("当日最大承载量(新增景区时必传)")
    private BigInteger maximumCapacityForTheDay;

    @ApiModelProperty("机构类型:1.非文旅委;2.文旅委(新增机构时必传)")
    private String organType;

    @ApiModelProperty("设备编号")
    private String equipmentNumber;

    @ApiModelProperty("文旅资源状态（1.应急）")
    private Integer emergency;

    @ApiModelProperty("应急文旅资源ID")
    private String emergencyId;

    @ApiModelProperty("空为景区；不空为机构")
    private String resourceType;

}
