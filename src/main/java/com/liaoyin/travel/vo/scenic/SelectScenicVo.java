package com.liaoyin.travel.vo.scenic;

import com.liaoyin.travel.base.entity.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * 查询文旅资源管理参数
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-21 21:52
 */
@Data
public class SelectScenicVo extends Page {

    @ApiModelProperty("区域(暂定精确到区)id")
    private String districtId;

    @ApiModelProperty("文旅资源名字")
    private String scenicName;

    @ApiModelProperty("公司名字")
    private String corpName;

    @ApiModelProperty("审核状态(必传)【0:未审核；1:通过；2:拒绝】")
    @NotBlank(message = "审核状态不能为空")
    private String auditStatus;

    @ApiModelProperty("单位类型(必传)【1:文旅资源;2:集团】")
    @NotBlank(message = "单位类型不能为空")
    private String unitType;

    @ApiModelProperty("文旅资源管理员姓名")
    private String adminName;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("身份证号")
    private String identityCard;

    @ApiModelProperty("性别【1:男;2:女】")
    private String gender;

    @ApiModelProperty("管理员账号")
    private String account;

    @ApiModelProperty("机构类型:1.集团;2.文旅委(查询机构必传)")
    private String organType;


}
