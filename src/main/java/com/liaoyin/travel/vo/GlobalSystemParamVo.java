package com.liaoyin.travel.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import com.liaoyin.travel.base.entity.Page;

@Setter
@Getter
public class GlobalSystemParamVo extends Page{

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("参数code")
    private String paramCode;

    @ApiModelProperty("参数名")
    private String paramName;

    @ApiModelProperty("参数值")
    private String paramValue;

    @ApiModelProperty("参数描述")
    private String paramDesc;
}
