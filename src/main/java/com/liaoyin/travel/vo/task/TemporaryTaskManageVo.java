package com.liaoyin.travel.vo.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 临时任务管理显示
 *
 * @author kuang.jiazhuo
 * @date 2019-12-04 16:11
 */
@Data
public class TemporaryTaskManageVo{

    @ApiModelProperty("当前页")
    private Integer num;

    @ApiModelProperty("页大小")
    private Integer size;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务发布人id(管理端必传)")
    private String issuerID;

    @ApiModelProperty("任务领取人id")
    private String receiverId;

    @ApiModelProperty("部门id")
    private String teamId;

    @ApiModelProperty("任务类型")
    private String taskType;

    @ApiModelProperty("任务级别")
    private String taskLevel;

    @ApiModelProperty("任务类别")
    private String taskCategories;
}
