package com.liaoyin.travel.vo.task;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * TODO
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-12 21:50
 */
@Data
@Accessors(chain = true)
public class TaskIdAndUserIdVo {

    /**
     * 任务id
     */
    private String taskId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用于临时任务管理的关联(定时任务不传此参数)
     */
    private String taskMangerId;

    /**
     * 发布方式(1.定时任务;2.临时发布)
     */
    private String releaseWay;

    /**
     * 单位id
     */
    private String scenicId;
}
