package com.liaoyin.travel.vo.task;

import com.liaoyin.travel.entity.task.Task;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("发布任务vo")
@Data
public class InsertTaskVo {

	@ApiModelProperty("任务")
	private Task task;
	
	
	private TaskLineVo taskLineVo;
	
	/*@ApiModelProperty("任务线路列表")
	private List<TaskDetails> taskDetailsList;*/
	
	/*@ApiModelProperty("任务负责人列表")
	private List<String> userList;*/
	@ApiModelProperty("业务id【可关联 用户 部门 工种 的id】")
	private String businessId;


}
