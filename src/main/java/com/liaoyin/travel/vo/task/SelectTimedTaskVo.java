package com.liaoyin.travel.vo.task;

import com.liaoyin.travel.base.entity.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 定时任务发布类
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-26 20:46
 */
@Data
public class SelectTimedTaskVo extends Page {

    @ApiModelProperty("启用状态(0:不启用;1:启用)")
    private String enabled;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务类型【字典 taskType】")
    private String taskType;

    @ApiModelProperty("任务级别【字典 taskLevel】")
    private String taskLevel;

    @ApiModelProperty("任务类别【字典 taskCategories】")
    private String taskCategories;

    @ApiModelProperty("用户id/任务发放人")
    private String issuerId;

    @ApiModelProperty("用户id/任务领取人人")
    private String receiverId;

    @ApiModelProperty("部门id")
    private String teamId;

    @ApiModelProperty("工种id")
    private String workId;

    /**
     * 景区id
     */
    private String scenicId;
}
