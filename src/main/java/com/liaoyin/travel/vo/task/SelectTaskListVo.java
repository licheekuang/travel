package com.liaoyin.travel.vo.task;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("查询任务列表实体")
@Data
public class SelectTaskListVo{

	@ApiModelProperty("任务名称")
	 private String taskName;
	 
	 @ApiModelProperty("任务类型【字典 taskType】")
	 private String taskType;
	 
	 @ApiModelProperty("任务级别【字典 taskLevel】")
	 private String taskLevel;

	 @ApiModelProperty("任务状态【字典 taskStatus】")
	 private String taskStatus;
	 
	 @ApiModelProperty("任务类别【字典 taskCategories】")
	 private String taskCategories;

	 @ApiModelProperty("查询类型【1：前端员工端查询 2：前端管理端  3：后台查询】")
	 private String qurtyType;
	 
	 @ApiModelProperty("任务查询类别【1：全部任务 2：我的任务 3：我发出的任务】")
	 private String taskQurty;
	 
	 @ApiModelProperty("用户id/任务发放人")
	 private String userId;

	@ApiModelProperty("用户id/任务领取人人")
	private String id;
	 
	 @ApiModelProperty("任务时间")
	 private String taskTime;
	 
	 @ApiModelProperty("部门id")
	 private String teamId;
	 
	 @ApiModelProperty("是否是有效任务【1：有效任务】")
	 private String taskEndType;
	 
	 @ApiModelProperty("工种")
	 private String workId;
	 
	 @ApiModelProperty("任务id")
	 private String taskId;

	 @ApiModelProperty("任务开始时间")
	 private String startTaskTime;

	 @ApiModelProperty("结束任务时间")
	 private String endTaskTime;

	 @ApiModelProperty("当前页")
	 private Integer num;

	 @ApiModelProperty("页大小")
	 private Integer size;

	/**
	 * 景区id
	 */
	private String scenicId;
}
