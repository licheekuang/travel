package com.liaoyin.travel.vo.task;

import com.liaoyin.travel.base.entity.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 定时任务具体查询条件
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-26 16:05
 */
@Data
public class TimingTaskParameterVo extends Page {

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务类型【字典 taskType】")
    private Short taskType;

    @ApiModelProperty("任务级别【字典 taskLevel】")
    private Short taskLevel;

    @ApiModelProperty("任务类别【字典 taskCategories】")
    private Short taskCategories;

    @ApiModelProperty("任务说明")
    private String taskStatement;

    @ApiModelProperty("任务发布人id")
    private String userId;

    @ApiModelProperty("线路id")
    private String lineId;

    @ApiModelProperty("纬度")
    private String taskLat;

    @ApiModelProperty("经度")
    private String taskLog;

//    private Date taskEndTime;

}
