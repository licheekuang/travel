package com.liaoyin.travel.vo.task;

import java.util.List;

import com.liaoyin.travel.entity.task.TaskDetails;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class TaskLineVo {

	@ApiModelProperty("线路名称")
	private String lineName;
	
	@ApiModelProperty("任务详情列表")
	private List<TaskDetails> taskDetailsList;
}
