package com.liaoyin.travel.vo.task;

import com.liaoyin.travel.entity.task.Task;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 新增定时任务的参数
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-26 13:36
 */
@Data
public class InsertTimedTaskVo {

    @ApiModelProperty("任务")
    private Task task;

    @ApiModelProperty("业务id【可关联 用户 部门 工种 的id】")
    private String businessId;

    @ApiModelProperty("发布的星期字符串,以','隔开")
    private String pubWeeks;

    @ApiModelProperty("定时任务发布的时间(yyyy-MM-dd HH:mm)")
    private String pubTime;

    @ApiModelProperty("是否启动定时任务(0:否;1:是)")
    private String enabled;

    /**
     * 执行任务的地理信息(旅游移动管控系统需要的字段)
     */
    private String location;

    private TaskLineVo taskLineVo;

    private String publishType;

    private String publishTime;
}
