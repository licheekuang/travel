package com.liaoyin.travel.vo.file;

import lombok.Data;

@Data
public class FileInfo {
	//文件名
	private String fileName;
	//文件路径
	private String filePath;
	//url地址
	private String url;
	//文件类型
	private String fileType;
	//文件大小
	private Integer fileSize;
}
