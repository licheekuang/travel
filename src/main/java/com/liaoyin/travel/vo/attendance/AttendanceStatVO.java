package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: AttendanceStatVO
 * @description:
 * @create 2019/11/26 9:59
 **/
@Data
@ApiModel("考勤统计")
public class AttendanceStatVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("用户名")
    private String nickName;

    @ApiModelProperty("部门")
    private String teamName;

    @ApiModelProperty("出勤天数")
    private String attendanceCnt;

    @ApiModelProperty("迟到数量")
    private String lateCnt;

    @ApiModelProperty("缺卡次数")
    private String missCnt;

    @ApiModelProperty("旷工次数")
    private String absentCnt;
}
