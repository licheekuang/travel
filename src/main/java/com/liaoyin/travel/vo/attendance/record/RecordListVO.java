package com.liaoyin.travel.vo.attendance.record;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: RecordListVO
 * @description:
 * @create 2019/11/26 17:03
 **/
@Data
@ApiModel("考勤记录")
public class RecordListVO {

    @ApiModelProperty("旷工")
    private AbscentVO abscentVO;

    @ApiModelProperty("外出")
    private OutVO outVO;

    @ApiModelProperty("补卡")
    private ReapplyVO reapplyVO;

    @ApiModelProperty("缺卡")
    private MissVO missVO;

    @ApiModelProperty("迟到")
    private LateVO lateVO;

    @ApiModelProperty("请假")
    private LeaveVO leaveVO;
}
