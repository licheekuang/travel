package com.liaoyin.travel.vo.attendance.record;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: LateVO
 * @description:
 * @create 2019/11/26 16:52
 **/
@Data
@ApiModel("请假记录")
public class LeaveVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("请假开始日期")
    private String startTime;

    @ApiModelProperty("请假结束时间")
    private String endTime;

    @ApiModelProperty("请假时长")
    private String leaveTime;

    @ApiModelProperty("星期")
    private String weekDay;
}
