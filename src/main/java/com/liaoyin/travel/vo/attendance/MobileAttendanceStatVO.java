package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: MobileAttendanceStatVO
 * @description:
 * @create 2019/11/26 10:16
 **/
@Data
@ApiModel("移动端考勤统计")
public class MobileAttendanceStatVO {

    @ApiModelProperty("出勤天数")
    private String attendanceCnt;

    @ApiModelProperty("迟到数量")
    private String lateCnt;

    @ApiModelProperty("缺卡次数")
    private String missCnt;

    @ApiModelProperty("旷工次数")
    private String absentCnt;

    @ApiModelProperty("补卡数")
    private String reapplyCnt;

    @ApiModelProperty("请假数")
    private String leaveCnt;

    @ApiModelProperty("外出数")
    private String outCnt;
}
