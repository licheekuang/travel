package com.liaoyin.travel.vo.attendance.record;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: LateVO
 * @description:
 * @create 2019/11/26 16:52
 **/
@Data
@ApiModel("缺卡记录")
public class MissVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("考勤日期")
    private String attendanceDate;

    @ApiModelProperty("星期")
    private String weekDay;

    @ApiModelProperty("上班打卡时间")
    private String inTime;

    @ApiModelProperty("下班打卡时间")
    private String outTime;
}
