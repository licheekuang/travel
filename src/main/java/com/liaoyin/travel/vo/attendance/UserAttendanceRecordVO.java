package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: UserAttendanceRecordVO
 * @description:
 * @create 2019/11/28 17:16
 **/
@Data
@ApiModel("员工考勤记录")
public class UserAttendanceRecordVO {

    @ApiModelProperty("考勤开始时间")
    private String startTime;

    @ApiModelProperty("考勤结束时间")
    private String endTime;

    @ApiModelProperty("考勤日期")
    private String attendanceDate;

    @ApiModelProperty("上班打卡时间")
    private String inTime;

    @ApiModelProperty("上班考勤状态")
    private String inState;

    @ApiModelProperty("下班打卡时间")
    private String outTime;

    @ApiModelProperty("下班考勤状态")
    private String outState;

    @ApiModelProperty("最终考勤状态")
    private String finalState;
}
