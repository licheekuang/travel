package com.liaoyin.travel.vo.attendance;

import com.liaoyin.travel.entity.attendance.AttendanceSettings;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: AttendanceSettingVO
 * @description:
 * @create 2019/10/25 17:21
 **/
@Data
@ApiModel("打卡机设置信息")
public class AttendanceSettingVO extends AttendanceSettings {

    @ApiModelProperty("打卡机经度")
    private String longitude;

    @ApiModelProperty("打卡机纬度")
    private String latitude;
}
