package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: MobileAttendanceRecordVO
 * @description:
 * @create 2019/11/26 11:35
 **/
@Data
@ApiModel("移动端考勤记录")
public class MobileAttendanceRecordVO {

    @ApiModelProperty("用户ID")
    private String userId;

    @ApiModelProperty("部门")
    private String teamName;

    @ApiModelProperty("用户名")
    private String nickName;

    @ApiModelProperty("日期")
    private String attendanceDate;

    @ApiModelProperty("星期")
    private String weekDay;

    @ApiModelProperty("上班时间")
    private String inTime;

    @ApiModelProperty("下班时间")
    private String outTime;

    @ApiModelProperty("上班状态")
    private String inState;

    @ApiModelProperty("下班状态")
    private String outState;

    @ApiModelProperty("考勤状态")
    private String attendanceStatus;

    @ApiModelProperty("考勤状态值")
    private String attendanceStatusVal;
}
