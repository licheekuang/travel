package com.liaoyin.travel.vo.attendance.record;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: LateVO
 * @description:
 * @create 2019/11/26 16:52
 **/
@Data
@ApiModel("补卡记录")
public class ReapplyVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("补卡时间")
    private String renewTime;

    @ApiModelProperty("星期")
    private String weekDay;

    @ApiModelProperty("班次")
    private String shift;

    @ApiModelProperty("应打卡时间")
    private String clockTime;
}
