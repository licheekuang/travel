package com.liaoyin.travel.vo.attendance.record;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: LateVO
 * @description:
 * @create 2019/11/26 16:52
 **/
@Data
@ApiModel("外出记录")
public class OutVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("外出时长")
    private String outTime;

    @ApiModelProperty("星期")
    private String weekDay;
}
