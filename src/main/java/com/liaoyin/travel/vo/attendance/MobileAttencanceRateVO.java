package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 王海洋
 * @className: MobileAttencanceRateVO
 * @description:
 * @create 2019/11/26 11:45
 **/
@Data
@ApiModel("移动端考勤统计总览")
public class MobileAttencanceRateVO {

    @ApiModelProperty("各类型考勤统计")
    private MobileAttendanceStatVO mobileAttendanceStatVO;

    @ApiModelProperty("考勤记录")
    private List<MobileAttendanceRecordVO> attendanceRecordVOList;

    @ApiModelProperty("正常考勤次数")
    private Integer normalCnt = 0;

    @ApiModelProperty("异常考勤次数")
    private Integer unormalCnt = 0;

    @ApiModelProperty("考勤率")
    public BigDecimal getRate(){
        if(this.normalCnt==0 && this.unormalCnt==0){
            return BigDecimal.ZERO;
        }else{
            BigDecimal nomal = new BigDecimal(this.normalCnt);
            BigDecimal total = new BigDecimal(this.normalCnt+this.unormalCnt);
            BigDecimal rate = nomal.divide(total,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return rate;
        }
    }

    /*@ApiModelProperty("考勤正常次数")
    private Integer normalCnt = this.getNormalCnt();

    @ApiModelProperty("考勤异常次数")
    private Integer unormalCnt = this.getUnormalCnt();

    @ApiModelProperty("考勤率")
    private BigDecimal rate = this.getRate();*/
}
