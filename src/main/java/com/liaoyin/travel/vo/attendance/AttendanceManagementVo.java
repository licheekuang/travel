package com.liaoyin.travel.vo.attendance;

import com.liaoyin.travel.base.entity.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 考勤管理条件查询 vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-18 18:00
 */
@Data
@ApiModel("考勤管理的查询条件")
public class AttendanceManagementVo extends Page {

    @ApiModelProperty("考勤名称")
    private String checkingInName;

    /**
     * 景区id
     */
    private String scenicId;
}
