package com.liaoyin.travel.vo.attendance;

import lombok.Data;

/**
 * 原来的加入考勤的用户
 *
 * @author kuang.jiazhuo
 * @date 2019-11-19 15:26
 */
@Data
public class AttendanceOldUser{

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 考勤名称
     */
    private String checkingInName;
}
