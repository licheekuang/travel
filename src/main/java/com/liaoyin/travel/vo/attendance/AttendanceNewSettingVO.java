package com.liaoyin.travel.vo.attendance;

import com.liaoyin.travel.entity.attendance.AttendanceManagement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 前端获取打卡的设置信息
 *
 * @author kuang.jiazhuo
 * @date 2019-11-19 21:13
 */
@Data
public class AttendanceNewSettingVO extends AttendanceManagement {

    @ApiModelProperty("打卡的经度")
    private String longitude;

    @ApiModelProperty("打卡的纬度")
    private String latitude;
}
