package com.liaoyin.travel.vo.attendance;

import com.liaoyin.travel.entity.attendance.AttendanceManagement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 考勤管理信息
 *
 * @author kuang.jiazhuo
 * @date 2019-11-20 10:45
 */
@Data
public class AttendanceManageVo extends AttendanceManagement {

    @ApiModelProperty("当前员工要打卡的经度")
    private String longitude;

    @ApiModelProperty("当前员工要打卡的经度")
    private String latitude;
}
