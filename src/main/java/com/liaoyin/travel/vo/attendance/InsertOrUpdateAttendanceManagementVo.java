package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 新增或修改考勤管理设置的Vo
 *
 * @author kuang.jiazhuo
 * @date 2019-11-18 23:12
 */
@Data
@ApiModel("新增或修改考勤管理设置的入参")
public class InsertOrUpdateAttendanceManagementVo {

    private String id;

    @ApiModelProperty("考勤名称")
//    @NotBlank(groups={Insert.class},message = "考勤名称不能为空")
    private String checkingInName;

    @ApiModelProperty("考勤开始时间【HH:mm】")
//    @NotBlank(groups={Insert.class},message = "新增时必须传入考勤开始时间")
    private String attendanceStartTime;

    @ApiModelProperty("考勤结束时间【HH:mm】")
//    @NotBlank(groups={Insert.class},message = "新增时必须传入考勤结束时间")
    private String attendanceEndTime;

    @ApiModelProperty("考勤工作日字符串")
//    @NotBlank(groups={Insert.class},message = "新增时必须传入考勤工作日的相关参数")
    private String attendanceDays;

    @ApiModelProperty("设备表的id")
//    @NotBlank(groups={Insert.class},message = "新增时必须传入考勤结束时间")
    private String equipmentId;

    @ApiModelProperty("考勤范围(允许偏差的距离)")
//    @NotNull(groups={Insert.class},message = "新增时必须传入考勤考勤范围")
    private Double attendanceRange;

    @ApiModelProperty("参与考勤人员的id字符串集")
    private String participantId;

    @ApiModelProperty("是否启用(0:否;1:是)")
    private String isUsing;

    @ApiModelProperty("是否删除(0:否;1:是)")
    private String isDelete;


}
