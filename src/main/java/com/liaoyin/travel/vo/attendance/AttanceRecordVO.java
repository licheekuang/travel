package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: AttanceRecordVO
 * @description:
 * @create 2019/11/25 15:50
 **/
@Data
@ApiModel("考勤记录")
public class AttanceRecordVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("人员昵称")
    private String nickName;

    @ApiModelProperty("部门")
    private String teamName;

    @ApiModelProperty("出勤日期")
    private String dateOfAttendance;

    @ApiModelProperty("打卡开始时间")
    private String startTime;

    @ApiModelProperty("打卡结束时间")
    private String endTime;

    @ApiModelProperty("最终打卡状态")
    private String finalStatus;

    @ApiModelProperty("最终打卡状态值")
    private String finalStatusVal;

    @ApiModelProperty("考勤点名称")
    private String attendanceName;

    @ApiModelProperty("上班考勤状态")
    private String inState;

    @ApiModelProperty("上班考勤状态值")
    private String inStateVal;

    @ApiModelProperty("上班考勤时间")
    private String inTime;

    @ApiModelProperty("下班考勤状态")
    private String outState;

    @ApiModelProperty("下班考勤状态值")
    private String outStateVal;

    @ApiModelProperty("下班考勤时间")
    private String outTime;
}
