package com.liaoyin.travel.vo.attendance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 新增和修改考勤记录需要的参数
 *
 * @author kuang.jiazhuo
 * @date 2019-11-20 09:28
 */
@Data
public class InsertOrUpdateAttendanceRecordVo {


    @ApiModelProperty("打卡的经度")
    private String chockLongitude;

    @ApiModelProperty("打卡的纬度")
    private String chockLatitude;

    @ApiModelProperty("打卡班次(1:上班打卡;2:下班打卡)")
    private String clockFrequency;

}
