package com.liaoyin.travel.vo.announce;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author 王海洋
 * @className: AnnounceUserVO
 * @description:
 * @create 2019/11/29 17:46
 **/
@Data
@ApiModel("公告用户")
public class AnnounceUserVO {

    private String userId;

    private String holdOffice;

    private String nickName;

    private String teamName;

    private String office;
}
