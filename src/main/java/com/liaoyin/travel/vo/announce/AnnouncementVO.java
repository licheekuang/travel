package com.liaoyin.travel.vo.announce;

import com.liaoyin.travel.entity.Announcement;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 王海洋
 * @className: AnnouncementVO
 * @description:
 * @create 2019/11/29 16:05
 **/
@Data
@ApiModel("公告详情")
public class AnnouncementVO extends Announcement {

    @ApiModelProperty("接收公告的用户列表")
    private List<AnnounceUserVO> usersList;
}
