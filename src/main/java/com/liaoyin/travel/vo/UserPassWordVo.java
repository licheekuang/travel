package com.liaoyin.travel.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/***修改密码*******/
@Setter
@Getter
public class UserPassWordVo {

    @ApiModelProperty("原密码")
    private String oldPassword;
    @ApiModelProperty("新密码")
    private String newPassword;
    @ApiModelProperty("新密码1")
    private String replayPassword;
    @ApiModelProperty("账号")
    private String newUserName;
    @ApiModelProperty("密码长度")
    private Integer num;
    @ApiModelProperty("昵称")
    private String nickName;
}
