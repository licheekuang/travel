package com.liaoyin.travel.vo.rongyun;

import lombok.Data;

@Data
public class SendMessageVo {

	
	private String type;
	
	private String messageBody;
}
