package com.liaoyin.travel.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class RegisterVo {

    @ApiModelProperty("用户名或手机号码")
    private String phone;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("1.管理 2.员工 ")
    private Integer loginType;

}
