package com.liaoyin.travel.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 换部门经理确认参数
 *
 * @author kuang.jiazhuo
 * @date 2019-12-03 14:15
 */
@Data
public class ChangeManagerVo {

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("用户id")
    private String id;

    @ApiModelProperty("部门id")
    private String teamId;
}
