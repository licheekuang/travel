package com.liaoyin.travel.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 王海洋
 * @className: MobileAttendanceStatRequestVO
 * @description:
 * @create 2019/11/26 11:31
 **/
@Data
@ApiModel("移动端考勤统计请求参数")
public class MobileAttendanceStatRequestVO {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @ApiModelProperty("开始时间，yyyy-MM-dd")
    private String startDate = sdf.format(new Date());

    @ApiModelProperty("结束时间，yyyy-MM-dd")
    private String endDate = sdf.format(new Date());

    @ApiModelProperty("用户id,英文逗号隔开")
    private String userId;

    @ApiModelProperty("考勤状态")
    private String attendanceStatus;

    @ApiModelProperty(value = "部门id",hidden = true)
    private String teamIds;

    /**
     * 景区id
     */
    private String scenicId;
}
