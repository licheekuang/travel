package com.liaoyin.travel.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: AnnounceSearchRequestVO
 * @description:
 * @create 2019/11/29 17:17
 **/
@Data
@ApiModel("公告搜索请求VO")
public class AnnounceSearchRequestVO {

    @ApiModelProperty("搜索开始时间")
    private String startTime;

    @ApiModelProperty("搜索结束时间")
    private String endTime;

    @ApiModelProperty("搜索值")
    private String searchVal;

    @ApiModelProperty("页码")
    private Integer num;

    @ApiModelProperty("数据条数")
    private Integer size;

    @ApiModelProperty("景区id")
    private String scenicId;
}
