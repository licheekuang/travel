package com.liaoyin.travel.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 王海洋
 * @className: AttendanceRecordRequestVO
 * @description:
 * @create 2019/11/26 9:13
 **/
@Data
@ApiModel("考勤记录请求VO")
public class AttendanceRecordRequestVO {

    @ApiModelProperty("当前页")
    private Integer num;

    @ApiModelProperty("页大小")
    private Integer size;

    @ApiModelProperty("考勤状态")
    private String attendanceStatus;

    @ApiModelProperty("开始日期")
    private String startDate;

    @ApiModelProperty("结束日期")
    private String endDate;

    @ApiModelProperty("部门ID,英文逗号隔开")
    private String teamIds;

    @ApiModelProperty("用户ID,英文逗号隔开")
    private String userIds;

    @ApiModelProperty("员工状态")
    private String userStatus;

    /**
     * 景区id
     */
    private String scenicId;
}
