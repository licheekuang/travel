package com.liaoyin.travel.vo.request;

import com.liaoyin.travel.entity.Announcement;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author 王海洋
 * @className: AnnounceAddRequestVO
 * @description:
 * @create 2019/11/29 10:52
 **/
@ApiModel("添加公告请求VO")
public class AnnounceAddRequestVO extends Announcement {

    @ApiModelProperty(value = "接收公告的用户id，多个用户英文逗号隔开",required = true)
    private String userId;
}
