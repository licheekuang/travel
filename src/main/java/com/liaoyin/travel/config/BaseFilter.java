package com.liaoyin.travel.config;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class BaseFilter extends OncePerRequestFilter {

/*    @Value("${gmdj.pattern}")
    private String urlPattern;*/

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String s = httpServletRequest.getHeader("Referer");
        /*if(s!=null){
            String pattern = urlPattern;
            boolean isMatch = Pattern.matches(pattern, s);
            if(!isMatch){
                throw new BusinessException("error");
            }
        }*/
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
