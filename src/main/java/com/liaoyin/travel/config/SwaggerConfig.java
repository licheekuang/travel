package com.liaoyin.travel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：Swagger配置
 * @日期：Created in 2018/6/8 16:42
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
    public Docket createApi() {
        //可以添加多个header或参数
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder
                .parameterType("header")
                .name("access-token")
                .description("header中Authorization字段用于认证")
                .modelRef(new ModelRef("string"))
                //非必需，这里是全局配置，然而在登陆的时候是不用验证的
                .required(false).build();
        List<Parameter> aParameters = new ArrayList<Parameter>();
        aParameters.add(aParameterBuilder.build());
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("all-interface")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.liaoyin.travel.api"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(aParameters);
    }

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("图片视频上传")
				.genericModelSubstitutes(DeferredResult.class)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.liaoyin.travel.api.base"))
				.paths(PathSelectors.any())
				.build();
	}
	@Bean
	public Docket createRestApiBack() {
		
		//可以添加多个header或参数
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder
                .parameterType("header")
                .name("access-token")
                .description("header中Authorization字段用于认证")
                .modelRef(new ModelRef("string"))
                //非必需，这里是全局配置，然而在登陆的时候是不用验证的
                .required(false).build();
        List<Parameter> aParameters = new ArrayList<Parameter>();
        aParameters.add(aParameterBuilder.build());
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("后台接口")
				.genericModelSubstitutes(DeferredResult.class)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.liaoyin.travel.api.back"))
				.paths(PathSelectors.any())
				.build()
				.globalOperationParameters(aParameters);
	}
	
	@Bean
    public Docket clientApi(){
		
		//可以添加多个header或参数
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder
                .parameterType("header")
                .name("access-token")
                .description("header中Authorization字段用于认证")
                .modelRef(new ModelRef("string"))
                //非必需，这里是全局配置，然而在登陆的时候是不用验证的
                .required(false).build();
        List<Parameter> aParameters = new ArrayList<Parameter>();
        aParameters.add(aParameterBuilder.build());
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("移动端接口")
				.genericModelSubstitutes(DeferredResult.class)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.liaoyin.travel.api.moble"))
				.paths(PathSelectors.any())
				.build()
				.globalOperationParameters(aParameters);
    }
	
	

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("旅游 RESTful APIs")
                .description("旅游 RESTful APIs列表及具体使用说明。<br/>"
                        + "注意：<br/>"
                        + "1、在包org.party.admin.rest下所有RESTful APIs会自动生成接口文档（除了被@ApiIgnore指定的APIs）；<br/>"
                        + "2、@ApiOperation：在方法上标注旨在说明该方法的作用。必填；<br/>"
                        + "3、@ApiImplicitParams或@ApiImplicitParam:说明传入参数的名称、类型、是否必填。必填；<br/>"
                        + "4、@ApiModel：在model类上进行注解说明该model的作用；<br/>"
                        + "5、@ApiModelProperty：对模型中属性添加说明；<br/>"
                        + "6、@ApiIgnore：应用在Controller范围上，则当前Controller中的所有方法都会被忽略，如果应用在方法上，则对应用的方法忽略暴露API<br/><br/>"
                        + "<b>注意：所有返回值外层格式为<br/>{<br/>&nbsp;&nbsp;\"code\":\"code\",<br/>&nbsp;&nbsp;\"desc\":\"desc\",<br/>&nbsp;&nbsp;\"result\":{data}<br/>}<br/>"
                        + "code:代表返回码，desc:代表描述，result:代表实际返回的业务内容<br/>"
                        + "接口中返回值的Model跟Example Value项只显示{data}这部分内容</b>")
                .termsOfServiceUrl("http://localhost:xxxx/swagger-ui.html")
                //.contact("QQ:395089311")
                .version("1.0")
                .build();
    }


}
