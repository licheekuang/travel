package com.liaoyin.travel.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @项目名： 
 * @作者：zhou.ning
 * @描述：Swagger配置
 * @日期：Created in 2018/6/8 16:47
 */
@Configuration
class WebMvcConfig extends WebMvcConfigurerAdapter {
	
	@Value("${gate.file.realPath}")
	private String realPath;

	@Value("${gate.file.uploadPath}")
	private String uploadPath;
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        
        //将网络请求映射到本地路径
        registry.addResourceHandler("/upload/**").addResourceLocations("file:"+realPath + uploadPath);
       
    }


}
