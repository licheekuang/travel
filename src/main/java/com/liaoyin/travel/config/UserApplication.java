package com.liaoyin.travel.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class UserApplication extends WebMvcConfigurerAdapter{

	
	@Override
	  public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	    
		super.configureMessageConverters(converters);
	    // 初始化转换器
	    FastJsonHttpMessageConverter fastConvert = new FastJsonHttpMessageConverter();
	    // 初始化一个转换器配置
	    FastJsonConfig fastJsonConfig = new FastJsonConfig();
	    
	    //fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
	    fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
	    
	    //4.处理中文乱码问题
        List<MediaType> fastMediaTypes =  new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastConvert.setSupportedMediaTypes(fastMediaTypes);

	    // 将配置设置给转换器并添加到HttpMessageConverter转换器列表中
	    fastConvert.setFastJsonConfig(fastJsonConfig);
	    converters.add(fastConvert);
	  }
}
