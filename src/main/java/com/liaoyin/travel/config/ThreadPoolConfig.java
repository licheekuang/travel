package com.liaoyin.travel.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Configuration
public class ThreadPoolConfig {

	/**
	 * 数据收集配置，主要作用在于Spring启动时自动加载一个ExecutorService对象.
	 * @author lijing
	 * @date 2018/8/8
	 * 
	 * update by Cliff at 2027/11/03
	 */
	@Bean
    public ExecutorService getThreadPool(){
		ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("machine-pool-%d").build();
		ExecutorService executorService = new ThreadPoolExecutor(10,
				20, 0L,
				TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<>(1024),
				namedThreadFactory,
				new ThreadPoolExecutor.AbortPolicy());
		
        return executorService;
    }
}
