package com.liaoyin.travel.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liaoyin.travel.util.StringUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：防止页面重复提交
 * @日期：Created in 2018/6/8 11:09
 */
public class Signature {

	private static Logger logger= LoggerFactory.getLogger(Signature.class);

	/**
	 * @方法名：checkSign
	 * @描述： 防止页面重复提交
	 * @作者： zhou.ning
	 * @日期： Created in 2018/6/8 11:04
	 */
    public static boolean checkSign(HttpServletRequest request){
		boolean b = true;
		String nonceStr = request.getParameter("nonceStr");//随机字符串
		if(StringUtil.isEmpty(nonceStr)){//如果验证的字符串为空，表示不需要验证
			return  b;
		}
		Object attribute = request.getSession().getAttribute(nonceStr);
		request.getSession().setMaxInactiveInterval(30);
		if(attribute == null){//如果未用过，表示正常
			request.getSession().setAttribute(nonceStr,nonceStr);
		}else{
			logger.info("重复提交了："+nonceStr);
			return false;
		}
		return b;
    }

}
