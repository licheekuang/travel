package com.liaoyin.travel.api.external;

import com.alibaba.fastjson.JSON;
import com.liaoyin.travel.view.ApiExternal.ApiExternalAttendance;
import com.liaoyin.travel.view.ApiExternal.ApiExternalTask;
import com.liaoyin.travel.view.ApiExternal.ApiExternalUser;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.service.external.ApiExternalService;
import com.liaoyin.travel.util.AES256EncryptionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author privatePanda777@163.com
 * @title: ApiExternalController
 * @projectName travel
 * @description: TODO 对外提供 任务发布返回指派人信息、任务列表、考勤率及异常列表、任务完成后调用他们接口返回任务完成简报
 * @date 2019/8/128:40
 */
@RestController
@Api(tags={"对外API接口"})
@RequestMapping("/externalApi")
public class ApiExternalController {

    @Autowired
    private ApiExternalService apiExternalService;


    /**
    　* @description: TODO  提供给数软发布消防预警任务的接口
    　* @param [taskName, taskStatement, taskLat, taskLog]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/12 10:24
    　*/
    /*@PostMapping("/issueWarningTask")
    @ApiOperation(value = "发布消防预警任务", notes = "发布消防预警任务",response = ApiExternalUser.class)
    @ResponseBody*/
    public JsonRestResponse issueWarningTask(
            @ApiParam(value = "任务名称",required = true)@RequestParam(required = true)String taskName,
            @ApiParam(value = "任务说明",required = true)@RequestParam(required = true)String taskStatement,
            @ApiParam(value = "任务纬度",required = true)@RequestParam(required = true)String taskLat,
            @ApiParam(value = "任务经度",required = true)@RequestParam(required = true)String taskLog
    ){
        ApiExternalUser apiExternalUser = this.apiExternalService.issueWarningTask(taskName, taskStatement, taskLat, taskLog);
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(apiExternalUser), CommonConstant.AES_KEY));
    }

    /**
    　* @description: TODO  获取今日任务列表的接口 --数软
    　* @param []
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/16 8:51
    　*/
    @GetMapping("/getTaskList")
    @ApiOperation(value = "获取今日任务列表的接口" , notes = "获取任务列表的接口" , response = ApiExternalTask.class ,responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getTaskList(

    ){
        System.out.println(AES256EncryptionUtil.Aes256Decode(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(this.apiExternalService.getTaskList(null)),CommonConstant.AES_KEY),CommonConstant.AES_KEY));
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(this.apiExternalService.getTaskList(null)),CommonConstant.AES_KEY));
    }

    /**
    　* @description: TODO  获取今日考勤率及异常考勤列表 -- 数软
    　* @param []
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/16 8:52
    　*/
    @GetMapping("/getAttendanceStatisticsDetailedList")
    @ApiOperation(value = "获取今日考勤率及异常考勤列表" , notes = "获取今日考勤率及异常考勤列表" , response = ApiExternalAttendance.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getAttendanceStatisticsDetailedList(

    ){
        System.out.println(AES256EncryptionUtil.Aes256Decode(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(this.apiExternalService.getAttendanceStatisticsDetailedList()),CommonConstant.AES_KEY),CommonConstant.AES_KEY));
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(this.apiExternalService.getAttendanceStatisticsDetailedList()),CommonConstant.AES_KEY));
    }


    @GetMapping("/getUsesListByWorkerId")
    @ApiOperation(value = "根据工种获取员工列表" , notes = "根据工种获取员工列表" , response = ApiExternalUser.class , responseContainer = "List")
    @ResponseBody
    public JsonRestResponse getUsesList(){
        List<ApiExternalUser> usesList = this.apiExternalService.getUsesList("485f30c2c0b84551b8d10aa6a86afac7");
        System.out.println(AES256EncryptionUtil.Aes256Decode(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(usesList),CommonConstant.AES_KEY),CommonConstant.AES_KEY));
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(usesList),CommonConstant.AES_KEY));
    }

    /*@PostMapping("/cleaningTask")
    @ApiOperation(value = "对外的发布保洁任务", notes = "对外的发布保洁任务", response = ApiExternalUser.class )
    @ResponseBody*/
    public JsonRestResponse cleaningTask(
            @ApiParam(value = "指派人ID",required = true)@RequestParam(required = true)String userId,
            @ApiParam(value = "任务纬度",required = true)@RequestParam(required = true) String taskLat,
            @ApiParam(value = "任务经度",required = true)@RequestParam(required = true) String taskLog,
            @ApiParam(value = "任务名称",required = true)@RequestParam(required = true) String taskName,
            @ApiParam(value = "任务内容",required = true)@RequestParam(required = true) String taskStatement
    ){
        ApiExternalUser apiExternalUser = this.apiExternalService.cleaningTask(userId, taskLat, taskLog, taskName, taskStatement);
        System.out.println(AES256EncryptionUtil.Aes256Decode(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(apiExternalUser),CommonConstant.AES_KEY),CommonConstant.AES_KEY));
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(apiExternalUser),CommonConstant.AES_KEY));
    }

    @GetMapping("/getDateTask")
    @ApiOperation(value = "任务数据看板" , notes = "任务数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataTask(){
        Map<String, Integer> dataTask = this.apiExternalService.getDataTask();
        System.out.println(dataTask);
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(dataTask),CommonConstant.AES_KEY));
    }

    @GetMapping("/getDataMaterial")
    @ApiOperation(value = "物资数据看板" , notes = "物资数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataMaterial(){
        Map<String, Integer> dataMaterial = this.apiExternalService.getDataMaterial();
        System.out.println(dataMaterial);
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(dataMaterial),CommonConstant.AES_KEY));
    }

    @GetMapping("/getDataUser")
    @ApiOperation(value = "人员数据看板" , notes = "人员数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataUser(){
        Map<String, Integer> dataUser = this.apiExternalService.getDataUser();
        System.out.println(dataUser);
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(dataUser),CommonConstant.AES_KEY));
    }


    @GetMapping("/getDateEventAll")
    @ApiOperation(value = "事件数据看板" , notes = "事件数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataEventAll(){
        Map<String, Integer> dateEventAll = this.apiExternalService.getDataEventAll();
        System.out.println(dateEventAll);
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(dateEventAll),CommonConstant.AES_KEY));
    }

    @GetMapping("/getDataEventByYear")
    @ApiOperation(value = "年度事件数据看板" , notes = "年度事件数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataEventByYear(
            @ApiParam(value = "时间 yyyy",required = true)@RequestParam(required = true)String time
    ){
        Map<String, Object> dataEventByYear = this.apiExternalService.getDataEventByYear(time);
        System.out.println(dataEventByYear);
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(dataEventByYear),CommonConstant.AES_KEY));
    }


    @GetMapping("/getDateEventMonth")
    @ApiOperation(value = "月度事件数据看板" , notes = "月度事件数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDateEventMonth(
            @ApiParam(value = "时间 yyyy-MM",required = true)@RequestParam(required = true)String time
    ){
        Map<String, Object> dateEventMonth = this.apiExternalService.getDateEventMonth(time);
        System.out.println(dateEventMonth);
        return RestUtil.createResponse(AES256EncryptionUtil.Aes256Encode(JSON.toJSONString(dateEventMonth),CommonConstant.AES_KEY));
    }

}
