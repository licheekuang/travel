package com.liaoyin.travel.api.base;

import com.liaoyin.travel.entity.FileUpload;
import com.liaoyin.travel.util.FileUploadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.liaoyin.travel.vo.file.FileInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.service.base.FileService;
import com.liaoyin.travel.ueditor.ActionEnter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/file")
@Api(tags={"文件处理接口"})
public class FileController {

	@Autowired
	private FileService fileService;
	
	@Autowired
	private ActionEnter actionEnter;

	@ApiOperation(value = "文件上传", notes = "文件上传接口")
	@RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JsonRestResponse saveFile(@RequestParam("file") MultipartFile file) {
		FileInfo fileInfo = fileService.savefile(file);
		return RestUtil.createResponse(fileInfo);
	}

	@ApiOperation(value = "文件上传列表", notes = "文件上传列表接口")
	@RequestMapping(value = "/uploadList", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
	public JsonRestResponse saveFileList(@RequestParam("files") MultipartFile[] files) {
		List<FileInfo> fileInfoList = fileService.saveFileList(files);
		return RestUtil.createResponse(fileInfoList);
	}

	@ApiOperation(value = "图片上传", notes = "base64图片上传接口")
	@PostMapping("/uploadImg")
	public JsonRestResponse saveImg(@RequestParam String file) {
		FileInfo fileInfo = fileService.savefile(file);
		return RestUtil.createResponse(fileInfo);
	}
	
	@ApiOperation(value = "文件上传", notes = "文件上传接口")
	@RequestMapping(value = "/uploadUeditor",produces = "application/json;charset=UTF-8")
	public JsonRestResponse saveFileForUeditor(@RequestParam("file") MultipartFile file) {
		FileInfo fileInfo = fileService.savefile(file);
		return RestUtil.createResponse(fileInfo);
	}
	
	@RequestMapping(value = "/uploadForUeditor")
	public String uploadForUeditor(HttpServletRequest request, HttpServletResponse response) throws IOException, JSONException {								
		MultipartFile file=null;
		if(request instanceof MultipartHttpServletRequest) {
			file = ((MultipartHttpServletRequest)request).getFile("upfile");
			String result = actionEnter.exec(file);
			//response.sendRedirect("http://localhost:8080/ueditor1_4_3-utf8-jsp/test.html?obj="+UriEncoder.encode(result));
			System.out.println(result);
			return result;
		}
		return actionEnter.exec(file);
	}

	@RequestMapping(value = "/saveFileTest")
	public JsonRestResponse saveFileTest(String fileName,String originalFileName,String filePath,String fileType,String fileSize,String businessCode){
		FileUpload fileUpload = new FileUpload();
		fileUpload.setFileName(fileName);
		fileUpload.setOriginalFileName(originalFileName);
		fileUpload.setFilePath(filePath);
		fileUpload.setFileType(fileType);
		fileUpload.setFileSize(fileSize);
		return RestUtil.createResponse(FileUploadUtil.saveFileUpload(fileUpload,businessCode));
	}
}
