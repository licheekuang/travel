package com.liaoyin.travel.api.base;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.bean.Plupload;
import com.liaoyin.travel.service.base.PluploadService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

@Api(tags={"上传"})
@RestController
public class VideoController {

    @Autowired
    PluploadService pluploadService;

    //@Value("${gate.file.realPath}")
    private String realPath="E://www";

    /**Plupload文件上传处理方法*/
    @PostMapping(value="/pluploadUpload")
    public JsonRestResponse upload(Plupload plupload, HttpServletRequest request, HttpServletResponse response) {


        String fileDir = realPath;//文件保存的文件夹
        plupload.setRequest(request);//手动传入Plupload对象HttpServletRequest属性
        /*String userId = ((UserBasic)request.getSession().getAttribute("user")).getId();*/
        String path = "movie";

        //文件存储绝对路径,会是一个文件夹，项目相应Servlet容器下的"pluploadDir"文件夹，还会以用户唯一id作划分
        File dir = new File(fileDir+"/"+path);
        if(!dir.exists()){
            dir.mkdirs();//可创建多级目录，而mkdir()只能创建一级目录
        }
        //开始上传文件
        String urlPath = pluploadService.uploadMovie(plupload, dir,path);
        return RestUtil.createResponse(urlPath);
    }
   
}
