package com.liaoyin.travel.api.moble.register;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.ApiExternal.ApiExternalTask;
import com.liaoyin.travel.view.ApiExternal.ApiExternalTaskData;
import com.liaoyin.travel.view.user.ClassificationInfo;
import com.liaoyin.travel.view.user.ScaleUserView;
import com.liaoyin.travel.vo.RegisterVo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.attendance.Equipment;
import com.liaoyin.travel.entity.report.EventReport;
import com.liaoyin.travel.service.DictBasicService;
import com.liaoyin.travel.service.UsersService;
import com.liaoyin.travel.service.external.ApiExternalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Api(tags={"登录,个人信息接口"})
@RequestMapping(value = "/moble/register")
public class RegisterController {

	@Autowired
	private UsersService usersService;
	@Autowired
    private ApiExternalService apiExternalService;
	@Autowired
    private DictBasicService dictBasicService;

    @PostMapping("/login")
    @ResponseBody
    @ApiOperation(value = "登录", notes = "登录")
    public JsonRestResponse login(
    	  RegisterVo registerVo){
        return RestUtil.createResponse(this.usersService.loginByOpenId(registerVo));
    }

    @PostMapping("/refreshUserData")
    @ResponseBody
    @ApiOperation(value = "得到最新的用户数据", notes = "APP用户传入id获取最新的用户数据")
    public JsonRestResponse refreshUserData(
            @ApiParam(value = "用户id", required = true) @RequestParam String userId){
        return RestUtil.createResponse(this.usersService.refreshUserData(userId));
    }

    @PostMapping("/loginByFingerprint")
    @ApiOperation(value = "指纹登录根据用户的ID" , notes = "指纹登录根据用户的ID" , response = UserInfo.class)
    public JsonRestResponse loginByFingerprint(String uid){
        return RestUtil.createResponse(this.usersService.loginByFingerprint(uid));
    }
    
    @ApiOperation(value = "查询我的通讯录 --前端", notes = "查询我的通讯录 --前端", response = Users.class)
    @GetMapping("/selectMailListList")
    public JsonRestResponse selectMailListList(
    		@ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size
    	) {
       
        return RestUtil.createResponse(new PageInfo<>(this.usersService.selectMailListList(num, size)));
    }

    @ApiOperation(value = "查询我的通讯录(分部门层级) --前端", notes = "查询我的通讯录(分一二三级部门) --前端", response = ScaleUserView.class)
    @GetMapping("/selectMailListByScale")
    public JsonRestResponse selectMailListByScale() {
        ScaleUserView scaleUserView = this.usersService.selectMailListByScale();
        return RestUtil.createResponse(scaleUserView);
    }

    @ApiOperation(value = "查询登陆用户自己所在的部门通讯录", notes = "移动端-查询登陆用户自己所在的部门通讯录", response = ClassificationInfo.class)
    @GetMapping("/selectMailListByLogin")
    public JsonRestResponse selectMailListByLogin() {
        ClassificationInfo classificationInfo = this.usersService.selectNewMailListByScale();
        return RestUtil.createResponse(classificationInfo);
    }

    @ApiOperation(value = "根据部门id查询下级部门用户列表", notes = "移动端-根据部门id查询下级部门用户列表", response = ClassificationInfo.class)
    @GetMapping("/selectMailListByTeamId")
    public JsonRestResponse selectMailListByTeamId(
            @ApiParam(value = "部门id", required = true) @RequestParam String teamId){
        List<ClassificationInfo> list = this.usersService.selectMailListByTeamId(teamId);
        return RestUtil.createResponse(list);
    }
    
    @ApiOperation(value = "修改密码 --前端", notes = "修改密码--前端", response = Void.class)
    @GetMapping("/changePassword")
    public JsonRestResponse changePassword(
    		@ApiParam(value = "旧密码", required = true) @RequestParam String oldPassWord,
            @ApiParam(value = "新密码", required = true) @RequestParam String newPassword,
            @ApiParam(value = "确认密码", required = true) @RequestParam String confirmPassWord
    	) {
    	
    	this.usersService.changePassword(oldPassWord, newPassword, confirmPassWord);
        return RestUtil.createResponse();
    }

    @ApiOperation(value = "查询用户信息及头像", notes = "查询用户信息及头像", response = String.class)
    @GetMapping("/mobleSelectUsersById")
    public JsonRestResponse selectUsersById(String uid){
        return RestUtil.createResponse(this.usersService.selectUserById(uid));
    }

    @ApiOperation(value = "上传当前经纬度", notes = "上传当前经纬度", response = void.class)
    @GetMapping("/uploadLatAndLog")
    public JsonRestResponse uploadLatAndLog(
            @ApiParam(value = "用户ID", required = true) @RequestParam String uid,
            @ApiParam(value = "纬度", required = true) @RequestParam String lat,
            @ApiParam(value = "经度", required = true) @RequestParam String log,
            @ApiParam(value = "是否是登陆之后第一次调用", required = true) @RequestParam boolean isFirstCall,
            @ApiParam(value = "调用接口的时间(打印测试用的)", required = true) @RequestParam String time
    ){
        this.usersService.uploadLatAndLog(uid,lat,log,time,isFirstCall);
        return RestUtil.createResponse();
    }

    @ApiOperation(value = "获取所有物资类型", notes = "获取所有物资类型", response = Equipment.class)
    @GetMapping("/getAllMaterialType")
    public JsonRestResponse getAllMaterialType(){
        return RestUtil.createResponse(this.dictBasicService.selectDictBasicListByCodeAndValue("equipmentType","3"));
    }

    @ApiOperation(value = "获取物资 客流经纬度", notes = "获取物资 客流经纬度", response = Equipment.class)
    @GetMapping("/getLatAndLog")
    public JsonRestResponse getLatAndLog(
            @ApiParam(value = "查询类型(1:物资 2:客流)", required = true) @RequestParam Integer queryType,
            @ApiParam(value = "5消防栓 7路灯 8急救点 6车船点 查所有不传") @RequestParam(required = false)Integer equipmentType,
            @ApiParam(value = "当前经度", required = true) @RequestParam String currentLongitude,
            @ApiParam(value = "当前纬度", required = true) @RequestParam String currentLatitude
    ){
        return RestUtil.createResponse(this.usersService.getLatAndLog(queryType,equipmentType,currentLongitude,currentLatitude));
    }

    @ApiOperation(value = "获取事件经纬度-灾害", notes = "获取事件经纬度", response = void.class)
    @GetMapping("/getLatAndLogForEvent")
    public JsonRestResponse getLatAndLogForEvent(
            @ApiParam(value = "查询类型(1:待处理 2:已处理 3.已撤销)", required = true) @RequestParam Integer queryType,
            @ApiParam(value = "当前经度", required = true) @RequestParam String currentLongitude,
            @ApiParam(value = "当前纬度", required = true) @RequestParam String currentLatitude
    ){
        List<EventReport> list = this.usersService.getLatAndLogForEvent(queryType,currentLongitude,currentLatitude);
        return RestUtil.createResponse(list);
    }

    @ApiOperation(value = "获取指定用户的经纬度", notes = "获取指定用户的经纬度", response = void.class)
    @GetMapping("/getUserLatAndLog")
    public JsonRestResponse getUserLatAndLog(
            @ApiParam(value = "指定用户的ID" , required = true)@RequestParam(required = true)String userId
    ){
        return RestUtil.createResponse(this.usersService.getUserLatAndLog(userId));
    }


    @GetMapping("/getDateTask")
    @ApiOperation(value = "任务数据看板" , notes = "任务数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataTask(){
        Map<String, Integer> dataTask = this.apiExternalService.getDataTask();
        System.out.println(dataTask);
        return RestUtil.createResponse(dataTask);
    }

    @GetMapping("/getDataMaterial")
    @ApiOperation(value = "物资数据看板" , notes = "物资数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataMaterial(){
        Map<String, Integer> dataMaterial = this.apiExternalService.getDataMaterial();
        System.out.println(dataMaterial);
        return RestUtil.createResponse(dataMaterial);
    }

    @GetMapping("/getDataUser")
    @ApiOperation(value = "人员数据看板" , notes = "人员数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataUser(){
        Map<String, Integer> dataUser = this.apiExternalService.getDataUser();
        System.out.println(dataUser);
        return RestUtil.createResponse(dataUser);
    }

    @GetMapping("/getDateEventAll")
    @ApiOperation(value = "事件数据看板" , notes = "事件数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataEventAll(){
        Map<String, Integer> dateEventAll = this.apiExternalService.getDataEventAll();
        System.out.println(dateEventAll);
        return RestUtil.createResponse(dateEventAll);
    }

    @GetMapping("/getDataEventByYear")
    @ApiOperation(value = "年度事件数据看板" , notes = "年度事件数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDataEventByYear(
            @ApiParam(value = "时间 yyyy",required = true)@RequestParam(required = true)String time
    ){
        Map<String, Object> dataEventByYear = this.apiExternalService.getDataEventByYear(time);
        System.out.println(dataEventByYear);
        return RestUtil.createResponse(dataEventByYear);
    }

    @GetMapping("/getDateEventMonth")
    @ApiOperation(value = "月度事件数据看板" , notes = "月度事件数据看板" , response = void.class , responseContainer = "Map")
    @ResponseBody
    public JsonRestResponse getDateEventMonth(
            @ApiParam(value = "时间 yyyy-MM",required = true)@RequestParam(required = true)String time
    ){
        Map<String, Object> dateEventMonth = this.apiExternalService.getDateEventMonth(time);
        System.out.println(dateEventMonth);
        return RestUtil.createResponse(dateEventMonth);
    }

    @GetMapping("/getDataYear")
    @ApiOperation(value = "获取事件有数据的年份" , notes = "获取事件有数据的年份" , response = void.class , responseContainer = "List")
    @ResponseBody
    public JsonRestResponse getDataYear(){
        return RestUtil.createResponse(this.apiExternalService.getDataYear());
    }


    @GetMapping("/getDataMonth")
    @ApiOperation(value = "获取事件有数据的月份" , notes = "获取事件有数据的月份" , response = void.class , responseContainer = "List")
    @ResponseBody
    public JsonRestResponse getDataMonth(){
        return RestUtil.createResponse(this.apiExternalService.getDataMonth());
    }


    @GetMapping("/getDataTaskList")
    @ApiOperation(value = "根据任务类型查询今日任务列表" , notes = "根据任务类型查询今日任务列表" , response = ApiExternalTask.class , responseContainer = "List")
    @ResponseBody
    public JsonRestResponse getDataTaskList(
            @ApiParam(value = "任务类型 2普通任务 3巡更任务 不填所以", required = false)@RequestParam(required = false) String taskType
    ){
        return RestUtil.createResponse(this.apiExternalService.getTaskList(taskType));
    }

    @GetMapping("/getDataTaskByYear")
    @ApiOperation(value = "获取年度任务数据   类别" , notes = "获取年度任务数据   类别" , response = ApiExternalTaskData.class , responseContainer = "List")
    @ResponseBody
    public JsonRestResponse getDataTaskByYear(
            @ApiParam(value = "年份 yyyy",required = true)@RequestParam(required = true)String time
    ){
        return RestUtil.createResponse(this.apiExternalService.getDataTaskByYear(time));
    }

    @GetMapping("/getDataTaskYear")
    @ApiOperation(value = "获取有任务看板数据的年份" , notes = "获取有任务看板数据的年份" , response = String.class , responseContainer = "List")
    @ResponseBody
    public JsonRestResponse getDataTaskYear(){
        return RestUtil.createResponse(this.apiExternalService.getDataTaskYear());
    }
}
