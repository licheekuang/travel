package com.liaoyin.travel.api.moble.task;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.vo.task.InsertTaskVo;
import com.liaoyin.travel.vo.task.InsertTimedTaskVo;
import com.liaoyin.travel.vo.task.SelectTaskListVo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.task.Task;
import com.liaoyin.travel.entity.task.TaskCompletionRecord;
import com.liaoyin.travel.entity.task.TaskReceivingRecords;
import com.liaoyin.travel.service.task.TaskCompletionRecordService;
import com.liaoyin.travel.service.task.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@Api(tags={"任务接口"})
@RequestMapping("/moble/task")
public class TaskController {

	@Autowired
	private TaskService taskService;
	
	@Autowired
	private TaskCompletionRecordService taskCompletionRecordService;
	
    @PostMapping("/selectTaskList")
    @ResponseBody
    @ApiOperation(value = "查询任务列表--前端", notes = "查询任务列表--前端",response = Task.class)
    public JsonRestResponse selectTaskList(
    		@RequestBody SelectTaskListVo selectTaskListVo){
    	
        return RestUtil.createResponse(new PageInfo<>(this.taskService.selectTaskList(selectTaskListVo)));
    }
    
    @GetMapping("/selectTaskByIdOnMoble")
    @ResponseBody
    @ApiOperation(value = "根据id查询任务详情-前端", notes = "根据id查询任务详情-前端",response = Task.class)
    public JsonRestResponse selectTaskByIdOnMoble(
    		 @ApiParam(value = "任务id", required = true) @RequestParam(required = true) String id
    ){
        return RestUtil.createResponse(this.taskService.selectTaskByIdOnMoble(id));
    }
    
    @PostMapping("/insertTask")
    @ResponseBody
    @ApiOperation(value = "发布任务", notes = "发布任务",response = Void.class)
    public JsonRestResponse insertTask(
    		@RequestBody InsertTaskVo insertTaskVo){

    	this.taskService.insertTask(insertTaskVo);
        return RestUtil.createResponse();
    }

    @PostMapping("/insertTimedTask")
    @ResponseBody
    @ApiOperation(value = "发布定时任务", notes = "发布定时任务",response = InsertTimedTaskVo.class)
    public JsonRestResponse insertTimedTask(@RequestBody InsertTimedTaskVo insertTimedTaskVo){

        this.taskService.insertTimedTask(insertTimedTaskVo);
        return RestUtil.createResponse();
    }
    
    
    @GetMapping("/startTask")
    @ResponseBody
    @ApiOperation(value = "开始任务", notes = "开始任务",response = Void.class)
    public JsonRestResponse startTask(
    		 @ApiParam(value = "任务id", required = true) @RequestParam(required = true) String id){
    	
    	this.taskService.startTask(id);
        return RestUtil.createResponse();
    }
    
    
    @GetMapping("/applicationRevocationTask")
    @ResponseBody
    @ApiOperation(value = "申请撤销", notes = "申请撤销",response = Void.class)
    public JsonRestResponse applicationRevocationTask(
    	@ApiParam(value = "任务id", required = true) @RequestParam String id,
    	@ApiParam(value = "撤销理由【字典 reasonsRevocation】", required = true) @RequestParam String reasonsRevocation,
    	@ApiParam(value = "任务异常说明", required = true) @RequestParam String exceptionContext,
        @ApiParam(value = "昵称", required = true) @RequestParam String nickName
    ){
    	int result = this.taskService.applicationRevocationTask(id, exceptionContext,reasonsRevocation,nickName);
        return RestUtil.createResponse(result);
    }
    
    @GetMapping("/judgeCurrentTask")
    @ResponseBody
    @ApiOperation(value = "判断当前任务是否允许签到", notes = "判断当前任务是否允许签到",response = Map.class)
    public JsonRestResponse judgeCurrentTask(
    		@ApiParam(value = "任务详情id", required = true) @RequestParam String taskDetailsId,
    		@ApiParam(value = "签到纬度", required = true) @RequestParam String lat,
    		@ApiParam(value = "签到经度", required = true) @RequestParam String log,
            @ApiParam(value = "签到设备ID") @RequestParam(required = false) String equipmentId
    ){

        return RestUtil.createResponse(this.taskCompletionRecordService.judgeCurrentTask(taskDetailsId, lat, log,equipmentId));
    }
    
    
    @PostMapping("/completeTasks")
    @ResponseBody
    @ApiOperation(value = "完成任务", notes = "完成任务",response = Void.class)
    public JsonRestResponse completeTasks(
    	@RequestBody TaskCompletionRecord taskCompletionRecord){
    	
    	this.taskCompletionRecordService.insertTaskCompletionRecord(taskCompletionRecord);
        return RestUtil.createResponse();
    }

    @GetMapping("/selectTaskListByUserId")
    @ResponseBody
    @ApiOperation(value = "根据用户id查询今日任务列表" , notes = "根据用户id查询今日任务列表" , response = TaskReceivingRecords.class)
    public JsonRestResponse selectTaskListByUserId(
            @ApiParam(value = "用户id", required = true) @RequestParam(required = true) String userId,
            @ApiParam(value = "任务状态 1=未开始 2=进行中 3=已撤销 4=任务暂停 5=已完成", required = true) @RequestParam(required = true) String tastStatus
    ){

        return RestUtil.createResponse(taskService.selectTaskUserList(userId,tastStatus));
    }


}
