package com.liaoyin.travel.api.moble.rong;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.common.rest.BaseController;
import com.liaoyin.travel.dao.UserRongMapper;
import com.liaoyin.travel.entity.UserRong;
import com.liaoyin.travel.service.UserRongService;
import com.liaoyin.travel.util.RongUtils;
import com.liaoyin.travel.vo.rongyun.SendMessageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Controller
@Api(tags={"融云接口"})
@RequestMapping("/moble/rong")
public class RongController extends BaseController<UserRongService,UserRong> {

    @Resource
    UserRongMapper userRongMapper;

    /*@GetMapping("/selectTokenById")
    @ResponseBody
    @ApiOperation(value = "根据ID查询token", notes = "根据ID查询token",response = UserRong.class)
    public JsonRestResponse selectTokenById(@ApiParam(name = "id", value = "ID")@RequestParam(required = true)String id){
        return RestUtil.createResponse(this.baseService.selectById(id));
    }*/

    @GetMapping("/selectTokenByUserId")
    @ResponseBody
    @ApiOperation(value = "根据用户ID查询token", notes = "根据用户ID查询token",response = UserRong.class)
    public JsonRestResponse selectTokenByUserId(
    		@ApiParam(name = "userId", value = "用户ID")@RequestParam(required = true)String userId,
    		@ApiParam(name = "type", value = "1:新增  2：更新")@RequestParam(required = true)String type){
        return RestUtil.createResponse(this.baseService.selectTokenByUserId(userId,type));
    }

    @PostMapping("/testToken")
    @ResponseBody
    @ApiOperation(value = "测试获取融云token", notes = "测试获取融云token",response = UserRong.class)
    public JsonRestResponse testToken(
            @ApiParam(name = "userId", value = "用户ID")@RequestParam(required = true)String userId,
            @ApiParam(name = "type", value = "1:新增  2：更新")@RequestParam(required = true)String type){
        String result = null;
        try {
            result = RongUtils.getToken(userId,type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return RestUtil.createResponse(result);
    }

    @PostMapping("/countRong")
    @ApiOperation(value = "统计注册融云用户的数量", notes = "统计注册融云用户的数量",response = Integer.class)
    public JsonRestResponse countRong(){

        return RestUtil.createResponse(userRongMapper.selectCountAll());
    }

    @PostMapping("/deleteRongByAll")
    @ApiOperation(value = "清空融云用户数据库", notes = "清空融云用户数据库",response = Integer.class)
    public JsonRestResponse deleteRongByAll(){

        return RestUtil.createResponse(userRongMapper.deleteRongByAll());
    }

    /*@PostMapping("/pushMessage")
    @ResponseBody
    @ApiOperation(value = "提供给AI的消息推送接口", notes = "根据用户ID查询token",response = Void.class)
    public JsonRestResponse pushMessage(
    		@RequestBody SendMessageVo sendMessageVo){
    	
    	System.out.println(sendMessageVo.getMessageBody()+"//"+sendMessageVo.getType()+"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    	this.baseService.pushMessage(sendMessageVo);
    	return RestUtil.createResponse();
    }*/

    /*@PostMapping("/pushMessage")
    @ResponseBody
    @ApiOperation(value = "提供给AI的消息推送接口", notes = "根据用户ID查询token",response = Void.class)*/
    public JsonRestResponse pushMessage(
            @ApiParam(name = "messageBody", value = "加密消息")@RequestParam(required = true)String messageBody,
            @ApiParam(name = "type", value = "1:预警消息")@RequestParam(required = true)String type){

        SendMessageVo sendMessageVo = new SendMessageVo();
        sendMessageVo.setMessageBody(messageBody);
        sendMessageVo.setType(type);
        this.baseService.pushMessage(sendMessageVo);
        return RestUtil.createResponse();
    }


}
