package com.liaoyin.travel.api.moble.report;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.report.EventReport;
import com.liaoyin.travel.service.report.EventReportService;
import com.liaoyin.travel.service.report.EventUserService;
import com.liaoyin.travel.util.PartyUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author privatePanda777@163.com
 * @title: UserEventController
 * @projectName travel
 * @description: TODO 事件的接口
 * @date 2019/8/516:11
 */
@RestController
@Api(tags={"事件的接口"})
@RequestMapping("/moble/userEvent")
public class UserEventController {

    @Autowired
    private EventReportService eventReportService;

    @Autowired
    private EventUserService eventUserService;

    @ApiOperation(value = "事件上报" ,notes = "事件上报" , response = void.class)
    @PostMapping("/insertEvent")
    public JsonRestResponse insertEvent(
            @RequestBody EventReport eventReport){
        this.eventReportService.insertEventReport(eventReport);
        return RestUtil.createResponse();
    }

    @ApiOperation(value = "查询事件列表" , notes = "查询事件列表" , response = EventReport.class)
    @GetMapping("/selectEventList")
    public JsonRestResponse selectEventList(
            @ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
            @ApiParam(value = "事件类型(1:不良事件上报;2:一键报警)")@RequestParam(required = false)Integer eventType,
            @ApiParam(value = "事件时间")@RequestParam(required = false)String eventTime,
            @ApiParam(value = "查询开始时间") @RequestParam(required = false) String startTime,
            @ApiParam(value = "查询结束时间") @RequestParam(required = false) String endTime,
            @ApiParam(value = "事件上报人id,可多选,用字符串隔开") @RequestParam(required = false) String reportPersonIds,
            @ApiParam(value = "上报事件进度(1:待处理;2:已处理;3:已撤销)") @RequestParam(required = false) Integer eventProgress,
            @ApiParam(value = "查询类型（1:查询所有,2:上报给我的）")@RequestParam(required = false)Integer queryType
    ){
        PageInfo<EventReport> pageInfo = this.eventReportService.selectEventListByCondition(num,size,eventType,eventTime,startTime,endTime,reportPersonIds,eventProgress,queryType);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "根据上报人id查询事件列表" , notes = "根据上报人id等条件查询事件列表" , response = EventReport.class)
    @GetMapping("/selectEventListByReportPerson")
    public JsonRestResponse selectEventListByReportPerson(
            @ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam(required = true) Integer size,
            @ApiParam(value = "事件类型" , required = false)@RequestParam(required = false)Integer eventType,
            @ApiParam(value = "事件时间" , required = false)@RequestParam(required = false)String eventTime,
            @ApiParam(value = "事件上报人id" , required = false)@RequestParam(required = false)String userId
    ){
        PageInfo<EventReport> pageInfo = this.eventReportService.selectEventListByReportPerson(num,size,eventType,userId,eventTime);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "查询事件详情" , notes = "查询事件详情", response = EventReport.class)
    @GetMapping("/selectEventDetail")
    public JsonRestResponse selectEventDetail(@ApiParam(value = "事件ID", required = true) @RequestParam String eventId){
        return RestUtil.createResponse(this.eventReportService.selectEventDetail(eventId));
    }

    @ApiOperation(value = "更新事件状态" , notes = "更新事件状态", response = void.class)
    @GetMapping("/updateEventProgress")
    public JsonRestResponse updateEventProgress(
            @ApiParam(value = "更新事件状态(2:已处理,3:已撤销)", required = true) @RequestParam Integer eventProgress,
            @ApiParam(value = "事件ID", required = true) @RequestParam String eventId
    ){
        return RestUtil.createResponse(this.eventReportService.updateEventProgress(eventProgress,eventId));
    }

    @ApiOperation(value = "通过mp3相对路径和文件名下载音频" , notes = "通过mp3相对路径和文件名下载音频", response = void.class)
    @GetMapping(value = "/downAudio")
    public void downAudioFileByPathAndName(
            @ApiParam(value = "mp3的相对路径", required = true) @RequestParam String audioPath,
            @ApiParam(value = "文件名", required = true) @RequestParam String fileName) {
        eventReportService.downAudioFileByPathAndName(PartyUtil.getResponse(),audioPath,fileName);
        // return RestUtil.createResponse();
    }


}
