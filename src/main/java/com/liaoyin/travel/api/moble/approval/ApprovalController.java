package com.liaoyin.travel.api.moble.approval;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.moble.approval.ApprovalDetailsView;
import com.liaoyin.travel.view.moble.approval.ApprovalIsVisibleView;
import com.liaoyin.travel.view.moble.approval.ApprovalRecordCommunalView;
import com.liaoyin.travel.view.user.UserView;
import com.liaoyin.travel.vo.approval.*;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.common.validate.Insert;
import com.liaoyin.travel.service.approval.ApprovalRecordService;
import com.liaoyin.travel.service.approval.VisualRangeSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * TODO
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 15:07
 */
@RestController
@Api(tags={"移动端审批接口"})
@RequestMapping("/moble/ApprovalController")
public class ApprovalController {

    @Resource
    ApprovalRecordService approvalRecordService;

    @Resource
    VisualRangeSettingService visualRangeSettingService;

    /*@PostMapping("/whetherTheApprovalIsVisible")
    @ResponseBody
    @ApiOperation(value = "查询自己是否有可见【补卡/请假/申请】审批的权限", notes = "用户传入审批类型，查询自己是否有可见该审批的权限")
    public JsonRestResponse whet/selectEquipmentListBackherTheApprovalIsVisible(@ApiParam(value = "审批类型(1.补卡申请;2.请假申请;3.外出申请)", required = true) @RequestParam(required = true) String approvalType){
        boolean isVisible = this.visualRangeSettingService.whetherTheApprovalIsVisible(approvalType);
        return RestUtil.createResponse(isVisible);
    }*/

    @PostMapping("/whetherTheApprovalIsVisible")
    @ResponseBody
    @ApiOperation(value = "查询自己是否有可见【补卡/请假/申请】审批的权限", notes = "登录用户查询自己是否有可见【补卡/请假/申请】审批的权限",response = ApprovalIsVisibleView.class)
    public JsonRestResponse whetherTheApprovalIsVisible(){
       List<ApprovalIsVisibleView> approvalisVisibleViews = this.visualRangeSettingService.whetherTheApprovalIsVisibleByLoginUser();
        return RestUtil.createResponse(approvalisVisibleViews);
    }

    @PostMapping("/checkYourApprover")
    @ResponseBody
    @ApiOperation(value = "查询自己的审批人", notes = "发审批时,查询自己的审批人是谁",response= UserView.class)
    public JsonRestResponse checkYourApprover(@ApiParam(value = "请求类型(1.补卡申请;2.请假申请;3.外出申请)", required = true) @RequestParam(required = true) String approvalStatus){
        UserView userView = this.approvalRecordService.checkYourApprover(approvalStatus);
        return RestUtil.createResponse(userView);
    }

    @PostMapping("/insertAskForLeaveRecord")
    @ResponseBody
    @ApiOperation(value = "发起请假审批", notes = "员工发起请假审批",response= AskForLeaveRequestVo.class)
    public JsonRestResponse insertAskForLeaveRecord(@RequestBody @Validated({Insert.class}) AskForLeaveRequestVo askForLeaveRequestVo){
        int result = this.approvalRecordService.insertAskForLeaveRecord(askForLeaveRequestVo);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/insertGoOutRecord")
    @ResponseBody
    @ApiOperation(value = "发起外出审批", notes = "员工发起外出审批",response = GoOutRecordVo.class)
    public JsonRestResponse insertGoOutRecord(@RequestBody @Validated({Insert.class}) GoOutRecordVo goOutRecordVo){
        int result = this.approvalRecordService.insertGoOutRecord(goOutRecordVo);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/insertCardReissueRecord")
    @ResponseBody
    @ApiOperation(value = "发起补卡审批", notes = "员工发起补卡审批",response= CardReissueRecordVo.class)
    public JsonRestResponse insertCardReissueRecord(@RequestBody CardReissueRecordVo cardReissueRecordVo){
        int result = this.approvalRecordService.insertCardReissueRecord(cardReissueRecordVo);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/selectApprovalRecordByApprover")
    @ResponseBody
    @ApiOperation(value = "管理人员查询自己的审批记录", notes = "管理人员登录后查询自己的审批记录",response= ApprovalRecordCommunalView.class)
    public JsonRestResponse selectApprovalRecordByApprover(
            @ApiParam(value = "页码", required = true) @RequestParam Integer num,
            @ApiParam(value = "页大小", required = true) @RequestParam Integer size
    ){
        PageInfo<ApprovalRecordCommunalView> pageInfo = this.approvalRecordService.selectApprovalRecordByApprover(num,size);
        return RestUtil.createResponse(pageInfo);
    }

    @PostMapping("/selectRequestRecordByInitiatorId")
    @ResponseBody
    @ApiOperation(value = "员工查看自己的请求记录", notes = "员工查看自己的请求记录",response= ApprovalRecordCommunalView.class)
    public JsonRestResponse selectRequestRecordByInitiatorId(
            @ApiParam(value = "页码", required = true) @RequestParam Integer num,
            @ApiParam(value = "页大小", required = true) @RequestParam Integer size,
            @ApiParam(value = "审批类型(1.补卡申请;2.请假申请;3.外出申请)", required = true) @RequestParam String approvalType
    ){
        PageInfo<ApprovalRecordCommunalView> pageInfo = this.approvalRecordService.selectRequestRecordByInitiatorId(num,size,approvalType);
        return RestUtil.createResponse(pageInfo);
    }

    @PostMapping("/revocationApprovalRecord")
    @ResponseBody
    @ApiOperation(value = "撤销自己发出的请求", notes = "员工传入审批记录id撤销自己发出的还未被审批的请求")
    public JsonRestResponse revocationApprovalRecord(@ApiParam(value = "审批记录id", required = true) @RequestParam String approvalRecordId){
       int result = this.approvalRecordService.revocationApprovalRecord(approvalRecordId);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/selectApprovalDetailsViewByApprovalId")
    @ResponseBody
    @ApiOperation(value = "查询审批记录详情", notes = "根据审批记录id查询审批记录的详情",response= ApprovalDetailsView.class)
    public JsonRestResponse selectApprovalDetailsViewByApprovalIdAndType(
            @ApiParam(value = "审批记录id", required = true) @RequestParam String approvalId
    ){
        ApprovalDetailsView approvalDetailsView = this.approvalRecordService.selectApprovalDetailsViewByApprovalId(approvalId);
        return RestUtil.createResponse(approvalDetailsView);
    }

    @PostMapping("/disposeCardReissueApproval")
    @ResponseBody
    @ApiOperation(value = "处理补卡审批", notes = "管理端处理补卡审批",response= CardReissueApprovalVo.class)
    public JsonRestResponse disposeCardReissueApproval(@RequestBody CardReissueApprovalVo cardReissueApprovalVo){
        int result = this.approvalRecordService.disposeCardReissueApproval(cardReissueApprovalVo);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/disposeAskForLeaveApproval")
    @ResponseBody
    @ApiOperation(value = "处理请假审批", notes = "管理端处理请假审批",response= AskForLeaveApprovalVo.class)
    public JsonRestResponse disposeAskForLeaveApproval(@RequestBody AskForLeaveApprovalVo askForLeaveApprovalVo){
        int result = this.approvalRecordService.disposeAskForLeaveApproval(askForLeaveApprovalVo);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/disposeGoOutApproval")
    @ResponseBody
    @ApiOperation(value = "处理外出审批", notes = "管理端处理外出审批",response= GoOutApprovalVo.class)
    public JsonRestResponse disposeGoOutApproval(@RequestBody GoOutApprovalVo goOutApprovalVo){
        int result = this.approvalRecordService.disposeGoOutApproval(goOutApprovalVo);
        return RestUtil.createResponse(result);
    }

}
