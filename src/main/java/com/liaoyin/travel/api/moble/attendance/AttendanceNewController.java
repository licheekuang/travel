package com.liaoyin.travel.api.moble.attendance;

import com.alibaba.fastjson.JSON;
import com.liaoyin.travel.view.mine.attendance.AttendanceMonitoringView;
import com.liaoyin.travel.view.moble.attendance.AttendanceSetView;
import com.liaoyin.travel.view.moble.attendance.AttendanceWeekDayView;
import com.liaoyin.travel.view.moble.attendance.LackOfCardRecordView;
import com.liaoyin.travel.view.moble.attendance.StaffTaiyakiAttendanceInfoView;
import com.liaoyin.travel.vo.attendance.AttendanceManageVo;
import com.liaoyin.travel.vo.attendance.InsertOrUpdateAttendanceRecordVo;
import com.liaoyin.travel.vo.attendance.MobileAttencanceRateVO;
import com.liaoyin.travel.vo.attendance.record.RecordListVO;
import com.liaoyin.travel.vo.request.MobileAttendanceStatRequestVO;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.service.attendance.AttendanceNewService;
import com.liaoyin.travel.service.attendance.AttendanceRecordService;
import com.liaoyin.travel.service.attendance.AttendanceUserService;
import com.liaoyin.travel.service.attendance.ManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 考勤打卡新需求移动端接口
 *
 * @author kuang.jiazhuo
 * @date 2019-11-19 19:06
 */
@RestController
@Api(tags={"考勤打卡(新需求)"})
@RequestMapping("/moble/newAttendanceMoble")
@Slf4j
public class AttendanceNewController {

    @Resource
    AttendanceNewService attendanceNewService;

    @Resource
    AttendanceRecordService attendanceRecordService;

    @Resource
    ManagementService managementService;

    @Resource
    AttendanceUserService attendanceUserService;


    @GetMapping("/back/judgeCheckInIsAllowed")
    @ResponseBody
    @ApiOperation(value = "后台判断当前员工是否在允许的签到范围", notes = "前端传入经纬度，后台判断当前员工是否在允许的签到范围",response= Map.class,responseContainer="Map")
    public JsonRestResponse judgeCheckInIsAllowed(
            @ApiParam(value = "经度", required = true) @RequestParam(required = true) String lat,
            @ApiParam(value = "纬度", required = true) @RequestParam(required = true) String log
    ){
        boolean result = this.attendanceNewService.judgeCheckInIsAllowed(lat,log,null);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/insertOrUpdateAttendanceRecord")
    @ResponseBody
    @ApiOperation(value = "前端打卡", notes = "前端打卡",response= InsertOrUpdateAttendanceRecordVo.class)
    public JsonRestResponse insertOrUpdateAttendanceRecord(
            @RequestBody InsertOrUpdateAttendanceRecordVo insertOrUpdateAttendanceRecordVo){
        int result = this.attendanceRecordService.insertOrUpdateAttendanceRecord(insertOrUpdateAttendanceRecordVo);
        return RestUtil.createResponse(result);
    }

    @PostMapping("/getCurrentAttendanceSetting")
    @ResponseBody
    @ApiOperation(value = "获取员工的打卡设置信息", notes = "获取员工的打卡设置信息",response= AttendanceManageVo.class)
    public JsonRestResponse getCurrentAttendanceSetting(){
        AttendanceManageVo attendanceManageVo = this.managementService.selectAttendanceManageVoByUserId(null);
        return RestUtil.createResponse(attendanceManageVo);
    }

    @PostMapping("/getTheDayIsClock")
    @ResponseBody
    @ApiOperation(value = "获取员工当天的考勤信息", notes = "获取员工当天的考勤信息",response= StaffTaiyakiAttendanceInfoView.class)
    public JsonRestResponse getTheDayIsClock(@ApiParam(value = "日期,yyyy-MM-dd", required = true) @RequestParam String date){
        StaffTaiyakiAttendanceInfoView view = this.attendanceNewService.getTheDayIsClock(date);
        return RestUtil.createResponse(view);
    }

    @PostMapping("/selectLackOfCardRecordByLoginUser")
    @ResponseBody
    @ApiOperation(value = "获取登录员工的缺卡记录", notes = "获取登录员工的缺卡记录",response= LackOfCardRecordView.class)
    public JsonRestResponse selectLackOfCardRecordByLoginUser(){
        List<LackOfCardRecordView> list = this.attendanceRecordService.selectLackOfCardRecordByLoginUser();
        return RestUtil.createResponse(list);
    }

    @PostMapping("/selectAttendanceDay")
    @ResponseBody
    @ApiOperation(value = "员工查询自己的考勤工作日", notes = "员工查询自己的考勤工作日",response= StaffTaiyakiAttendanceInfoView.class)
    public JsonRestResponse selectAttendanceDay(){
        List<AttendanceWeekDayView> attendanceWeekDayViews = this.attendanceUserService.selectAttendanceDay(null);
        return RestUtil.createResponse(attendanceWeekDayViews);
    }

    @PostMapping("/selectAttendanceDateTime")
    @ResponseBody
    @ApiOperation(value = "员工查询自己的考勤工作时间", notes = "员工查询自己的考勤日和打卡时间",response= AttendanceSetView.class)
    public JsonRestResponse selectAttendanceDateTime(){
        AttendanceSetView attendanceSetView = this.attendanceNewService.selectAttendanceDateTime(null);
        return RestUtil.createResponse(attendanceSetView);
    }

    @PostMapping("/isEnableAttendance")
    @ApiOperation(value = "当前是否可以打卡", notes = "当前是否可以打卡")
    public JsonRestResponse isEnableAttendance(){
        return RestUtil.createResponse(this.attendanceNewService.isEnableAttendance());
    }

    @PostMapping("/getMobileAttendanceStat")
    @ResponseBody
    @ApiOperation(value = "考勤统计", notes = "考勤统计", response = MobileAttencanceRateVO.class)
    public JsonRestResponse getMobileAttendanceStat(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){
        MobileAttencanceRateVO mobileAttencanceRateVO = this.attendanceNewService.getMobileAttendanceStat(mobileAttendanceStatRequestVO);
        log.info(JSON.toJSONString(mobileAttencanceRateVO));
        return RestUtil.createResponse(mobileAttencanceRateVO);
    }

    @PostMapping("/getAttendanceRecordByType")
    @ResponseBody
    @ApiOperation(value = "考勤记录明细", notes = "考勤记录明细", response = RecordListVO.class)
    public JsonRestResponse getAttendanceRecordByType(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){
        List<Map<String,String>> recordList = this.attendanceNewService.getAttendanceRecordByType(mobileAttendanceStatRequestVO);
        return RestUtil.createResponse(recordList);
    }

    @PostMapping("/selectAttendanceMonitoringByDate")
    @ResponseBody
    @ApiOperation(value = "考勤监控", notes = "移动端根据日期查询考勤监控数据", response = AttendanceMonitoringView.class)
    public JsonRestResponse selectAttendanceMonitoringByDate(
            @ApiParam(value = "查询日期 yyyy-MM-dd", required = true) @RequestParam String  date){
        AttendanceMonitoringView attendanceMonitoringView = this.attendanceNewService.selectAttendanceMonitoringByDate(date);
        return RestUtil.createResponse(attendanceMonitoringView);
    }

}
