package com.liaoyin.travel.api.moble.report;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.report.UserBriefing;
import com.liaoyin.travel.service.report.UserBriefingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Api(tags={"员工简报接口"})
@RequestMapping("/moble/userBriefing")
public class UserBriefingController {

	@Autowired
	private UserBriefingService userBriefingService;

    @PostMapping("/selectMyUserBriefingList")
    @ResponseBody
    @ApiOperation(value = " 查询简报--前后端", notes = " 查询简报--前后端",response = UserBriefing.class)
    public JsonRestResponse selectMyUserBriefingList(
    		 @ApiParam(value = "页数", required = true) @RequestParam Integer num,
    		 @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
    		 @ApiParam(value = "查询时间") @RequestParam(required = false) String qurtyDate,
             @ApiParam(value = "查询类型【1：前端 2：后台】") @RequestParam(required = false) String qurtyType,
    		 @ApiParam(value = "用户id【可为空】") @RequestParam(required = false) String userId){
    	
        return RestUtil.createResponse(new PageInfo<>(this.userBriefingService.selectMyUserBriefingList(num, size, userId, qurtyDate,qurtyType)));
    }
    
    @GetMapping("/selectUserBriefingById")
    @ResponseBody
    @ApiOperation(value = "根据id查询简报详情", notes = "根据id查询简报详情-前端",response = UserBriefing.class)
    public JsonRestResponse selectUserBriefingById(
    		 @ApiParam(value = "简报id", required = true) @RequestParam String id){
        return RestUtil.createResponse(this.userBriefingService.selectUserBriefingById(id));
    }
    
    @PostMapping("/insertUserBriefing")
    @ResponseBody
    @ApiOperation(value = "新增简报--前端", notes = "新增简报--前端",response = Integer.class)
    public JsonRestResponse insertUserBriefing(
    		@RequestBody UserBriefing userBriefing){
    	
    	int result = this.userBriefingService.insertUserBriefing(userBriefing);
        return RestUtil.createResponse(result);
    }
    
    
    @GetMapping("/deleteUserBriefingByIds")
    @ResponseBody
    @ApiOperation(value = "批量删除", notes = "批量删除",response = Void.class)
    public JsonRestResponse deleteUserBriefingByIds(
    		 @ApiParam(value = "简报ids", required = true) @RequestParam(required = true) String ids){
    	
    	this.userBriefingService.deleteUserBriefingByIds(ids);
        return RestUtil.createResponse();
    }
      
}
