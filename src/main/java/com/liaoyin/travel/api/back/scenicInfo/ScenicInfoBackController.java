package com.liaoyin.travel.api.back.scenicInfo;

import com.alibaba.fastjson.*;
import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.scenic.ScenicInfoView;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.exception.*;
import com.liaoyin.travel.service.scenicInfo.ScenicInfoService;
import com.liaoyin.travel.util.*;
import com.liaoyin.travel.vo.scenic.AuditScenicVo;
import com.liaoyin.travel.vo.scenic.InsertOrUpdateScenicVo;
import com.liaoyin.travel.vo.scenic.SelectScenicVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.codec.digest.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.nio.charset.*;
import java.text.ParseException;
import java.util.*;

/**
 * 超级管理员进行景区管理
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-21 11:21
 */
@RestController
@Api(tags = {"单位管理"})
@RequestMapping("/back/scenicManagement")
public class ScenicInfoBackController {

    @Resource
    private ScenicInfoService scenicInfoService;


    @ApiOperation(value = "新增或更新-单位信息", notes = "后台-新增或更新-单位信息", response = InsertOrUpdateScenicVo.class)
    @PostMapping(value = "/insertOrUpdateScenicInfo")
    public JsonRestResponse insertOrUpdateScenicInfo(@RequestBody @Validated InsertOrUpdateScenicVo insertOrUpdateScenicVo) throws ParseException {
        int result = this.scenicInfoService.insertOrUpdateScenicInfo(insertOrUpdateScenicVo);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "重新申请单位账号", notes = "后台-已通过和已拒绝的重新申请单位账号", response = InsertOrUpdateScenicVo.class)
    @PostMapping(value = "/reapplyScenicInfo")
    public JsonRestResponse reapplyScenicInfo(@RequestBody @Validated InsertOrUpdateScenicVo insertOrUpdateScenicVo) throws ParseException {
        int result = this.scenicInfoService.reapplyScenicInfo(insertOrUpdateScenicVo);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "按条件查询单位管理的信息", notes = "按条件查询单位管理的信息(带分页)", response = SelectScenicVo.class)
    @PostMapping(value = "/selectScenicInfoViewByCondition")
    public JsonRestResponse selectScenicInfoViewByCondition(@RequestBody SelectScenicVo selectScenicVo) {
        PageInfo<ScenicInfoView> pageInfo = this.scenicInfoService.selectScenicInfoViewByCondition(selectScenicVo);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "根据id和审核状态查询单位详情", notes = "根据id和审核状态查询单位详情", response = ScenicInfoView.class)
    @GetMapping(value = "/selectScenicInfoViewByIdAndAuditStatus")
    public JsonRestResponse selectScenicInfoViewByIdAndAuditStatus(@ApiParam(value = "单位id", required = true) @RequestParam String scenicId,
                                                                   @ApiParam(value = "审核状态【0:未审核;1:通过;2:拒绝;】", required = true) @RequestParam String auditStatus) {
        ScenicInfoView scenicInfoView = this.scenicInfoService.selectScenicInfoViewByIdAndAuditStatus(scenicId, auditStatus);
        return RestUtil.createResponse(scenicInfoView);
    }

    @ApiOperation(value = "审核单位", notes = "通过或拒绝单位的申请", response = AuditScenicVo.class)
    @PostMapping(value = "/auditScenic")
    public JsonRestResponse auditScenic(@RequestBody @Validated AuditScenicVo auditScenicVo) {
        int result = this.scenicInfoService.auditScenic(auditScenicVo);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "删除单位管理信息", notes = "根据ids字符串批量(物理)删除景区管理")
    @PostMapping(value = "/deleteScenicInfoByIds")
    public JsonRestResponse deleteScenicInfoByIds(@ApiParam(value = "要删除的id集合", required = true) @RequestParam String ids) {
        int result = this.scenicInfoService.deleteScenicInfoByIds(ids);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "Excel导入单位数据", notes = "Excel导入单位数据", response = boolean.class)
    @PostMapping(value = "/importScenicInfo")
    public JsonRestResponse importScenicInfoExcel(@ApiParam(value = "excel导入文件", required = true) @RequestParam("file") MultipartFile file,
                                                  @ApiParam(value = "单位类型(1.景区;2.机构)", required = true) @RequestParam String unitType) {

        Boolean b = false;
        String fileName = file.getOriginalFilename();
        b = scenicInfoService.importScenicInfoExcel(fileName, file, unitType);
        return RestUtil.createResponse(b);
    }

    @Value("${scenic.emergencyResourceUrl}")
    private String emergencyResourceUrl;
    @Value("${scenic.secret}")
    private String secret;
    @Value("${scenic.appKey}")
    private String appKey;

    @ApiOperation(value = "获取应急文旅资源", notes = "获取应急文旅资源")
    @GetMapping(value = "/emergencyResource")
    public Object getEmergencyResource(String keyWords) {

        String time = String.valueOf(new Date().getTime() / 1000);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json;charset=utf-8");
        headers.put("timestamp", time);
        headers.put("appKey", appKey);
        if (!StringUtils.isBlank(keyWords)) {
            headers.put("keyWords", keyWords);
        }
        String signSrc = "timestamp=" + time + "&secret=" + secret;
        String sign = DigestUtils.md5Hex(signSrc.getBytes(Charset.forName("UTF-8"))).toUpperCase();
        headers.put("sign", sign);
        String result = HttpUtils.get(emergencyResourceUrl, headers);
        if (StringUtils.isBlank(result)){
            throw new BusinessException("Socket.is.TimeOut","网络不稳定...");
        }
        return JSONObject.parse(result);

    }
}
