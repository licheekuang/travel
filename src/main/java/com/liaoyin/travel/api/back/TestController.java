package com.liaoyin.travel.api.back;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.service.TravelMobileControlSystemService;
import com.liaoyin.travel.service.report.EventReportService;
import com.liaoyin.travel.util.PartyUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * TODO
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-30 16:40
 */
@Api(tags={"测试接口"})
@RestController
@RequestMapping(value = "/back/test")
public class TestController {

    @Autowired
    TravelMobileControlSystemService travelMobileControlSystemService;

    @Autowired
    EventReportService eventReportService;

    /*@ApiOperation(value = "测试")
    @GetMapping(value = "/test")
    public void tokenValidate(
            @ApiParam(value = "mp3的相对路径", required = true) @RequestParam String audioPath,
            @ApiParam(value = "文件名", required = true) @RequestParam String fileName) {
        // travelMobileControlSystemService.callTaskAdd();
        // travelMobileControlSystemService.callAttendanceAdd();
        // travelMobileControlSystemService.callStaffInfoModify();
        eventReportService.downFile(PartyUtil.getResponse(),audioPath,fileName);
        // return RestUtil.createResponse();
    }*/
}
