package com.liaoyin.travel.api.back.scenicInfo;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 应急疫情防控申报系统
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-07 14:17
 */
@Api(tags={"应急疫情防控申报管理"})
@RestController
@RequestMapping("/epidemicPrevention")
public class EpidemicPreventionController {

    @Autowired
    private UsersService usersService;
    @PostMapping("/login")
    @ResponseBody
    @ApiOperation(value = "登录", notes = "登录",response = UserInfo.class)
    public JsonRestResponse login(@ApiParam(value = "账号(手机号)", required = true) @RequestParam String phone,
                                  @ApiParam(value = "密码", required = true)  @RequestParam String password){
        return RestUtil.createResponse(this.usersService.register(phone,password));
    }
}
