package com.liaoyin.travel.api.back;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.user.ChangeManagerView;
import com.liaoyin.travel.view.user.IfThereIsAView;
import com.liaoyin.travel.vo.ChangeManagerVo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.task.Task;
import com.liaoyin.travel.entity.task.TaskDetails;
import com.liaoyin.travel.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.ParseException;
import java.util.List;

/**
 * @author Panda
 * @title: UserManagementController
 * @projectName travel
 * @description: TODO 用户管理模块
 * @date 2019/7/24 14:47
 */
@Api(tags={"用户管理"})
@RestController
@RequestMapping(value = "/usersManagement")
public class UserManagementController {

    @Autowired
    private UsersService usersService;

    /**
    　* @description: TODO 后台管理员新增一个用户或更改用户信息
    　* @param [user]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/7/24 15:17
    　*/
    @ApiOperation(value = "前端用户新增或修改", notes = "前端用户新增或修改", response = Void.class)
    @PostMapping("/insertOrUpdateUser")
    public JsonRestResponse insertOrUpdateUsers(@RequestBody Users user){
        try {
            usersService.insertOrUpdateUsers(user);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return RestUtil.createResponse();
    }

    @ApiOperation(value = "确认是否换部门负责人", notes = "如果已经存在部门负责人会给出弹窗提示", response = ChangeManagerView.class)
    @PostMapping("/notarizeChangeDepartmentManager")
    public JsonRestResponse notarizeChangeDepartmentManager(@RequestBody ChangeManagerVo changeManagerVo){
        ChangeManagerView changeManagerView = usersService.notarizeChangeDepartmentManager(changeManagerVo);
        return RestUtil.createResponse(changeManagerView);
    }


    /**
    　* @description: TODO  根据用户ID查询其详细信息
    　* @param [uid]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/7/24 15:36
    　*/
    @ApiOperation(value = "根据用户ID查询其详细信息", notes = "根据用户ID查询其详细信息", response = Users.class)
    @GetMapping("/selectUsersById")
    public JsonRestResponse selectUserById(String uid){
        Users users = usersService.selectUsersById(uid);
        return RestUtil.createResponse(users);
    }


    /**
    　* @description: TODO  根据用户ID删除用户
    　* @param [user]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/7/24 15:30
    　*/
    @ApiOperation(value = "根据用户ID删除或批量删除用户", notes = "根据用户ID删除或批量删除用户", response = void.class)
    @GetMapping("/deleteUsersById")
    public JsonRestResponse deleteUserById(String uid){
            int i = usersService.physicsDeleteUsersById(uid);
        return RestUtil.createResponse(i);
    }

    /**
    　* @description: TODO  通过Excel表批量导入数据
    　* @param [file]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/7/25 09:52
    　*/
    @ApiOperation(value = "Excel导入用户数据", notes = "Excel导入用户数据", response = boolean.class)
    @PostMapping(value = "/importColl")
    public JsonRestResponse importUsersForExcel(@ApiParam(value = "excel导入文件", required = true) @RequestParam("file") MultipartFile file){
        String fileName = file.getOriginalFilename();
        return RestUtil.createResponse(usersService.importUsersForExcel(fileName,file));
    }

    /**
    　* @description: TODO 根据用户ID重置密码
    　* @param [uid]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/7/25 12:59
    　*/
    @ApiOperation(value = "重置用户密码", notes = "重置用户密码", response = void.class)
    @GetMapping("/resetPassword")
    public JsonRestResponse resetPassword(String uid){
        int i = usersService.restPassword(uid);
        return RestUtil.createResponse(i);
    }

    /**
    　* @description: TODO 根据用户ID查询已领取的任务列表
    　* @param [uid]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/7/26 10:10
    　*/
    @ApiOperation(value = "根据用户ID查询已领取的任务列表" , notes = "根据用户ID查询已领取的任务列表", response = Task.class)
    @GetMapping("/getTaskListByUid")
    public JsonRestResponse selectTaskListByUid(
            @ApiParam(value = "用户ID", required = true) @RequestParam String uid,
            @ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
            @ApiParam(value = "任务类型") @RequestParam(required = false) String taskType,
            @ApiParam(value = "任务级别") @RequestParam(required = false) String taskLevel,
            @ApiParam(value = "任务类别") @RequestParam(required = false) String taskCategories,
            @ApiParam(value = "任务名称") @RequestParam(required = false) String taskName

            ){

        List<Task> taskListByUid = usersService.getTaskListByUid(uid,num,size,taskType,taskLevel,taskCategories,taskName);
        return RestUtil.createResponse(new PageInfo<Task>(taskListByUid));
    }

    /**
    　* @description: TODO  根据任务ID查询任务详情及路线信息
    　* @param [taskId]
    　* @return com.liaoyin.travel.base.api.JsonRestResponse
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/7/26 11:49
    　*/
    @ApiOperation(value = "根据任务ID查询任务详情及路线信息" , notes = "根据任务ID查询任务详情及路线信息", response = TaskDetails.class)
    @GetMapping("/getTaskDetailByTid")
    public JsonRestResponse selectTaskDetailByTid(String taskId){

        return RestUtil.createResponse(usersService.selectTaskDetailByTid(taskId));
    }

    @ApiOperation(value = "查询指定的【人员/部门/工种】是否存在" , notes = "根据指定类型和businessId查询指定的【人员/部门/工种】是否存在", response = IfThereIsAView.class)
    @GetMapping("/getIfThereIsAViewByIsSpecifyAndBusinessId")
    public JsonRestResponse getIfThereIsAViewByIsSpecifyAndBusinessId( @ApiParam(value = "指定类型(1:人员；2:部门；3:工种)", required = true) @RequestParam Short isSpecify,
                                                                       @ApiParam(value = "businessId", required = true) @RequestParam String businessId){
        return RestUtil.createResponse(usersService.getIfThereIsAViewByIsSpecifyAndBusinessId(isSpecify,businessId));
    }

}
