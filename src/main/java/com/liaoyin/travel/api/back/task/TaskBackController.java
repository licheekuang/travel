package com.liaoyin.travel.api.back.task;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.moble.task.TemporaryTaskManageView;
import com.liaoyin.travel.view.moble.task.TimedTaskBackView;
import com.liaoyin.travel.vo.task.SelectTaskListVo;
import com.liaoyin.travel.vo.task.SelectTimedTaskVo;
import com.liaoyin.travel.vo.task.TemporaryTaskManageVo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.task.*;
import com.liaoyin.travel.service.task.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/***@项目名：旅投
 @作者：lijing
 * @描述：controller banner
 * @日期：Created in 2019/7/16 15:49
 */
@Api(tags={"任务管理"})
@RestController
@RequestMapping(value = "/back/task")
public class TaskBackController {

	
	@Autowired
	private TaskService taskService;
	@Autowired
    private TaskExceptionDescriptionService taskExceptionDescriptionService;
	@Autowired
	private TaskReceivingRecordsService taskReceivingRecordsService;
    @Autowired
    private TimingTaskParameterService timingTaskParameterService;
    @Autowired
    private TimingTaskSpecificReleaseTimeService timingTaskSpecificReleaseTimeService;

    @PostMapping("/selectTaskBackList")
    @ResponseBody
    @ApiOperation(value = "查询任务列表", notes = "查询任务列表",response = Task.class)
    public JsonRestResponse selectTaskBackList(
            @RequestBody SelectTaskListVo selectTaskListVo){

        return RestUtil.createResponse(new PageInfo<>(this.taskService.selectTaskList(selectTaskListVo)));
    }

    @PostMapping("/selectTimedTaskBackList")
    @ResponseBody
    @ApiOperation(value = "后台查询定时任务参数列表", notes = "后台查询定时任务参数列表",response = TimedTaskBackView.class)
    public JsonRestResponse selectTimedTaskBackList(
            @RequestBody SelectTimedTaskVo selectTimedTaskVo){
        PageInfo<TimedTaskBackView> pageInfo = this.timingTaskParameterService.selectTimedTaskBackList(selectTimedTaskVo);
        return RestUtil.createResponse(pageInfo);
    }

    @PostMapping("/selectTemporaryTaskManageByCondition")
    @ResponseBody
    @ApiOperation(value = "查询临时任务管理列表(已废弃)", notes = "(已废弃)后台&管理端-按条件查询临时任务管理列表",response = TemporaryTaskManageView.class)
    public JsonRestResponse selectTemporaryTaskManageByCondition(@RequestBody TemporaryTaskManageVo temporaryTaskManageVo){
        PageInfo<TemporaryTaskManageView> pageInfo = this.taskService.selectTemporaryTaskManageByCondition(temporaryTaskManageVo);
        return RestUtil.createResponse(pageInfo);
    }

    @GetMapping("/selectTimedTaskById")
    @ResponseBody
    @ApiOperation(value = "后台-根据id查询定时任务详情", notes = "后台-根据id查询任务详情",response = TimedTaskBackView.class)
    public JsonRestResponse selectTimedTaskById(@ApiParam(value = "定时任务参数id", required = true) @RequestParam(required = true) String timedTaskId){
        TimedTaskBackView timedTaskBackView = this.timingTaskParameterService.selectTimedTaskById(timedTaskId);
        return RestUtil.createResponse(timedTaskBackView);
    }

    @PostMapping("/selectTimingTaskSpecificReleaseTimeByTimedTaskId")
    @ResponseBody
    @ApiOperation(value = "后台查询定时任务具体发布时间记录列表", notes = "后台-查询定时任务具体发布时间记录",response = TimingTaskSpecificReleaseTime.class)
    public JsonRestResponse selectTimingTaskSpecificReleaseTimeByTimedTaskId(
            @ApiParam(value = "定时任务参数id", required = true) @RequestParam(required = true) String timedTaskId,
            @ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
            @ApiParam(value = "页大小", required = true) @RequestParam(required = true) Integer size){
        PageInfo<TimingTaskSpecificReleaseTime> pageInfo =
                this.timingTaskSpecificReleaseTimeService.selectTimingTaskSpecificReleaseTimeByTimedTaskId(timedTaskId,num,size);
        return RestUtil.createResponse(pageInfo);
    }

    @GetMapping("/selectTaskByIdOnBack")
    @ResponseBody
    @ApiOperation(value = "根据id查询任务详情-后台", notes = "根据id查询任务详情-后台",response = Task.class)
    public JsonRestResponse selectTaskByIdOnBack(
    		 @ApiParam(value = "任务id", required = true) @RequestParam(required = true) String id){
    	
        return RestUtil.createResponse(this.taskService.selectTaskByIdOnBack(id));
    }
    
    
    @GetMapping("/deleteTaskByIdOnBack")
    @ResponseBody
    @ApiOperation(value = "根据id删除任务-后台", notes = "根据id删除任务-后台",response = Void.class)
    public JsonRestResponse deleteTaskByIdOnBack(
    		 @ApiParam(value = "任务ids", required = true) @RequestParam String ids){
    	
    	this.taskService.deleteTaskByIdOnBack(ids);
        return RestUtil.createResponse();
    }

    @GetMapping("/revokeTaskById")
    @ResponseBody
    @ApiOperation(value = "根据id撤销任务", notes = "移动端-根据id撤销任务",response = Integer.class)
    public JsonRestResponse revokeTaskById(
            @ApiParam(value = "任务id", required = true) @RequestParam String id){

        int result = this.taskService.revokeTaskById(id);
        return RestUtil.createResponse(result);
    }

    @GetMapping("/deleteTimingTaskParameterByIdOnBack")
    @ResponseBody
    @ApiOperation(value = "根据id删除定时任务-后台", notes = "根据id删除定时任务-后台",response = TimingTaskParameter.class)
    public JsonRestResponse deleteTimingTaskParameterByIdOnBack(
            @ApiParam(value = "任务ids", required = true) @RequestParam(required = true) String ids){

        int result = this.taskService.deleteTimingTaskParameterByIdOnBack(ids);
        return RestUtil.createResponse(result);
    }

    @GetMapping("/selectExceptionTaskList")
    @ResponseBody
    @ApiOperation(value = "查询异常任务列表", notes = "查询异常任务列表-后台",response = TaskExceptionDescription.class)
    public JsonRestResponse selectExceptionTaskList(
            @ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
            @ApiParam(value = "异常任务状态")@RequestParam(required = false)Integer exceptionStatus,
            @ApiParam(value = "任务名称")@RequestParam(required = false)String taskName,
            @ApiParam(value = "查询类型【1：前端 2：后台】")@RequestParam(required = false)String qurtyType

    ){
        PageInfo<TaskExceptionDescription> pageInfo = this.taskExceptionDescriptionService.selectNewExceptionTaskList(num,size,exceptionStatus,qurtyType,taskName);
        return RestUtil.createResponse(pageInfo);
    }

    @GetMapping("/updateTimedTaskByIdAndEnable")
    @ResponseBody
    @ApiOperation(value = "更新定时任务启用状态", notes = "更新定时任务启用状态-后台",response = void.class)
    public JsonRestResponse updateTimedTaskByIdAndEnable(
            @ApiParam(value = "定时任务参数id" , required = true)@RequestParam(required = true)String timedTaskId,
            @ApiParam(value = "启用状态(0:不启用;1:启用)" , required = true)@RequestParam(required = true)String enabled
    ){

        this.timingTaskParameterService.updateTimedTaskByIdAndEnable(timedTaskId,enabled);
        return RestUtil.createResponse();
    }

    @GetMapping("/updateExceptionTask")
    @ResponseBody
    @ApiOperation(value = "更新异常任务状态", notes = "更新异常任务状态-后台",response = TimingTaskParameter.class)
    public JsonRestResponse updateExceptionTask(
            @ApiParam(value = "异常任务id" , required = true) @RequestParam String exceptionId,
            @ApiParam(value = "更新异常任务的状态" , required = true) @RequestParam Integer exceptionStatus,
            @ApiParam(value = "更新异常任务的处理意见" , required = true) @RequestParam String exeptionContext,
            @ApiParam(value = "任务id" , required = true) @RequestParam String taskId,
            @ApiParam(value = "申请用户ID" , required = true) @RequestParam String userId
    ){
        
    	this.taskExceptionDescriptionService.updateExceptionTask(exceptionId, exceptionStatus, exeptionContext, taskId, userId);
        return RestUtil.createResponse();
    }

    @GetMapping("/selectExceptionTaskById")
    @ResponseBody
    @ApiOperation(value = "查询异常任务详情", notes = "查询异常任务详情",response = TaskExceptionDescription.class)
    public JsonRestResponse selectExceptionTaskById(
            @ApiParam(value = "异常任务ID" , required = true)@RequestParam String exceptionTaskId
    ){
        return RestUtil.createResponse(this.taskExceptionDescriptionService.selectExceptionTaskById(exceptionTaskId));
    }

    @PostMapping("/deleteExceptionTaskByIds")
    @ResponseBody
    @ApiOperation(value = "删除或批量删除异常任务", notes = "删除或批量删除异常任务",response = TaskExceptionDescription.class)
    public JsonRestResponse deleteExceptionTaskByIds(
            @ApiParam(value = "异常任务ids多个用英文逗号隔开", required = true)@RequestParam(required = true)String ids
    ){
        return RestUtil.createResponse(this.taskExceptionDescriptionService.deleteTaskExceptionDescription(ids));
    }
    
    
    @PostMapping("/selectTaskReceivingRecordsByTaskId")
    @ResponseBody
    @ApiOperation(value = "查询任务领取列表", notes = "查询任务领取列表",response = TaskReceivingRecords.class)
    public JsonRestResponse selectTaskReceivingRecordsByTaskId(
    		@RequestBody SelectTaskListVo selectTaskListVo
    ){
        return RestUtil.createResponse(new PageInfo<>(this.taskReceivingRecordsService.selectTaskReceivingRecordsByTaskIdBack(selectTaskListVo)));
    }
    
    
    @GetMapping("/selectTaskReceivingRecordsdetailsById")
    @ResponseBody
    @ApiOperation(value = "查询任务领取详情", notes = "查询任务领取详情",response = TaskReceivingRecords.class)
    public JsonRestResponse selectTaskReceivingRecordsdetailsById(
    		 @ApiParam(value = "id" , required = true)@RequestParam(required = true)String id
    ){
    	
        return RestUtil.createResponse(this.taskReceivingRecordsService.selectTaskReceivingRecordsdetailsById(id));
    }
    
    
    
}