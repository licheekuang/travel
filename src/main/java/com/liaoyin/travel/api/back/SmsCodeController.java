package com.liaoyin.travel.api.back;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.service.SmsCodeService;

@Api(tags={"验证码管理"})
@RestController
@RequestMapping(value = "/smsCode")
public class SmsCodeController {

    @Autowired
    private SmsCodeService smsCodeService;

//    @ApiOperation(value = "查询单条用户信息", notes = "查询单条用户信息", response = SUsers.class)
//    @PostMapping(value = "/selectSingleUser")
//    @ResponseBody
//    public JsonRestResponse selectSingleUser(SUsers user) {
//        user = userService.selectSingleUser(user.getId());
//        return  RestUtil.createResponse(user);
//    }
//
//    @ApiOperation(value = "修改用户", notes = "修改用户", response = SUsers.class)
//    @PostMapping(value = "/updateUser")
//    @ResponseBody
//    public JsonRestResponse updateUser(@RequestBody SUsers user) {
//        Integer result = userService.updateUser(user);
//        JsonRestResponse resp = new JsonRestResponse();
//        if(result!=1){
//            resp.setCode("error");
//            resp.setDesc("用户帐号（手机号）重复");
//        }else{
//            resp = RestUtil.createResponse();
//        }
//        return resp;
//    }
//
//    @ApiOperation(value = "删除用户", notes = "删除用户", response = SUsers.class)
//    @PostMapping(value = "/deleteUser")
//    @ResponseBody
//    public JsonRestResponse deleteUser(SUsers user)  {
//        Map<String, Object> map = new HashMap<>();
//        JsonRestResponse resp = new JsonRestResponse();
//        user.setDelFlag("1");
//        map = userService.deleteUser(user);
//        if("success".equals(map.get("code"))){
//            resp = RestUtil.createResponse();
//        }else{
//            resp.setCode(map.get("code").toString());
//            resp.setDesc(map.get("msg").toString());
//        }
//        return resp;
//    }

    @ApiOperation(value = "生成验证码", notes = "生成验证码", response = Users.class)
    @PostMapping(value = "/insertSmsCode")
    public JsonRestResponse insertSmsCode(@ApiParam(value = "手机号", required = true) @RequestParam(required = true) String mobilePhone,
                                          @ApiParam(value = "功能code：注册（yhzc），登录（yhdl），忘记密码（wjmm）", required = true) @RequestParam(required = true) String functionCode) {
        smsCodeService.insertSmsCode(mobilePhone, functionCode);
        return RestUtil.createResponse();
    }
//
//    @ApiOperation(value = "列表查询用户", notes = "列表查询用户（组织ID必填）", response = SUsers.class)
//    @PostMapping(value = "/selectUserByPage")
//    @ResponseBody
//    public JsonRestResponse selectUserByPage(SUsers user) {
//        PageInfo<SUsers> p = new PageInfo<>(this.userService.selectUserByPage(user));
//        return RestUtil.createResponse(p);
//    }
//    //用户密码登录auth
//    @Loggable(describe = "登录系统", optType = LogConstant.SELECT, module = "用户管理")
//    @ApiOperation(value = "用户密码登录auth", notes = "用户密码登录auth", response = SUsers.class)
//    @PostMapping("login")
//    public JsonRestResponse login(SUsers user, HttpServletRequest request ) {
//        Integer checkResult = userService.checkMobileCode(user.getMobilePhone(), user.getValidCode());
//        JsonRestResponse resp = new JsonRestResponse();
//        if(checkResult==3) {
//            resp.setCode("error ");
//            resp.setDesc("验证码错误");
//            return resp;
//        }else if(checkResult!=1) {
//            resp.setCode("error ");
//            resp.setDesc("验证码失效");
//            return resp;
//        }
//        SUsers result = userService.login(user);
//        if(result!=null) {
//            //生成token
//            String token = jwtTokenUtil.generateToken(user.getAccount());
//            result.setToken(token);
//            //将用户信息保存到redis
//            RedisUtil.set(token, result, expiration);
//            resp = RestUtil.createResponse(result);
//        }else{
//            resp.setCode("user is error");
//            resp.setDesc("登录失败");
//        }
//        return resp;
//    }
//    @GetMapping(value = "/tokenValidate")
//    public JsonRestResponse tokenValidate() {
//
//        return RestUtil.createResponse();
//    }
//    @ApiOperation(value = "退出登录", notes = "退出登录", response = Void.class)
//    @PostMapping("/logout")
//    public JsonRestResponse logout(HttpServletRequest request){
//        String token = PartyUtil.getCurrentToken();
//        RedisUtil.del(token);
//        return RestUtil.createResponse();
//    }
//    @ApiOperation(value = "验证码发送", notes = "验证码发送", response = Void.class)
//    @PostMapping("/sendMobileCode")
//    public JsonRestResponse sendMobileCode(String phoneNumber){
//        HashMap<String, Object> map = userService.sendMobileCode(phoneNumber, smsOnOff, smsKey, smsPassword);
//        JsonRestResponse resp = new JsonRestResponse();
//        if("success".equals(map.get("code").toString())) {
//            return RestUtil.createResponse(1);
//        }else{
//            resp.setCode("error");
//            resp.setDesc(map.get("msg").toString());
//            return resp;
//        }
//    }
//    @ApiOperation(value = "验证码验证", notes = "验证码验证", response = Void.class)
//    @PostMapping("/checkMobileCode")
//    public JsonRestResponse checkMobileCode(String mobilePhone, String validCode){
//        Integer result = userService.checkMobileCode(mobilePhone, validCode);
//        JsonRestResponse resp = new JsonRestResponse();
//        if(result==1) {
//            return RestUtil.createResponse(1);
//        }else{
//            resp.setCode("error ");
//            resp.setDesc("验证码验证失败");
//            return resp;
//        }
//    }
//    @Loggable(describe = "忘记密码", optType = LogConstant.UPDATE, module = "用户管理")
//    @ApiOperation(value = "忘记密码", notes = "忘记密码", response = SUsers.class)
//    @PostMapping("/updateResetPwd")
//    public JsonRestResponse updateResetPwd(@ApiParam(value = "账户", required = true) @RequestParam(required = true) String account,
//                                           @ApiParam(value = "手机号", required = true) @RequestParam(required = true) String mobilePhone,
//                                           @ApiParam(value = "验证码", required = true) @RequestParam(required = true) String smsCheckCode,
//                                           @ApiParam(value = "新密码", required = true) @RequestParam(required = true) String newPassword) {
//
//
//        Integer result = userService.resetPwd(account, mobilePhone, smsCheckCode, newPassword);
//        JsonRestResponse resp = new JsonRestResponse();
//        if(result==0) {
//            String token = PartyUtil.getCurrentToken();
//            RedisUtil.del(token);
//            resp = RestUtil.createResponse(result);
//        }else if(result == 1){
//            resp.setCode("user is error");
//            resp.setDesc("登录账户或者手机号错误");
//        }else if(result == 2){
//            resp.setCode("validCheck is error");
//            resp.setDesc("验证码错误");
//        }
//        return resp;
//    }
//    @Loggable(describe = "重置密码", optType = LogConstant.UPDATE, module = "用户管理")
//    @ApiOperation(value = "重置密码", notes = "重置密码", response = SUsers.class)
//    @PostMapping("/updateResetUserPwd")
//    public JsonRestResponse updateResetUserPwd(@ApiParam(value = "用户ID", required = true) @RequestParam(required = true) String id) {
//
//
//        HashMap<String, Object> map = userService.updateResetUserPwd(id, smsOnOff, smsKey, smsPassword);
//        JsonRestResponse resp = new JsonRestResponse();
//        if(map.get("code").equals("success")) {
//            resp = RestUtil.createResponse();
//        }else{
//            resp.setCode(map.get("code").toString());
//            resp.setDesc(map.get("msg").toString());
//        }
//        return resp;
//    }
//    @Loggable(describe = "修改密码", optType = LogConstant.UPDATE, module = "用户管理")
//    @ApiOperation(value = "修改密码", notes = "修改密码", response = SUsers.class)
//    @PostMapping("/updatePwd")
//    public JsonRestResponse updatePwd(@ApiParam(value = "原密码", required = true) @RequestParam(required = true) String oldPassword,
//                                      @ApiParam(value = "新密码", required = true) @RequestParam(required = true) String password,
//                                      @ApiParam(value = "账户名", required = true) @RequestParam(required = true) String account) {
//        SUsers sUsers = new SUsers();
//        sUsers.setPassword(password);
//        sUsers.setOldPassword(oldPassword);
//        sUsers.setAccount(account);
//        Integer result = userService.updatePwd(sUsers);
//        JsonRestResponse resp = new JsonRestResponse();
//        if(result==1) {
//            String token = PartyUtil.getCurrentToken();
//            RedisUtil.del(token);
//            resp = RestUtil.createResponse(result);
//        }else if(result==2){
//            resp.setCode("password is error");
//            resp.setDesc("原密码错误");
//        }else{
//            resp.setCode("update password failure");
//            resp.setDesc("密码修改失败");
//        }
//        return resp;
//    }
}