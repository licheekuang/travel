package com.liaoyin.travel.api.back;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.common.rest.BaseController;
import com.liaoyin.travel.entity.DictBasic;
import com.liaoyin.travel.service.DictBasicService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

@Api(tags={"字典项管理"})
@RestController
@RequestMapping("/dictBasic")
public class DictBasicController extends BaseController<DictBasicService, DictBasic> {
	
	/**
	 * 
	 * 方   法  名：selectDictBasic
	 * 创建日期：2017年8月31日 下午2:28:02
	 * 创   建  者：zhou.ning
	 * 描          述：多基本字典编码表查询
	 * @param code
	 * @return
	 */
	@ApiOperation(value = "多基本字典编码表查询", notes = "多基本字典编码表查询", response = DictBasic.class)
	@PostMapping("/selectDictBasic")
	public JsonRestResponse selectDictBasic(
			@ApiParam(name = "code", value = "字典类型编码（多个以逗号分隔）", required = false) @RequestParam String code) {
		
		String[] strings = code.split(",");
		
		return RestUtil.createResponse(this.baseService.selectDictBasics(strings));
	}
	
	/**
	 * 
	 * 方   法  名：selectDictBasicsWithGroup
	 * 创建日期：2017年9月12日 下午5:41:53
	 * 创   建  者：zhou.ning
	 * 描          述：根据字典code和分组code查询
	 * @param code
	 * @return
	 */
	@ApiOperation(value = "根据字典code和分组code查询", notes = "根据字典code和分组code查询", response = DictBasic.class)
	@GetMapping("/selectDictBasicsWithGroup")
	public JsonRestResponse selectDictBasicsWithGroup(
			@ApiParam(name = "code", value = "字典类型编码【格式：字典code|组code,字典code|组code】（多个以逗号分隔）", required = false) @RequestParam String code) {
		
		String[] strings = code.split(",");
		return RestUtil.createResponse(this.baseService.selectDictBasicsWithGroup(strings));
	}


	/**
	 *
	 * 方 法 名：selectAllDiceByCode
	 * 创建日期：2017/9/1 10:18
	 * 创 建 者：du.juncheng
	 * 描    述：通过code查询所有字典(构建树)
	 * @return
	 */
	@ApiOperation(value = "通过code查询所有字典(构建树)", notes = "通过code查询所有字典(构建树)")
	@GetMapping("/selectAllDiceByCodeBuildTree")
	public JsonRestResponse selectAllDiceByCodeBuildTree(@ApiParam(name = "code", value = "字典类型编码") @RequestParam String code){
		return RestUtil.createResponse(this.baseService.selectAllDiceByCodeBuildTree(code));
	}
	/**
	 * 
	 * 方   法  名：selectByCodeAndGroupcodeBuildTree
	 * 创建日期：2017年11月7日 下午9:18:12
	 * 创   建  者：zhou.ning
	 * 描          述：通过code和groupCode查询所有字典列表(构建树)
	 * @param code
	 * @return
	 */
	@ApiOperation(value = "通过code和groupCode查询所有字典列表(构建树)", notes = "通过code和groupCode查询所有字典列表(构建树)", response = DictBasic.class)
	@GetMapping("/selectByCodeAndGroupcodeBuildTree")
	public JsonRestResponse selectByCodeAndGroupcodeBuildTree(@ApiParam(name = "code", value = "字典类型编码") @RequestParam String code){
		return RestUtil.createResponse(this.baseService.selectByCodeAndGroupcodeBuildTree(code));
	}
	
	
	/**
	 * 
	 * 方   法  名：selectDictBasicList
	 * 创建日期：2018年08月1日 下午9:18:12
	 * 创   建  者：lijing
	 * 描          述：查询字典列表
	 * @param 
	 * @return
	 */
	@ApiOperation(value = "查询字典列表", notes = "查询字典列表", response = DictBasic.class)
	@GetMapping("/selectDictBasicList")
	public JsonRestResponse selectDictBasicList(
			 @ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
             @ApiParam(value = "每页条数", required = true) @RequestParam(required = true) Integer size,
             @ApiParam(value = "字典类型编码", required = false) @RequestParam(required = false) String code,
             @ApiParam(value = "是否禁用【0是1否】", required = false) @RequestParam(required = false) String isUsing){
		return RestUtil.createResponse(new PageInfo<>(this.baseService.selectDictBasicList(num, size, code, isUsing)));
	}
	
	/**
	 * 
	 * 方   法  名：insertOrUpdateDictBasic
	 * 创建日期：2018年08月1日 下午9:18:12
	 * 创   建  者：lijing
	 * 描          述：新增或修改
	 * @param 
	 * @return
	 */
	@ApiOperation(value = "新增或修改", notes = "新增或修改", response = Void.class)
	@PostMapping("/insertOrUpdateDictBasic")
	public JsonRestResponse insertOrUpdateDictBasic(@RequestBody DictBasic dictBasic ){
		this.baseService.insertOrUpdateDictBasic(dictBasic);
		return RestUtil.createResponse();
	}
	
	/**
	 * 
	 * 方   法  名：selectDictBasicById
	 * 创建日期：2018年08月1日 下午9:18:12
	 * 创   建  者：lijing
	 * 描          述：根据id查询
	 * @param 
	 * @return
	 */
	@ApiOperation(value = "根据id查询", notes = "根据id查询", response = DictBasic.class)
	@GetMapping("/selectDictBasicById")
	public JsonRestResponse selectDictBasicById( 
			 @ApiParam(value = "id", required = true) @RequestParam(required = true) String id){
		
		return RestUtil.createResponse(this.baseService.selectDictBasicById(id));
	}
	
	
	/**
	 *
	 * 方 法 名：insertDictBasic
	 * 创建日期：2018年08月1日 下午9:18:12
	 * 创 建 者：li.jing
	 * 描    述：生成字典值
	 * @return 
	 */
	@ApiOperation(value = "生成字典值", notes = "生成字典值", response = Void.class)
	@GetMapping("/selectDictBasicMAXfuncValue")
	public JsonRestResponse selectDictBasicMAXfuncValue( 
			@ApiParam(name = "id",value = "字典ID") @RequestParam(required = false) String id,
			@ApiParam(name = "code",value = "字典code") @RequestParam(required = true) String code){		
		return RestUtil.createResponse(this.baseService.selectDictBasicMAXfuncValue(id,code));
	}

}
