package com.liaoyin.travel.api.back;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.Banner;
import com.liaoyin.travel.service.BannerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/***@项目名：
 @作者：zhou.ning
 * @描述：controller banner
 * @日期：Created in 2018/6/8 15:49
 */
@Api(tags={"轮播图片"})
@RestController
@RequestMapping(value = "/banner")
public class BannerController {

	@Autowired
	private BannerService bannerService;
	
	 @ApiOperation(value = "查询banner列表", notes = "查询banner列表", response = Banner.class)
	 @GetMapping("/selectBannerList")
	 public JsonRestResponse selectBannerList(
			 @ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
             @ApiParam(value = "每页条数", required = true) @RequestParam(required = true) Integer size,
             @ApiParam(value = "名称", required = false) @RequestParam(required = false) String bannertype,
             @ApiParam(value = "描述", required = false) @RequestParam(required = false) String bannerDesc) {
		
		return RestUtil.createResponse(new PageInfo<>(this.bannerService.selectBannerList(num, size, bannertype,bannerDesc)));
	}
	 
	 @ApiOperation(value = "根据id查询banner", notes = "根据id查询banner", response = Banner.class)
	 @GetMapping("/selectBannerById")
	 public JsonRestResponse selectBannerById(
			 @ApiParam(value = "id", required = true) @RequestParam(required = true) String id) {
		
		return RestUtil.createResponse(this.bannerService.selectBannerById(id));
	}
	 
	 @ApiOperation(value = "新增或修改banner", notes = "新增或修改banner", response = Void.class)
	 @PostMapping("/insertOrUpdateBanner")
	 public JsonRestResponse insertOrUpdateBanner(
			@RequestBody Banner banner) {
		 this.bannerService.insertOrUpdateBanner(banner);
		return RestUtil.createResponse();
	}
	 
	 @ApiOperation(value = "根据id删除banner", notes = "根据id删除banner", response = Void.class)
	 @GetMapping("/deleteBannerByIds")
	 public JsonRestResponse deleteBannerByIds(
			 @ApiParam(value = "ids", required = true) @RequestParam(required = true) String ids) {
		this.bannerService.deleteBannerByIds(ids);
		return RestUtil.createResponse();
	}
}