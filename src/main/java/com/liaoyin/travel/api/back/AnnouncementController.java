package com.liaoyin.travel.api.back;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.vo.announce.AnnouncementVO;
import com.liaoyin.travel.vo.request.AnnounceAddRequestVO;
import com.liaoyin.travel.vo.request.AnnounceSearchRequestVO;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.Announcement;
import com.liaoyin.travel.service.AnnouncementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author privatePanda@163.com
 * @title: AnnouncementController
 * @projectName travel
 * @description: TODO 发布公告、通知的Controller
 * @date 2019/9/24 15:30
 */
@RestController
@Api(tags={"向所有用户发布公告、通知"})
@RequestMapping("/back/announcement")
public class AnnouncementController {

    @Autowired
    private AnnouncementService announcementService;



    @PostMapping("/insertOrUpdateAnnouncement")
    @ResponseBody
    @ApiOperation(value = "新增或修改", notes = "新增或修改",response = void.class)
    public JsonRestResponse insertOrUpdateAnnouncement(
            @RequestBody AnnounceAddRequestVO announcement
    ){
        int i = this.announcementService.insertOrUpdateAnnouncement(announcement);
        return RestUtil.createResponse(i);
    }

    @PostMapping("/deleteAnnouncement")
    @ResponseBody
    @ApiOperation(value = "根据ids删除公告", notes = "根据ids删除公告",response = void.class)
    public JsonRestResponse deleteAnnouncement(
            @ApiParam(value = "ids 用逗号隔开" , required = true)@RequestParam String ids){
        return RestUtil.createResponse(this.announcementService.deleteAnnouncement(ids));
    }

    //查
    @GetMapping("/selectAnnouncementList")
    @ResponseBody
    @ApiOperation(value = "条件查询通知、公告", notes = "条件查询通知、公告",response = Announcement.class)
    public JsonRestResponse selectAnnouncementList(
            @ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
            @ApiParam(value = "标题")@RequestParam(required = false) String title,
            @ApiParam(value = "内容" )@RequestParam(required = false) String content,
            @ApiParam(value = "发布时间")@RequestParam(required = false) String time,
            @ApiParam(value = "发布人昵称")@RequestParam(required = false) String userName
    ){
        List<Announcement> announcements = this.announcementService.selectAnnouncementList(num, size, title, content, time);
        return RestUtil.createResponse(new PageInfo<>(announcements));
    }

    @GetMapping("/selectAnnouncementByPrimaryKey")
    @ResponseBody
    @ApiOperation(value = "查询通知、公告详情", notes = "查询通知、公告详情",response = AnnouncementVO.class)
    public JsonRestResponse selectAnnouncementByPrimaryKey(
            @ApiParam(value = "通知、公告id" , required = true)@RequestParam String id)
    {
        return RestUtil.createResponse(this.announcementService.selectAnnouncementByPrimaryKey(id));
    }

    @PostMapping("/searchAnnounceList")
    @ResponseBody
    @ApiOperation(value = "搜索公告列表", notes = "搜索公告列表",response = Announcement.class)
    public JsonRestResponse searchAnnounceList(AnnounceSearchRequestVO requestVO){
        return RestUtil.createResponse(this.announcementService.searchAnnounceList(requestVO));
    }
}
