package com.liaoyin.travel.api.back;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.vo.UserPassWordVo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.BackUser;
import com.liaoyin.travel.service.back.BackUserService;

import com.liaoyin.travel.vo.scenic.ScenicBackUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags={"后台用户管理"})
@RequestMapping("/back/backUser")
public class BackUserController {

    @Autowired
	private BackUserService backUserService;
    
    
    @ApiOperation(value = "查询后台用户列表", notes = "查询后台用户列表", response = BackUser.class)
	@GetMapping("/selectBackUserList")
	 public JsonRestResponse selectBackUserList(
			@ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
            @ApiParam(value = "用户类型（1-注册用户；2-商家；3-一般管理员；4-超级管理员）") @RequestParam(required = false) String userType,
            @ApiParam(value = "名称") @RequestParam(required = false) String nickName,
            @ApiParam(value = "是否启用【0是1否】") @RequestParam(required = false) String isUsing) {

		return RestUtil.createResponse(new PageInfo<>(this.backUserService.selectBackUserList(num, size, userType, nickName, isUsing)));
	}
    
    
    /**
     * @method: selectCollectionById
     * @Description: 
     * @param: [id]
     * @return:com.liaoyin.taisiya.response.JsonRestResponse
     * @author: lijing
     * @Date: 2018/5/21 11:41
     **/
    @GetMapping("/selectBackUserById")
    @ResponseBody
    @ApiOperation(value = "", notes = "",response = BackUser.class)
    public JsonRestResponse selectBackUserById(
    		@ApiParam(name = "id", value = "")@RequestParam String id){
       
    	return RestUtil.createResponse(this.backUserService.selectBackUserById(id));
    }
    /**
     * @method: insertOrUpdateBackUser
     * @Description: 新增或修改
     * @param: [id]
     * @return:com.liaoyin.taisiya.response.JsonRestResponse
     * @author: lijing
     * @Date: 2018/5/21 11:41
     **/
    @PostMapping("/insertOrUpdateBackUser")
    @ResponseBody
    @ApiOperation(value = "新增或修改", notes = "新增或修改",response = Void.class)
    public JsonRestResponse insertOrUpdateBackUser(@RequestBody BackUser backUser){
    	this.backUserService.insertOrUpdateBackUser(backUser);
    	return RestUtil.createResponse();
    }

    @PostMapping("/insertOrUpdateScenicBackUser")
    @ResponseBody
    @ApiOperation(value = "新增或修改单位管理员", notes = "单位总管理员新增或修改单位(景区或机构)子管理员",response = Integer.class)
    public JsonRestResponse insertOrUpdateScenicBackUser(@RequestBody ScenicBackUserVo scenicBackUserVo){
        int result = this.backUserService.insertOrUpdateScenicBackUser(scenicBackUserVo);
        return RestUtil.createResponse(result);
    }
   
    @GetMapping("/deleteBackUserById")
    @ResponseBody
    @ApiOperation(value = "删除后台用户", notes = "删除后台用户",response = Void.class)
    public JsonRestResponse deleteBackUserById(
    		@ApiParam(name = "ids", value = "")@RequestParam(required = true) String ids){
    	this.backUserService.deleteBackUserById(ids);
    	return RestUtil.createResponse();
    }
    
    @PostMapping("/updateUserPassWordByIdAll")
    @ResponseBody
    @ApiOperation(value = "后台用户修改密码", notes = "后台用户修改密码",response = Integer.class)
    public JsonRestResponse updateUserPassWordByIdAll(
    		@RequestBody UserPassWordVo userPassWordVo){
    	
    	return RestUtil.createResponse(this.backUserService.updateUserPassWordByIdAll(userPassWordVo));
    }
    
    @GetMapping("/selectThisByUserName")
    @ResponseBody
    @ApiOperation(value = "根据account后台用户", notes = "根据account后台用户",response = Integer.class)
    public JsonRestResponse selectThisByUserName(
    		@ApiParam(name = "account", value = "账户",required = true)@RequestParam(required = true) String account){
    	
    	return RestUtil.createResponse(this.backUserService.selectThisByUserName(account));
    }
    
    @PostMapping("/logout")
    @ResponseBody
    @ApiOperation(value = "退出", notes = "退出",response = Void.class)
    public JsonRestResponse logout(){
    	this.backUserService.logout();
    	return RestUtil.createResponse();
    }
    
    @PostMapping("/resetPassword")
    @ResponseBody
    @ApiOperation(value = "重置密码", notes = "重置密码",response = Void.class)
    public JsonRestResponse resetPassword(
    		@ApiParam(name = "id", value = "id")@RequestParam(required = true) String id){
    	this.backUserService.resetPassword(id);
    	return RestUtil.createResponse();
    }
}
