package com.liaoyin.travel.api.back.attendance;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.entity.attendance.PunchInRecord;
import com.liaoyin.travel.view.mine.attendance.AttendanceManagementView;
import com.liaoyin.travel.vo.attendance.AttanceRecordVO;
import com.liaoyin.travel.vo.attendance.AttendanceManagementVo;
import com.liaoyin.travel.vo.attendance.AttendanceStatVO;
import com.liaoyin.travel.vo.attendance.InsertOrUpdateAttendanceManagementVo;
import com.liaoyin.travel.vo.request.AttendanceRecordRequestVO;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.attendance.AttendanceManagement;
import com.liaoyin.travel.service.attendance.AttendanceRecordService;
import com.liaoyin.travel.service.attendance.ManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 考勤管理后台接口
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-18 14:19
 */
@RestController
@Api(tags={"后台-考勤管理"})
@RequestMapping("/back/attendanceManagement")
public class AttendanceManagementController {

    @Autowired
    private ManagementService managementService;

    @Autowired
    private AttendanceRecordService attendanceRecordService;

    /**
     * @方法名：insertOrUpdateAttendanceManagement
     * @描述： 新增或修改考勤管理
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/18 14:24
     */
    @ApiOperation(value = "新增或修改考勤管理", notes = "新增或更新考勤管理", response= InsertOrUpdateAttendanceManagementVo.class)
    @PostMapping(value = "/insertOrUpdateAttendanceManagement")
    public JsonRestResponse insertOrUpdateAttendanceManagement(@RequestBody InsertOrUpdateAttendanceManagementVo insertOrUpdateAttendanceManagementVo){
        int result = this.managementService.insertOrUpdateAttendanceManagement(insertOrUpdateAttendanceManagementVo);
        return RestUtil.createResponse(result);
    }

    /**
     * @方法名：deleteAttendanceManagement
     * @描述： 根据ids字符串批量(物理)删除考勤管理
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/18 18:07
     */
    @ApiOperation(value = "删除考勤管理", notes = "根据ids字符串批量(物理)删除考勤管理", response= AttendanceManagement.class)
    @PostMapping(value = "/deleteAttendanceManagement")
    public JsonRestResponse deleteAttendanceManagement(@ApiParam(value = "要删除的id集合", required = true) @RequestParam String ids){
        int result = this.managementService.deleteAttendanceManagementByIds(ids);
        return RestUtil.createResponse(result);
    }

    /**
     * @方法名：selectAttendanceManagementViewByCondition
     * @描述： 后台-按条件查询考勤管理(带分页)
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/18 18:07
     */
    @ResponseBody
    @ApiOperation(value = "后台-按条件查询考勤管理(带分页)",notes = "后台-按条件查询考勤管理(带分页)",response= AttendanceManagementView.class)
    @PostMapping(value = "/selectAttendanceManagementViewByCondition" )
    public JsonRestResponse selectAttendanceManagementViewByCondition(@RequestBody AttendanceManagementVo attendanceManagementVo)
    {
        PageInfo<AttendanceManagementView> pageInfo = this.managementService.selectAttendanceManagementViewByCondition(attendanceManagementVo);
        return RestUtil.createResponse(pageInfo);
    }

    /**
     * @方法名：selectAttendanceManagementById
     * @描述： 后台-根据id查询考勤管理详情
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/18 19:27
     */
    @ApiOperation(value = "后台-查询考勤管理详情",notes = "后台-根据id查询考勤管理详情",response= AttendanceManagement.class)
    @GetMapping(value = "/selectAttendanceManagementById")
    public JsonRestResponse selectAttendanceManagementById(@ApiParam(value = "考勤管理的id", required = true) @RequestParam String id)
    {
        AttendanceManagement attendanceManagement = this.managementService.selectAttendanceManagementById(id);
        return RestUtil.createResponse(attendanceManagement);
    }

    @PostMapping("/getAttanceRecordList")
    @ApiOperation(value = "考勤记录", notes = "考勤记录",response = AttanceRecordVO.class)
    public JsonRestResponse getAttanceRecordList(AttendanceRecordRequestVO attendanceRecordRequestVO){
        return RestUtil.createResponse(this.attendanceRecordService.getAttanceRecordList(attendanceRecordRequestVO));
    }

    @PostMapping("/getAttendanceStatList")
    @ApiOperation(value = "考勤统计", notes = "考勤统计",response = AttendanceStatVO.class)
    public JsonRestResponse getAttendanceStatList(AttendanceRecordRequestVO attendanceRecordRequestVO){
        return RestUtil.createResponse(this.attendanceRecordService.getAttendanceStatList(attendanceRecordRequestVO));
    }

    @GetMapping("/selectAttendanceRecordListById")
    @ResponseBody
    @ApiOperation(value = "查询指定用户的考勤记录", notes = "后台-查询指定用户的考勤记录",response= PunchInRecord.class,responseContainer="List")
    public JsonRestResponse selectAttendanceRecordListById(
            @ApiParam(value = "开始日期", required = true) @RequestParam String startTime,
            @ApiParam(value = "结束日期", required = true) @RequestParam String endTime,
            @ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
            @ApiParam(value = "用户id", required = true) @RequestParam String userId
    ){
        PageInfo<AttanceRecordVO> pageInfo = this.attendanceRecordService.selectAttendanceRecordListById(startTime,endTime,num,size,userId);
        return RestUtil.createResponse(pageInfo);
    }
}
