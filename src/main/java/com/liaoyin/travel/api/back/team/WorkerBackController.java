package com.liaoyin.travel.api.back.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.team.Worker;
import com.liaoyin.travel.service.team.WorkerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.*;

/***@项目名：旅投
 @作者：lijing
 * @描述：controller banner
 * @日期：Created in 2019/7/12 15:49
 */
@Api(tags={"工种列表"})
@RestController
@RequestMapping(value = "/back/worker")
public class WorkerBackController {
	
	@Autowired
	private WorkerService workerService;

	@ApiOperation(value = "查询工种列表", notes = "查询工种列表", response = Worker.class)
	 @GetMapping("/selectWorkerList")
	 public JsonRestResponse selectWorkerList(
			@ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam(required = true) Integer size,
            @ApiParam(value = "工种名称", required = false) @RequestParam(required = false) String workerName,
            @ApiParam(value = "工种编码", required = false) @RequestParam(required = false) String workerCode,
            @ApiParam(value = "是否启用【0：未启用 1：启用】", required = false) @RequestParam(required = false) String isUsing
           ) {
		
		return RestUtil.createResponse(new PageInfo<>(this.workerService.selectWorkerList(num, size, workerName, workerCode, isUsing)));
	}
	 
	 @ApiOperation(value = "根据id查询工种", notes = "根据id查询工种", response = Worker.class)
	 @GetMapping("/selectWorkerById")
	 public JsonRestResponse selectWorkerById(
			 @ApiParam(value = "id", required = true) @RequestParam(required = true) String id) {
		
		return RestUtil.createResponse(this.workerService.selectworkerById(id));
	}
	 
	 @ApiOperation(value = "新增或修改Worker", notes = "新增或修改Worker", response = Void.class)
	 @PostMapping("/insertOrUpdateWorker")
	 public JsonRestResponse insertOrUpdateWorker(
			@RequestBody Worker worker) {
		
		 this.workerService.insertOrUpdateWorker(worker);
		return RestUtil.createResponse();
	}
	 
	 @ApiOperation(value = "根据id删除Worker", notes = "根据id删除Worker", response = Integer.class)
	 @GetMapping("/deleteWorkerByIds")
	 public JsonRestResponse deleteWorkerByIds(
			 @ApiParam(value = "ids", required = true) @RequestParam String ids) {
		 String[] split = ids.split(",");
		 List<String> idList = Arrays.asList(ids.split(","));
		 int result = workerService.deleteByIds(idList);
		 return RestUtil.createResponse(result);
	}
}

