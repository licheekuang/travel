package com.liaoyin.travel.api.back.task;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.task.TaskLine;
import com.liaoyin.travel.service.task.TaskLineService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/***@项目名：旅投
 @作者：lijing
 * @描述：controller banner
 * @日期：Created in 2019/7/16 15:49
 */
@Api(tags={"任务线路线路管理"})
@RestController
@RequestMapping(value = "/back/taskLine")
public class TaskLineBackController {

	
	@Autowired
	private TaskLineService taskLineService;
	
	
    @PostMapping("/selectTaskLineBackList")
    @ResponseBody
    @ApiOperation(value = "查询任务线路列表", notes = "查询任务线路列表",response = TaskLine.class)
    public JsonRestResponse selectTaskLineBackList(
    		@ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam(required = true) Integer size,
            @ApiParam(value = "名称", required = false) @RequestParam(required = false) String linekName){
    	
        return RestUtil.createResponse(new PageInfo<>(this.taskLineService.selectTaskLineBackList(num, size, linekName)));
    }
    
    @PostMapping("/insertOrUpdateTaskLine")
    @ResponseBody
    @ApiOperation(value = "新增或修改", notes = "新增或修改",response = Void.class)
    public JsonRestResponse insertOrUpdateTaskLine(
    		@RequestBody @Validated TaskLine taskLine ){

        return RestUtil.createResponse(this.taskLineService.insertOrUpdateTaskLine(taskLine));
    }
    
    
    @GetMapping("/selectTaskLineByIdOnBack")
    @ResponseBody
    @ApiOperation(value = "根据id查询任务线路详情-后台", notes = "根据id查询任务线路详情-后台",response = TaskLine.class)
    public JsonRestResponse selectTaskLineByIdOnBack(
    		 @ApiParam(value = "任务线路id", required = true) @RequestParam String id){
    	
        return RestUtil.createResponse(this.taskLineService.selectTaskLineByIdOnBack(id,1));
    }
    
    
    @GetMapping("/deleteTaskLineByIdOnBack")
    @ResponseBody
    @ApiOperation(value = "根据id删除任务线路-后台", notes = "根据id删除任务线路-后台",response = TaskLine.class)
    public JsonRestResponse deleteTaskLineByIdOnBack(
    		 @ApiParam(value = "任务线路ids", required = true) @RequestParam(required = true) String ids){
    	
    	int result = this.taskLineService.deleteTaskLineByIdOnBack(ids);
        return RestUtil.createResponse(result);
    }
    
    
    
    
    
}