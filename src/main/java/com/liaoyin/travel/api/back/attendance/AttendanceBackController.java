package com.liaoyin.travel.api.back.attendance;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.attendance.AttendanceSettings;
import com.liaoyin.travel.entity.attendance.Equipment;
import com.liaoyin.travel.entity.attendance.PunchInRecord;
import com.liaoyin.travel.service.attendance.AttendanceSettingsService;
import com.liaoyin.travel.service.attendance.EquipmentService;
import com.liaoyin.travel.service.attendance.PunchInRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Controller
@Api(tags={"后台考勤打卡(已废弃)"})
@RequestMapping("/back/attendanceBack")
public class AttendanceBackController {

   @Autowired
   private AttendanceSettingsService attendanceSettingsService;
   @Autowired
   private EquipmentService equipmentService;
   @Autowired
   private PunchInRecordService punchInRecordService;

   
   /******************************************************考勤设置start********************************************************/
    @GetMapping("/selectAttendanceSettingsListBack")
    @ResponseBody
    @ApiOperation(value = "后台查询考勤设置列表", notes = "后台按条件查询考勤设置列表",response=AttendanceSettings.class,responseContainer="List")
    public JsonRestResponse selectAttendanceSettingsListBack(
    		 @ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
             @ApiParam(value = "每页条数", required = true) @RequestParam(required = true) Integer size,
             @ApiParam(value = "名称", required = false) @RequestParam(required = false) String checkName,
             @ApiParam(value = "班组id", required = false) @RequestParam(required = false) String teamId,
             @ApiParam(value = "是否启用【0：禁用 1：启用】", required = false) @RequestParam(required = false) String isUsing
    		){
        return RestUtil.createResponse(new PageInfo<>(this.attendanceSettingsService.selectAttendanceSettingsListBack(num, size, checkName, isUsing,teamId)));
    }
    
    @PostMapping("/insertOrUpdateAttendanceSettings")
    @ResponseBody
    @ApiOperation(value = "前端查询考勤设置", notes = "前端查询考勤设置",response=Void.class)
    public JsonRestResponse insertOrUpdateAttendanceSettings(
    		@RequestBody AttendanceSettings attendanceSettings
    		){
    		this.attendanceSettingsService.insertOrUpdateAttendanceSettings(attendanceSettings);
        return RestUtil.createResponse();
    }
    
    @GetMapping("/selectAttendanceSettingsById")
    @ResponseBody
    @ApiOperation(value = "根据id查询考勤设置", notes = "根据id查询考勤设置",response=AttendanceSettings.class)
    public JsonRestResponse selectAttendanceSettingsById(
    		 @ApiParam(value = "id", required = true) @RequestParam(required = true) String id
    		){
        return RestUtil.createResponse(this.attendanceSettingsService.selectAttendanceSettingsById(id));
    }
    
    @GetMapping("/deleteAttendanceSettingsById")
    @ResponseBody
    @ApiOperation(value = "删除考勤设置", notes = "删除考勤设置",response=Void.class)
    public JsonRestResponse deleteAttendanceSettingsById(
    		 @ApiParam(value = "ids", required = true) @RequestParam(required = true) String ids
    		){
    	this.attendanceSettingsService.deleteAttendanceSettingsById(ids);
        return RestUtil.createResponse();
    }
    
    /****************************************考勤设置 end*****************************************************************/
    
    
    /****************************************设备管理start*************************************************************************/
    
    @GetMapping("/selectEquipmentListBack")
    @ResponseBody
    @ApiOperation(value = "前端查询考勤设置", notes = "前端查询考勤设置",response=Equipment.class,responseContainer="List")
    public JsonRestResponse selectEquipmentListBack(
    		 @ApiParam(value = "当前页", required = true) @RequestParam(required = true) Integer num,
             @ApiParam(value = "每页条数", required = true) @RequestParam(required = true) Integer size,
             @ApiParam(value = "设备名称", required = false) @RequestParam(required = false) String equipmentName,
             @ApiParam(value = "设备类型【字典【equipmentType】", required = false) @RequestParam(required = false) String equipmentType,
             @ApiParam(value = "是否启用【0：禁用 1：启用】", required = false) @RequestParam(required = false) String isUsing
    		){
    	
        return RestUtil.createResponse(new PageInfo<>(this.equipmentService.selectEquipmentListOnBack(num, size, equipmentType, equipmentName, isUsing)));
    }
    
    @PostMapping("/insertOrUpdateEquipment")
    @ResponseBody
    @ApiOperation(value = "新增或修改设备", notes = "新增或修改设备",response=Void.class)
    public JsonRestResponse insertOrUpdateEquipment(
    		@RequestBody @Validated Equipment equipment
    		){
    		this.equipmentService.insertOrUpdateEquipment(equipment);
        return RestUtil.createResponse();
    }
    
    @GetMapping("/selectEquipmentById")
    @ResponseBody
    @ApiOperation(value = "根据id查询设备", notes = "根据id查询设备",response=Equipment.class)
    public JsonRestResponse selectEquipmentById(
    		 @ApiParam(value = "id", required = true) @RequestParam(required = true) String id
    		){
    	
        return RestUtil.createResponse(this.equipmentService.selectEquipmentById(id));
    }
    
    @GetMapping("/deleteEquipmentById")
    @ResponseBody
    @ApiOperation(value = "删除设备", notes = "删除设备",response=Void.class)
    public JsonRestResponse deleteEquipmentById(
    		 @ApiParam(value = "ids", required = true) @RequestParam(required = true) String ids
    		){
    	this.equipmentService.deleteEquipmentById(ids);
        return RestUtil.createResponse();
    }
    
    
    /*****************************************设备管理end************************************************************************/

    @GetMapping("/selectPunchInRecordListById")
    @ResponseBody
    @ApiOperation(value = "查询指定用户的考勤记录", notes = "查询指定用户的考勤记录",response= PunchInRecord.class,responseContainer="List")
    public JsonRestResponse selectPunchInRecordListById(
            @ApiParam(value = "开始日期", required = true) @RequestParam String startTime,
            @ApiParam(value = "结束日期", required = true) @RequestParam String endTime,
            @ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
            @ApiParam(value = "用户id", required = true) @RequestParam String userId
    ){

        return RestUtil.createResponse(new PageInfo<>(this.punchInRecordService.selectPunchInRecordListOnBack(startTime,endTime,num,size,userId)));
    }

}
