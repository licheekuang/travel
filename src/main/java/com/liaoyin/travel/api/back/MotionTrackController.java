package com.liaoyin.travel.api.back;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.MotionTrack;
import com.liaoyin.travel.service.MotionTrackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户轨迹查看的Controller
 *
 * @author 周明智
 * @date 2019/9/27 10:45
 */
@RequestMapping("/all/userMotionTrack")
@RestController
@Api(tags={"用户运动轨迹"})
public class MotionTrackController {

    @Autowired
    private MotionTrackService motionTrackService;

    @GetMapping("/selectMotionTrackListByUserid")
    @ApiOperation(value = "根据用户id 获取最近6个运动轨迹记录点" , notes = "根据用户id 获取最近6个运动轨迹记录点" , response = MotionTrack.class)
    public JsonRestResponse selectMotionTrackListByUserid(
            @ApiParam(value = "用户id",required = true)@RequestParam String userId)
    {
        return RestUtil.createResponse(this.motionTrackService.selectMotionTrackByUserId(userId));
    }

    @GetMapping("/selectMotionTrackByUserIdAndTime")
    @ApiOperation(value = "根据用户id 获取时间区间的运动轨迹" , notes = "根据用户id 获取时间区间的运动轨迹" , response = MotionTrack.class)
    public JsonRestResponse selectMotionTrackByUserIdAndTime(
            @ApiParam(value = "用户id",required = true)@RequestParam String userId,
            @ApiParam(value = "开始时间 yyyy-MM-dd HH:mm:ss",required = true)@RequestParam String startTime,
            @ApiParam(value = "结束时间 yyyy-MM-dd HH:mm:ss",required = true)@RequestParam String endTime
    ){
        return RestUtil.createResponse(this.motionTrackService.selectMotionTrackByUserIdAndTime(userId, startTime, endTime));
    }

}
