package com.liaoyin.travel.api.back.scenicInfo;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.scenic.StaffHealthCheckInfo;
import com.liaoyin.travel.view.scenic.TouristHealthCheckInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.scenicInfo.EpidemicManage;
import com.liaoyin.travel.service.scenicInfo.EpidemicManageService;
import com.liaoyin.travel.vo.scenic.StaffHealthCheckInfoVo;
import com.liaoyin.travel.vo.scenic.TouristHealthCheckInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 疫情管理Controller
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-22 22:55
 */
@RestController
@Api(tags={"疫情管理"})
@RequestMapping("/back/epidemicManage")
public class EpidemicManageController {

    @Resource
    EpidemicManageService epidemicManageService;

    @ApiOperation(value = "查询疫情管理信息", notes = "查询疫情管理信息(带分页)", response= EpidemicManage.class)
    @PostMapping(value = "/selectEpidemicInfo")
    public JsonRestResponse selectEpidemicInfo(@ApiParam(value = "当前页码", required = true) @RequestParam Integer num,
                                               @ApiParam(value = "每页条数", required = true) @RequestParam Integer size){
        PageInfo<EpidemicManage> pageInfo = this.epidemicManageService.selectEpidemicInfo(num,size);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "启用(禁用)疫情管理", notes = "查询疫情管理信息", response= EpidemicManage.class)
    @PostMapping(value = "/updateEpidemicManage")
    public JsonRestResponse updateEpidemicManage(@ApiParam(value = "疫情管理id", required = true) @RequestParam String id,
                                                 @ApiParam(value = "0:禁用;1:启用", required = true) @RequestParam String isUsing){
       int result = this.epidemicManageService.updateEpidemicManage(id,isUsing);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "新增或更新-疫情管理信息", notes = "后台-新增或更新-疫情管理信息", response= EpidemicManage.class)
    @PostMapping(value = "/insertOrUpdateEpidemicManage")
    public JsonRestResponse insertOrUpdateEpidemicManage(@RequestBody EpidemicManage epidemicManage){
        int result = this.epidemicManageService.insertOrUpdateEpidemicManage(epidemicManage);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "根据id查询疫情管理详情", notes = "根据id查询疫情管理详情", response= EpidemicManage.class)
    @PostMapping(value = "/selectEpidemicManageById")
    public JsonRestResponse selectEpidemicManageById(@ApiParam(value = "疫情管理Id", required = true) @RequestParam String id){
        EpidemicManage epidemicManage = this.epidemicManageService.selectEpidemicManageById(id);
        return RestUtil.createResponse(epidemicManage);
    }

    @ApiOperation(value = "根据ids删除疫情管理", notes = "根据ids批量物理删除疫情管理", response= EpidemicManage.class)
    @PostMapping(value = "/deleteEpidemicManageByIds")
    public JsonRestResponse deleteEpidemicManageByIds(
            @ApiParam(value = "要删除的id字符串，用英文逗号隔开", required = true) @RequestParam String ids
    ){
       int result = this.epidemicManageService.deleteEpidemicManageByIds(ids);
        return RestUtil.createResponse(result);
    }


    @ApiOperation(value = "按条件查询景区游客信息", notes = "按条件查询景区游客信息(带分页)", response= TouristHealthCheckInfo.class)
    @PostMapping(value = "/getPageTouristHealthCheckInfo")
    public JsonRestResponse getPageTouristHealthCheckInfo(@RequestBody TouristHealthCheckInfoVo touristHealthCheckInfoVo){
        PageInfo<TouristHealthCheckInfo> pageInfo = this.epidemicManageService.getPageTouristHealthCheckInfo(touristHealthCheckInfoVo);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "按条件查询景区员工信息", notes = "按条件查询景区员工信息(带分页)", response= StaffHealthCheckInfo.class)
    @PostMapping(value = "/getPageStaffHealthCheckInfo")
    public JsonRestResponse getPageStaffHealthCheckInfo(@RequestBody StaffHealthCheckInfoVo staffHealthCheckInfoVo){
        PageInfo<StaffHealthCheckInfo> pageInfo = this.epidemicManageService.getPageStaffHealthCheckInfo(staffHealthCheckInfoVo);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "按条件导出景区游客excel文件", notes = "按条件导出景区游客excel文件", response= TouristHealthCheckInfo.class)
    @PostMapping(value = "/exportTouristHealthCheckInfoExcelByDate")
    public JsonRestResponse exportTouristHealthCheckInfoExcelByDate(
            @ApiParam(value = "开始时间（yyyy-MM-dd）") @RequestParam(required = false) String beginTime,
            @ApiParam(value = "结束时间（yyyy-MM-dd）") @RequestParam(required = false) String endTime,
            @ApiParam(value = "健康状态(002001:正常;002002:异常)") @RequestParam(required = false) String healthState){
        String url = this.epidemicManageService.exportTouristHealthCheckInfoExcelByDate(beginTime,endTime,healthState);
        return RestUtil.createResponse(url);
    }

    @ApiOperation(value = "按条件导出景区员工excel文件", notes = "按条件导出景区员工excel文件", response= StaffHealthCheckInfo.class)
    @PostMapping(value = "/exportStaffHealthCheckInfoExcel")
    public JsonRestResponse exportStaffHealthCheckInfoExcel(
            @ApiParam(value = "健康状态(002001:正常;002002:异常)") @RequestParam(required = false) String healthState){
        String url = this.epidemicManageService.exportStaffHealthCheckInfoExcel(healthState);
        return RestUtil.createResponse(url);
    }
}
