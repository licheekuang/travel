//package com.liaoyin.travel.api.back.attendance;
//
//
//import com.liaoyin.travel.service.attendance.AttendanceRecordService;
//import io.swagger.annotations.Api;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
///**
// * 考勤记录
// *
// * @author kuang.jiazhuo
// * @date 2019-11-19 14:09
// */
//@Controller
//@Api(tags={"后台-考勤记录"})
//@RequestMapping("/attendanceRecord/back")
//public class AttendanceRecordController {
//
//    @Autowired
//    private AttendanceRecordService attendanceRecordService;

//    /**
//     * @方法名：insertOrUpdateAttendanceManagement
//     * @描述： 新增或修改考勤管理
//     * @作者： Kuang.JiaZhuo
//     * @日期： Created in 2019/11/18 14:24
//     */
//    @ApiOperation(value = "新增或修改考勤记录", notes = "新增或更新考勤记录", response= InsertOrUpdateAttendanceManagementVo.class)
//    @PostMapping(value = "/insertOrUpdateAttendanceRecord")
//    public JsonRestResponse insertOrUpdateAttendanceRecord(@RequestBody @Validated({Insert.class})InsertOrUpdateAttendanceManagementVo insertOrUpdateAttendanceManagementVo){
//        int result = this.attendanceRecordService.insertOrUpdateAttendanceRecord(insertOrUpdateAttendanceManagementVo);
//        return RestUtil.createResponse(result);
//    }
//
//    /**
//     * @方法名：deleteAttendanceManagement
//     * @描述： 根据ids字符串批量(物理)删除考勤管理
//     * @作者： Kuang.JiaZhuo
//     * @日期： Created in 2019/11/18 18:07
//     */
//    @ApiOperation(value = "删除考勤管理", notes = "根据ids字符串批量(物理)删除考勤管理", response= AttendanceManagement.class)
//    @PostMapping(value = "/deleteAttendanceManagement")
//    public JsonRestResponse deleteAttendanceManagement(@ApiParam(value = "要删除的id集合", required = true) @RequestParam String ids){
//        int result = this.attendanceManagementService.deleteAttendanceManagementByIds(ids);
//        return RestUtil.createResponse(result);
//    }
//
//    /**
//     * @方法名：selectAttendanceManagementViewByCondition
//     * @描述： 后台-按条件查询考勤管理(带分页)
//     * @作者： Kuang.JiaZhuo
//     * @日期： Created in 2019/11/18 18:07
//     */
//    @ApiOperation(value = "后台-按条件查询考勤管理(带分页)",notes = "后台-按条件查询考勤管理(带分页)",response= AttendanceManagementView.class)
//    @PostMapping(value = "/selectAttendanceManagementViewByCondition")
//    public JsonRestResponse selectAttendanceManagementViewByCondition(@RequestBody AttendanceManagementVo attendanceManagementVo)
//    {
//        PageInfo<AttendanceManagementView> pageInfo = this.attendanceManagementService.selectAttendanceManagementViewByCondition(attendanceManagementVo);
//        return RestUtil.createResponse(pageInfo);
//    }
//
//    /**
//     * @方法名：selectAttendanceManagementById
//     * @描述： 后台-根据id查询考勤管理详情
//     * @作者： Kuang.JiaZhuo
//     * @日期： Created in 2019/11/18 19:27
//     */
//    @ApiOperation(value = "后台-查询考勤管理详情",notes = "后台-根据id查询考勤管理详情",response= AttendanceManagement.class)
//    @PostMapping(value = "/selectAttendanceManagementById")
//    public JsonRestResponse selectAttendanceManagementById(@ApiParam(value = "考勤管理的id", required = true) @RequestParam String id)
//    {
//        AttendanceManagement attendanceManagement = this.attendanceManagementService.selectAttendanceManagementById(id);
//        return RestUtil.createResponse(attendanceManagement);
//    }
//}
