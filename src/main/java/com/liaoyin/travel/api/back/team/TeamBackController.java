package com.liaoyin.travel.api.back.team;

import com.liaoyin.travel.view.user.UserTeamBackView;
import com.liaoyin.travel.view.user.UserTeamView;
import com.liaoyin.travel.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.team.Team;
import com.liaoyin.travel.entity.team.TeamWorker;
import com.liaoyin.travel.service.team.TeamService;
import com.liaoyin.travel.service.team.TeamWorkerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.*;

/***@项目名：旅投
 @作者：lijing
 * @描述：controller Team
 * @日期：Created in 2019/7/12 15:49
 */
@Api(tags={"部门列表"})
@RestController
@RequestMapping(value = "/back/team")
public class TeamBackController {

	@Autowired
	private TeamService teamService;
	@Autowired
	private TeamWorkerService teamWorkerService;
	
	 @ApiOperation(value = "查询部门列表", notes = "查询部门列表", response = Team.class)
	 @GetMapping("/selectTeamList")
	 public JsonRestResponse selectTeamList(
			 @ApiParam(value = "当前页", required = true) @RequestParam Integer num,
             @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
             @ApiParam(value = "名称") @RequestParam(required = false) String teamName,
             @ApiParam(value = "部门级别") @RequestParam(required = false) String teamLevel,
             @ApiParam(value = "上级部门id") @RequestParam(required = false) String parentId,
             @ApiParam(value = "部门编码") @RequestParam(required = false) String teamCode,
             @ApiParam(value = "是否启用【0：未启用 1：启用】") @RequestParam(required = false) String isUsing
        ) {
		
		return RestUtil.createResponse(new PageInfo<>(this.teamService.selectTeamList(num, size, teamName, teamLevel, parentId, teamCode, isUsing)));
	}
	 
	 @ApiOperation(value = "根据id查询部门", notes = "根据id查询部门", response = Team.class)
	 @GetMapping("/selectTeamById")
	 public JsonRestResponse selectTeamById(
			 @ApiParam(value = "id", required = true) @RequestParam(required = true) String id
	 ) {
		
		return RestUtil.createResponse(this.teamService.selectTeamById(id));
	}
	 
	 @ApiOperation(value = "新增或修改Team", notes = "新增或修改Team", response = Void.class)
	 @PostMapping("/insertOrUpdateTeam")
	 public JsonRestResponse insertOrUpdateTeam(
			@RequestBody Team team) {
		this.teamService.insertOrUpdateTeam(team);
		return RestUtil.createResponse();
	}
	 
	 @ApiOperation(value = "根据id删除Team", notes = "根据id删除Team", response = Integer.class)
	 @GetMapping("/deleteTeamByIds")
	 public JsonRestResponse deleteTeamByIds(
			 @ApiParam(value = "ids", required = true) @RequestParam String ids) {
		 String[] split = ids.split(",");
		 List<String> idList = Arrays.asList(ids.split(","));
		 int r = teamService.deleteByIds(idList);
/*		 for (String s : split) {
			 Team team = new Team();
			 team.setId(s);
			 team.setIsDelete((short)1);
			 this.teamService.insertOrUpdateTeam(team);
		 }*/
		 return RestUtil.createResponse(r);
	}
	 
	 @ApiOperation(value = "查询部门下的所有工种", notes = "查询部门下的所有工种", response = TeamWorker.class)
	 @GetMapping("/selectTeamWorkerListByTeamId")
	 public JsonRestResponse selectTeamWorkerListByTeamId(
			 @ApiParam(value = "部门id", required = false) @RequestParam(required = false) String id) {
		
		return RestUtil.createResponse(this.teamWorkerService.selectTeamWorkerListByTeamId(id));
	}

	@ApiOperation(value = "查询部门下的所有员工", notes = "查询部门下的所有员工", response = Users.class)
	@GetMapping("/selectTeamUserList")
	public JsonRestResponse selectTeamUserList(
			@ApiParam(value = "部门id", required = true) @RequestParam String id
	){
		List<Users> list = this.teamWorkerService.selectUsersListByTeamId(id);
	 	return RestUtil.createResponse(list);
	}

	@ApiOperation(value = "按部门分组查询员工信息", notes = "按部门分组查询员工信息", response = UserTeamView.class)
	@GetMapping("/selectTeamUserByGroups")
	public JsonRestResponse selectTeamUserByGroups(){
		List<UserTeamView> list = this.teamService.selectTeamUserViewByAll();
		return RestUtil.createResponse(list);
	}

	@ApiOperation(value = "按部门分组查询员工信息(后台管理系统)", notes = "后台管理系统特供-按部门分组查询员工信息", response = UserTeamBackView.class)
	@GetMapping("/selectTeamUserByGroupsBack")
	public JsonRestResponse selectTeamUserByGroupsBack(){
		List<UserTeamBackView> list = this.teamService.selectTeamUserBackViewByAll();
		return RestUtil.createResponse(list);
	}
}