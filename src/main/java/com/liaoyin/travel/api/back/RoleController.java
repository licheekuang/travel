package com.liaoyin.travel.api.back;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.common.rest.BaseController;
import com.liaoyin.travel.entity.Role;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.RoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags={"角色"})
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController<RoleService, Role>{
	
	/**
	 * 
	 * 方   法  名：selectRoleByID
	 * 创建日期：2017年12月1日 下午4:34:42
	 * 创   建  者：li.jing
	 * 描          述：根据ID查询角色
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "根据id查询角色角色列表", notes = "根据id查询角色列表",response=Role.class)
	@GetMapping("/selectRoleByID")
	public JsonRestResponse selectRoleByID(
			@ApiParam(value="ID",required=true) @RequestParam(required=true) String id){
		Role o =this.baseService.selectRoleByID(id);	
    	return RestUtil.createResponse(o); 
    }

	/** 方   法  名：selectRoleListByUserIdPage
	 * 创建日期：2017年12月1日 下午4:34:42
	 * 创   建  者：li.jing
	 * 描          述：根据用户id查询角色列表分页
	 * @param Role
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "根据用户id查询角色列表", notes = "根据用户id查询角色列表",response=Role.class)
	@PostMapping("/selectRoleList")
	public JsonRestResponse selectRoleList(@ApiParam(value="创建人ID") @RequestParam(required=false) String createUserId,			
             @ApiParam(name = "search",value = "搜索内容") @RequestParam(required = false) String search,
             @ApiParam(name = "roleAffiliate",value = "角色归属") @RequestParam(required = false) String roleAffiliate,
             @ApiParam(name = "attr1",value = "当前状态") @RequestParam(required = false) String attr1,
             @ApiParam(value = "第几页", required = true) @RequestParam Integer num,
             @ApiParam(value = "一页多少条", required = true) @RequestParam Integer size){
		return RestUtil.createResponse(new PageInfo<Role>(this.baseService.selectRoleList(num,size,createUserId,search,roleAffiliate,attr1)));
	}
	/**
	 * 
	 * 方   法  名：insertRole
	 * 创建日期：2017年12月1日 下午4:34:42
	 * 创   建  者：li.jing
	 * 描          述：新增角色
	 * @param menu
	 * @return
	 */
	@ApiOperation(value="新增角色",notes="新增角色", response = Void.class)
	@PostMapping("/insertRole")
	public JsonRestResponse insertRole(@RequestBody Role role){
		this.baseService.insertRole(role);
		return RestUtil.createResponse();
	}
	

	/**
	 * @方法名：updateRole
	 * @描述： 角色修改
	 * @作者： li.jing
	 * @日期： 2017年12月1日 下午4:34:42
	 */
	@ApiOperation(value = "角色修改", notes = "角色修改 ", response = Void.class)
	@PostMapping("/updateRole")
	public JsonRestResponse updateRole(@RequestBody Role role) throws Exception {
		if(role.getId().isEmpty()) {
			throw new BusinessException("id.is.null");
		}
		this.baseService.updateRole(role);
		return RestUtil.createResponse();
	}

	/**
	 * @方法名：deleteRole
	 * @描述： 删除
	 * @作者： li.jing
	 * @日期： 2017年12月1日 下午4:34:42
	 */
	@ApiOperation(value="删除角色,逻辑删除",notes="删除角色，逻辑删除", response = Void.class)
	@PostMapping("/deleteRole")
	public JsonRestResponse deleteRole(@ApiParam(name="id",value = "角色ID") @RequestParam String id,
			@ApiParam(name="deleteUserId",value = "删除人ID") @RequestParam String deleteUserId){
				
		return RestUtil.createResponse(this.baseService.deleteRole(id,deleteUserId));
	}

	/**
	 * @方法名：copyThisRoleById
	 * @描述： 根据ID复制角色
	 * @作者： li.jing
	 * @日期： 2017年12月1日 下午4:34:42
	 */
	@ApiOperation(value = "根据id复制角色", notes = "根据id复制角色",response=Role.class)
	@PostMapping("/copyThisRoleById")
	public JsonRestResponse copyThisRoleById(
			@ApiParam(value="ID",required=true) @RequestParam(required=true) String id){
		int k =this.baseService.copyThisRoleById(id);	
    	return RestUtil.createResponse(k); 
    }

	/**
	 * @方法名：selectAllRoleList
	 * @描述： 根据查询所有角色列表
	 * @作者： li.jing
	 * @日期： 2017年12月1日 下午4:34:42
	 */
	@ApiOperation(value = "根据查询所有角色列表", notes = "根据查询所有角色列表",response=Role.class)
	@PostMapping("/selectAllRoleList")
	public JsonRestResponse selectAllRoleList(@ApiParam(value="ID") @RequestParam(required=false) String id			
            ){
		return RestUtil.createResponse(this.baseService.selectAllRoleList(id));
	}
	
	

	/**
	 * @方法名：selectRoleIdFindMenuAndFunction
	 * @描述： 根据查询所有角色关联的菜单和功能数量
	 * @作者： li.jing
	 * @日期： 2017年12月1日 下午4:34:42
	 */
	@ApiOperation(value = "根据查询所有角色关联的菜单和功能数量", notes = "根据查询所有角色关联的菜单和功能数量",response=Role.class)
	@PostMapping("/selectRoleIdFindMenuAndFunction")
	public JsonRestResponse selectRoleIdFindMenuAndFunction(@ApiParam(value="角色ID") @RequestParam(required=false) String id){
		return RestUtil.createResponse(this.baseService.selectRoleIdFindMenuAndFunction(id));
	}

	/**
	 * @方法名：updateRoleThisMenuAndFunc
	 * @描述： 根据id进行权限分配
	 * @作者： li.jing
	 * @日期： 2017年12月12日 下午4:34:42
	 */
	@ApiOperation(value = "根据id进行权限分配", notes = "根据id进行权限分配",response=Void.class)
	@PostMapping("/updateRoleThisMenuAndFunc")
	public JsonRestResponse updateRoleThisMenuAndFunc(
			@ApiParam(name="id",value="角色ID") @RequestParam(required=false) String id,
			//@ApiParam(name="funId",value="功能ID集合") @RequestParam(required=false) String funId,
			@ApiParam(name="menuId",value="菜单ID集合") @RequestParam(required=false) String menuId){
		
		this.baseService.updateRoleThisMenuAndFunc(id, null, menuId);
		return RestUtil.createResponse();
	}
	

	/**
	 * @方法名：selectRoleThisMenuAndFuncByRoleId
	 * @描述： 根据角色id查询分配的菜单和功能
	 * @作者： li.jing
	 * @日期： 2017年12月12日 下午4:34:42
	 */
	@ApiOperation(value = "根据角色id查询分配的菜单和功能", notes = "根据角色id查询分配的菜单和功能",response=Void.class)
	@PostMapping("/selectRoleThisMenuAndFuncByRoleId")
	public JsonRestResponse selectRoleThisMenuAndFuncByRoleId(
			@ApiParam(name="id",value="角色ID") @RequestParam String id){
		
		//this.baseService.updateRoleThisMenuAndFunc(id);
		return RestUtil.createResponse(this.baseService.selectRoleThisMenuAndFuncByRoleId(id));
	}
}
