package com.liaoyin.travel.api.back.approval;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.view.moble.approval.ApprovalIsVisibleBackView;
import com.liaoyin.travel.view.moble.approval.PacketRecordDetailsView;
import com.liaoyin.travel.view.moble.back.approval.ApprovalRecordBackView;
import com.liaoyin.travel.vo.approval.ApprovalPacketRecordVo;
import com.liaoyin.travel.vo.approval.ApprovalRecordBackVo;
import com.liaoyin.travel.vo.approval.InsertOrUpdateApprovalRecordVo;
import com.liaoyin.travel.vo.approval.InsertOrUpdateApprovalVisualRangeVo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.entity.approval.AskForLeavePacketRecord;
import com.liaoyin.travel.entity.approval.CardReissuePacketRecord;
import com.liaoyin.travel.entity.approval.GoOutPacketRecord;
import com.liaoyin.travel.service.approval.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * TODO
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 09:26
 */
@RestController
@Api(tags={"后台审批设置"})
@RequestMapping("/back/approvalSettings")
public class ApprovalBackController {

    @Resource
    private VisualRangeSettingService visualRangeSettingService;

    @Resource
    private CardReissuePacketRecordService cardReissuePacketRecordService;

    @Resource
    private AskForLeavePacketRecordService askForLeavePacketRecordService;

    @Resource
    private GoOutPacketRecordService goOutPacketRecordService;

    @Resource
    private ApprovalRecordService approvalRecordService;

    @ApiOperation(value = "查询-审批【可见范围】设置", notes = "后台-查询-审批【可见范围】设置", response= ApprovalIsVisibleBackView.class)
    @PostMapping(value = "/selectApprovalVisualRangeSettingByAll")
    public JsonRestResponse selectApprovalVisualRangeSettingByAll(){
        List<ApprovalIsVisibleBackView> approvalIsVisibleBackViews = this.visualRangeSettingService.selectApprovalVisualRangeSettingByAll();
        return RestUtil.createResponse(approvalIsVisibleBackViews);
    }

    @ApiOperation(value = "启用或停用某种审批", notes = "启用或停用可见范围(停用了可见范围，移动端就查不到该审批类型,相当于停用)")
    @PostMapping(value = "/usingOrDisableApprovalType")
    public JsonRestResponse usingOrDisableApprovalTypeByApprovalType(
            @ApiParam(value = "0:停用;1:启用", required = true) @RequestParam String onOff,
            @ApiParam(value = "1.补卡;2:请假;3:外出", required = true) @RequestParam String approvalType
    ){
        int result = this.visualRangeSettingService.usingOrDisableApprovalType(onOff,approvalType);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "新增或更新-审批【可见范围】设置", notes = "后台-新增或更新-审批【可见范围】设置", response= InsertOrUpdateApprovalVisualRangeVo.class)
    @PostMapping(value = "/insertOrUpdateApprovalVisualRangeSetting")
    public JsonRestResponse insertOrUpdateApprovalVisualRangeSetting(@RequestBody InsertOrUpdateApprovalVisualRangeVo insertOrUpdateApprovalVisualRangeVo){
        int result = this.visualRangeSettingService.insertOrUpdateApprovalVisualRangeSetting(insertOrUpdateApprovalVisualRangeVo);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "新增或更新-【补卡】申请的审批分组", notes = "后台-新增或更新(包括物理删除)-【补卡】申请的审批分组", response= InsertOrUpdateApprovalRecordVo.class)
    @PostMapping(value = "/insertOrUpdateCardReissuePacketRecord")
    public JsonRestResponse insertOrUpdateCardReissuePacketRecord(@RequestBody InsertOrUpdateApprovalRecordVo cardReissuePacketRecordVo){
        int result = this.cardReissuePacketRecordService.insertOrUpdateApprovalCardReissuePacketRecord(cardReissuePacketRecordVo);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "根据id和类型查询审批分组的详情", notes = "根据id查询和审批类型审批分组的详情", response= InsertOrUpdateApprovalRecordVo.class)
    @GetMapping(value = "/selectPacketDetailsById")
    public JsonRestResponse selectPacketDetailsById(
            @ApiParam(value = "id", required = true) @RequestParam String id,
            @ApiParam(value = "审批类型(1.补卡;2:请假;3:外出)", required = true) @RequestParam String approvalType
                                                               ){
        PacketRecordDetailsView packetRecordDetailsView = new PacketRecordDetailsView();
        if(VariableConstants.STRING_CONSTANT_1.equals(approvalType)){
            packetRecordDetailsView = this.cardReissuePacketRecordService.selectCardReissuePacketDetailsById(id);
        }
        if(VariableConstants.STRING_CONSTANT_2.equals(approvalType)){
            packetRecordDetailsView = this.askForLeavePacketRecordService.selectAskForLeavePacketDetailsById(id);
        }
        if(VariableConstants.STRING_CONSTANT_3.equals(approvalType)){
            packetRecordDetailsView = this.goOutPacketRecordService.selectGoOutPacketDetailsById(id);
        }
        return RestUtil.createResponse(packetRecordDetailsView);
    }

    @ApiOperation(value = "按条件查询【补卡】审批分组列表", notes = "后台-按条件查询【补卡】审批分组列表(带分页)", response= ApprovalPacketRecordVo.class)
    @PostMapping(value = "/selectCardReissuePacketRecord")
    public JsonRestResponse selectCardReissuePacketRecordByCondition(@RequestBody ApprovalPacketRecordVo approvalPacketRecordVo){
        PageInfo<CardReissuePacketRecord> pageInfo = this.cardReissuePacketRecordService.selectCardReissuePacketRecordByCondition(approvalPacketRecordVo);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "新增或更新-【请假】申请的审批分组", notes = "后台-新增或更新(包括物理删除)-【请假】申请的审批分组", response= InsertOrUpdateApprovalRecordVo.class)
    @PostMapping(value = "/insertOrUpdateAskForLeavePacketRecord")
    public JsonRestResponse insertOrUpdateAskForLeavePacketRecord(@RequestBody InsertOrUpdateApprovalRecordVo askForLeavePacketRecordVo){
        int result = this.askForLeavePacketRecordService.insertOrUpdateAskForLeavePacketRecord(askForLeavePacketRecordVo);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "按条件查询【请假】审批分组列表", notes = "后台-按条件查询【请假】审批分组列表(带分页)", response= ApprovalPacketRecordVo.class)
    @PostMapping(value = "/selectAskForLeavePacketRecordByCondition")
    public JsonRestResponse selectAskForLeavePacketRecordByCondition(@RequestBody ApprovalPacketRecordVo approvalPacketRecordVo){
        PageInfo<AskForLeavePacketRecord> pageInfo = this.askForLeavePacketRecordService.selectAskForLeavePacketRecordByCondition(approvalPacketRecordVo);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "新增或更新-【外出】申请的审批分组", notes = "后台-新增或更新(包括物理删除)-【外出】申请的审批分组", response= InsertOrUpdateApprovalRecordVo.class)
    @PostMapping(value = "/insertOrUpdateGoOutPacketRecord")
    public JsonRestResponse insertOrUpdateGoOutPacketRecord(@RequestBody InsertOrUpdateApprovalRecordVo goOutPacketRecordVo){
        int result = this.goOutPacketRecordService.insertOrUpdateGoOutPacketRecord(goOutPacketRecordVo);
        return RestUtil.createResponse(result);
    }

    @ApiOperation(value = "按条件查询【外出】审批分组列表", notes = "后台-按条件查询【外出】审批分组列表(带分页)", response= ApprovalPacketRecordVo.class)
    @PostMapping(value = "/selectGoOutPacketRecordByCondition")
    public JsonRestResponse selectGoOutPacketRecordByCondition(@RequestBody ApprovalPacketRecordVo approvalPacketRecordVo){
        PageInfo<GoOutPacketRecord> pageInfo= this.goOutPacketRecordService.selectGoOutPacketRecordByCondition(approvalPacketRecordVo);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "后台-按条件查询审批记录列表", notes = "后台-按条件审批记录列表(带分页)", response= ApprovalRecordBackView.class)
    @PostMapping(value = "/selectApprovalRecordBackViewByCondition")
    public JsonRestResponse selectApprovalRecordBackViewByCondition(@RequestBody ApprovalRecordBackVo approvalRecordBackVo){
        PageInfo<ApprovalRecordBackView> pageInfo = this.approvalRecordService.selectApprovalRecordBackViewByCondition(approvalRecordBackVo);
        return RestUtil.createResponse(pageInfo);
    }

    @ApiOperation(value = "批量删除审批分组", notes = "按类型批量删除审批分组")
    @PostMapping(value = "/deletePacketRecordByIds")
    public JsonRestResponse deletePacketRecordByIds(
            @ApiParam(value = "id字符串集", required = true) @RequestParam String ids,
            @ApiParam(value = "审批类型(1.补卡;2:请假;3:外出)", required = true) @RequestParam String approvalType){
        int result = 0;
        if(VariableConstants.STRING_CONSTANT_1.equals(approvalType)){
            result = this.cardReissuePacketRecordService.deleteCardReissuePacketRecordByIds(ids);
        }
        if(VariableConstants.STRING_CONSTANT_2.equals(approvalType)){
            result = this.askForLeavePacketRecordService.deleteAskForLeavePacketRecordByIds(ids);
        }
        if(VariableConstants.STRING_CONSTANT_3.equals(approvalType)){
            result = this.goOutPacketRecordService.deleteGoOutPacketRecordByIds(ids);
        }
        return RestUtil.createResponse(result);
    }

}
