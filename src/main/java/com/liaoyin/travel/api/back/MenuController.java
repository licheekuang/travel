package com.liaoyin.travel.api.back;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.common.rest.BaseController;
import com.liaoyin.travel.entity.Menu;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.MenuService;
import com.liaoyin.travel.service.scenicInfo.LowerLevelAdminMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

@Api(tags={"后台查询菜单"})
@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController<MenuService, Menu> {

    @Autowired
    LowerLevelAdminMenuService levelAdminMenuService;

    /**
     * 方   法  名：selectMenu
     * 创建日期：2017年8月8日 下午6:01:52
     * 创   建  者：zhou.ning
     * 描          述：后台菜单查询
     *
     * @return
     */
    @ApiOperation(value = "后台菜单查询", notes = "")
    @PostMapping("/selectMenu")
    public JsonRestResponse selectMenu() {

        String partymemberId = "1";

        return RestUtil.createResponse(this.baseService.selectMenu(partymemberId));
    }


    /**
     * 方   法  名：selectMenuByPage
     * 创建日期：2017年8月15日 下午8:50:30
     * 创   建  者：du.juncheng
     * 描          述：后台查询菜单(带分页)
     *
     * @return
     */
    @ApiOperation(value = "后台查询菜单(带分页)", notes = "后台查询菜单(带分页)")
    @PostMapping("/selectMenuByPage")
    public JsonRestResponse selectMenuByPage(@RequestBody Menu menu,
                                             @ApiParam(name = "page", value = "当前页") Integer page,
                                             @ApiParam(name = "rows", value = "每页显示数") Integer rows) {
        if (menu == null) {
            menu = new Menu();
	        menu.setSize(rows);
	        menu.setNum(page);
        }
        return RestUtil.createResponse(this.baseService.selectList(menu));
    }


    /**
     * 方   法  名：insertMenu
     * 创建日期：2017年12月06日 下午9:03:40
     * 创   建  者：li.jing
     * 描          述：新增菜单
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "新增菜单", notes = "新增菜单")
    @PostMapping("/insertMenu")
    public JsonRestResponse insertMenu(@RequestBody Menu menu) {
        this.baseService.insertMenu(menu);
        return RestUtil.createResponse();
    }


    /**
     * 方   法  名：updateMenu
     * 创建日期：2017年8月15日 下午9:12:26
     * 创   建  者：du.juncheng
     * 描          述：修改菜单
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "修改菜单", notes = "修改菜单")
    @PostMapping("/updateMenu")
    public JsonRestResponse updateMenu(@RequestBody Menu menu) {
        if (menu.getId().isEmpty()) {
            throw new BusinessException("msg.validate.empty", "菜单ID为空");
        }
        menu.setCreateUserId("111");
        menu.setUpdateTime(new Date());
        this.baseService.updateSelectiveById(menu);
        return RestUtil.createResponse();
    }


    /**
     * 方   法  名：deleteMenu
     * 创建日期：2017年8月15日 下午9:13:41
     * 创   建  者：du.juncheng
     * 描          述：删除菜单
     *
     * @return
     */
    @ApiOperation(value = "删除菜单", notes = "删除菜单")
    @PostMapping("/deleteMenu/{id}")
    public JsonRestResponse deleteMenu(@ApiParam(value = "菜单ID", required = true) @RequestParam @PathVariable String id) {
        Menu menu = new Menu();
        menu.setId(id);
        menu.setIsDelete(Short.parseShort("1"));
        menu.setCreateUserId("111");
        menu.setUpdateTime(new Date());
        this.baseService.updateSelectiveById(menu);
        return RestUtil.createResponse();
    }


    /**
     * 方   法  名：selectMenu
     * 创建日期：2017年12月06日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：获取菜单所有
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "获取菜单所有", notes = "获取菜单所有")
    @PostMapping("/selectMenuAllById")
    public JsonRestResponse selectMenuAllById(@ApiParam(value = "菜单ID", required = false) @RequestParam String id) {

        return RestUtil.createResponse(this.baseService.selectMenuAllById(id));
    }

    /**
     * 方   法  名：selectMenu
     * 创建日期：2017年12月06日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：获取菜单所有
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "获取菜单所有", notes = "获取菜单所有")
    @PostMapping("/selectMenuAll")
    public JsonRestResponse selectMenuAll() {

        return RestUtil.createResponse(this.baseService.selectMenuAll());
    }
    @ApiOperation(value = "获取所有菜单并且判断角色是否添加", notes = "获取所有菜单并且判断角色是否添加")
    @PostMapping("/selectMenuByRoleIdIsAdd")
    public JsonRestResponse selectMenuByRoleIdIsAdd(
            @ApiParam(value = "角色ID", required = true) @RequestParam String id
    ) {

        return RestUtil.createResponse(this.baseService.selectMenuByRoleIdIsAdd(id));
    }
    /**
     * 方   法  名：selectMenu
     * 创建日期：2017年12月06日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：生成菜单全编码
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "生成菜单全编码", notes = "生成菜单全编码")
    @PostMapping("/selectMenuCreatCodeById")
    public JsonRestResponse selectMenuCreatCodeById(@ApiParam(value = "菜单ID", required = true) @RequestParam String id,
                                                    @ApiParam(value = "菜单level", required = true) @RequestParam String level) {

        return RestUtil.createResponse(this.baseService.selectMenuMAXfuncCode(id, level));
    }


    /**
     * 方   法  名：selectMenuByPage
     * 创建日期：2017年12月08日 下午8:50:30
     * 创   建  者：li.jing
     * 描          述：后台查询菜单(带分页)
     *
     * @return
     */
    @ApiOperation(value = "后台查询菜单(带分页)", notes = "后台查询菜单(带分页)")
    @PostMapping("/selectMenuListByPage")
    public JsonRestResponse selectMenuListByPage(@ApiParam(name = "id", value = "ID") String id,
                                                 @ApiParam(name = "num", value = "当前页") Integer num,
                                                 @ApiParam(name = "size", value = "每页显示数") String size) {

        return RestUtil.createResponse(new PageInfo<>(this.baseService.selectMenuListByPage(id, num, size)));
    }

    /**
     * 方   法  名：selectMenuById
     * 创建日期：2017年12月06日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：生成菜单全编码
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "菜单id查询", notes = "菜单id查询")
    @GetMapping("/selectMenuById")
    public JsonRestResponse selectMenuById(@ApiParam(value = "菜单ID", required = true) @RequestParam String id) {

        return RestUtil.createResponse(this.baseService.selectMenuById(id));
    }

    /**
     * 方   法  名：updateMenu
     * 创建日期：2017年12月08日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：修改菜单
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "修改菜单", notes = "修改菜单")
    @PostMapping("/updateMenuThis")
    public JsonRestResponse updateMenuThis(@RequestBody Menu menu) {
        this.baseService.updateMenuThis(menu);
        return RestUtil.createResponse();
    }

    /**
     * 方   法  名：deleteMenu
     * 创建日期：2017年12月08日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：修改菜单
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "删除菜单--删除后要同步删除菜单与角色的关联表项", notes = "删除菜单")
    @PostMapping("/deleteMenuByThis")
    public JsonRestResponse deleteMenuByThis(
            @ApiParam(name = "id", value = "ID") @RequestParam(required = true) String id,
            @ApiParam(name = "deleteUserId", value = "ID") @RequestParam(required = false)String deleteUserId) {
        this.baseService.deleteMenuByThis(id, deleteUserId);
        return RestUtil.createResponse();
    }

    /**
     * 方   法  名：selectMenuAllThis
     * 创建日期：2017年12月09日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：获取菜单所有--带返回下级菜单数量
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "获取菜单所有--带返回下级菜单数量", notes = "获取菜单所有--带返回下级菜单数量")
    @PostMapping("/selectMenuAllThis")
    public JsonRestResponse selectMenuAllThis(@ApiParam(value = "菜单ID", required = false) @RequestParam @PathVariable String id) {

        return RestUtil.createResponse(this.baseService.selectMenuAllThis(id));
    }

    /**
     * 方   法  名：selectMenu
     * 创建日期：2017年12月06日 下午17:28:40
     * 创   建  者：li.jing
     * 描          述：获取菜单所有
     *
     * @param orgId
     * @return
     */
    @ApiOperation(value = "获取菜单所有---带返回下级菜单数量--不传id", notes = "获取菜单所有---带返回下级菜单数量--不传id")
    @PostMapping("/selectMenuAllThisMenu")
    public JsonRestResponse selectMenuAllThisMenu(@ApiParam(name = "orgId", value = "组织Id") String orgId) {
        Map<String, Object> map = this.baseService.selectMenuAllThisMenu(orgId);
        return RestUtil.createResponse(map);
    }

    /**
     * @方法名：selectMenuAllThisScenicLowerLevelAdminMenu
     * @描述： 获取单位子管理员的所有菜单---带返回下级菜单数量
     * @作者： kjz
     * @日期： Created in 2020/5/14 17:48
     */
    @ApiOperation(value = "获取单位子管理员的所有菜单---带返回下级菜单数量", notes = "获取单位子管理员的所有菜单---带返回下级菜单数量")
    @PostMapping("/selectMenuAllThisScenicLowerLevelAdminMenu")
    public JsonRestResponse selectMenuAllThisScenicLowerLevelAdminMenu() {
        Map<String, Object> map = this.baseService.selectMenuAllThisScenicLowerLevelAdminMenu();
        return RestUtil.createResponse(map);
    }

    /**
     * 方   法  名：selectMenuByPage
     * 创建日期：2017年12月08日 下午8:50:30
     * 创   建  者：li.jing
     * 描          述：后台查询菜单(带分页)
     *
     * @return
     */
    @ApiOperation(value = "后台查询菜单(带分页)", notes = "后台查询菜单(带分页)")
    @PostMapping("/selectMenuListByPageByThis")
    public JsonRestResponse selectMenuListByPageByThis(@ApiParam(name = "id", value = "ID", required = false) String id,
                                                       @ApiParam(name = "num", value = "当前页", required = false) Integer num,
                                                       @ApiParam(name = "size", value = "每页显示数", required = false) String size,
                                                       @ApiParam(name = "dataType", value = "当前状态(0:禁用  1:开启)", required = false) String dataType,
                                                       @ApiParam(name = "search", value = "菜单标题  、菜单编码", required = false) String search) {

        return RestUtil.createResponse(new PageInfo<>(this.baseService.selectMenuListByPageByThis(id, num, size, dataType, search)));
    }

    /**
     * 方   法  名:selectMenuByOrgId
     * 创 建 日 期:2018/1/2/002 10:08
     * 创  建   者:zhang.zhipeng
     * 描       述:组织中人员设置权限查询
     * 参       数:[orgId]
     * 返  回   值:org.party.common.msg.JsonRestResponse
     */
    @ApiOperation(value = "组织中人员设置权限查询", notes = "组织中人员设置权限查询")
    @PostMapping("/selectMenuByOrgId")
    public JsonRestResponse selectMenuByOrgId(@ApiParam(name = "orgId", value = "orgId") String orgId,
                                              @ApiParam(name = "userId", value = "userId") String userId) {
        return RestUtil.createResponse(this.baseService.selectMenuByOrgIdOnlyMenuId(orgId,userId));
    }

    /**
     * @方法名：updateBackUserThisMenu
     * @描述： 根据后台用户id对单位(景区)子管理员进行权限分配
     * @作者： kjz
     * @日期： Created in 2020/5/14 10:58
     */
    @ApiOperation(value = "根据后台用户id进行权限分配", notes = "根据后台用户id对单位(景区)子管理员进行权限分配",response=Integer.class)
    @PostMapping("/updateBackUserThisMenu")
    public JsonRestResponse updateBackUserThisMenu(@ApiParam(value = "后台用户id" , required = true)@RequestParam String backUserId,
                                                   @ApiParam(value="菜单ID集合") @RequestParam String menuIds){
        int result = this.levelAdminMenuService.insertScenicBackUserMenu(menuIds,backUserId);
        return RestUtil.createResponse(result);
    }

    /**
     * @方法名：selectBackUserThisMenuByUserId
     * @描述： 根据后台用户id查询景区子管理员分配的菜单
     * @作者： kjz
     * @日期： Created in 2020/5/14 11:22
     */
    @ApiOperation(value = "根据后台用户id查询分配的菜单", notes = "根据后台用户id查询景区子管理员分配的菜单",response=Map.class)
    @PostMapping("/selectBackUserThisMenuByUserId")
    public JsonRestResponse selectBackUserThisMenuByUserId(
            @ApiParam(value="后台用户id") @RequestParam String backUserId){
        Map<String, Object> result = this.levelAdminMenuService.selectBackUserThisMenuByUserId(backUserId);
        return RestUtil.createResponse(result);
    }

    /**
     * @方法名：updateScenicRoleThisMenu
     * @描述： 根据菜单id字符串更新景区子管理员可以分配的菜单
     * @作者： kjz
     * @日期： Created in 2020/5/14 16:07
     */
    @ApiOperation(value = "更新景区子管理员可以分配的菜单", notes = "根据菜单id字符串更新景区子管理员可以分配的菜单",response=Integer.class)
    @PostMapping("/updateScenicRoleThisMenu")
    public JsonRestResponse updateScenicRoleThisMenu(
            @ApiParam(value="菜单ID集合,根据,分割的字符串") @RequestParam String menuIds){

        int result = this.baseService.updateScenicRoleThisMenu(menuIds);
        return RestUtil.createResponse(result);
    }

    /**
     * @方法名：selectScenicRoleThisMenu
     * @描述： 查询景区子管理员可以进行分配的菜单
     * @作者： kjz
     * @日期： Created in 2020/5/14 16:37
     */
    @ApiOperation(value = "查询景区子管理员可以分配的菜单", notes = "查询景区子管理员可以进行分配的菜单",response=Map.class)
    @PostMapping("/selectScenicRoleThisMenu")
    public JsonRestResponse selectScenicRoleThisMenu(){
        Map<String, Object> result = this.baseService.selectScenicRoleThisMenu();
        return RestUtil.createResponse(result);
    }

}
