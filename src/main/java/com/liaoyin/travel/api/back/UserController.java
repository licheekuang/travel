package com.liaoyin.travel.api.back;

import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.entity.BackUser;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.service.UsersService;
import com.liaoyin.travel.service.back.BackUserService;
import com.liaoyin.travel.vo.UserPassWordVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags={"登录用户管理"})
@RestController
@RequestMapping(value = "/sUsers")
public class UserController {

    @Autowired
    private BackUserService backUserService;
    @Autowired
    private UsersService usersService;
    
    @ApiOperation(value = "刷新token", notes = "刷新token", response = Void.class)
    @PostMapping(value = "/tokenValidate")
    public JsonRestResponse tokenValidate() {
        return RestUtil.createResponse();
    }

    @ApiOperation(value = "后台用户密码登录", notes = "后台用户密码登录", response = BackUser.class)
    @PostMapping("/loginByPassword")
    public JsonRestResponse loginByPassword(@ApiParam(value = "账户", required = true) @RequestParam String account,
                                            @ApiParam(value = "密码", required = true) @RequestParam String password) {
        UserInfo userInfo = backUserService.loginByPassword(account, password);
        return RestUtil.createResponse(userInfo);
    }

    @ApiOperation(value = "后台用户新增或修改", notes = "后台用户新增或修改", response = BackUser.class)
    @PostMapping("/insertOrUpdateBackUser")
    public JsonRestResponse insertOrUpdateBackUser(@RequestBody BackUser backUser) {
        backUserService.insertOrUpdateBackUser(backUser);
        return RestUtil.createResponse();
    }

    /* @ApiOperation(value = "手机验证码登录", notes = "手机验证码登录", response = Users.class)
     @PostMapping("/loginByPhoneCode")
     public JsonRestResponse loginByPhoneCode(@ApiParam(value = "手机号", required = true) @RequestParam(required = true) String account,
                                             @ApiParam(value = "手机验证码", required = true) @RequestParam(required = true) String phoneCode) {
         UserPO userPO = usersService.loginByPhoneCode(account, phoneCode);
         return RestUtil.createResponse(userPO);
     }*/
    @ApiOperation(value = "后台用户删除", notes = "后台用户删除", response = Void.class)
    @PostMapping("/deleteBackUser")
    public JsonRestResponse deleteBackUser(
            @ApiParam(value = "ids", required = true) @RequestParam(required = true) String ids
    ) {
        backUserService.deleteBackUserById(ids);
        return RestUtil.createResponse();
    }

    @ApiOperation(value = "后台用户详情", notes = "后台用户详情", response = BackUser.class)
    @GetMapping("/selectBackUserId")
    public JsonRestResponse selectBackUserId(
            @ApiParam(value = "id", required = true) @RequestParam String id
    ) {
        return RestUtil.createResponse(backUserService.selectBackUserById(id));
    }

    @ApiOperation(value = "修改密码", notes = "修改密码", response = Void.class)
    @GetMapping("/updateUserPassWordByIdAll")
    public JsonRestResponse updateUserPassWordByIdAll(@RequestBody UserPassWordVo userPassWordVo) {
        backUserService.updateUserPassWordByIdAll(userPassWordVo);
        return RestUtil.createResponse();
    }
    @ApiOperation(value = "退出登录", notes = "退出登录", response = Void.class)
    @GetMapping("/logout")
    public JsonRestResponse logout() {
        backUserService.logout();
        return RestUtil.createResponse();
    }
    @ApiOperation(value = "绑定用户", notes = "绑定用户", response = Void.class)
    @GetMapping("/bindEnterpriseUsers")
    public JsonRestResponse bindEnterpriseUsers(
            @ApiParam(value = "userId", required = true) @RequestParam String userId

    ) {
        backUserService.bindEnterpriseUsers(userId);
        return RestUtil.createResponse();
    }
    @ApiOperation(value = "解除绑定", notes = "绑定用户", response = Void.class)
    @GetMapping("/cancelTheBinding")
    public JsonRestResponse cancelTheBinding() {
        backUserService.cancelTheBinding();
        return RestUtil.createResponse();
    } 
    
    /*********************************前端用户***************************************************************/
   
    @ApiOperation(value = "查询前端用户列表", notes = "查询前端用户列表", response = Users.class)
    @GetMapping("/selectUsersList")
    public JsonRestResponse selectUsersList(
    		@ApiParam(value = "当前页", required = true) @RequestParam Integer num,
            @ApiParam(value = "每页条数", required = true) @RequestParam Integer size,
        	@ApiParam(value = "手机号码") @RequestParam(required = false) String phone,
            @ApiParam(value = "用户类型（1-员工端；2-管理端）") @RequestParam(required = false) String userType,
            @ApiParam(value = "昵称") @RequestParam(required = false) String nickName,
            @ApiParam(value = "部门id") @RequestParam(required = false) String teamId,
            @ApiParam(value = "担任职务【1：部门经理 2：员工】") @RequestParam(required = false) String assumeOffice,
            @ApiParam(value = "工种id") @RequestParam(required = false) String workId,
            @ApiParam(value = "是否启用") @RequestParam(required = false) String isUsing
    	) {
       
        return RestUtil.createResponse(new PageInfo<>(this.usersService.selectUsersList(num, size, phone, userType, nickName, teamId, assumeOffice, workId, isUsing)));
    } 
   
}