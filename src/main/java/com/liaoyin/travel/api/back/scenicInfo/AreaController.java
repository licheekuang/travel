package com.liaoyin.travel.api.back.scenicInfo;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.entity.scenicInfo.AreaCity;
import com.liaoyin.travel.entity.scenicInfo.AreaDistrict;
import com.liaoyin.travel.entity.scenicInfo.AreaProvince;
import com.liaoyin.travel.service.scenicInfo.AreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * TODO
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-22 13:49
 */
@RestController
@Api(tags={"区域管理"})
@RequestMapping("/back/areaManagement")
public class AreaController {

    Logger logger = LoggerFactory.getLogger(AreaController.class);

    @Resource
    AreaService areaService;

    @ApiOperation(value = "查询省级区域", notes = "查询省级区域", response= AreaProvince.class)
    @PostMapping(value = "/selectAreaProvince")
    public JsonRestResponse selectAreaProvince(){
        List<AreaProvince> list = this.areaService.selectAreaProvince();
        return RestUtil.createResponse(list);
    }

    @ApiOperation(value = "根据省级区域id查询市级区域信息", notes = "根据省级区域id查询市级区域信息", response= AreaCity.class)
    @PostMapping(value = "/selectAreaCityByProvinceId")
    public JsonRestResponse selectAreaCityByProvinceId(@ApiParam(value = "省编码id", required = true) @RequestParam String provinceId){
        List<AreaCity> list = this.areaService.selectAreaCityByProvinceId(provinceId);
        return RestUtil.createResponse(list);
    }

    @ApiOperation(value = "根据市级区域id查询区县区域信息", notes = "根据市级区域id查询区县区域信息", response= AreaDistrict.class)
    @PostMapping(value = "/selectAreaDistrictByCityId")
    public JsonRestResponse selectAreaDistrictByCityId(@ApiParam(value = "市编码id", required = true) @RequestParam String cityId){
        logger.info("===start===");
        long startTime = System.currentTimeMillis();
        List<AreaDistrict> list = this.areaService.selectAreaDistrictByCityId(cityId);
        long endTime = System.currentTimeMillis();
        logger.error("test error!");
        logger.debug("costTime[{}ms]",endTime-startTime);
        logger.info("===end===");
        return RestUtil.createResponse(list);
    }
}
