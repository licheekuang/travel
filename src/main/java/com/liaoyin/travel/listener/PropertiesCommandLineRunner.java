package com.liaoyin.travel.listener;

import com.liaoyin.travel.ueditor.ConfigManager;
import com.liaoyin.travel.util.ParamUtil;
import com.liaoyin.travel.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.servlet.MultipartConfigElement;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

@Component
public class PropertiesCommandLineRunner implements CommandLineRunner {

    @Value("${project.properties.files}")
    private String files;

    @Autowired
    private ConfigManager configManager;

    /*@Value("${project.properties.ueditorConfig}")
    private String config;*/

    /**
     * 加载 message.properties(保存异常信息的文件) 到 ParamUtil类 中
     * @param args
     * @throws Exception
     */
	@Override
    public void run(String... args) throws Exception {
    	//configManager.initEnv();//加载富文本配置文件
    	multipartConfigElement();//文件上传配置
    	ParamUtil.getGlobalSystemParamListAll();
        System.err.println("files="+files);
    	try {
           if (StringUtils.isNoneBlank(files)) {
                Properties prop = new Properties();
                files = files.trim();
               System.err.println("files.trim()="+files.trim());
                String[] f = files.split(",");

                for (String fileName : f) {
                    System.err.println("fileName ="+fileName);
                    prop.load(new InputStreamReader(PropertiesCommandLineRunner.class.getClassLoader().getResourceAsStream(fileName), "UTF-8"));
                }
               System.err.println("prop="+prop);
                PropertiesUtil.init(prop);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	/**
	* @date:2018/3/5 14:02
	* @author:hejr
	* @description: 文件上传配置
	*/
	@Bean
	public MultipartConfigElement multipartConfigElement() {
	  MultipartConfigFactory factory = new MultipartConfigFactory();
	  //单个文件最大
	  factory.setMaxFileSize("50MB");
	  /// 设置总上传数据总大小
	  factory.setMaxRequestSize("1024MB");
	  return factory.createMultipartConfig();
	}
	
}
