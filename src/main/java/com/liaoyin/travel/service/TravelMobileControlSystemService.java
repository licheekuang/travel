package com.liaoyin.travel.service;

import com.alibaba.fastjson.JSON;

import com.liaoyin.travel.dao.UsersMapper;
import com.liaoyin.travel.dao.task.TimingTaskParameterMapper;
import com.liaoyin.travel.service.task.TimingTaskParameterService;
import com.liaoyin.travel.view.travelMobileControlSystem.AttendanceView;
import com.liaoyin.travel.dao.attendance.AttendanceManagementMapper;
import com.liaoyin.travel.service.attendance.ManagementService;
import com.liaoyin.travel.util.MD5Util;
import com.liaoyin.travel.util.RestTemplateUtil;


import com.liaoyin.travel.view.travelMobileControlSystem.StaffInfoView;
import com.liaoyin.travel.view.travelMobileControlSystem.TaskAddView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 旅游移动管控系统 service
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-30 15:36
 */
@Service
public class TravelMobileControlSystemService {

    @Autowired
    ManagementService managementService;

    @Autowired
    UsersService usersService;

    @Autowired
    TimingTaskParameterService timingTaskParameterService;

    @Resource
    AttendanceManagementMapper attendanceManagementMapper;

    @Resource
    UsersMapper usersMapper;

    @Resource
    TimingTaskParameterMapper timingTaskParameterMapper;

    /**
     * 请求地址
     */
    @Value("${travelControl.url}")
    private String url;

    /**
     * 合作商标志编码
     */
    @Value("${travelControl.appKey}")
    private String appKey;

    /**
     * 验签秘钥
     */
    @Value("${travelControl.appSecret}")
    private String appSecret;

    /**
     * 修改员工信息接口
     */
    private final String staffInfoModify = "/travelControl/staffInfo/modify";

    /**
     * 考勤信息新增接口
     */
    private final String attendanceAdd = "/travelControl/attendance/add";

    /**
     * 生成任务计划接口
     */
    private final String taskAdd = "/travelControl/task/add";

    /**
     * 考虑到数据量大的情况,如果总数据超过了这个数量限制就分批次调用
     */
    private final Integer pageSize = 2000;

    /**
     * @方法名：callStaffInfoModify
     * @描述： 调用【考勤信息新增接口】
     * @作者： kjz
     * @日期： Created in 2020/3/31 14:02
     */
    public void callAttendanceAdd(){
        String callUrl = url+attendanceAdd;

        Integer count = attendanceManagementMapper.countAttendanceManagement();
        //考虑数据量大的情况,超过pageSize就分别传入
        if(count>pageSize){
            int pages = getPagesByCountAndPageSize(count,pageSize);
            for(int i =1;i<=pages;i++){
                List<AttendanceView> list = this.managementService.getAttendanceViewListByPage(i,pageSize);
                if(list.size()>0){
                    String json = JSON.toJSONString(list);
                    String result = post(callUrl,json);
                }
            }
        }else{
            List<AttendanceView> list = this.managementService.getAttendanceViewList();
            if(list.size()>0){
                String json = JSON.toJSONString(list);
//                System.err.println("json="+json);
               String result = post(callUrl,json);
                System.out.println("考勤信息新增接口="+result);
            }
        }

    }

    /**
     * @方法名：callStaffInfoModify
     * @描述： 调用【修改员工信息接口】
     * @作者： kjz
     * @日期： Created in 2020/3/31 18:22
     */
    public void callStaffInfoModify(){
        String callUrl = url+staffInfoModify;

        Integer count = usersMapper.countUsers();
        //考虑数据量大的情况,超过pageSize就分别传入
        if(count>pageSize){
            int pages = getPagesByCountAndPageSize(count,pageSize);
            for(int i =1; i <= pages;i++){
                List<StaffInfoView> list = this.usersService.getStaffInfoViewListPage(i,pageSize);
                if(list.size()>0){
                    String json = JSON.toJSONString(list);
                    String result = post(callUrl,json);
                }
            }
        }else{
            List<StaffInfoView> list = this.usersService.getStaffInfoViewList();
            if(list.size()>0){
                String json = JSON.toJSONString(list);
//                System.err.println("json="+json);
                String result = post(callUrl,json);
                System.out.println("修改员工信息接口="+result);
            }
        }

    }

    /**
     * @方法名：callTaskAdd
     * @描述： 调用【生成任务计划接口】
     * @作者： kjz
     * @日期： Created in 2020/4/1 16:05
     */
    public void callTaskAdd(){
        String callUrl = url+taskAdd;

        Integer count = timingTaskParameterMapper.countTimingTaskParameter();
        //考虑数据量大的情况,超过pageSize就分别传入
        if(count>pageSize){
            int pages = getPagesByCountAndPageSize(count,pageSize);
            for(int i =1;i<=pages;i++){
                List<TaskAddView> list = this.timingTaskParameterService.getTaskAddViewListPage(i,pageSize);
                if(list.size()>0){
                    String json = JSON.toJSONString(list);
                    String result = post(callUrl,json);
                }
            }
        }else{
            List<TaskAddView> list = this.timingTaskParameterService.getTaskAddViewList();
            if(list.size()>0){
                String json = JSON.toJSONString(list);
//                System.err.println("json="+json);
                String result = post(callUrl,json);
                System.out.println("生成任务计划接口="+result);
            }
        }
    }

    /**
     * @方法名：getPagesByCountAndPageSize
     * @描述： 通过总数据量和分页大小得到总页数
     * @作者： kjz
     * @日期： Created in 2020/4/1 17:25
     */
    public int getPagesByCountAndPageSize(Integer count, Integer pageSize) {
        //总页数
        int pages = count/pageSize;
        if(count % pageSize > 0){
            pages += 1;
        }
        return pages;
    }

    /**
     * @方法名：post
     * @描述： post调用旅游云移动管控接口
     * @作者： kjz
     * @日期： Created in 2020/3/31 15:42
     */
    public String post(String url, String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        headers.add("appKey",appKey);
        String timestamp = String.valueOf(System.currentTimeMillis());
        System.err.println("timestamp="+timestamp);
        headers.add("timestamp", timestamp);
        String sign = MD5Util.MD5Upper(json+timestamp+appSecret);
        System.err.println("sign="+sign);
        headers.add("sign", sign);
        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);
        return RestTemplateUtil.getInstance().postForObject(url, requestEntity, String.class);
    }



}
