package com.liaoyin.travel.service;


import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.vo.GlobalSystemParamVo;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.GlobalSystemParamMapper;
import com.liaoyin.travel.entity.GlobalSystemParam;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.ParamUtil;
import com.liaoyin.travel.util.UUIDUtil;

import java.util.Date;
import java.util.List;

@Service
public class GlobalSystemParamService extends BaseService<GlobalSystemParamMapper, GlobalSystemParam> {


    public List<GlobalSystemParam> selectGlobalSystemParamList(GlobalSystemParamVo globalSystemParamVo) {
    	PageHelper.startPage(globalSystemParamVo.getNum(),globalSystemParamVo.getSize());
    	List<GlobalSystemParam> list = this.mapper.selectGlobalSystemParamList(globalSystemParamVo);
    	return list;
    }

    public void insertGlobalSystemParam(GlobalSystemParam globalSystemParam) {
       String id = UUIDUtil.getUUID();
    	globalSystemParam.setId(id);
        globalSystemParam.setCreateTime(new Date());
        this.mapper.insertSelective(globalSystemParam);
        if(globalSystemParam.getConpouId()!= null && !"".equals(globalSystemParam.getConpouId())) {
        	String[] couponId = globalSystemParam.getConpouId().split(",");
        	
        }
    }


    public void updateGlobalSystemParam(GlobalSystemParam globalSystemParam) {
    	if(globalSystemParam.getId()== null) {
    		throw new BusinessException("id.is.null","id为空");
    	}
    	 if(globalSystemParam.getConpouId()!= null && !"".equals(globalSystemParam.getConpouId())) {
         	String[] couponId = globalSystemParam.getConpouId().split(",");
         	if(couponId != null && couponId.length >0) {
         	}
         }
    	
        this.mapper.updateByPrimaryKeySelective(globalSystemParam);
    }

    public GlobalSystemParam selectGlobalSystemParamListByCondition(GlobalSystemParam globalSystemParam) {
        return null;
    }

    
    public GlobalSystemParam selectGlobalSystemParamById(String id) {
    	if(id == null) {
    		throw new BusinessException("id.is.null","id为空");
    	}
    	GlobalSystemParam g = this.mapper.selectByPrimaryKey(id);
    	if(g != null && !"".equals(g)) {
    		
    	}
    	return g;
    }
	/**
	 * @Description 悬赏金手续费设置
	 * @Author  rzy
	 * @date 2018/12/18 18:02
	 */
	public void  updateRewardServiceCharge(Integer serviceCharge){
		if (serviceCharge==null || serviceCharge>100 ||serviceCharge<0){
			throw new BusinessException("rest.fail","手续费设置错误");
		}
		this.mapper.updateRewardServiceCharge(serviceCharge);
		//更新系统参数
		ParamUtil.getGlobalSystemParamListAll();
	}
	public GlobalSystemParam selectRewardGold(){
		return this.mapper.selectRewardGold();
	}
}
