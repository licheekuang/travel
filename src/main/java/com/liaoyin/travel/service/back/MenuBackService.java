package com.liaoyin.travel.service.back;

import org.springframework.stereotype.Service;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.MenuMapper;
import com.liaoyin.travel.entity.Menu;

/**
 * @program: travel
 * @Description: 后台菜单管理
 * @author: rzy
 * @create: 2018-11-28 15:46
 **/
@Service
public class MenuBackService extends BaseService<MenuMapper,Menu> {
}
