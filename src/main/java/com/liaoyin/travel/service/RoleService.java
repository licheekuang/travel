package com.liaoyin.travel.service;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.dao.RoleFuncMapper;
import com.liaoyin.travel.dao.RoleMapper;
import com.liaoyin.travel.dao.RoleMenuMapper;
import com.liaoyin.travel.entity.FileUpload;
import com.liaoyin.travel.entity.Menu;
import com.liaoyin.travel.entity.Role;
import com.liaoyin.travel.entity.RoleFunc;
import com.liaoyin.travel.entity.RoleMenu;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.FileUploadUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service
public class RoleService extends BaseService<RoleMapper, Role>{

	@Resource
	private RoleMenuMapper roleMenuMapper;
	@Resource
	private RoleFuncMapper roleFuncMapper;
	@Autowired
	private MenuService menuService;
	/**
	 * 
	 * 方   法  名：selectRoleByID
	 * 创建日期：2017年11月25日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：根据ID查询角色
	 * @param id
	 * @return
	 */
	@ReadOnlyProperty
	public Role selectRoleByID(String id){
		Role Role = this.mapper.selectByPrimaryKey(id);
		if(Role==null) {
			throw new BusinessException("msg.Role.isNull");
		}
		if(Role.getRoleIcon() != null && !"".equals(Role.getRoleIcon())) {
			Role.setFileupload(FileUploadUtil.getFileUpload(Role.getRoleIcon()));
		}
		return Role;
	}

	/**
	 * 
	 * 方   法  名：insertRole
	 * 创建日期：2017年11月25日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：新增角色
	 * @param Role
	 */
	@Transactional(rollbackFor = Exception.class)
	public void insertRole(Role Role) {
		String id= UUIDUtil.getUUID();
		if(Role.getFileupload() != null && !"".equals(Role.getFileupload())) {
			FileUploadUtil.saveFileUpload(Role.getFileupload(),id);
		}
		Role.setIsDelete(false);
		Role.setCreateTime(new Date());
		Role.setCreateTime(new Date());
		Role.setId(id);
		try {			
			//选择继承角色
			if(Role.getExtendRoleId() != null && !"".equals(Role.getExtendRoleId())) {
				//复制权限
				List<RoleMenu> rmList = this.roleMenuMapper.selectMenuByMenuId(Role.getExtendRoleId());
				if(rmList!=null && rmList.size() >0) {
					for(RoleMenu r:rmList) {
						r.setRoleId(id);
					}
				}
				/*//复制功能
				List<RoleFunc> funList = this.roleFuncMapper.selectFunctionByroleId(Role.getExtendRoleId());
				if(funList!=null && funList.size() >0){
					for(RoleFunc rf:funList) {
						rf.setRoleId(id);
					}
				}*/
				//this.roleFuncMapper.insertList(funList);				
				this.roleMenuMapper.insertList(rmList);				
			}
			this.mapper.insertSelective(Role);
		} catch (Exception e) {
			// TODO: handle exception
		}
	
	}
	
	/**
	 * 
	 * 方   法  名：updateRole
	 * 创建日期：2017年11月25日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：修改角色
	 * @param
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateRole(Role rl) {
		Role r = this.mapper.selectByPrimaryKey(rl.getId());
		if(r == null) {
			throw new BusinessException("msg.Role.isNull");
		}
		if(rl.getFileupload() != null) {
			FileUpload fileupload = rl.getFileupload();
			r.setRoleIcon(FileUploadUtil.saveFileUpload(rl.getFileupload(), rl.getId()));
		}
		
		r.setRoleDesc(rl.getRoleDesc());
		r.setRoleName(rl.getRoleName());
		r.setUpdateTime(new Date());
		r.setAttr1(rl.getAttr1());
		r.setRoleAffiliate(rl.getRoleAffiliate());
		r.setExtendRoleId(rl.getExtendRoleId());
		
		try {
			//选择继承角色
			if(rl.getExtendRoleId() != null && !"".equals(rl.getExtendRoleId())) {
				//首先删除此角色已有的权限
				this.roleMenuMapper.deleteRoleMenuByroleId(rl.getId());
				//复制权限
				List<RoleMenu> rmList = this.roleMenuMapper.selectMenuByMenuId(rl.getExtendRoleId());
				if(rmList!=null && rmList.size() >0) {
					for(RoleMenu ro:rmList) {
						ro.setRoleId(rl.getId());
					}
				}
				this.roleMenuMapper.insertList(rmList);					
				//复制功能
				/*List<RoleFunc> funList = this.roleFuncMapper.selectFunctionByroleId(rl.getExtendRoleId());
				if(funList!=null && funList.size() >0){
					//首先删除此角色已有的功能
					this.roleFuncMapper.deleteFunctionByroleId(rl.getId());
					for(RoleFunc rf:funList) {
						rf.setRoleId(rl.getId());
					}
				}
				this.roleFuncMapper.insertList(funList);*/
			}
			this.mapper.updateByPrimaryKeySelective(r);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/**
	 * 
	 * 方   法  名：selectRoleListByUserId
	 * 创建日期：2017年11月25日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：查询角色列表
	 * @param ,roleName
	 * @return
	 */
	public List<Role> selectRoleList(Integer num,Integer size,String createUserId,String roleName,String roleAffiliate,String attr1){
		PageHelper.startPage(num,size);
		if(roleName!=null && !"".equals(roleName)) {
			roleName = roleName.trim();
		}
		List<Role> list = this.mapper.selectRoleList(createUserId,roleName,roleAffiliate,attr1);
		if(list != null &&list.size() >0) {
			for(Role r :list) {
				if(r.getRoleIcon() != null && !"".equals(r.getRoleIcon())) {
					FileUpload ico = FileUploadUtil.getFileUpload(r.getRoleIcon());
					if(ico!=null) {
						r.setFullFilePath(CommonConstant.FILE_SERVER+ico.getFilePath());
					}
				}
				
			}
		}
		return list;
	}
	

	/**
	 * @方法名：deleteRole
	 * @描述： 删除角色列表
	 * @作者： li.jing
	 * @日期： 2017年11月25日 下午4:32:28
	 */
	@Transactional(rollbackFor = Exception.class)
	public Integer deleteRole(String id,String deleteUserId){
		
		Role r = this.mapper.selectByPrimaryKey(id);
		r.setDeleteTime(new Date());
		r.setDeleteUserId(deleteUserId);
		r.setIsDelete(true);
		int k = 0;
		//判断当前角色下是否绑定有用户
		Integer i = this.mapper.selectIsExistBackUser(id);
		if (i>0){
			throw new BusinessException("isExist.backUser");
		}
		//首先删除此角色已有的权限
		this.roleMenuMapper.deleteRoleMenuByroleId(id);
		//删除此角色的所有功能
		this.roleFuncMapper.deleteFunctionByroleId(id);
		try {			
			this.mapper.updateByPrimaryKey(r);
		} catch (Exception e) {
			// TODO: handle exception
			k = 1;
		}
		return k;
	}

	/**
	 * @方法名：copyThisRoleById
	 * @描述： 根据id复制角色
	 * @作者： li.jing
	 * @日期： 2017年12月02日 下午4:32:28
	 */
	@ReadOnlyProperty
	public Integer copyThisRoleById(String id){
		Role Role = this.mapper.selectByPrimaryKey(id);
		int k = 0;
		if(Role==null) {
			throw new BusinessException("msg.Role.isNull");
		}
		
		Role.setId(UUIDUtil.getUUID());
		Role.setRoleName(Role.getRoleName()+"-复制"+System.currentTimeMillis());
		this.insert(Role);
		//复制权限
		List<RoleMenu> rmList = this.roleMenuMapper.selectMenuByMenuId(id);
		if(rmList!=null && rmList.size() >0) {
			for(RoleMenu r:rmList) {
				r.setRoleId(Role.getId());
			}
		}
		//复制功能
		List<RoleFunc> funList = this.roleFuncMapper.selectFunctionByroleId(id);
		if(funList!=null && funList.size() >0){
			for(RoleFunc rf:funList) {
				rf.setRoleId(Role.getId());
			}
		}
		if(rmList!= null && rmList.size() >0) {
			this.roleMenuMapper.insertList(rmList);
		}
		if(funList != null && funList.size() >0) {
			this.roleFuncMapper.insertList(funList);
		}
		return k;
	}

	/**
	 * 
	 * 方   法  名：selectRoleListByUserId
	 * 创建日期：2017年11月25日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：根据查询所有角色列表
	 * @param id
	 * @return
	 */
	public List<Role> selectAllRoleList(String id){
		List<Role> list = this.mapper.selectAllRoleList(id);
		
		return list;
	}
	
	/**
	 * 
	 * 方   法  名：selectRoleListByUserId
	 * 创建日期：2017年12月09日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：根据查询所有角色关联的菜单和功能数量
	 * @param id
	 * @return
	 */
	public Map<String, Object> selectRoleIdFindMenuAndFunction(String id){
		
		Map<String, Object> map = new HashMap<String,Object>();
		List<RoleMenu> rmList = this.roleMenuMapper.selectMenuByMenuId(id);
		List<RoleFunc> funList = this.roleFuncMapper.selectFunctionByroleId(id);
		map.put("menuNum", rmList.size());
		map.put("funNum", funList.size());
		return map;
	}

	/**
	 * @方法名：updateRoleThisMenuAndFunc
	 * @描述： 添加角色的菜单和功能
	 * @作者： li.jing
	 * @日期： 2017年12月09日 下午4:32:28
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateRoleThisMenuAndFunc(String id,String funId,String menuId){
		List<RoleFunc> insertfunList = new ArrayList<RoleFunc>();
		List<RoleMenu> insertMenuList = new ArrayList<RoleMenu>();
		
		List<String> list = new ArrayList<String>();

		//角色和功能的关联
		if(funId != null && !"".equals(funId)) {
			String[] fun = funId.split(",");
			if(fun.length >0) {
				List<RoleFunc> funList = this.roleFuncMapper.selectFunctionByroleId(id);			
				RoleFunc insertR = null;
				if(funList!=null && funList.size() >0) {
					list.clear();
					for(String s: fun) {
						list.add(s);
						for(RoleFunc r:funList) {
							if(s!=null && !"".equals(s)&& s.equals(r.getFuncId())) {								
								list.remove(s);
								break;
							}
						}
					}
					if(list!= null && list.size() >0) {
						for(String s: list) {
							insertR = new RoleFunc();
							insertR.setFuncId(s);
							insertR.setRoleId(id);
							insertR.setId(UUIDUtil.getUUID().replaceAll("-",""));	
							insertfunList.add(insertR);
							insertR = null;
						}
					}
					
				}else {
					for(String s: fun) {
						insertR = new RoleFunc();
						insertR.setFuncId(s);
						insertR.setRoleId(id);
						insertR.setId(UUIDUtil.getUUID().replaceAll("-",""));	
						insertfunList.add(insertR);
						insertR = null;
					}
				}
				
			}
		}


		list.clear();

		//角色和菜单的关联
		if(menuId != null && !"".equals(menuId)) {
			String[] menu = menuId.split(",");
			List<RoleMenu> rmList =null;
			if(menu.length >0) {
				RoleMenu insertM= null;
				rmList = this.roleMenuMapper.selectMenuByMenuId(id);												
				if(rmList!=null && rmList.size()>0) {					
					for(String s:menu) {						
						list.add(s);
						for(RoleMenu r:rmList) {
							if(s.equals(r.getMenuId())) {													
								list.remove(s);
								break;
							}
						}
					}					
					if(list.size() >0) {
						for(String s:list) {							
							insertM = new RoleMenu();
							insertM.setId(UUIDUtil.getUUID());
							insertM.setMenuId(s);
							insertM.setRoleId(id);
							insertM.setCreateTime(LocalDateTime.now());
							insertMenuList.add(insertM);
							insertM = null;			
						}
					}					
					//删除数据库中已授权，本次取消授权的项
					this.roleMenuMapper.deleteRoleMenuByMenuIdS(menu,id);					
				}else {
					for(String s:menu) {
						if(s!=null && !"".equals(s)) {
							insertM = new RoleMenu();
							insertM.setId(UUIDUtil.getUUID().replaceAll("-",""));
							insertM.setMenuId(s);
							insertM.setRoleId(id);
							insertMenuList.add(insertM);
							insertM = null;
						}
					}					
				}				
			}	
		}
		if(insertfunList != null && insertfunList.size() >0) {
			this.roleFuncMapper.insertFunctionByroleId(insertfunList);
		}
		
		if(insertMenuList != null && insertMenuList.size() >0) {
			this.roleMenuMapper.insertList(insertMenuList);
		}
		list = null;
	}
	
	/**
	 * 
	 * 方   法  名：selectRoleThisMenuAndFuncByRoleId
	 * 创建日期：2017年12月09日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：根据角色id查询分配的菜单和功能
	 * @param id
	 * @return
	 */
	public Map<String, Object> selectRoleThisMenuAndFuncByRoleId(String id){
		
		Map<String, Object> map = new HashMap<String,Object>();
		List<RoleMenu> rmList = this.roleMenuMapper.selectMenuByMenuId(id);
		List<String> rmStrList = new ArrayList<String>();
		if(rmList != null && rmList.size() >0) {
			for(RoleMenu rm:rmList) {
				rmStrList.add(rm.getMenuId());
			}
		}		
		List<RoleFunc> funList = this.roleFuncMapper.selectFunctionByroleId(id);
		List<String> rfStrList = new ArrayList<String>();
		if(funList!=null && funList.size() >0) {
			for(RoleFunc rf:funList) {
				rfStrList.add(rf.getFuncId());
			}
		}
		map.put("menu",rmStrList);
		map.put("func", rfStrList);
		return map;
	}
	
	
	/**
	 * 
	 * 方   法  名：selectbooleanByRoleId
	 * 创建日期：2017年12月09日 下午4:32:28
	 * 创   建  者：li.jing
	 * 描          述：判断角色有没操作商家权限
	 * @param
	 * @return
	 */
	public Boolean selectbooleanByRoleId(String roleId,String menuId) {
		boolean b = false;
		List<Menu> list = this.menuService.selectMenuListByRoleId(roleId);
		if(list != null && list.size() >0) {
			for(Menu m:list) {
				if(m.getId().equals(menuId)) {
					b = true;
					break;
				}
			}
		}
		return b;
	}
}
