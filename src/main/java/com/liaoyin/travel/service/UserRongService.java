package com.liaoyin.travel.service;
import com.liaoyin.travel.constant.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.liaoyin.travel.vo.rongyun.SendMessageVo;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.UserRongMapper;
import com.liaoyin.travel.entity.UserRong;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.AES256EncryptionUtil;
import com.liaoyin.travel.util.RongUtils;
import com.liaoyin.travel.util.UUIDUtil;
import com.qiniu.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserRongService extends BaseService<UserRongMapper, UserRong> {
	
	@Autowired
	private UsersService usersService;

    public UserRong selectTokenByUserId(String id,String type) {
        //JSONObject jsonObject = new JSONObject();
        UserRong userRong = new UserRong();
        userRong.setUserId(id);
        List<UserRong> list = this.mapper.select(userRong);
        if (!list.isEmpty()) {
            return list.get(0);
        } else {
            try {
                String tonken = RongUtils.getToken(id,type);
                if(tonken == null) {
                	throw new BusinessException("rong.is.null");
                }
                userRong = new UserRong();
                userRong.setToken(tonken);
                userRong.setId(UUIDUtil.getUUID());
                userRong.setUserId(id);
                userRong.setCreateTime(new Date());
                this.mapper.insertSelective(userRong);
                return userRong;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    /***
     * 
         * @方法名：pushMessage
         * @描述： 提供给AI的消息推送接口
         * @作者： lijing
         * @日期： 2019年8月17日
     */
    public void pushMessage(SendMessageVo sendMessageVo) {
    
    	if(sendMessageVo.getMessageBody() == null) {
    		throw new BusinessException("body.is.null");
    	}
    	if(sendMessageVo.getType() == null) {
    		throw new BusinessException("body.is.null");
    	}
    	//参数解密
    	String message = AES256EncryptionUtil.Aes256Decode(sendMessageVo.getMessageBody(),CommonConstant.AES_KEY);
    	if(message == null) {
    		throw new BusinessException("body.is.null");
    	}
    	//System.out.println(message);
    	JSONObject json = JSONObject.parseObject(message);
    	String interfaceType = json.getString("interfaceType");
    	String interfaceName = json.getString("interfaceName");
    	String code = json.getString("code");
    	String messgae = json.getString("messgae");
    	String sendUserId = json.getString("sendUserId");
    	if(interfaceType==null || interfaceName ==null || code==null || messgae==null||sendUserId==null) {
    		throw new BusinessException("body.is.null");
    	}
    	//获取所有的管理端
    	List<String> list = this.usersService.selectUsersIdListByManager("");
    	if(list == null || list.size() ==0) {
    		throw new BusinessException("");
    	}
    	String toUserId = StringUtils.join(list, "&");
		JSONObject js = JSONObject.parseObject(message);
		js.put("content",sendMessageVo.getType()+","+messgae);
		js.put("extra",interfaceName);
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("fromUserId", sendUserId);//发送人用户 Id
    	map.put("toUserId", toUserId);//接收用户Id
    	map.put("objectName","RC:TxtMsg");//消息类型
    	map.put("content", js.toString());//发送消息内容
    	String b="";
    	json=null;
    	js = null;
    	list = null;
    	try {
			b = RongUtils.groupManagement(map, "6");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	map=null;
    	if(!"SUCCESS".equals(b)) {
    		throw new BusinessException("rest.fail");
    	}
    	
    }
    
    
}
