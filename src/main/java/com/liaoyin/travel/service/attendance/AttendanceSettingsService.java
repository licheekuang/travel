package com.liaoyin.travel.service.attendance;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.vo.attendance.AttendanceSettingVO;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.attendance.AttendanceSettingsMapper;
import com.liaoyin.travel.entity.attendance.AttendanceSettings;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.DateUtil;
import com.liaoyin.travel.util.ParamUtil;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @项目名：旅投
 * @作者：lijing
 * @描述：考勤设置
 * @日期：Created in 2019/7/10 14:55
 */
@Service
public class AttendanceSettingsService extends BaseService<AttendanceSettingsMapper, AttendanceSettings> {
   
	 private Log logger = LogFactory.getLog(this.getClass());
	/***
	 * 
	     * @方法名：selectAttendanceSettingsListBack
	     * @描述： 查询考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public List<AttendanceSettings> selectAttendanceSettingsListBack(Integer num,Integer size,String checkName,String isUsing,String teamId){
		
		PageHelper.startPage(num,size);
		List<AttendanceSettings> list = this.mapper.selectAttendanceSettingsList(checkName, isUsing,teamId);
		return list;
	}
	
	/****
	 * 
	     * @方法名：insertOrUpdateAttendanceSettings
	     * @描述： 新增或修改考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public void insertOrUpdateAttendanceSettings(AttendanceSettings attendanceSettings) {
		
		UserInfo user = PartyUtil.getCurrentUserInfo();
		if(user == null) {
			throw new BusinessException("not.login");
		}
		if(attendanceSettings.getId()!=null && !"".equals(attendanceSettings.getId())) {
			attendanceSettings.setUpdateTime(new Date());
			attendanceSettings.setUpdateUserId(user.getId());
			this.mapper.updateByPrimaryKeySelective(attendanceSettings);
			logger.info(user.getAccount()+"于"+DateUtil.formatyyyyMMddHHmmss(new Date())+"新增修改设置"+attendanceSettings.getId());
		}else {
			AttendanceSettings att= selectAttendanceSettingsByCheckShiftsAndTeamId(attendanceSettings.getCheckShifts()+"", attendanceSettings.getTeamId());
			if(att!=null) {
				throw new BusinessException("attendanceSettings.is.exit");
			}
			String id = UUIDUtil.getUUID();
			attendanceSettings.setId(id);
			attendanceSettings.setCreatTime(new Date());
			attendanceSettings.setCreatUserId(user.getId());
			attendanceSettings.setIsDelete((short)0);
			attendanceSettings.setIsUsing((short)1);
			this.mapper.insertSelective(attendanceSettings);
			logger.info(user.getAccount()+"于"+DateUtil.formatyyyyMMddHHmmss(new Date())+",新增考勤设置"+id);
		}
	}
	
	/***
	 * 
	     * @方法名：selectAttendanceSettingsById
	     * @描述： 根据id查询考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public AttendanceSettings selectAttendanceSettingsById(String id) {
		
		if(id == null) {
			throw new BusinessException("id.is.null");
		}
		AttendanceSettings att = this.mapper.selectByPrimaryKey(id);
		return att;
	}
	
	/*****
	 * 
	     * @方法名：selectAttendanceSettingsByCheckShiftsAndTeamId
	     * @描述： 根据班次查询打卡设置
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	public AttendanceSettings selectAttendanceSettingsByCheckShiftsAndTeamId(String checkShifts,String teamId) {
		
		AttendanceSettings att = this.mapper.selectAttendanceSettingsByCheckShiftsAndTeamId(checkShifts.trim(), teamId);
		return att;
	}

	/**
	 * @author 王海洋
	 * @methodName: getCurrentAttendanceSetting
	 * @methodDesc: 获取当前需要打卡的打卡班次信息
	 * @description:
	 * @param:
	 * @return
	 * @create 2019-10-25 17:25
	 **/
	public List<AttendanceSettingVO> getCurrentAttendanceSetting(){
		UserInfo user = PartyUtil.getCurrentUserInfo();

		System.err.println("user="+user);

		if(user == null) {
			throw new BusinessException("user.isNotLogin");
		}

		return this.mapper.getCurrentAttendanceSetting(user.getId());
	}
	
	/****
	 * 
	     * @方法名：deleteAttendanceSettingsById
	     * @描述： 删除考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public void deleteAttendanceSettingsById(String ids){
		
		if(ids == null) {
			throw new BusinessException("id.is.null");
		}
		UserInfo user = PartyUtil.getCurrentUserInfo();
		if(user == null) {
			throw new BusinessException("not.login");
		}
		String[] id = ids.split(",");
		this.mapper.deleteAttendanceSettingsById(id, user.getId());
		
	}
	
	/****
	 * 
	     * @方法名：selectAttendanceSettingsByEquipmentId
	     * @描述： 根据设备id查询考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	public List<AttendanceSettings> selectAttendanceSettingsByEquipmentId(String[] equipmentId){
		
		List<AttendanceSettings> list= this.mapper.selectAttendanceSettingsByEquipmentId(equipmentId);
		return list;
	}
	
	/****
	 * 
	     * @方法名：selectMAXcheckShifts
	     * @描述： 查询最大的班次
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	public AttendanceSettings selectMAXcheckShifts(String teamId) {
		
		AttendanceSettings att = this.mapper.selectMAXcheckShifts(teamId);
		return att;
	}
	
	
	/*************************************APP***********************************************************************/
	
	/****
	 * 
	     * @方法名：selectAttendanceSettingsListOnMoble
	     * @描述： 前端查询考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public List<AttendanceSettings> selectAttendanceSettingsListOnMoble(UserInfo user){
		if(user == null) {
			UserInfo userInfo = PartyUtil.getCurrentUserInfo();
			if(userInfo== null) {
				throw new BusinessException("not.login");
			}
			user = userInfo;
			userInfo=null;
		}
		
		List<AttendanceSettings> list = null;
		//判断打卡班次
		String isDailyAttendanceCardOpened = ParamUtil.getValue("isDailyAttendanceCardOpened");
		/*if(isDailyAttendanceCardOpened !=null && "1".equals(isDailyAttendanceCardOpened)) {
			//开启班组打卡模式
			if(user.getTeamId()!=null && !"".equals(user.getTeamId())) {
				list= this.mapper.selectAttendanceSettingsList("","1",user.getTeamId());
			}else {
				throw new BusinessException("team.is.null");
			}
		}else {
			//全员通用打卡模式
			list= this.mapper.selectAttendanceSettingsList("","1","");
		}*/
		//全员通用打卡模式
		list= this.mapper.selectAttendanceSettingsList("","1","");
		return list;
	}
}
