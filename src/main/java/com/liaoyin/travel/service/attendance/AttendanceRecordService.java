package com.liaoyin.travel.service.attendance;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.attendance.AttendanceRecordMapper;
import com.liaoyin.travel.entity.approval.OutgoingStatusDisplay;
import com.liaoyin.travel.entity.attendance.AttendanceManagement;
import com.liaoyin.travel.entity.attendance.AttendanceRecord;
import com.liaoyin.travel.entity.attendance.AttendanceWorkday;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.DictBasicService;
import com.liaoyin.travel.service.approval.OutgoingStatusDisplayService;
import com.liaoyin.travel.util.*;
import com.liaoyin.travel.view.moble.attendance.AttendanceSetView;
import com.liaoyin.travel.view.moble.attendance.AttendanceWeekDayView;
import com.liaoyin.travel.view.moble.attendance.LackOfCardRecordView;
import com.liaoyin.travel.vo.approval.AgreedGoOutVo;
import com.liaoyin.travel.vo.approval.AgreedToAskForLeaveVo;
import com.liaoyin.travel.vo.attendance.*;
import com.liaoyin.travel.vo.request.AttendanceRecordRequestVO;
import com.liaoyin.travel.vo.request.MobileAttendanceStatRequestVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 考勤记录service
 *
 * @author kuang.jiazhuo
 * @date 2019-11-19 14:12
 */
@Service
public class AttendanceRecordService extends BaseService<AttendanceRecordMapper, AttendanceRecord> {

    @Resource
    private ManagementService managementService;
    @Resource
    private AttendanceWorkdayService attendanceWorkdayService;
    @Resource
    private AttendanceNewService attendanceNewService;
    @Resource
    private OutgoingStatusDisplayService outgoingStatusDisplayService;

    @Autowired
    private DictBasicService dictBasicService;

    /**
     * @方法名：insertOrUpdateAttendanceRecord
     * @描述： 新增或修改打卡记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/20 10:53
     */
    @Transactional(rollbackFor=BusinessException.class)
    public int insertOrUpdateAttendanceRecord(InsertOrUpdateAttendanceRecordVo insertOrUpdateAttendanceRecordVo) {
        //返回的结果
        int result = 0;
        /** 先判断登录与否顺便拿到用户信息 */
        UserInfo userInfo = PartyUtil.getCurrentUserInfo();
        if(userInfo == null) {
            throw new BusinessException("user.isNotLogin");
        }
        String userId = userInfo.getId();

        /** 景区id */
        String scenicId = userInfo.getScenicId();

        //打卡传入的经度
        String chockLongitude = insertOrUpdateAttendanceRecordVo.getChockLongitude();

        //打卡传入的纬度
        String chockLatitude = insertOrUpdateAttendanceRecordVo.getChockLatitude();

        //打卡时传入的班次
        String clockFrequency = insertOrUpdateAttendanceRecordVo.getClockFrequency();

        if(chockLongitude==null || chockLatitude==null){
            throw new BusinessException("punch.in.latitude.and.longitude");
        }

        /** 业务逻辑判断 */
        //获取到本用户考勤管理的打卡信息
        AttendanceManageVo attendanceManageVo = this.managementService.selectAttendanceManageVoByUserId(userId);
        if(attendanceManageVo==null){
            throw new BusinessException("attendance.point.not.added");
        }

        //考勤管理设置的经度
        String setLog = attendanceManageVo.getLongitude();
        //考勤管理设置的纬度
        String setLat = attendanceManageVo.getLatitude();
        //考勤管理设置的范围
        double attendanceRange = attendanceManageVo.getAttendanceRange();
        /** 判断是否在打卡范围内*/
        boolean determineScope = this.attendanceNewService.determineIfItIsInTheAttendanceRange(chockLongitude,chockLatitude,setLog,setLat,attendanceRange);
        if(!determineScope){
            throw new BusinessException("range.is.out");
        }

        //管理设置的上班打卡时间
        LocalTime attendanceStartTime = attendanceManageVo.getAttendanceStartTime();
        //管理机设置的下班打卡时间
        LocalTime attendanceEndTime = attendanceManageVo.getAttendanceEndTime();

        /** 要把设置的上下班打卡时间格式化成 HH:mm 的字符串*/
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        //设置的下班打卡时间的 HH:mm 字符串
        String attendanceEndTimeFormatter = attendanceEndTime.format(formatter);
        //设置的上班打卡时间的 HH:mm 字符串
        String attendanceStartTimeFormatter = attendanceStartTime.format(formatter);
        //当天(今天)是星期几
        Integer whatDay = DateUtil.getWeekDay();

        /** 判断今天是不是考勤工作日*/
        AttendanceWorkday attendanceWorkday = this.attendanceWorkdayService.selectAttendanceWorkdayByAttendanceIdAndWhatDay(attendanceManageVo.getId(),whatDay);
        if(attendanceWorkday==null){
            throw new BusinessException("you.can.not.punch.in.today");
        }

        //今天的日期
       LocalDate nowDate = LocalDate.now();
        //查询今天考勤记录，如果存在记录，说明打卡当天是否已经打过卡或者有请假外出的审批
        AttendanceRecord attendanceRecord = this.mapper.selectAttendanceRecordByUserIdAndDateOfAttendance(userId,nowDate);


        //当前时间 HH:mm 的字符串
        String localTimeNowFormatter = LocalTime.now().format(formatter);


        //如果当天没有考勤记录,说明当天没有请假外出，正常执行打卡的流程
        if(attendanceRecord==null){
            /** 新增*/
            AttendanceRecord insertRecord = new AttendanceRecord()
                    .setId(SnowIdUtil.getInstance().nextId())
                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                    .setCreateTime(LocalDateTime.now())
                    .setUserId(userId)
                    .setWhatDay(whatDay)
                    .setDateOfAttendance(nowDate)
                    .setAttendanceId(attendanceManageVo.getId())
                    .setClockFrequency(1)
                    //景区id
                    .setScenicId(scenicId);

            /** 1.如果新增时,当前时间>=下班打卡时间,那就只能打下班卡，上班缺卡*/
            if(attendanceEndTimeFormatter.compareTo(localTimeNowFormatter)<=0){

                //不能打上班卡
                if(VariableConstants.STRING_CONSTANT_1.equals(clockFrequency)){
                    throw new BusinessException("It's.closing.time");
                }

                //下班打卡的考勤状态：正常
                String clockOutState = VariableConstants.STRING_CONSTANT_1;
                //上班考勤状态:缺卡
                String clockInState = VariableConstants.STRING_CONSTANT_2;
                //当天的最终考勤状态:缺卡
                String finalAttendanceStatus = getFinalAttendanceStatus(clockInState,clockOutState);

                insertRecord
                        .setClockInState(clockInState)
                        .setClockOutState(clockOutState)
                        .setFinalAttendanceStatus(finalAttendanceStatus)
                        .setClockOutTime(LocalTime.now())
                        .setChockOutLongitude(chockLongitude)
                        .setChockOutLatitude(chockLatitude);

            }else{
                /** 2.如果新增时,当前时间小于下班设置时间，则是打上班卡*/
                //不能打上班卡
                if(VariableConstants.STRING_CONSTANT_1.equals(clockFrequency)){
                    insertRecord.setClockInTime(LocalTime.now())
                            .setChockInLongitude(chockLongitude)
                            .setChockInLatitude(chockLatitude);

                    /** 2.1 当前时间>上班打卡的设置时间，则上班打卡迟到*/
                    if(localTimeNowFormatter.compareTo(attendanceStartTimeFormatter)>0){
                        insertRecord.setClockInState(VariableConstants.STRING_CONSTANT_3);
                    }else{
                        /** 2.2 当前时间<=上班打卡的设置时间，则上班打卡正常*/
                        insertRecord.setClockInState(VariableConstants.STRING_CONSTANT_1);
                    }
                }else if(VariableConstants.STRING_CONSTANT_2.equals(clockFrequency)){
                    throw new BusinessException("It's.late.for.work");
                }
            }
            result = this.mapper.insertSelective(insertRecord);

        }else if(attendanceRecord != null){
            /** 更新(走到这个分支,是打过上班卡或者当天有请假外出的)*/

            //上班考勤状态
            String clockInState = attendanceRecord.getClockInState();
            //下班打卡的考勤状态
            String clockOutState = attendanceRecord.getClockOutState();
            /** 打上班卡 */
            if(VariableConstants.STRING_CONSTANT_1.equals(clockFrequency)){
                //上班考勤状态如果是请假或外出,直接不能打卡
                if(VariableConstants.STRING_CONSTANT_6.equals(clockInState)){
                    throw new BusinessException("not.clock.in.when.you.ask.for.leave");
                }
                if(VariableConstants.STRING_CONSTANT_12.equals(clockInState)){
                    throw new BusinessException("not.punch.out.when.you're.out");
                }
                if(VariableConstants.STRING_CONSTANT_2.equals(clockInState)){
                    throw new BusinessException("work.is.already.short.of.cards");
                }
                if(clockInState==null){
                    //没有请假外出，也没打过卡
                    /** 1.已经超过或刚好等于下班时间*/
                    if(attendanceEndTimeFormatter.compareTo(localTimeNowFormatter)<=0) {
                        //不能打上班卡
                        throw new BusinessException("It's.closing.time");
                    }else{
                        /** 2. 当前时间>上班打卡的设置时间，则上班打卡迟到*/
                        if(localTimeNowFormatter.compareTo(attendanceStartTimeFormatter)>0){
                            attendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_3);
                            //如果当天下午请假或外出
                            if(VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
                                //迟到+请假
                                attendanceRecord.setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_7);
                            }else if(VariableConstants.STRING_CONSTANT_12.equals(clockOutState)){
                                //迟到+外出 = 迟到
                                attendanceRecord.setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_3);
                            }
                        }else{
                            /** 3. 当前时间<=上班打卡的设置时间，则上班打卡正常*/
                            attendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_1);
                            //如果当天下午请假或外出
                            if(VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
                                //请假
                                attendanceRecord.setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_6);
                            }else if(VariableConstants.STRING_CONSTANT_12.equals(clockOutState)){
                                //正常+外出 = 正常
                                attendanceRecord.setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_1);
                            }
                        }
                    }

                }
                attendanceRecord.setClockInTime(LocalTime.now()).setChockInLongitude(chockLongitude).setChockInLatitude(chockLatitude);
            }

            /** 打下班卡*/
            if(VariableConstants.STRING_CONSTANT_2.equals(clockFrequency)){
                //下班考勤状态如果是请假或外出,直接不能打卡
                if(VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
                    throw new BusinessException("not.clock.in.when.you.ask.for.leave");
                }
                if(VariableConstants.STRING_CONSTANT_12.equals(clockOutState)){
                    throw new BusinessException("not.punch.out.when.you're.out");
                }
                if(clockOutState==null){
                    //没有请假外出，也没打过卡
                    /** 1.还没有到下班时间*/
                    if(attendanceEndTimeFormatter.compareTo(localTimeNowFormatter)>0) {
                        //不能打下班卡
                        throw new BusinessException("you.can't.punch.in.before.work.time");
                    }
                    /** 2. 当前时间>=下班打卡的设置时间，则正常下班打卡*/
                    if(localTimeNowFormatter.compareTo(attendanceEndTimeFormatter)>=0){
                        clockOutState = VariableConstants.STRING_CONSTANT_1;
                        attendanceRecord.setClockOutState(clockOutState).setFinalAttendanceStatus(getFinalAttendanceStatus(clockInState,clockOutState));
                    }
                }
                attendanceRecord.setClockOutTime(LocalTime.now()).setChockOutLongitude(chockLongitude).setChockOutLatitude(chockLatitude);

            }
            int frequency = attendanceRecord.getClockFrequency()==null?0:attendanceRecord.getClockFrequency();
            attendanceRecord.setUpdateTime(LocalDateTime.now()).setClockFrequency(frequency+1);
            result = this.mapper.updateByPrimaryKeySelective(attendanceRecord);
        }
            return result;
    }

    /**
     * @方法名：selectAttendanceRecordByUserIdAndDateOfAttendance
     * @描述： 根据userId和考勤日期查询考勤打卡记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/20 20:20
     */
    public AttendanceRecord selectAttendanceRecordByUserIdAndDateOfAttendance(String userId, LocalDate date) {
        AttendanceRecord attendanceRecord = this.mapper.selectAttendanceRecordByUserIdAndDateOfAttendance(userId,date);
        return attendanceRecord;
    }

    /**
     * 定时任务新增或更新考勤记录
     * @param attendanceRecord
     * @param sign【1.更新;2.新增】
     */
    @Transactional(rollbackFor=BusinessException.class)
    public int updateAttendanceRecordByTiming(AttendanceRecord attendanceRecord, int sign ) {
        if(1==sign){
            attendanceRecord.setUpdateTime(LocalDateTime.now());
            return  this.mapper.updateByPrimaryKeySelective(attendanceRecord);
        }else{
            attendanceRecord.setId(SnowIdUtil.getInstance().nextId())
                    .setCreateTime(LocalDateTime.now())
                    .setIsDelete(VariableConstants.STRING_CONSTANT_0);
            return this.mapper.insertSelective(attendanceRecord);
        }
    }

    /**
     * @方法名：selectLackOfCardRecordByLoginUser
     * @描述： 获取登录员工的缺卡记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/22 23:41
     */
    public List<LackOfCardRecordView> selectLackOfCardRecordByLoginUser() {
        /** 先判断登录与否顺便拿到用户信息 */
        UserInfo userInfo = PartyUtil.getCurrentUserInfo();
        if(userInfo == null) {
            throw new BusinessException("user.isNotLogin");
        }
        String userId = userInfo.getId();
        List<LackOfCardRecordView> lackOfCardRecordViews = new ArrayList<>();
        List<AttendanceRecord> list = this.mapper.selectLackOfCardRecordByUserId(userId);
        if(list==null){
            throw new BusinessException("no.card.shortage");
        }
        list.forEach(attendanceRecord -> {
            //外出开始时间:HH:mm
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

            LackOfCardRecordView lackOfCardRecordView = new LackOfCardRecordView();
            AttendanceManagement attendanceManagement = this.managementService.selectAttendanceManagementById(attendanceRecord.getAttendanceId());
            String attendanceStartTimeStr = null;
            String attendanceEndTimeStr = null;
            LocalTime attendanceStartTime = LocalTime.MIDNIGHT;
            LocalTime attendanceEndTime = LocalTime.MIDNIGHT;
            if(attendanceManagement!=null){
                attendanceStartTime = attendanceManagement.getAttendanceStartTime();
                attendanceEndTime = attendanceManagement.getAttendanceEndTime();

                attendanceStartTimeStr = attendanceStartTime.format(formatter);
                attendanceEndTimeStr = attendanceEndTime.format(formatter);
            }

            String finalAttendanceStatus = attendanceRecord.getFinalAttendanceStatus();
            LocalDate dateOfAttendance = attendanceRecord.getDateOfAttendance();

            String dateStr = DateUtil.localDateSwitchString(dateOfAttendance);
            Integer whatDay = attendanceRecord.getWhatDay();
            String whatDayStr = DateUtil.numberSwitchWhatDayStr(whatDay);
            String assemble = dateStr+" "+whatDayStr;
            String lackOfCardDivisions = "";
            System.err.println("lackOfCardDivisions="+lackOfCardDivisions);
            /** 缺卡+迟到 ==》肯定是下午缺卡*/
            if(VariableConstants.STRING_CONSTANT_4.equals(finalAttendanceStatus)){
                lackOfCardDivisions = assemble + " "+"下午";
                lackOfCardRecordView.setShift(VariableConstants.STRING_CONSTANT_2)
                        .setClockTime(attendanceEndTimeStr).setClockTimeDisplay(LocalDateTime.of(dateOfAttendance,attendanceEndTime));
            }else if(VariableConstants.STRING_CONSTANT_5.equals(finalAttendanceStatus)){
                /** 如果是旷工，那么要新增两条缺卡逻辑*/
                lackOfCardDivisions = assemble + " "+"上午";
                lackOfCardRecordView.setShift(VariableConstants.STRING_CONSTANT_1)
                        .setClockTime(attendanceStartTimeStr).setClockTimeDisplay(LocalDateTime.of(dateOfAttendance,attendanceStartTime));
                //多加一条下午打卡
                String xiaWuqueKaXianShi = assemble + " "+"下午";
                LackOfCardRecordView xiaWuDaKa = new LackOfCardRecordView()
                        .setShift(VariableConstants.STRING_CONSTANT_2)
                        .setClockTime(attendanceEndTimeStr)
                        .setClockTimeDisplay(LocalDateTime.of(dateOfAttendance,attendanceEndTime))
                        .setLackOfCardId(attendanceRecord.getId())
                        .setLackOfCardDivisions(xiaWuqueKaXianShi);
                lackOfCardRecordViews.add(xiaWuDaKa);
            } else if(VariableConstants.STRING_CONSTANT_2.equals(finalAttendanceStatus)){
                /** 请假和缺卡的要考虑是上午还是下午缺卡*/
                if(VariableConstants.STRING_CONSTANT_2.equals(attendanceRecord.getClockInState())){
                    lackOfCardDivisions = assemble + " "+"上午";
                    lackOfCardRecordView.setShift(VariableConstants.STRING_CONSTANT_1)
                            .setClockTime(attendanceStartTimeStr).setClockTimeDisplay(LocalDateTime.of(dateOfAttendance,attendanceStartTime));
                }
                if(VariableConstants.STRING_CONSTANT_2.equals(attendanceRecord.getClockOutState())){
                    lackOfCardDivisions = assemble + " "+"下午";
                    lackOfCardRecordView.setShift(VariableConstants.STRING_CONSTANT_2)
                            .setClockTime(attendanceStartTimeStr).setClockTimeDisplay(LocalDateTime.of(dateOfAttendance,attendanceEndTime));
                }
            }
            System.err.println("lackOfCardDivisions="+lackOfCardDivisions);
            lackOfCardRecordView.setLackOfCardDivisions(lackOfCardDivisions).setLackOfCardId(attendanceRecord.getId());
            lackOfCardRecordViews.add(lackOfCardRecordView);
        });
        return lackOfCardRecordViews;
    }

    /**
     * @author 王海洋
     * @methodName: getLackRecord
     * @methodDesc: 获取缺卡
     * @description:
     * @param: [mobileAttendanceStatRequestVO]
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * @create 2019-11-29 11:12
     **/
    public List<Map<String,String>> getLackRecord(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){

        List<AttendanceRecord> list = this.mapper.getLackRecord(mobileAttendanceStatRequestVO);
        if(list==null){
            return null;
        }
        List<Map<String,String>> lackList = Lists.newArrayList();
        list.forEach(attendanceRecord -> {
            //外出开始时间:HH:mm
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

            Map<String,String> map = Maps.newHashMap();
            AttendanceManagement attendanceManagement = this.managementService.getById(attendanceRecord.getAttendanceId());
            String attendanceStartTimeStr = "";
            String attendanceEndTimeStr = "";
            LocalTime attendanceStartTime = LocalTime.MIDNIGHT;
            LocalTime attendanceEndTime = LocalTime.MIDNIGHT;
            if(attendanceManagement!=null){
                attendanceStartTime = attendanceManagement.getAttendanceStartTime();
                attendanceEndTime = attendanceManagement.getAttendanceEndTime();

                attendanceStartTimeStr = attendanceStartTime.format(formatter);
                attendanceEndTimeStr = attendanceEndTime.format(formatter);
            }

            String finalAttendanceStatus = attendanceRecord.getFinalAttendanceStatus();
            LocalDate dateOfAttendance = attendanceRecord.getDateOfAttendance();

            String dateStr = DateUtil.localDateSwitchString(dateOfAttendance);
            Integer whatDay = attendanceRecord.getWhatDay();
            String whatDayStr = DateUtil.numberSwitchWhatDayStr(whatDay);
            String assemble = dateStr+" "+whatDayStr;
            String lackOfCardDivisions = "";
            map.put("weekDay",whatDay.toString());
            map.put("attendanceStatus","缺卡");
            map.put("timeLong","");
            map.put("endTime",attendanceEndTimeStr);
            /** 缺卡+迟到 ==》肯定是下午缺卡*/
            if(VariableConstants.STRING_CONSTANT_4.equals(finalAttendanceStatus)){
                lackOfCardDivisions = attendanceEndTimeStr;//"下午"
                map.put("startTime",attendanceEndTimeStr);
                map.put("endTime","");
            }else if(VariableConstants.STRING_CONSTANT_5.equals(finalAttendanceStatus)){
                /** 如果是旷工，那么要新增两条缺卡逻辑*/
                lackOfCardDivisions = attendanceStartTimeStr;//"上午"
                map.put("startTime",attendanceStartTimeStr);
                //多加一条下午打卡
                String xiaWuqueKaXianShi = attendanceEndTimeStr;//"下午"
                Map<String,String> noonMap = Maps.newHashMap();
                noonMap.put("weekDay",whatDay.toString());
                noonMap.put("attendanceDate",dateStr + " " + xiaWuqueKaXianShi);
                noonMap.put("attendanceStatus","缺卡");
                noonMap.put("timeLong","");
                noonMap.put("startTime","");
                noonMap.put("endTime","");
                lackList.add(noonMap);
            } else if(VariableConstants.STRING_CONSTANT_2.equals(finalAttendanceStatus)){
                /** 请假和缺卡的要考虑是上午还是下午缺卡*/
                if(VariableConstants.STRING_CONSTANT_2.equals(attendanceRecord.getClockInState())){
                    lackOfCardDivisions = attendanceStartTimeStr;//"上午"
                    map.put("startTime",attendanceStartTimeStr);
                    map.put("endTime","");
                }
                if(VariableConstants.STRING_CONSTANT_2.equals(attendanceRecord.getClockOutState())){
                    lackOfCardDivisions = attendanceEndTimeStr;//"下午"
                    map.put("startTime",attendanceEndTimeStr);
                    map.put("endTime","");
                }
            }
            map.put("attendanceDate",dateStr+ ' ' +lackOfCardDivisions);
            map.put("startTime","");
            lackList.add(map);
        });
        return lackList;
    }

    /**
     * @方法名：disposeCardReissueApproval
     * @描述： 补卡
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/24 13:02
     */
    @Transactional(rollbackFor=BusinessException.class)
    public int disposeCardReissueApproval(String shift, String attendanceRecordId) {
        int result = 0;
        AttendanceRecord attendanceRecord = this.selectAttendanceRecordById(attendanceRecordId);
       String clockInState = attendanceRecord.getClockInState();
       String clockOutState = attendanceRecord.getClockOutState();
        System.err.println("attendanceRecordId="+attendanceRecordId);
        System.err.println("shift="+shift);
        /** 上午补卡 */
        if(VariableConstants.STRING_CONSTANT_1.equals(shift)){
            result = this.mapper.onTheMorningOfCard(attendanceRecordId);
            if(result>0){
                //下午考勤状态-正常
                if(VariableConstants.STRING_CONSTANT_1.equals(clockOutState)){
                    //最终考勤状态-改为-正常
                    this.mapper.updateFinalAttendanceStatusSetOne(attendanceRecordId);
                }
                //下午考勤状态-请假
                if(VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
                    //最终考勤状态-改为-请假
                    this.mapper.updateFinalAttendanceStatusSetSix(attendanceRecordId);
                }
                //下午考勤状态-缺卡
                if(VariableConstants.STRING_CONSTANT_2.equals(clockOutState)){
                    //最终考勤状态-改为-缺卡
                    this.mapper.updateFinalAttendanceStatusSetTwo(attendanceRecordId);
                }
                //下午考勤状态-补卡
                if(VariableConstants.STRING_CONSTANT_9.equals(clockOutState)){
                    //最终考勤状态-改为-正常
                    this.mapper.updateFinalAttendanceStatusSetOne(attendanceRecordId);
                }
                //下午考勤状态-外出
                if(VariableConstants.STRING_CONSTANT_12.equals(clockOutState)){
                    //最终考勤状态-改为-正常
                    this.mapper.updateFinalAttendanceStatusSetOne(attendanceRecordId);
                }
            }
        }else{
            /** 下午补卡*/
            result = this.mapper.onTheAfternoonOfCard(attendanceRecordId);
            if(result>0){
                //上午考勤状态-正常
                if(VariableConstants.STRING_CONSTANT_1.equals(clockInState)){
                    //最终考勤状态-改为-正常
                    this.mapper.updateFinalAttendanceStatusSetOne(attendanceRecordId);
                }
                //上午勤状态-迟到
                if(VariableConstants.STRING_CONSTANT_3.equals(clockInState)){
                    //最终考勤状态-改为-迟到
                    this.mapper.updateFinalAttendanceStatusSetThree(attendanceRecordId);
                }
                //上午勤状态-请假
                if(VariableConstants.STRING_CONSTANT_6.equals(clockInState)){
                    //最终考勤状态-改为-请假
                    this.mapper.updateFinalAttendanceStatusSetSix(attendanceRecordId);
                }
                //上午考勤状态-缺卡
                if(VariableConstants.STRING_CONSTANT_2.equals(clockInState)){
                    //最终考勤状态-改为-缺卡
                    this.mapper.updateFinalAttendanceStatusSetTwo(attendanceRecordId);
                }
                //上午考勤状态-补卡
                if(VariableConstants.STRING_CONSTANT_9.equals(clockInState)){
                    //最终考勤状态-改为-正常
                    this.mapper.updateFinalAttendanceStatusSetOne(attendanceRecordId);
                }
                //上午考勤状态-外出
                if(VariableConstants.STRING_CONSTANT_12.equals(clockInState)){
                    //最终考勤状态-改为-正常
                    this.mapper.updateFinalAttendanceStatusSetOne(attendanceRecordId);
                }
            }
        }
        return result;
    }

    /**
     * @方法名：disposeAskForLeaveApproval
     * @描述： 请假
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/24 15:24
     */
    @Transactional(rollbackFor=BusinessException.class)
    public int disposeAskForLeaveApproval(AgreedToAskForLeaveVo agreedToAskForLeaveVo) {

        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        int result = 0;
        //被考勤的请假人id
        String initiatorId = agreedToAskForLeaveVo.getInitiatorId();
        //开始请假的日期
        LocalDate startLeaveDate = agreedToAskForLeaveVo.getStartLeaveDate();
        //开始请假当天的上下午标识
        String startLeaveTime = agreedToAskForLeaveVo.getStartLeaveTime();
        //结束请假的日期
        LocalDate leaveEndDate = agreedToAskForLeaveVo.getLeaveEndDate();
        //结束请假的上午下午的标识
        String leaveEndTime = agreedToAskForLeaveVo.getLeaveEndTime();
        //请假时长
        double duration = agreedToAskForLeaveVo.getDuration();
        /** 请假逻辑判断*/
        //查询到用户应该考勤的打卡时间和工作日信息
        AttendanceSetView attendanceSetView = this.attendanceNewService.selectAttendanceDateTime(initiatorId);
        //上班应该打卡的时间
        LocalTime clockInTime = attendanceSetView.getClockInTime();
        //下班应该打卡的时间
        LocalTime clockOutTime = attendanceSetView.getClockOutTime();

        //得到需要考勤的星期X(数字)
        List<Integer> attendanceWeeks = this.getAttendanceWeeks(attendanceSetView.getWeekDays());

        //返回申请人的考勤管理id
        String attendanceManagementId = this.managementService.selectManagementIdByUserId(initiatorId);
        //计算开始请假日期和结束请假日期的天数差
        System.err.println("startLeaveDate="+startLeaveDate);
        System.err.println("leaveEndDate="+leaveEndDate);
        int numberOfDaysDifference = DateUtil.calculateLocalDateDaysDifference(startLeaveDate,leaveEndDate);

        /** 开始请假和结束请假在同一天*/
        if(0==numberOfDaysDifference){
            //先判断当天是不是要参与考勤
            int week = startLeaveDate.getDayOfWeek().getValue();
            boolean isWorkday = this.isAttendance(week,attendanceWeeks);
            //在考勤工作日内
            if(isWorkday){
                AttendanceRecord ordAttendanceRecord = this.selectAttendanceRecordByUserIdAndDateOfAttendance(initiatorId,startLeaveDate);
                if(ordAttendanceRecord==null){
                    //新增
                    AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                            .setId(SnowIdUtil.getInstance().nextId())
                            .setUserId(initiatorId)
                            .setDateOfAttendance(startLeaveDate)
                            .setWhatDay(startLeaveDate.getDayOfWeek().getValue())
                            .setAttendanceId(attendanceManagementId)
                            .setCreateTime(LocalDateTime.now())
                            .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                            //景点id
                            .setScenicId(scenicId);
                    if("上午".equals(startLeaveTime) && "上午".equals(leaveEndTime)){
                        newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_6);
                    }else if("上午".equals(startLeaveTime) && "下午".equals(leaveEndTime)){
                        newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_6);
                        newAttendanceRecord.setClockOutState(VariableConstants.STRING_CONSTANT_6);
                    }else if("下午".equals(startLeaveTime) && "下午".equals(leaveEndTime)){
                        newAttendanceRecord.setClockOutState(VariableConstants.STRING_CONSTANT_6);
                    }
                    newAttendanceRecord.setFinalAttendanceStatus(getFinalAttendanceStatus(newAttendanceRecord.getClockInState(),newAttendanceRecord.getClockOutState()));
                    result = this.mapper.insertSelective(newAttendanceRecord);
                }else{
                    //更新
                    String clockInState = ordAttendanceRecord.getClockInState();
                    String clockOutState = ordAttendanceRecord.getClockOutState();
                    if("上午".equals(startLeaveTime) && "上午".equals(leaveEndTime)){
                        clockInState = VariableConstants.STRING_CONSTANT_6;
                    }else if("上午".equals(startLeaveTime) && "下午".equals(leaveEndTime)){
                        clockInState = VariableConstants.STRING_CONSTANT_6;
                        clockOutState =  VariableConstants.STRING_CONSTANT_6;
                    }else if("下午".equals(startLeaveTime) && "下午".equals(leaveEndTime)){
                        clockOutState =  VariableConstants.STRING_CONSTANT_6;
                    }
                    ordAttendanceRecord.setUpdateTime(LocalDateTime.now())
                            .setClockInState(clockInState)
                            .setClockOutState(clockOutState)
                            .setFinalAttendanceStatus(getFinalAttendanceStatus(clockInState,clockOutState));
                    result = this.mapper.updateAttendanceRecordSelective(ordAttendanceRecord);
                    System.err.println("clockInState="+clockInState);
                    System.err.println("clockOutState="+clockOutState);
                    System.err.println("当天已存在记录执行请假更新数量="+result);
                }

            }
        }
        /** 开始请假和结束请假耗时两天及以上*/
        if(1<=numberOfDaysDifference){
            //得到所有的请假日期
            List<LocalDate> localDates = DateUtil.getAscDateList(startLeaveDate,leaveEndDate);
            for(int i=0;i<localDates.size();i++){
                //先判断当天是不是要参与考勤
                LocalDate thatDay = localDates.get(i);
                int week = thatDay.getDayOfWeek().getValue();
                boolean isWorkday = this.isAttendance(week,attendanceWeeks);
                if(isWorkday){
                    AttendanceRecord ordAttendanceRecord = this.selectAttendanceRecordByUserIdAndDateOfAttendance(initiatorId,localDates.get(i));
                    if(0==i){
                        if(ordAttendanceRecord==null){
                            //新增
                            AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                                    .setId(SnowIdUtil.getInstance().nextId())
                                    .setUserId(initiatorId)
                                    .setDateOfAttendance(thatDay)
                                    .setWhatDay(thatDay.getDayOfWeek().getValue())
                                    .setAttendanceId(attendanceManagementId)
                                    .setCreateTime(LocalDateTime.now())
                                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                                    //景点id
                                    .setScenicId(scenicId);
                            if("上午".equals(startLeaveTime)){
                                newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_6);
                                newAttendanceRecord.setClockOutState(VariableConstants.STRING_CONSTANT_6);
                                newAttendanceRecord.setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_6);
                            }else if("下午".equals(startLeaveTime) ){
                                newAttendanceRecord.setClockOutState(VariableConstants.STRING_CONSTANT_6);
                            }
                            result += this.mapper.insertSelective(newAttendanceRecord);
                        }else{
                            //更新
                            String clockInState = ordAttendanceRecord.getClockInState();
                            String clockOutState = ordAttendanceRecord.getClockOutState();
                            if("上午".equals(startLeaveTime) ){
                                clockInState = VariableConstants.STRING_CONSTANT_6;
                                clockOutState =  VariableConstants.STRING_CONSTANT_6;
                            }else if("下午".equals(startLeaveTime)){
                                clockOutState =  VariableConstants.STRING_CONSTANT_6;
                            }
                            ordAttendanceRecord.setUpdateTime(LocalDateTime.now())
                                    .setClockInState(clockInState)
                                    .setClockOutState(clockOutState)
                                    .setFinalAttendanceStatus(getFinalAttendanceStatus(clockInState,clockOutState));
                            result += this.mapper.updateAttendanceRecordSelective(ordAttendanceRecord);
                        }
                    }else if(localDates.size()-1==i){
                        if(ordAttendanceRecord==null){
                            //新增
                            AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                                    .setId(SnowIdUtil.getInstance().nextId())
                                    .setUserId(initiatorId)
                                    .setDateOfAttendance(thatDay)
                                    .setWhatDay(thatDay.getDayOfWeek().getValue())
                                    .setAttendanceId(attendanceManagementId)
                                    .setCreateTime(LocalDateTime.now())
                                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                                    //景点id
                                    .setScenicId(scenicId);
                            if("上午".equals(leaveEndTime)){
                                newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_6);
                            }else if("下午".equals(leaveEndTime)){
                                newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_6);
                                newAttendanceRecord.setClockOutState(VariableConstants.STRING_CONSTANT_6);
                                newAttendanceRecord.setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_6);
                            }
                            result += this.mapper.insertSelective(newAttendanceRecord);
                        }else{
                            //更新
                            String clockInState = ordAttendanceRecord.getClockInState();
                            String clockOutState = ordAttendanceRecord.getClockOutState();
                            if("上午".equals(leaveEndTime) ){
                                clockInState = VariableConstants.STRING_CONSTANT_6;
                            }else if("下午".equals(leaveEndTime)){
                                clockInState = VariableConstants.STRING_CONSTANT_6;
                                clockOutState =  VariableConstants.STRING_CONSTANT_6;
                            }
                            ordAttendanceRecord.setUpdateTime(LocalDateTime.now())
                                    .setClockInState(clockInState)
                                    .setClockOutState(clockOutState)
                                    .setFinalAttendanceStatus(getFinalAttendanceStatus(clockInState,clockOutState));
                            result += this.mapper.updateAttendanceRecordSelective(ordAttendanceRecord);
                        }
                    }else{
                        if(ordAttendanceRecord==null){
                            //新增
                            AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                                    .setId(SnowIdUtil.getInstance().nextId())
                                    .setUserId(initiatorId)
                                    .setDateOfAttendance(thatDay)
                                    .setWhatDay(thatDay.getDayOfWeek().getValue())
                                    .setAttendanceId(attendanceManagementId)
                                    .setCreateTime(LocalDateTime.now())
                                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                                    .setClockInState(VariableConstants.STRING_CONSTANT_6)
                                    .setClockOutState(VariableConstants.STRING_CONSTANT_6)
                                    .setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_6)
                                    //景点id
                                    .setScenicId(scenicId);
                            result += this.mapper.insertSelective(newAttendanceRecord);
                        }else{
                            //更新
                            ordAttendanceRecord.setUpdateTime(LocalDateTime.now())
                                    .setClockInState(VariableConstants.STRING_CONSTANT_6)
                                    .setClockOutState(VariableConstants.STRING_CONSTANT_6)
                                    .setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_6);
                            result += this.mapper.updateAttendanceRecordSelective(ordAttendanceRecord);
                        }

                    }
                }

            }

        }
        return result;
    }

    /**
     * @方法名：disposeGoOutApproval
     * @描述： 外出
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/24 19:54
     */
    @Transactional(rollbackFor=BusinessException.class)
    public int disposeGoOutApproval(AgreedGoOutVo agreedGoOutVo) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        //返回结果
        int result = 0;

        /**  本次审批所传参数 */

        //审批发起人的id
        String initiatorId = agreedGoOutVo.getInitiatorId();

        //外出开始时间
        LocalDateTime departureTime = agreedGoOutVo.getDepartureTime();

        //外出结束时间
        LocalDateTime endOfOuting = agreedGoOutVo.getEndOfOuting();

        //外出共计时长
        String duration = agreedGoOutVo.getDuration();

        //外出开始的日期: yyyy-HH-mm
        LocalDate absenceStartDate = departureTime.toLocalDate();

        //外出结束的日期: yyyy-HH-mm
        LocalDate endOfOutingDate = endOfOuting.toLocalDate();

        //外出开始时间:HH:mm
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        String departureTimeHHmm =  departureTime.toLocalTime().format(formatter);

        //外出结束时间:HH:mm
        String endOfOutingTimeHHmm = endOfOuting.toLocalTime().format(formatter);

        //返回申请人的考勤管理id
        String attendanceManagementId = this.managementService.selectManagementIdByUserId(initiatorId);


        /**  用于本次比较判断的管理设置参数 */

        //查询到用户应该考勤的打卡时间和工作日信息
        AttendanceSetView attendanceSetView = this.attendanceNewService.selectAttendanceDateTime(initiatorId);
        //上班应该打卡的时间
        LocalTime clockInTime = attendanceSetView.getClockInTime();
        String clockInTimeStr = clockInTime.format(formatter);

        //下班应该打卡的时间
        LocalTime clockOutTime = attendanceSetView.getClockOutTime();
        String clockOutTimeStr = clockOutTime.format(formatter);

        //得到需要考勤的星期数
        List<Integer> attendanceWeeks = this.getAttendanceWeeks(attendanceSetView.getWeekDays());

        //计算开始外出日期和结束外出日期的天数差
        int numberOfDaysDifference = DateUtil.calculateLocalDateDaysDifference(absenceStartDate,endOfOutingDate);

        /** 开始外出和结束外出在同一天*/
        if(0==numberOfDaysDifference){
            //先判断当天是不是考勤工作日,决定是否要参与考勤
            int week = absenceStartDate.getDayOfWeek().getValue();
            boolean isWorkday = this.isAttendance(week,attendanceWeeks);
            if(isWorkday){
                //外出时间显示
                String outGoingDisplay = DateUtil.getOutGoingDisplayDate(departureTime,endOfOuting);
                //判断外出日期是否大于等于今天
                boolean isGteThatDay =DateUtil.judgeOutOfTimeIsGteThatDay(absenceStartDate);
                //是否执行定时任务
                boolean isTimedTask = true;
                //先判断当天有没有已存在的考勤记录
                AttendanceRecord oldAttendanceRecord = this.selectAttendanceRecordByUserIdAndDateOfAttendance(initiatorId,absenceStartDate);
                if(oldAttendanceRecord==null){
                    //新增
                    String newRecordId = SnowIdUtil.getInstance().nextId();
                    AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                            .setId(newRecordId)
                            .setUserId(initiatorId)
                            .setDateOfAttendance(absenceStartDate)
                            .setWhatDay(absenceStartDate.getDayOfWeek().getValue())
                            .setAttendanceId(attendanceManagementId)
                            .setCreateTime(LocalDateTime.now())
                            .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                            //景点id
                            .setScenicId(scenicId);

                    //1.外出开始时间 <= 上班时间 && 外出结束时间 >= 下班时间 ==》 当天最终考勤外出(正常)
                    if(departureTimeHHmm.compareTo(clockInTimeStr)<=0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>=0){
                        newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_12)
                                .setClockOutState(VariableConstants.STRING_CONSTANT_12)
                                .setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_1)
                                .setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                .setGoOutDateDisplay(outGoingDisplay);
                        isTimedTask = false;
                    }
                    //2.外出开始时间 <= 上班时间 && 外出结束时间 < 下班时间 ==》 上午考勤
                    if(departureTimeHHmm.compareTo(clockInTimeStr)<=0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)<0){
                        newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_12)
                                .setGoOutDateDisplay(outGoingDisplay);
                    }
                    //3.上班时间 < 外出开始时间< 下班时间 && 外出结束时间 >= 下班时间  ==》 下午考勤
                    if(departureTimeHHmm.compareTo(clockInTimeStr)>0 &&
                            departureTimeHHmm.compareTo(clockOutTimeStr)<0 &&
                            endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>=0){
                        newAttendanceRecord.setClockOutState(VariableConstants.STRING_CONSTANT_12)
                                .setGoOutDateDisplay(outGoingDisplay);
                    }
                    //4.上班时间<开始外出时间 && 结束外出时间<下班时间
                    if(departureTimeHHmm.compareTo(clockInTimeStr)>0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)<0){
                        newAttendanceRecord.setGoOutDateDisplay(outGoingDisplay);
                    }
                    //5.无效时间:外出开始时间 >= 下班时间 或 外出结束时间 <= 上班时间
                    if((departureTimeHHmm.compareTo(clockOutTimeStr)>=0 || endOfOutingTimeHHmm.compareTo(clockInTimeStr)<=0)){
                        //无效时间也不做定时任务
                        isTimedTask = false;
                    }
                    //外出日期如果大于等于今天
                    if(isGteThatDay){
                        //如果不是全天外出,需要实时更新外出的状态(定时任务)
                        if(isTimedTask){
                            OutgoingStatusDisplay outgoingStatusDisplay = new OutgoingStatusDisplay()
                                    .setAttendanceRecordId(newRecordId)
                                    .setDepartureTime(departureTime)
                                    .setEndOfOuting(endOfOuting)
                                    .setClockInTime(clockInTimeStr)
                                    .setClockOutTime(clockOutTimeStr);
                            //新增外出状态定时任务信息
                            int num = this.outgoingStatusDisplayService.insertOutgoingStatusDisplay(outgoingStatusDisplay);
                            System.err.println("新增num="+num);
                        }
                    }
                    result = this.mapper.insertSelective(newAttendanceRecord);
                }else{
                    //更新
                    String clockInState = oldAttendanceRecord.getClockInState();
                    String clockOutState = oldAttendanceRecord.getClockOutState();
                    //1.外出开始时间 <= 上班时间 && 外出结束时间 >= 下班时间 ==》 当天最终考勤外出
                    if(departureTimeHHmm.compareTo(clockInTimeStr)<=0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>=0){
                        clockInState = VariableConstants.STRING_CONSTANT_12;
                        clockOutState = VariableConstants.STRING_CONSTANT_12;
                        oldAttendanceRecord.setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                .setGoOutDateDisplay(outGoingDisplay);
                        isTimedTask = false;
                    }
                    //2.外出开始时间 <= 上班时间 && 外出结束时间 < 下班时间 ==》 上午考勤
                    if(departureTimeHHmm.compareTo(clockInTimeStr)<=0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)<0){
                        clockInState = VariableConstants.STRING_CONSTANT_12;
                        oldAttendanceRecord.setGoOutDateDisplay(outGoingDisplay);
                    }
                    //3.上班时间 < 外出开始时间< 下班时间 && 外出结束时间 >= 下班时间
                    if(departureTimeHHmm.compareTo(clockInTimeStr)>0 &&
                            departureTimeHHmm.compareTo(clockOutTimeStr)<0 &&
                            endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>=0){
                        clockOutState = VariableConstants.STRING_CONSTANT_12;
                        oldAttendanceRecord.setGoOutDateDisplay(outGoingDisplay);
                    }
                    //4.上班时间<开始外出时间 && 结束外出时间<下班时间
                    if(departureTimeHHmm.compareTo(clockInTimeStr)>0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)<0){
                        oldAttendanceRecord.setGoOutDateDisplay(outGoingDisplay);
                    }
                    //5.无效时间:外出开始时间 >= 下班时间 或 外出结束时间 <= 上班时间
                    if((departureTimeHHmm.compareTo(clockOutTimeStr)>=0 || endOfOutingTimeHHmm.compareTo(clockInTimeStr)<=0)){
                        //无效时间也不做定时任务
                        isTimedTask = false;
                    }
                    //外出日期如果大于等于今天
                    if(isGteThatDay){
                        //如果不是全天外出,需要实时更新外出的状态(定时任务)
                        if(isTimedTask){
                            OutgoingStatusDisplay outgoingStatusDisplay = new OutgoingStatusDisplay()
                                    .setAttendanceRecordId(oldAttendanceRecord.getAttendanceId())
                                    .setDepartureTime(departureTime)
                                    .setEndOfOuting(endOfOuting)
                                    .setClockInTime(clockInTimeStr)
                                    .setClockOutTime(clockOutTimeStr);
                            //新增外出状态定时任务信息
                            this.outgoingStatusDisplayService.insertOutgoingStatusDisplay(outgoingStatusDisplay);
                        }
                    }
                    oldAttendanceRecord.setClockInState(clockInState)
                            .setClockOutState(clockOutState)
                            .setFinalAttendanceStatus(getFinalAttendanceStatus(clockInState,clockOutState));
                    result = this.mapper.updateAttendanceRecordSelective(oldAttendanceRecord);
                }
            }
        }
        /** 开始外出的时间和结束外出的时间不止一天*/
        if(0<numberOfDaysDifference){
            //从小到大取出所有的外出日期
            List<LocalDate> localDates = DateUtil.getAscDateList(absenceStartDate,endOfOutingDate);
            for(int i=0;i<localDates.size();i++){
                //先判断当天是不是考勤工作日,决定是否要参与考勤
                LocalDate thatDay = localDates.get(i);
                int week = thatDay.getDayOfWeek().getValue();
                boolean isWorkday = this.isAttendance(week,attendanceWeeks);
                if(isWorkday){
                    //外出时间显示
                    String outGoingDisplay = DateUtil.getOutGoingDisplayDate(departureTime,endOfOuting);
                    //判断外出日期是否大于等于今天
                    boolean isGteThatDay =DateUtil.judgeOutOfTimeIsGteThatDay(absenceStartDate);
                    //是否执行定时任务
                    boolean isTimedTask = true;

                    AttendanceRecord oldAttendanceRecord = this.selectAttendanceRecordByUserIdAndDateOfAttendance(initiatorId,localDates.get(i));
                    if(0==i){
                        //外出的第一天只判断外出开始时间
                        if(oldAttendanceRecord==null){
                            //新增
                            String newRecordId = SnowIdUtil.getInstance().nextId();
                            AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                                    .setId(newRecordId)
                                    .setUserId(initiatorId)
                                    .setDateOfAttendance(absenceStartDate)
                                    .setWhatDay(absenceStartDate.getDayOfWeek().getValue())
                                    .setAttendanceId(attendanceManagementId)
                                    .setCreateTime(LocalDateTime.now())
                                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                                    //景点id
                                    .setScenicId(scenicId);
                            //1.外出开始时间 <= 上班时间 ==> 当天最终考勤外出(正常)
                            if(departureTimeHHmm.compareTo(clockInTimeStr)<=0){
                                newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_12)
                                        .setClockOutState(VariableConstants.STRING_CONSTANT_12)
                                        .setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_1)
                                        .setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                        .setGoOutDateDisplay(outGoingDisplay);
                                isTimedTask = false;
                            }
                            //2.上班时间 < 外出开始时间 <下班时间 ==> 下午外出
                            if(departureTimeHHmm.compareTo(clockInTimeStr)>0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>0){
                                newAttendanceRecord.setClockOutState(VariableConstants.STRING_CONSTANT_12)
                                        .setGoOutDateDisplay(outGoingDisplay);
                            }
                            //3.无效时间:外出开始时间 >= 下班时间
                            if(departureTimeHHmm.compareTo(clockOutTimeStr)>=0){
                                //无效时间也不做定时任务
                                isTimedTask = false;
                            }
                            //外出日期如果大于等于今天
                            if(isGteThatDay){
                                //如果不是全天外出,需要实时更新外出的状态(定时任务)
                                if(isTimedTask){
                                    OutgoingStatusDisplay outgoingStatusDisplay = new OutgoingStatusDisplay()
                                            .setAttendanceRecordId(newRecordId)
                                            .setDepartureTime(departureTime)
                                            .setEndOfOuting(endOfOuting)
                                            .setClockInTime(clockInTimeStr)
                                            .setClockOutTime(clockOutTimeStr);
                                    //新增外出状态定时任务信息
                                    this.outgoingStatusDisplayService.insertOutgoingStatusDisplay(outgoingStatusDisplay);
                                }
                            }
                                result += this.mapper.insertSelective(newAttendanceRecord);
                        }else{
                            //更新
                            String clockInState = oldAttendanceRecord.getClockInState();
                            String clockOutState = oldAttendanceRecord.getClockOutState();

                            //1.外出开始时间 <= 上班时间 ==> 当天最终考勤外出(正常)
                            if(departureTimeHHmm.compareTo(clockInTimeStr)<=0){
                                clockInState = VariableConstants.STRING_CONSTANT_12;
                                clockOutState = VariableConstants.STRING_CONSTANT_12;
                                oldAttendanceRecord.setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                        .setGoOutDateDisplay(outGoingDisplay);
                                isTimedTask = false;
                            }
                            //2.上班时间 < 外出开始时间 <下班时间 ==> 下午外出
                            if(departureTimeHHmm.compareTo(clockInTimeStr)>0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>0){
                                clockOutState = VariableConstants.STRING_CONSTANT_12;
                                oldAttendanceRecord.setGoOutDateDisplay(outGoingDisplay);
                            }
                            //3.无效时间:外出开始时间 >= 下班时间
                            if(departureTimeHHmm.compareTo(clockOutTimeStr)>=0){
                                //无效时间也不做定时任务
                                isTimedTask = false;
                            }
                            //外出日期如果大于等于今天
                            if(isGteThatDay){
                                //如果不是全天外出,需要实时更新外出的状态(定时任务)
                                if(isTimedTask){
                                    OutgoingStatusDisplay outgoingStatusDisplay = new OutgoingStatusDisplay()
                                            .setAttendanceRecordId(oldAttendanceRecord.getAttendanceId())
                                            .setDepartureTime(departureTime)
                                            .setEndOfOuting(endOfOuting)
                                            .setClockInTime(clockInTimeStr)
                                            .setClockOutTime(clockOutTimeStr);
                                    //新增外出状态定时任务信息
                                    this.outgoingStatusDisplayService.insertOutgoingStatusDisplay(outgoingStatusDisplay);
                                }
                            }
                            oldAttendanceRecord.setClockInState(clockInState).setClockOutState(clockOutState)
                                    .setFinalAttendanceStatus(getFinalAttendanceStatus(clockInState,clockOutState))
                                    .setUpdateTime(LocalDateTime.now());
                            result += this.mapper.updateAttendanceRecordSelective(oldAttendanceRecord);
                        }
                    }else if(localDates.size()-1==i){
                        //外出的最后一天只判断外出结束时间
                        if(oldAttendanceRecord==null){
                            //新增
                            String newRecordId = SnowIdUtil.getInstance().nextId();
                            AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                                    .setId(newRecordId)
                                    .setUserId(initiatorId)
                                    .setDateOfAttendance(absenceStartDate)
                                    .setWhatDay(absenceStartDate.getDayOfWeek().getValue())
                                    .setAttendanceId(attendanceManagementId)
                                    .setCreateTime(LocalDateTime.now())
                                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                                    //景点id
                                    .setScenicId(scenicId);

                            //1.外出结束时间 >= 下班时间 ==> 当天最终考勤外出(正常)
                            if(endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>=0){
                                newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_12)
                                        .setClockOutState(VariableConstants.STRING_CONSTANT_12)
                                        .setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_1)
                                        .setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                        .setGoOutDateDisplay(outGoingDisplay);
                                isTimedTask = false;
                            }
                            //2.上班时间 < 外出结束时间 <下班时间 ==> 上午外出
                            if(endOfOutingTimeHHmm.compareTo(clockInTimeStr)>0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)<0){
                                newAttendanceRecord.setClockInState(VariableConstants.STRING_CONSTANT_12)
                                        .setGoOutDateDisplay(outGoingDisplay);
                            }
                            //3.无效时间:外出开始时间 <= 上班时间
                            if(endOfOutingTimeHHmm.compareTo(clockInTimeStr)<=0){
                                //无效时间也不做定时任务
                                isTimedTask = false;
                            }
                            //外出日期如果大于等于今天
                            if(isGteThatDay){
                                //如果不是全天外出,需要实时更新外出的状态(定时任务)
                                if(isTimedTask){
                                    OutgoingStatusDisplay outgoingStatusDisplay = new OutgoingStatusDisplay()
                                            .setAttendanceRecordId(newRecordId)
                                            .setDepartureTime(departureTime)
                                            .setEndOfOuting(endOfOuting)
                                            .setClockInTime(clockInTimeStr)
                                            .setClockOutTime(clockOutTimeStr);
                                    //新增外出状态定时任务信息
                                    this.outgoingStatusDisplayService.insertOutgoingStatusDisplay(outgoingStatusDisplay);
                                }
                            }
                            result += this.mapper.insertSelective(newAttendanceRecord);
                        }else{
                            //更新
                            String clockInState = oldAttendanceRecord.getClockInState();
                            String clockOutState = oldAttendanceRecord.getClockOutState();
                            //1.外出结束时间 >= 下班时间 ==> 当天最终考勤外出(正常)
                            if(endOfOutingTimeHHmm.compareTo(clockOutTimeStr)>=0){
                                clockInState = VariableConstants.STRING_CONSTANT_12;
                                clockOutState = VariableConstants.STRING_CONSTANT_12;
                                oldAttendanceRecord
                                        .setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                        .setGoOutDateDisplay(outGoingDisplay);
                                isTimedTask = false;
                            }
                            //2.上班时间 < 外出结束时间 <下班时间 ==> 上午外出
                            if(endOfOutingTimeHHmm.compareTo(clockInTimeStr)>0 && endOfOutingTimeHHmm.compareTo(clockOutTimeStr)<0){
                                clockInState = VariableConstants.STRING_CONSTANT_12;
                                oldAttendanceRecord.setGoOutDateDisplay(outGoingDisplay);
                            }
                            //3.无效时间:外出开始时间 <= 上班时间
                            if(endOfOutingTimeHHmm.compareTo(clockInTimeStr)<=0){
                                //无效时间也不做定时任务
                                isTimedTask = false;
                            }
                            //外出日期如果大于等于今天
                            if(isGteThatDay){
                                //如果不是全天外出,需要实时更新外出的状态(定时任务)
                                if(isTimedTask){
                                    OutgoingStatusDisplay outgoingStatusDisplay = new OutgoingStatusDisplay()
                                            .setAttendanceRecordId(oldAttendanceRecord.getId())
                                            .setDepartureTime(departureTime)
                                            .setEndOfOuting(endOfOuting)
                                            .setClockInTime(clockInTimeStr)
                                            .setClockOutTime(clockOutTimeStr);
                                    //新增外出状态定时任务信息
                                    this.outgoingStatusDisplayService.insertOutgoingStatusDisplay(outgoingStatusDisplay);
                                }
                            }
                            oldAttendanceRecord.setUpdateTime(LocalDateTime.now())
                                    .setClockInState(clockInState)
                                    .setClockOutState(clockOutState)
                                    .setFinalAttendanceStatus(getFinalAttendanceStatus(clockInState,clockOutState));
                            result += this.mapper.updateAttendanceRecordSelective(oldAttendanceRecord);
                        }
                    }else{
                        //外出超过2天，中间的日期直接把当天的考勤状态改为外出
                        if(oldAttendanceRecord==null){
                            //新增
                            String newRecordId = SnowIdUtil.getInstance().nextId();
                            AttendanceRecord newAttendanceRecord =  new AttendanceRecord()
                                    .setId(newRecordId)
                                    .setUserId(initiatorId)
                                    .setDateOfAttendance(absenceStartDate)
                                    .setWhatDay(absenceStartDate.getDayOfWeek().getValue())
                                    .setAttendanceId(attendanceManagementId)
                                    .setCreateTime(LocalDateTime.now())
                                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                                    .setClockInState(VariableConstants.STRING_CONSTANT_12)
                                    .setClockOutState(VariableConstants.STRING_CONSTANT_12)
                                    .setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_1)
                                    .setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                    .setGoOutDateDisplay(outGoingDisplay)
                                    //景点id
                                    .setScenicId(scenicId);
                            result += this.mapper.insertSelective(newAttendanceRecord);
                        }else{
                            //更新
                            oldAttendanceRecord.setUpdateTime(LocalDateTime.now())
                                    .setClockInState(VariableConstants.STRING_CONSTANT_12)
                                    .setClockOutState(VariableConstants.STRING_CONSTANT_12)
                                    .setFinalAttendanceStatus(VariableConstants.STRING_CONSTANT_1)
                                    .setIsGoOut(VariableConstants.STRING_CONSTANT_1)
                                    .setGoOutDateDisplay(outGoingDisplay);
                            result += this.mapper.updateAttendanceRecordSelective(oldAttendanceRecord);
                        }
                    }
                }

            }

        }

        return result;
    }

    /**
     * @方法名：getAttendanceWeeks
     * @描述： 根据考勤管理的日期设置,得到需要考勤的星期X（数字）
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 21:21
     */
    public List<Integer> getAttendanceWeeks(List<AttendanceWeekDayView> weekDays) {
        //得到需要进行考勤的星期
        List<Integer> attendanceWeeks = new ArrayList<>();
        weekDays.forEach(weekDayView ->{
            if(weekDayView.isAttendance()){
                attendanceWeeks.add(weekDayView.getWeekDay());
            }
        });
        return attendanceWeeks;
    }

    /**
     * @方法名：getFinalAttendanceStatus
     * @描述： 根据上班考勤状态和下班考勤状态返回最终考勤状态
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/24 23:37
     */
    public String getFinalAttendanceStatus(String clockInState, String clockOutState) {
        /**
         * 正常+正常 or 正常+外出 or 外出+正常 or 外出+外出 or 补卡+补卡 or 补卡+外出 or 外出+补卡 or 正常+补卡 or 补卡+正常  ==》不做判断，默认正常
         */
        String finalAttendanceStatus = VariableConstants.STRING_CONSTANT_1;
        if(clockInState == null || clockOutState == null){
            return null;
        }
        /** 1. 上班缺卡+下班正常 = 缺卡 */
        if(VariableConstants.STRING_CONSTANT_2.equals(clockInState) && VariableConstants.STRING_CONSTANT_1.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_2;
        }
        /** 2.上班正常+下班缺卡 = 缺卡 */
        if(VariableConstants.STRING_CONSTANT_1.equals(clockInState) && VariableConstants.STRING_CONSTANT_2.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_2;
        }
        /** 3.上班迟到+下班正常 = 迟到 */
        if(VariableConstants.STRING_CONSTANT_3.equals(clockInState) && VariableConstants.STRING_CONSTANT_1.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_3;
        }
        /** 4.上班迟到+下班缺卡 = 迟到+缺卡 */
        if(VariableConstants.STRING_CONSTANT_3.equals(clockInState) && VariableConstants.STRING_CONSTANT_2.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_4;
        }
        /** 5.上班缺卡+下班缺卡 = 矿工 */
        if(VariableConstants.STRING_CONSTANT_2.equals(clockInState) && VariableConstants.STRING_CONSTANT_2.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_5;
        }
        /** 6.上班请假+下班正常 = 请假 */
        if(VariableConstants.STRING_CONSTANT_6.equals(clockInState) && VariableConstants.STRING_CONSTANT_1.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_6;
        }
        /** 7.上班正常+下班请假 = 请假 */
        if(VariableConstants.STRING_CONSTANT_1.equals(clockInState) && VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_6;
        }
        /** 8.上班迟到+下班请假 = 迟到+请假 */
        if(VariableConstants.STRING_CONSTANT_3.equals(clockInState) && VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_7;
        }
        /** 9.上班缺卡+下班请假 = 缺卡+请假 */
        if(VariableConstants.STRING_CONSTANT_2.equals(clockInState) && VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_8;
        }
        /** 10.上班请假+下班缺卡 = 缺卡+请假 */
        if(VariableConstants.STRING_CONSTANT_6.equals(clockInState) && VariableConstants.STRING_CONSTANT_2.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_8;
        }
        /** 11.上班外出+下班缺卡 = 缺卡 */
        if(VariableConstants.STRING_CONSTANT_12.equals(clockInState) && VariableConstants.STRING_CONSTANT_2.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_2;
        }
        /** 12.上班缺卡+下班外出 = 缺卡 */
        if(VariableConstants.STRING_CONSTANT_2.equals(clockInState) && VariableConstants.STRING_CONSTANT_12.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_2;
        }
        /** 13.上班迟到+下班外出 = 迟到 */
        if(VariableConstants.STRING_CONSTANT_3.equals(clockInState) && VariableConstants.STRING_CONSTANT_12.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_3;
        }
        /** 14.上班请假+下班请假 = 请假 */
        if(VariableConstants.STRING_CONSTANT_6.equals(clockInState) && VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_6;
        }
        /** 15.上班请假+下班补卡 = 请假 */
        if(VariableConstants.STRING_CONSTANT_6.equals(clockInState) && VariableConstants.STRING_CONSTANT_9.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_6;
        }
        /** 16.上班补卡+下班请假 = 请假 */
        if(VariableConstants.STRING_CONSTANT_9.equals(clockInState) && VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_6;
        }
        /** 17.上班迟到+下班补卡 = 迟到 */
        if(VariableConstants.STRING_CONSTANT_3.equals(clockInState) && VariableConstants.STRING_CONSTANT_9.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_3;
        }
        /** 18.上班缺卡+下班补卡 = 缺卡 */
        if(VariableConstants.STRING_CONSTANT_2.equals(clockInState) && VariableConstants.STRING_CONSTANT_9.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_2;
        }
        /** 19.上班补卡+下班缺卡 = 缺卡 */
        if(VariableConstants.STRING_CONSTANT_9.equals(clockInState) && VariableConstants.STRING_CONSTANT_2.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_2;
        }
        /** 20.上班请假+下班外出 = 请假 */
        if(VariableConstants.STRING_CONSTANT_6.equals(clockInState) && VariableConstants.STRING_CONSTANT_12.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_6;
        }
        /** 21.上班外出+下班请假 = 请假 */
        if(VariableConstants.STRING_CONSTANT_12.equals(clockInState) && VariableConstants.STRING_CONSTANT_6.equals(clockOutState)){
            finalAttendanceStatus = VariableConstants.STRING_CONSTANT_6;
        }
        return finalAttendanceStatus;
    }

    /**
     * @方法名：selectAttendanceRecordById
     * @描述： 根据id查询考勤记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/24 14:05
     */
    public AttendanceRecord selectAttendanceRecordById(String id) {
        AttendanceRecord attendanceRecord = this.mapper.selectAttendanceRecordById(id);
        return  attendanceRecord;
    }

    /**
     * @方法名：isAttendance
     * @描述： 传入星期数和管理设置的考勤工作日的星期判断是否是参与考勤的日期
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 14:30
     */
    public boolean isAttendance(int week, List<Integer> attendanceWeeks) {
        boolean isAttendance = false;
        for(int i=0;i<attendanceWeeks.size();i++){
            if(week==attendanceWeeks.get(i)){
                isAttendance = true;
                break;
            }
        }
        return isAttendance;
    }

    /**
     * @author 王海洋
     * @methodName: hasLeaveOrOut
     * @methodDesc: 是否有请假外出
     * @description:
     * @param: [time, userId]
     * @return boolean
     * @create 2019-11-25 19:48
     **/
    public boolean hasLeaveOrOut(String time,String userId){
        int leave = this.mapper.getLeaveCntByDate(time,userId);
        int out = this.mapper.getOutCntByDate(time,userId);
        if(leave>=1){
            return true;
        }
        if(out>=1){
            return true;
        }
        return false;
    }

    /**
     * @方法名：getAttanceRecordList
     * @描述： 考勤记录
     * @作者： 王海洋
     * @日期： Created in 2019-11-26 9:27
     */
    public PageInfo<AttanceRecordVO> getAttanceRecordList(AttendanceRecordRequestVO attendanceRecordRequestVO){
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();
        attendanceRecordRequestVO.setScenicId(scenicId);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(StringUtil.isNotEmpty(attendanceRecordRequestVO.getStartDate()) && StringUtil.isNotEmpty(attendanceRecordRequestVO.getEndDate())){
            try {
                Date end = sdf.parse(attendanceRecordRequestVO.getEndDate());
                if(end.after(new Date())){
                    throw new BusinessException("date.is.now");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else{
            attendanceRecordRequestVO.setEndDate(sdf.format(new Date()));
        }
        PageHelper.startPage(attendanceRecordRequestVO.getNum(),attendanceRecordRequestVO.getSize());
        List<AttanceRecordVO> attanceRecordVOList = this.mapper.getAttanceRecordList(attendanceRecordRequestVO);
        if(attanceRecordVOList!=null && attanceRecordVOList.size()>0){
            attanceRecordVOList.forEach(attanceRecordVO -> {
                if(StringUtil.isNotEmpty(attanceRecordVO.getInState())){
                    attanceRecordVO.setInStateVal(dictBasicService.getDisplayByCodeVal("attendanceStatus",attanceRecordVO.getInState()));
                }
                if(StringUtil.isNotEmpty(attanceRecordVO.getOutState())){
                    attanceRecordVO.setOutStateVal(dictBasicService.getDisplayByCodeVal("attendanceStatus",attanceRecordVO.getOutState()));
                }
                if(StringUtil.isNotEmpty(attanceRecordVO.getFinalStatus())){
                    attanceRecordVO.setFinalStatusVal(dictBasicService.getDisplayByCodeVal("attendanceStatus",attanceRecordVO.getFinalStatus()));
                }
            });
        }
        System.err.println("-----考勤记录-----");
        System.err.println(JSON.toJSON(attanceRecordVOList));
        PageInfo<AttanceRecordVO> pageInfo = new PageInfo<>(attanceRecordVOList);
        return pageInfo;
    }

    /**
     * @author 王海洋
     * @methodName: getAttendanceStatList
     * @methodDesc: 获取考勤统计记录
     * @description:
     * @param: [attendanceRecordRequestVO]
     * @return com.github.pagehelper.PageInfo<com.liaoyin.travel.vo.attendance.AttendanceStatVO>
     * @create 2019-11-26 10:06
     **/
    public PageInfo<AttendanceStatVO> getAttendanceStatList(AttendanceRecordRequestVO attendanceRecordRequestVO){
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();
        attendanceRecordRequestVO.setScenicId(scenicId);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(StringUtil.isNotEmpty(attendanceRecordRequestVO.getStartDate()) && StringUtil.isNotEmpty(attendanceRecordRequestVO.getEndDate())){
            try {
                Date end = sdf.parse(attendanceRecordRequestVO.getEndDate());
                if(end.after(new Date())){
                    throw new BusinessException("date.is.now");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else{
            attendanceRecordRequestVO.setEndDate(sdf.format(new Date()));
        }
        Page page = PageHelper.startPage(attendanceRecordRequestVO.getNum(),attendanceRecordRequestVO.getSize());
        List<AttendanceStatVO> attanceRecordVOList = this.mapper.getAttendanceStatList(attendanceRecordRequestVO);
        for(AttendanceStatVO attendanceStatVO:attanceRecordVOList){
            MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO = new MobileAttendanceStatRequestVO();
            BeanUtils.copyProperties(attendanceRecordRequestVO,mobileAttendanceStatRequestVO);
            mobileAttendanceStatRequestVO.setUserId(attendanceStatVO.getId());
            List<Map<String,String>> lists = this.getLackRecord(mobileAttendanceStatRequestVO);
            if(lists!=null && lists.size()>0){
                attendanceStatVO.setMissCnt(String.valueOf(lists.size()));
            }else{
                attendanceStatVO.setMissCnt("0");
            }
        }
        PageInfo<AttendanceStatVO> pageInfo = new PageInfo<>(attanceRecordVOList);
        return pageInfo;
    }

    /**
     * @author 王海洋
     * @methodName: getMobileAttendanceStat
     * @methodDesc: 移动端打卡统计
     * @description:
     * @param: [userId, startDate, endDate]
     * @return com.liaoyin.travel.vo.attendance.MobileAttendanceStatVO
     * @create 2019-11-26 10:43
     **/
    public MobileAttendanceStatVO getMobileAttendanceStat(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){

//        //出勤天数
//        String attendanceCnt = this.mapper.getAttendanceCntByMobileAttendanceStatRequestVO(mobileAttendanceStatRequestVO);
//        //迟到数量
//        String lateCnt;
//        //缺卡次数
//        String missCnt;
//        //旷工次数
//        String absentCnt;
//        //补卡数
//        String reapplyCnt;
//        //请假数
//        String leaveCnt;
//        //外出数
//        String outCnt;

      return this.mapper.getMobileAttendanceStat(mobileAttendanceStatRequestVO);
    }

    /**
     * @author 王海洋
     * @methodName: 正常考勤次数，最终状态为正常范围内
     * @methodDesc:
     * @description:
     * @param:
     * @return
     * @create 2019-12-02 17:01
     **/
    public List<String> getNormalAttendanceCnt(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){
        return this.mapper.getNormalAttendanceCnt(mobileAttendanceStatRequestVO);
    }

    /**
     * @author 王海洋
     * @methodName: 异常考勤次数，最终状态为异常范围内
     * @methodDesc:
     * @description:
     * @param:
     * @return
     * @create 2019-12-02 17:02
     **/
    public int getAbnormalAttendanceCnt(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){
        return this.mapper.getAbnormalAttendanceCnt(mobileAttendanceStatRequestVO);
    }

    /**
     * @author 王海洋
     * @methodName: getRecordList
     * @methodDesc: 获取移动端考勤记录
     * @description:
     * @param: [mobileAttendanceStatRequestVO]
     * @return java.util.List<com.liaoyin.travel.vo.attendance.MobileAttendanceRecordVO>
     * @create 2019-11-26 14:19
     **/
    public List<MobileAttendanceRecordVO> getRecordList(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){
        return this.mapper.getRecordList(mobileAttendanceStatRequestVO);
    }

    public List<Map<String,String>> getAttendanceRecordByType(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO){
        return this.mapper.getAttendanceRecordByType(mobileAttendanceStatRequestVO);
    }

    /**
     * @方法名：selectAttendanceRecordListById
     * @描述： 查询指定用户的考勤记录
     * @作者： kjz
     * @日期： Created in 2020/4/21 11:42
     */
    public PageInfo<AttanceRecordVO> selectAttendanceRecordListById(String startTime, String endTime, Integer num, Integer size, String userId) {
        UserInfo activeUser = PartyUtil.getCurrentUserInfo();
        //单位id
        String scenicId = activeUser.getScenicId();
        PageHelper.startPage(num,size);
        List<AttanceRecordVO> list = this.mapper.selectAttendanceRecordListById(startTime,endTime,userId,scenicId);
        list.forEach(attanceRecordVO -> {
            attanceRecordVO.setInStateVal(DictUtil.getDisplay("attendanceStatus",attanceRecordVO.getInState()));
            attanceRecordVO.setOutStateVal(DictUtil.getDisplay("attendanceStatus",attanceRecordVO.getOutState()));
            attanceRecordVO.setFinalStatusVal(DictUtil.getDisplay("attendanceStatus",attanceRecordVO.getFinalStatus()));
        });
        PageInfo<AttanceRecordVO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }
}
