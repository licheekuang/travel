package com.liaoyin.travel.service.attendance;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.attendance.EquipmentMapper;
import com.liaoyin.travel.entity.attendance.AttendanceSettings;
import com.liaoyin.travel.entity.attendance.Equipment;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @项目名：Equipment
 * @作者：lijing
 * @描述：设备列表
 * @日期：Created in 2019/7/10 14:55
 */
@Service
public class EquipmentService extends BaseService<EquipmentMapper, Equipment> {
	
	 private Log logger = LogFactory.getLog(this.getClass());
	
	 @Autowired
	 private AttendanceSettingsService attendanceSettingsService;
	 /****
	  * 
	      * @方法名：selectEquipmentListOnBack
	      * @描述： 后台查询设备列表【带分页】
	      * @作者： lijing
	      * @日期： 2019年7月10日
	  */
	public List<Equipment> selectEquipmentListOnBack(Integer num,Integer size,String equipmentType,
			String equipmentName,String isUsing){

		UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
		if(curveUserInfo == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = curveUserInfo.getScenicId();

		PageHelper.startPage(num,size);
		List<Equipment> list = this.mapper.selectEquipmentList(equipmentType, equipmentName, isUsing,scenicId);
		return list;
	}
	
	/****
	 * 
	     * @方法名：insertOrUpdateEquipment
	     * @描述： 新增或修改设备
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public void insertOrUpdateEquipment(Equipment equipment) {
		
		UserInfo user = PartyUtil.getCurrentUserInfo();
		if(user == null) {
			throw new BusinessException("2010");
		}

		/** 景区id */
		String scenicId = user.getScenicId();

		if(equipment.getId()!=null && !"".equals(equipment.getId())) {
			
			this.mapper.updateByPrimaryKeySelective(equipment);
		}else {
			equipment.setCreatTime(new Date());
			equipment.setCreatUserId(user.getId());
			equipment.setId(UUIDUtil.getUUID());
			equipment.setIsDelete((short)0);
			equipment.setScenicId(scenicId);
			this.mapper.insertSelective(equipment);
		}
	}
	/****
	 * 
	     * @方法名：selectEquipmentById
	     * @描述： 根据id查询设备
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public Equipment selectEquipmentById(String id) {
		if(id == null) {
			throw new BusinessException("id.is.null");
		}
		Equipment eq = this.mapper.selectByPrimaryKey(id);
		return eq;
	}
	/****
	 * 
	     * @方法名：deleteEquipmentById
	     * @描述： 删除设备
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	public void deleteEquipmentById(String ids) {
		if(ids == null) {
			throw new BusinessException("id.is.null");
		}
		String[] id = ids.split(",");
		List<AttendanceSettings> list = this.attendanceSettingsService.selectAttendanceSettingsByEquipmentId(id);
		if(list !=null && list.size() >0) {
			throw new BusinessException("","还有"+list.size()+"关联项未删除，请先删除关联项");
		}
		this.mapper.deleteEquipmentById(id);
	}
    
}
