package com.liaoyin.travel.service.attendance;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.attendance.AttendanceWorkdayMapper;
import com.liaoyin.travel.entity.attendance.AttendanceWorkday;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 考勤工作日 service
 *
 * @author kuang.jiazhuo
 * @date 2019-11-19 09:11
 */
@Service
public class AttendanceWorkdayService extends BaseService<AttendanceWorkdayMapper, AttendanceWorkday> {

    /**
     *新增考勤工作日
     * @param attendanceDays
     * @param attendanceId
     * @param flag 新增或修改标识【1:新增;2:更新】
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertOrUpdateAttendanceWorks(String scenicId,String attendanceDays, String attendanceId, int flag) {
        String[] ids = attendanceDays.split(",");
        List<String> list = Arrays.asList(ids);
        if(flag==1){
            /** 新增 */
            List<AttendanceWorkday> attendanceWorkdays = new ArrayList<>();
            list.forEach(weekDay->{
                AttendanceWorkday attendanceWorkday = new AttendanceWorkday()
                        //id
                        .setId(SnowIdUtil.getInstance().nextId())
                        //考勤管理表id
                        .setAttendanceId(attendanceId)
                        //用户表id
                        .setWeekDay(Integer.parseInt(weekDay))
                        //创建时间
                        .setCreateTime(LocalDateTime.now())
                        //是否删除
                        .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                        //景区id
                        .setScenicId(scenicId);
                attendanceWorkdays.add(attendanceWorkday);
            });
            this.mapper.insertList(attendanceWorkdays);
        }else{
            /** 更新 */
            //先删除原来的用户关联考勤数据
            Example example = new Example(AttendanceWorkday.class);
            example.and().andEqualTo("attendanceId", attendanceId);
            this.mapper.deleteByExample(example);
            //然后重新执行新增
            this.insertOrUpdateAttendanceWorks(scenicId,attendanceDays,attendanceId,1);
        }
    }

    /**
     * @方法名：selectAttendanceWorkdayByAttendanceId
     * @描述： 根据考勤管理id查询考勤的工作日List
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/19 10:00
     */
    public List<AttendanceWorkday> selectAttendanceWorkdayByAttendanceId(String attendanceId) {
        Example example = new Example(AttendanceWorkday.class);
        example.and().andEqualTo("attendanceId", attendanceId);
        List<AttendanceWorkday> list = this.mapper.selectByExample(example);
        return list;
    }

    /**
     * @方法名：selectAttendanceWorkdayByAttendanceIdAndWhatDay
     * @描述： 根据考勤管理id查询考勤和星期X查询考勤的工作日
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/20 16:00
     */
    public AttendanceWorkday selectAttendanceWorkdayByAttendanceIdAndWhatDay(String attendanceId, Integer whatDay) {
        AttendanceWorkday attendanceWorkday = this.mapper.selectAttendanceWorkdayByAttendanceIdAndWhatDay(attendanceId,whatDay);
        return attendanceWorkday;
    }

    /**
     * @方法名：selectAttendanceWorkdayByUserId
     * @描述： 根据员工id查询考勤工作日
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/23 21:22
     */
    public List<AttendanceWorkday> selectAttendanceWorkdayByUserId(String userId) {
        List<AttendanceWorkday> attendanceWorkdayList = this.mapper.selectAttendanceWorkdayByUserId(userId);
        return attendanceWorkdayList;
    }
}
