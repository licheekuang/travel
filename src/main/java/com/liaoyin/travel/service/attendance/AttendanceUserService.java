package com.liaoyin.travel.service.attendance;

import com.liaoyin.travel.view.mine.attendance.AttendanceDistanceInfoView;
import com.liaoyin.travel.view.moble.attendance.AttendanceWeekDayView;
import com.liaoyin.travel.vo.attendance.AttendanceOldUser;
import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.attendance.AttendanceUserMapper;
import com.liaoyin.travel.entity.attendance.AttendanceUser;
import com.liaoyin.travel.entity.attendance.AttendanceWorkday;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 参与考勤的用户关联 service
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-18 16:12
 */
@Service
public class AttendanceUserService extends BaseService<AttendanceUserMapper, AttendanceUser> {

    @Resource
    AttendanceWorkdayService attendanceWorkdayService;

    /**
     * 新增或修改考勤和用户关联表
     * @param scenicId 景区id
     * @param participantIds 参与考勤的用户id集
     * @param attendanceId 考勤管理id
     * @param flag 新增或修改标识【1:新增;2:更新】
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertOrUpdateAttendanceUser(String scenicId,String participantIds, String attendanceId, int flag) {
        String[] ids = participantIds.split(",");
        List<String> list = Arrays.asList(ids);
        if(flag==1){
            /** 新增 */
            List<AttendanceUser> attendanceUsers = new ArrayList<>();
            list.forEach(userId->{
                AttendanceUser attendanceUser = new AttendanceUser()
                        //id
                        .setId(SnowIdUtil.getInstance().nextId())
                        //考勤管理表id
                        .setAttendanceId(attendanceId)
                        //用户表id
                        .setUserId(userId)
                        //创建时间
                        .setCreatTime(LocalDateTime.now())
                        //是否删除
                        .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                        //景区id
                        .setScenicId(scenicId);
                attendanceUsers.add(attendanceUser);
            });
            this.mapper.insertList(attendanceUsers);
        }else{
            /** 更新 */
            //先删除原来的用户关联考勤数据
            Example example = new Example(AttendanceUser.class);
            example.and().andEqualTo("attendanceId", attendanceId);
            this.mapper.deleteByExample(example);
            this.mapper.deleteByParticipant(participantIds);
            //然后重新执行新增
            this.insertOrUpdateAttendanceUser(scenicId,participantIds,attendanceId,1);
        }
    }

    /**
     * @方法名：deleteAttendanceUsersByAttendanceId
     * @描述： 根据考勤管理的id批量物理删除考勤-用户关联数据
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/18 17:42
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteAttendanceUsersByAttendanceId(String attendanceId) {
        int result = this.mapper.deleteAttendanceUsersByAttendanceId(attendanceId);
        return result;
    }

    /**
     * @方法名：selectAttendanceUserByAttendanceId
     * @描述： 根据考勤管理的idc查询考勤管理和用户的关联数据
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/19 10:40
     */
    public List<AttendanceUser> selectAttendanceUserByAttendanceId(String attendanceId) {
        Example example = new Example(AttendanceUser.class);
        example.and().andEqualTo("attendanceId", attendanceId);
        List<AttendanceUser> list = this.mapper.selectByExample(example);
        return list;
    }

    /**
     * @方法名：selectUserIdsByAttendanceTime
     * @描述： 查询打卡时间和传入的上下班时间不一致的加入了考勤点的用户
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/19 15:31
     */
    public List<AttendanceOldUser> selectUserIdsByAttendanceTime(LocalTime attendanceStartTime, LocalTime attendanceEndTime) {
        List<AttendanceOldUser> list = this.mapper.selectUserIdsByAttendanceTime(attendanceStartTime,attendanceEndTime);
        return list;
    }

    /**
     * @方法名：selectAttendanceOldUserByUserIds
     * @描述： 通过用户的字符串id集查询已经绑定考勤名称的用户信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/19 17:31
     */
    public List<AttendanceOldUser> selectAttendanceOldUserByUserIds(String participantIds) {
        String[] ids = participantIds.split(",");
        List<String> list = Arrays.asList(ids);
        List<AttendanceOldUser> attendanceOldUserList = this.mapper.selectAttendanceOldUserByUserIds(list);
        return attendanceOldUserList;
    }

    /**
     * @方法名：selectAttendanceDistanceInfoViewByUserId
     * @描述： 通过用户id查询考勤管理的距离信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/19 20:06
     */
    public AttendanceDistanceInfoView selectAttendanceDistanceInfoViewByUserId(String userId,String scenicId) {
        AttendanceDistanceInfoView attendanceDistanceInfoView = this.mapper.selectAttendanceDistanceInfoViewByUserId(userId,scenicId);
        return attendanceDistanceInfoView;
    }

    /**
     * @方法名：selectAttendanceUserByAll
     * @描述： 查询出所有的考勤-用户 关联
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/20 19:29
     */
    public List<AttendanceUser> selectAttendanceUserByAll() {
        List<AttendanceUser> list = this.mapper.selectAll();
        return list;
    }

    /**
     * @方法名：selectAttendanceDay
     * @描述： 员工查询自己的考勤工作日
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/23 21:14
     */
    public List<AttendanceWeekDayView> selectAttendanceDay(String userId) {
        if(userId==null){
            UserInfo userInfo = PartyUtil.getCurrentUserInfo();
            if(userInfo == null) {
                throw new BusinessException("user.isNotLogin");
            }
            userId = userInfo.getId();
        }
        List<AttendanceWorkday> attendanceWorkdayList = this.attendanceWorkdayService.selectAttendanceWorkdayByUserId(userId);
        List<AttendanceWeekDayView> weekDayViews  = new ArrayList<>();
        Integer[] weekDays = { 1, 2, 3, 4, 5, 6, 7 };
        for(int i=0;i<weekDays.length;i++){
            AttendanceWeekDayView attendanceWeekDayView = new AttendanceWeekDayView();
            attendanceWeekDayView.setWeekDay(weekDays[i]);
            attendanceWeekDayView.setAttendance(false);
            for(AttendanceWorkday attendanceWorkday:attendanceWorkdayList){
                int weekDay = attendanceWorkday.getWeekDay();
                if(weekDay==weekDays[i]){
                    attendanceWeekDayView.setAttendance(true);
                    break;
                }
            }
            weekDayViews.add(attendanceWeekDayView);
        }
        return weekDayViews;
    }

    /**
     * @方法名：selectAttendanceUserByUserId
     * @描述： 根据用户id查询自己的考勤-用户关联信息
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/29 9:48
     */
    public List<AttendanceUser> selectAttendanceUserByUserId(String userId) {
        List<AttendanceUser> list = this.mapper.selectAttendanceUserByUserId(userId);
        return list;
    }
}
