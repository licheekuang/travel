package com.liaoyin.travel.service.team;

import com.alibaba.fastjson.JSON;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.task.TaskReceivingRecordsMapper;
import com.liaoyin.travel.dao.team.TeamWorkerMapper;
import com.liaoyin.travel.entity.MotionTrack;
import com.liaoyin.travel.entity.UserTeam;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.task.Task;
import com.liaoyin.travel.entity.team.*;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.MotionTrackService;
import com.liaoyin.travel.service.UserTeamService;
import com.liaoyin.travel.util.FileUploadUtil;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * @项目名：
 * @作者：lijing
 * @描述：-部门和工种关联表
 * @日期：Created in 2018/6/20 14:55
 */
@Service
public class TeamWorkerService extends BaseService<TeamWorkerMapper, TeamWorker> {

	@Resource
	private TaskReceivingRecordsMapper taskReceivingRecordsMapper;
	@Autowired
	private UserTeamService userTeamService;
	@Autowired
	private MotionTrackService motionTrackService;


	/****
	 * 
	     * @方法名：selectTeamWorkerListByTeamId
	     * @描述： 查询部门下的所有工种
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	public List<TeamWorker> selectTeamWorkerListByTeamId(String teamId){
		if(teamId== null) {
			throw new BusinessException("id.is.null");
		}
		List<TeamWorker> list = this.mapper.selectTeamWorkerList(teamId);
		return list;
	}
	
	/***
	 * 
	     * @方法名：insertTeamWorker
	     * @描述： 新增
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	public void insertTeamWorker(List<TeamWorker> list,String teamId) {
		
		deleteTeamWorkerByTeamId(teamId);
		for(TeamWorker t:list) {
			t.setCreatTime(new Date());
			t.setId(UUIDUtil.getUUID());
			t.setTeamId(teamId);
		}
		this.mapper.insertList(list);
	}
	/***
	 * 
	     * @方法名：deleteTeamWorkerByTeamId
	     * @描述： 通过部门id删除
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	public void deleteTeamWorkerByTeamId(String teamId) {
		
		this.mapper.deleteTeamWorkerByTeamId(teamId);
	}

	/**
	 * @方法名：selectUsersListByTeamId
	 * @描述： 查询部门下的所有员工
	 * @作者： liJing
	 * @日期： Created in 2019/10/2 10:43
	 */
	public List<Users> selectUsersListByTeamId(String teamId){
		//获取当前用户的登录信息
		UserInfo userInfo = PartyUtil.getCurrentUserInfo();
		if(userInfo == null) {
			throw new BusinessException("not.login");
		}
		//获取当前用户的部门信息
		List<UserTeam> userTeamList = userInfo.getUserTeamList();
		//团队不存在,报错
		if(userTeamList==null || userTeamList.size() == 0) {
			throw new BusinessException("team.is.null");
		}
		System.err.println("userTeamList="+userTeamList);
		/*StringBuffer stringBuffer = new StringBuffer();
		for (UserTeam team : userTeamList) {
			stringBuffer.append(team.getTeamId()+ ",");
		}*/
		List<Users> users = this.mapper.selectUsersListByTeamId(/*stringBuffer.toString()*/teamId);

		for (Users user : users) {
			//当前用户结束循环(因为登录就已经有了部门相关信息了)
			if (user.getId() == userInfo.getId()) {
				continue;
			}
			//赋值部门信息
			user.setUserTeamList(this.userTeamService.selectUserTeam(user.getId(),null));
			if (user.getHeadPicId() != null){
				user.setFileUpload(FileUploadUtil.getFileUpload(user.getHeadPicId()));
			}
			if (gpsDataMap.size()>0){
				String o = gpsDataMap.get(user.getId());
				if ( null != o){
					String[] split = o.split(",");
					user.setLat(split[0]);
					user.setLog(split[1]);
				}
			} else {
				List<MotionTrack> motionTracks = this.motionTrackService.selectMotionTrackByUserId(user.getId());
				if(motionTracks!=null && motionTracks.size()>0){
					MotionTrack motionTrack = motionTracks.get(0);
					user.setLat(motionTrack.getuLat());
					user.setLog(motionTrack.getuLog());
				}
			}
			List<Task> tasks = taskReceivingRecordsMapper.selectTaskByUserNow(user.getId());
			if (null != tasks && tasks.size() > 0 ) {
				user.setTask(tasks.get(0));
			}
		}
		System.err.println("----部门员工打印----");
		System.err.println(JSON.toJSON(users));
		return users;
	}

	/**
	 * @方法名：selectTeamWorkerByTeamIdAndWorkerId
	 * @描述： 根据部门id和工种id查询部门和工种关联信息
	 * @作者： kjz
	 * @日期： Created in 2020/2/27 17:01
	 */
	public TeamWorker selectTeamWorkerByTeamIdAndWorkerId(String teamId, String workId) {
		return this.mapper.selectTeamWorkerByTeamIdAndWorkerId(teamId,workId);
	}

	/**
	 * 判断部门是否已经关联工种
	 * @param workIds
	 * @return
	 */
	public List<String> judgeWorkerIsCorrelationTeam(List<String> workIds) {
		return this.mapper.judgeWorkerIsCorrelationTeam(workIds);
	}

	/**
	 * 批量删除工种-部门关系表
	 * @param workerIds
	 * @return
	 */
	public int deleteWorkerTeamByWorkerIds(List<String> workerIds) {
		return this.mapper.deleteWorkerTeamByWorkerIds(workerIds);
	}


}
