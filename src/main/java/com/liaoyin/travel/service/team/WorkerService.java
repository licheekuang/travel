package com.liaoyin.travel.service.team;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.*;
import com.liaoyin.travel.dao.team.WorkerMapper;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.team.Worker;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @项目名：旅投
 * @作者：lijing
 * @描述：工种列表
 * @日期：Created in 2018/6/20 14:55
 */
@Service
public class WorkerService extends BaseService<WorkerMapper, Worker> {

	@Autowired
	private TeamWorkerService teamWorkerService;
	@Autowired
	private UsersMapper usersMapper;
    
	
	/****
	 * 
	     * @方法名：selectWorkerList
	     * @描述： 查询工种列表
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	public List<Worker> selectWorkerList(Integer num,Integer size
			,String workerName,String workerCode,String isUsing){

		UserInfo currentUserInfo = PartyUtil.getCurrentUserInfo();
		if(currentUserInfo == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = currentUserInfo.getScenicId();

		PageHelper.startPage(num,size);
		List<Worker> list = this.mapper.selectWorkerList(workerName, workerCode, isUsing,scenicId);
		return list;
	}
	/******
	 * 
	     * @方法名：insertOrUpdateWorker
	     * @描述： 新增或修改
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	public void insertOrUpdateWorker(Worker worker) {
		UserInfo currentUserInfo = PartyUtil.getCurrentUserInfo();
		if(currentUserInfo == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = currentUserInfo.getScenicId();
		if (null != worker.getWorkerContext() && worker.getWorkerContext().length() > 200){
			throw new BusinessException("string.is.toLong");
		}

		String workId = worker.getId();

		//判断工种名字是否重复
		String workerName = worker.getWorkerName();
		judgeWorkerNameIsReDo(workerName,scenicId,workId);

		//判断工种编码是否重复
		String workerCode = worker.getWorkerCode();
		judgeWorkerCodeIsReDo(workerCode,scenicId,workId);

		if(workId !=null && !"".equals(workId)) {
			/** 禁用工种的时候要判断是否关联 */
			Short isUsing = worker.getIsUsing();
			System.err.println(isUsing==0);
			if(isUsing==0){
				List<Users> usersList = this.usersMapper.selectUsersListByWorkId(workId,scenicId);
				if(usersList.size()>0){
					throw new BusinessException("worker.relevance.user");
				}
			}

			this.mapper.updateByPrimaryKeySelective(worker);
		}else {
			worker.setCreatTime(LocalDateTime.now())
					.setId(UUIDUtil.getUUID())
					.setIsDelete((short)0)
					.setScenicId(scenicId);
			this.mapper.insertSelective(worker);
		}
	}

	/**
	 * @方法名：judgeWorkerCodeIsReDo
	 * @描述： 判断工种编码是否重复，若重复抛出异常
	 * @作者： kjz
	 * @日期： Created in 2020/3/7 12:59
	 */
	private void judgeWorkerCodeIsReDo(String workerCode, String scenicId, String workId) {
		List<Worker> workerList = this.mapper.selectWorkerListByCode(workerCode,scenicId,workId);
		if(workerList.size()>0){
			throw new BusinessException("workerCode.is.exist");
		}
	}

	/**
	 * @方法名：judgeWorkerNameIsReDo
	 * @描述： 判断工种名字是否重复,若重复抛出异常
	 * @作者： kjz
	 * @日期： Created in 2020/3/7 12:58
	 */
	private void judgeWorkerNameIsReDo(String workerName, String scenicId, String workId) {
		List<Worker> workerList = this.mapper.selectWorkerListByName(workerName,scenicId,workId);
		if(workerList.size()>0){
			throw new BusinessException("workerName.is.exist");
		}
	}

	/****
	 * 
	     * @方法名：selectworkerById
	     * @描述： 通过id查询
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	public Worker selectworkerById(String id) {
		if(id == null) {
			throw new BusinessException("id.is.null");
		}
		Worker w = this.mapper.selectByPrimaryKey(id);
		return w;
	}

	/**
	　* @description: TODO 根据工种名称查询工种ID
	　* @param [name]
	　* @return com.liaoyin.travel.entity.team.Worker
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/7/25 17:25
	　*/
	public Worker selectworkerByName(String name){
		UserInfo user = PartyUtil.getCurrentUserInfo();
		if(user == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = user.getScenicId();
		if(null == name) {
			throw new BusinessException("work_name.is.null");
		}
		return this.mapper.selectWorkerByName(name,scenicId);
	}

	/**
	 * 通过ID批量删除
	 * @param ids
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public int deleteByIds(List<String> ids){
		//判断部门是否已经关联工种
		judgeWorkerIsCorrelationTeam(ids);
		//判断员工是否已经关联工种
		judgeUserIsCorrelationWorker(ids);
		//批量删除工种
		this.mapper.deleteByidsBatch(ids);
		//批量删除工种-部门关系表
		return teamWorkerService.deleteWorkerTeamByWorkerIds(ids);

	}
	/**
	 * 判断部门是否已经关联工种
	 *
	 * @param ids
	 */
	private void judgeWorkerIsCorrelationTeam(List<String> ids) {
		List<String> teams = teamWorkerService.judgeWorkerIsCorrelationTeam(ids);
		if (teams != null && teams.size() > 0) {
			throw new BusinessException("Team.is.Correlation", "删除失败！工种【" + teams.toString() + "】已关联部门！");
		}
	}
	/**
	 * 判断员工是否已经关联工种
	 */
	private void judgeUserIsCorrelationWorker(List<String> workerIds) {
		List<String> teams = usersMapper.judgeUserIsCorrelationWorker(workerIds);
		if (teams != null && teams.size() > 0) {
			throw new BusinessException("Team.is.Correlation", "删除失败！工种【" + teams.toString() + "】已关联员工！");
		}
	}
}
