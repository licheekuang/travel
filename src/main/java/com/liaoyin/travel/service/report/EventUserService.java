package com.liaoyin.travel.service.report;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.report.EventUserMapper;
import com.liaoyin.travel.entity.report.EventUser;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @项目名：旅投
 * @作者：lijing
 * @描述：事件上报接收人
 * @日期：Created in 2019/7/24 14:55
 */
@Service
public class EventUserService extends BaseService<EventUserMapper, EventUser> {
    
	/****8
	 * 
	     * @方法名：insertEventUser
	     * @描述： 新增事件上报接收人
	     * @作者： lijing
	     * @日期： 2019年7月24日
	 */
	public void insertEventUser(List<String> list,String id,Integer reportType) {
		UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
		if(curveUserInfo == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = curveUserInfo.getScenicId();

		List<EventUser> eventUserList = new ArrayList<EventUser>();
		list.stream().forEach((s) ->{
			EventUser eventUser=new EventUser(); 
			eventUser.setCreatTime(new Date());
			eventUser.setEventId(id);
			eventUser.setId(UUIDUtil.getUUID());
			eventUser.setUserId(s);
			eventUser.setReportType(reportType.shortValue());
			eventUser.setScenicId(scenicId);
			eventUserList.add(eventUser);
		});
		this.mapper.insertList(eventUserList);
	}
}
