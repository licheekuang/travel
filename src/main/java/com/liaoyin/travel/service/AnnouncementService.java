package com.liaoyin.travel.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.AnnouncementMapper;
import com.liaoyin.travel.dao.AnnouncementUserMapper;
import com.liaoyin.travel.entity.Announcement;
import com.liaoyin.travel.entity.AnnouncementUser;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.back.BackUserService;
import com.liaoyin.travel.util.*;
import com.liaoyin.travel.vo.announce.AnnounceUserVO;
import com.liaoyin.travel.vo.announce.AnnouncementVO;
import com.liaoyin.travel.vo.request.AnnounceAddRequestVO;
import com.liaoyin.travel.vo.request.AnnounceSearchRequestVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author privatePanda@163.com
 * @title: AnnouncementService
 * @projectName travel
 * @description: TODO  向所有用户发布公告、通知的service
 * @date 2019/9/2415:33
 */
@Service
public class AnnouncementService extends BaseService<AnnouncementMapper, Announcement> {

    @Resource
    AnnouncementUserMapper announcementUserMapper;

    @Resource
    UsersService usersService;

    @Resource
    BackUserService backUserService;

    //增or改
    /**
    　* @description: TODO 发布或更改一个通知、公告
    　* @param [announcement]
    　* @return int
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/9/24 16:08
    　*/
    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdateAnnouncement(AnnounceAddRequestVO announceAddRequestVO){
        UserInfo currentUserInfo = PartyUtil.getCurrentUserInfo();
        if (null == currentUserInfo) {
            throw new BusinessException("not.login");
        }
        //景区id
        String scenicId = currentUserInfo.getScenicId();
        if(StringUtil.isEmpty(announceAddRequestVO.getUserId())){
            throw new BusinessException("user.is.null","公告接收人不能为空");
        }

        //返回结果
        int result = 0;
        String[] userIdArr = announceAddRequestVO.getUserId().split(",");
        if(StringUtil.isEmpty(announceAddRequestVO.getId())){
            String id = UUIDUtil.getUUID();
            Announcement announcement = new Announcement();
            BeanUtils.copyProperties(announceAddRequestVO,announcement);
            announcement.setId(id);
            announcement.setCreateTime(new Date());
            announcement.setUserId(currentUserInfo.getId());
            announcement.setUserName(currentUserInfo.getNickName());
            announcement.setDelFlag(Short.valueOf("0"));
            //景区id
            announcement.setScenicId(scenicId);
            List<AnnouncementUser> announcementUsers = Lists.newArrayList();
            for(String userId:userIdArr){
                AnnouncementUser announcementUser = new AnnouncementUser()
                    .setId(UUIDUtil.getUUID())
                    .setAnnouncementId(id)
                    .setUserId(userId)
                    .setCreateTime(DateUtil.getCurrentTime4Str());
                announcementUsers.add(announcementUser);
            }
            this.announcementUserMapper.insertList(announcementUsers);
            result = this.mapper.insertSelective(announcement);
        }else{
            this.announcementUserMapper.deleteByAnnounceId(announceAddRequestVO.getId());
            List<AnnouncementUser> announcementUsers = Lists.newArrayList();
            for(String userId:userIdArr){
                AnnouncementUser announcementUser = new AnnouncementUser()
                .setId(UUIDUtil.getUUID())
                .setAnnouncementId(announceAddRequestVO.getId())
                .setUserId(userId);
                announcementUsers.add(announcementUser);
            }
            this.announcementUserMapper.insertList(announcementUsers);
            Announcement announcement = new Announcement();
            BeanUtils.copyProperties(announceAddRequestVO,announcement);
            result = this.mapper.updateById(announcement);
        }
        return result;
    }
    //删
    /**
    　* @description: TODO  删除一个或多个
    　* @param [announcementId]
    　* @return int
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/9/24 16:08
    　*/
    @Transactional(rollbackFor = Exception.class)
    public int deleteAnnouncement(String ids){
        int i = 0;
        if(ids == null) {
            throw new BusinessException("id.is.null");
        }
        this.announcementUserMapper.deleteByAnnounceIds(ids);
        i = this.mapper.delateByIds(ids);
        return i;
    }

    //查
    /**
    　* @description: TODO  条件查询公告、通知
    　* @param [num, size, title, content, time, userName]
    　* @return java.util.List<com.liaoyin.travel.entity.Announcement>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/9/24 16:40
    　*/
    public List<Announcement> selectAnnouncementList(Integer num,Integer size,String title,String content,String time){
        UserInfo user = PartyUtil.getCurrentUserInfo();
        if(user == null ) {
            throw new BusinessException("not.login");
        }
        //景区id
        String scenicId = user.getScenicId();
        if (null != num && null != size){
            PageHelper.startPage(num,size);
        }
        List<Announcement> announcements = this.mapper.selectAnnouncementList(title, content, time, user.getId(),scenicId);
        return announcements;
    }

    /**
    　* @description: TODO  查询通知、公告详情
    　* @param [announcementId]
    　* @return com.liaoyin.travel.entity.Announcement
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/9/24 16:44
    　*/
    public AnnouncementVO selectAnnouncementByPrimaryKey(String announcementId){
        UserInfo currentUserInfo = PartyUtil.getCurrentUserInfo();
        if (null == currentUserInfo) {
            throw new BusinessException("not.login");
        }
        if(StringUtil.isEmpty(announcementId)){
            throw new BusinessException("announcementId.is.null","公告ID不能为空");
        }
        AnnouncementVO announcementVO = new AnnouncementVO();
        Announcement announcement = this.mapper.selectAnnouncementByPrimaryKey(announcementId);
        BeanUtils.copyProperties(announcement,announcementVO);
        List<AnnounceUserVO> announceUserVOList = this.mapper.getReceiveUserByAnnounceId(announcementId);
        for(AnnounceUserVO announceUserVO:announceUserVOList){
            String holdOffice = DictUtil.getDisplay("assumeOffice", announceUserVO.getOffice());
            announceUserVO.setHoldOffice(holdOffice);
        }
        announcementVO.setUsersList(announceUserVOList);
        return announcementVO;
    }

    /**
     * @author 王海洋
     * @methodName: searchAnnounceList
     * @methodDesc: 公告列表搜索
     * @description:
     * @param: [requestVO]
     * @return com.github.pagehelper.PageInfo<com.liaoyin.travel.entity.Announcement>
     * @create 2019-11-29 17:40
     **/
    public PageInfo<Announcement> searchAnnounceList(AnnounceSearchRequestVO requestVO){
        UserInfo currentUserInfo = PartyUtil.getCurrentUserInfo();
        if (null == currentUserInfo) {
            throw new BusinessException("not.login");
        }
        //景区id
        String scenicId = currentUserInfo.getScenicId();
        requestVO.setScenicId(scenicId);
        Page page = PageHelper.startPage(requestVO.getNum(),requestVO.getSize());
        List<Announcement> announcementList = this.mapper.getAnnounceList(requestVO);
        announcementList.forEach(announcement -> {
            String userId = announcement.getUserId();
            String name = usersService.selectNiceNameById(userId);
            if(name==null){
                name = backUserService.selectNickNameById(userId);
            }
            announcement.setUserName(name);
        });
        PageInfo<Announcement> pageInfo = new PageInfo<>(announcementList);
        return pageInfo;
    }

}
