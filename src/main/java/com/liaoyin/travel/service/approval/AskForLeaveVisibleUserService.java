package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.approval.ApprovalAskForLeaveVisibleUserMapper;
import com.liaoyin.travel.entity.approval.AskForLeaveVisibleUser;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 请假申请-可见用户权限
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 09:47
 */
@Service
public class AskForLeaveVisibleUserService extends BaseService<ApprovalAskForLeaveVisibleUserMapper, AskForLeaveVisibleUser> {

    /**
     * 批量新增或更新请假申请可见人
     * @param everyMenIdList
     * @param sign【1:新增;2:更新】
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdateApprovalAskForLeaveVisibleUser(List<String> everyMenIdList, int sign) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        int result = 0;
        if(1==sign){
            //新增
            List<AskForLeaveVisibleUser> askForLeaveVisibleUserList = new ArrayList<>();
            everyMenIdList.forEach(userId ->{
                AskForLeaveVisibleUser askForLeaveVisibleUser = new AskForLeaveVisibleUser()
                        .setId(SnowIdUtil.getInstance().nextId()).setUserId(userId).setCreateTime(LocalDateTime.now())
                        .setIsDelete(VariableConstants.STRING_CONSTANT_0).setScenicId(scenicId);
                askForLeaveVisibleUserList.add(askForLeaveVisibleUser);
            });
            result = this.mapper.insertList(askForLeaveVisibleUserList);
        }else{
            //更新
            int deleteRow = this.mapper.deleteApprovalAskForLeaveVisibleUserAll();
            result = this.insertOrUpdateApprovalAskForLeaveVisibleUser(everyMenIdList,1);
        }
        return result;
    }

    /**
     * @方法名：countAskForLeaveVisibleUserByUserId
     * @描述： 根据用户id统计用户的请假申请-权限表的数量
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/22 15:49
     */
    public int countAskForLeaveVisibleUserByUserId(String userId) {
        Example example = new Example(AskForLeaveVisibleUser.class);
        example.and().andEqualTo("userId", userId);
        int count = this.mapper.selectCountByExample(example);
        return count;
    }

    /**
     * @方法名：selectAskForLeaveVisibleUserByScenicId
     * @描述： 根据景区id查询【用户请假申请-权限表】
     * @作者： kjz
     * @日期： Created in 2020/2/26 10:05
     */
    public List<AskForLeaveVisibleUser> selectAskForLeaveVisibleUserByScenicId(String scenicId) {
        return this.mapper.selectAskForLeaveVisibleUserByScenicId(scenicId);
    }
}
