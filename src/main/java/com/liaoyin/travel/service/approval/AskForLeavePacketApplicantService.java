package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.UsersMapper;
import com.liaoyin.travel.dao.approval.AskForLeavePacketApplicantMapper;
import com.liaoyin.travel.entity.approval.AskForLeavePacketApplicant;
import com.liaoyin.travel.entity.attendance.AttendanceUser;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.attendance.AttendanceUserService;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 请假申请-审批分组-请求人
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 18:55
 */
@Service
public class AskForLeavePacketApplicantService extends BaseService<AskForLeavePacketApplicantMapper, AskForLeavePacketApplicant> {

    @Resource
    AttendanceUserService attendanceUserService;

    @Resource
    UsersMapper usersMapper;

    /**
     * @方法名：insertOrUpdateApplicant
     * @描述： 新增或更新【请假申请-审批请求人】
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/21 19:14
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdateApplicant(List<String> applicantIdList, String packetId, int sign) {
        int result = 0;
        if(applicantIdList.size()==0){
            throw new BusinessException("the.requester.is.not.selected");
        }


        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        if(sign==1){
            /** 新增*/
            List<AskForLeavePacketApplicant> askForLeavePacketApplicantArrayList = new ArrayList<>();
            applicantIdList.forEach(userId ->{
                List<AttendanceUser> attendanceUserList = this.attendanceUserService.selectAttendanceUserByUserId(userId);
                if(attendanceUserList.size()==0){
                    throw new BusinessException("please.join.the.attendance.section.first");
                }
                List<AskForLeavePacketApplicant> oldAskForLeavePacketApplicants = this.mapper.selectApplicantByUserId(userId);
                System.err.println("Applicants="+oldAskForLeavePacketApplicants);
                if(oldAskForLeavePacketApplicants.size()>0){
                    String nikeName = this.usersMapper.selectNiceNameById(userId);
                    throw new BusinessException("applicants.can.only.join.one.group","申请人【"+nikeName+"】已经存在其他审批组中");
                }
                AskForLeavePacketApplicant askForLeavePacketApplicant = new AskForLeavePacketApplicant()
                        .setId(SnowIdUtil.getInstance().nextId()).setPacketId(packetId).setApplicantId(userId)
                        .setCreateTime(LocalDateTime.now()).setIsDelete(VariableConstants.STRING_CONSTANT_0)
                        //景区id
                        .setScenicId(scenicId);
                askForLeavePacketApplicantArrayList.add(askForLeavePacketApplicant);
            });
            result = this.mapper.insertList(askForLeavePacketApplicantArrayList);
        }else{
            /** 更新*/
            //1.先删除
            int removNum = this.deleteAskForLeavePacketApplicantByPacketId(packetId);
            if(removNum>0){
                //2.再新增
                result = this.insertOrUpdateApplicant(applicantIdList,packetId,1);
            }
        }
        return result;
    }

    /**
     * @方法名：deleteAskForLeavePacketApplicantByPacketId
     * @描述： 根据请假审批分组的id物理删除请假审批发起请求人
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/21 19:11
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteAskForLeavePacketApplicantByPacketId(String packetId) {
        Example example = new Example(AskForLeavePacketApplicant.class);
        example.and().andEqualTo("packetId", packetId);
        int result = this.mapper.deleteByExample(example);
        return result;
    }

    /**
     * @方法名：selectApplicantByPacketId
     * @描述： 根据分组id查询请假审批申请人
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 19:56
     */
    public List<AskForLeavePacketApplicant> selectApplicantByPacketId(String packetId) {
        List<AskForLeavePacketApplicant> list = this.mapper.selectApplicantByPacketId(packetId);
        return list;
    }
}
