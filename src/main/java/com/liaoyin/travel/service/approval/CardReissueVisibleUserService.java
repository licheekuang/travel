package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.approval.ApprovalCardReissueVisibleUserMapper;
import com.liaoyin.travel.entity.approval.CardReissueVisibleUser;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 补卡申请-可见用户权限
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 09:48
 */
@Service
public class CardReissueVisibleUserService extends BaseService<ApprovalCardReissueVisibleUserMapper, CardReissueVisibleUser> {

    /**
     * 批量新增或更新补卡申请可见人
     * @param everyMenIdList 可见人id字符集
     * @param sign【1:新增;2:更新】
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdateApprovalCardReissueVisible(List<String> everyMenIdList, int sign) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        int result = 0;
        if(1==sign){
            //新增
            List<CardReissueVisibleUser> cardReissueVisibleUserList = new ArrayList<>();
            everyMenIdList.forEach(userId ->{
                CardReissueVisibleUser cardReissueVisibleUser = new CardReissueVisibleUser()
                    .setId(SnowIdUtil.getInstance().nextId()).setUserId(userId).setCreateTime(LocalDateTime.now())
                    .setIsDelete(VariableConstants.STRING_CONSTANT_0).setScenicId(scenicId);
                cardReissueVisibleUserList.add(cardReissueVisibleUser);
            });
           result = this.mapper.insertList(cardReissueVisibleUserList);
        }else{
            //更新
            int deleteRow = this.mapper.deleteApprovalCardReissueVisibleUserAll();
            result = this.insertOrUpdateApprovalCardReissueVisible(everyMenIdList,1);
        }
        return result;
    }

    /**
     * @方法名：countCardReissueVisibleUserByUserId
     * @描述： 根据用户id统计用户的补卡申请-权限表的数量
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/22 15:43
     */
    public int countCardReissueVisibleUserByUserId(String userId) {
        Example example = new Example(CardReissueVisibleUser.class);
        example.and().andEqualTo("userId", userId);
        int count = this.mapper.selectCountByExample(example);
        return count;
    }

    /**
     * @方法名：selectCardReissueVisibleUserByScenicId
     * @描述： 根据景区id查询景区可见用户权限信息列表
     * @作者： kjz
     * @日期： Created in 2020/2/26 9:59
     */
    public List<CardReissueVisibleUser> selectCardReissueVisibleUserByScenicId(String scenicId) {
        return this.mapper.selectCardReissueVisibleUserByScenicId(scenicId);
    }
}
