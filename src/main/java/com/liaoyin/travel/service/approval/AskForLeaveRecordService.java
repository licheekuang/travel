package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.approval.AskForLeaveRecordMapper;
import com.liaoyin.travel.entity.approval.AskForLeaveRecord;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import com.liaoyin.travel.vo.approval.AskForLeaveRequestVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * 请假审批记录
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 16:54
 */
@Service
public class AskForLeaveRecordService extends BaseService<AskForLeaveRecordMapper, AskForLeaveRecord> {

    /**
     * @方法名：insertAskForLeaveRecord
     * @描述： 新增请假审批记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/22 19:58
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertAskForLeaveRecord(AskForLeaveRequestVo askForLeaveRequestVo, String approvalRecordId ) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        AskForLeaveRecord askForLeaveRecord = new AskForLeaveRecord()
                .setId(SnowIdUtil.getInstance().nextId())
                .setApprovalRecordId(approvalRecordId)
                .setLeaveStartTimeAndroid(askForLeaveRequestVo.getLeaveStartTime())
                .setLeaveEndTimeAndroid(askForLeaveRequestVo.getLeaveEndTime())
                .setLeaveTime(askForLeaveRequestVo.getLeaveTime())
                .setReasonForALeave(askForLeaveRequestVo.getReasonForALeave())
                .setImgUrl(askForLeaveRequestVo.getApprovalRecordVo().getImgUrl())
                .setAccessoryUrl(askForLeaveRequestVo.getAccessoryUrl())
                .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                .setCreateTime(LocalDateTime.now())
                //景区id
                .setScenicId(scenicId);
        int result = this.mapper.insert(askForLeaveRecord);
        return result;
    }


    /**
     * @方法名：selectAskForLeaveRecordByApprovalRecordId
     * @描述： 根据审批记录表id查看请假记录详情
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/23 14:36
     */
    public AskForLeaveRecord selectAskForLeaveRecordByApprovalRecordId(String approvalRecordId) {
        AskForLeaveRecord askForLeaveRecord = this.mapper.selectAskForLeaveRecordByApprovalRecordId(approvalRecordId);
        return askForLeaveRecord;
    }
}
