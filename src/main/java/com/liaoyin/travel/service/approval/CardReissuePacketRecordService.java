package com.liaoyin.travel.service.approval;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.business.util.AttendanceUtil;
import com.liaoyin.travel.view.moble.approval.PacketRecordDetailsView;
import com.liaoyin.travel.view.user.UserView;
import com.liaoyin.travel.vo.approval.ApprovalPacketRecordVo;
import com.liaoyin.travel.vo.approval.InsertOrUpdateApprovalRecordVo;
import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.approval.ApprovalCardReissuePacketRecordMapper;
import com.liaoyin.travel.entity.approval.CardReissuePacketApplicant;
import com.liaoyin.travel.entity.approval.CardReissuePacketRecord;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.UsersService;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import com.liaoyin.travel.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 补卡申请-审批分组
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 15:55
 */
@Service
public class CardReissuePacketRecordService extends BaseService<ApprovalCardReissuePacketRecordMapper, CardReissuePacketRecord> {

    @Resource
    CardReissuePacketApplicantService cardReissuePacketApplicantService;

    @Resource
    UsersService usersService;

    /**
     * @方法名：insertOrUpdateApprovalCardReissuePacketRecord
     * @描述： 新增或修改补卡申请的审批分组
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/21 16:06
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdateApprovalCardReissuePacketRecord(InsertOrUpdateApprovalRecordVo cardReissuePacketRecordVo) {
        //获取到用户的登录信息,判断用户是否登录
        UserInfo user = PartyUtil.getCurrentUserInfo();
        if(user == null) {
            throw new BusinessException("user.isNotLogin");
        }
        /** 景区id */
        String scenicId = user.getScenicId();
        int result = 0;
        //补卡申请表的id
        String id = cardReissuePacketRecordVo.getPacketId();
        //申请人id字符串集
        String applicantIds = cardReissuePacketRecordVo.getApplicantIdS();
        String[] applicantIdArray = applicantIds.split(",");
        List<String> applicantIdList = Arrays.asList(applicantIdArray);
        //审批人id
        String approvedBy = cardReissuePacketRecordVo.getApprovedBy();
        //为了防止前端传入的审批人最后一位有,要执行一次去掉最后一个字符操作
        approvedBy = approvedBy.substring(0, approvedBy.length()-1);
        /** 审批人只能选择管理员*/
        if(AttendanceUtil.isNotAdministrators(approvedBy)){
            throw new BusinessException("management.is.approvedBy");
        }
        //分组名称
        String packetName = cardReissuePacketRecordVo.getPacketName().trim();
        //是否删除
        String isDelete = cardReissuePacketRecordVo.getIsDelete();

        CardReissuePacketRecord cardReissuePacketRecord = new CardReissuePacketRecord();
        if(id==null || id.equals("")){
            //判断分组名字是否重复
            judgePacketNameRepetition(packetName,null);
            /** 新增*/
            String packetId = SnowIdUtil.getInstance().nextId();
            cardReissuePacketRecord
                    .setId(packetId)
                    .setCreateBy(user.getId())
                    .setCreateTime(LocalDateTime.now())
                    .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                    .setPacketName(packetName)
                    .setApprovedBy(approvedBy)
                    .setScenicId(scenicId);
            result = this.mapper.insert(cardReissuePacketRecord);
            if(result>0){
                this.cardReissuePacketApplicantService.insertOrUpdateApplicant(applicantIdList,packetId,1);
            }
        }else{
            /** 更新*/
            //判断分组名字是否重复
            judgePacketNameRepetition(packetName,id);
            //如果删除
            if(StringUtil.isNotBlank(isDelete) && VariableConstants.STRING_CONSTANT_1.equals(isDelete)){
                result = this.deleteApprovalCardReissuePacketRecordById(id);
            }else{
                //不是删除
                cardReissuePacketRecord
                        .setId(id)
                        .setUpdateBy(user.getId())
                        .setUpdateTime(LocalDateTime.now())
                        .setPacketName(packetName)
                        .setApprovedBy(approvedBy);

                result = this.mapper.updateById(cardReissuePacketRecord);
                if(result>0){
                    this.cardReissuePacketApplicantService.insertOrUpdateApplicant(applicantIdList,cardReissuePacketRecord.getId(),2);
                }
            }

        }
        return result;
    }

    /**
     * @方法名：judgePacketNameRepetition
     * @描述： 判断分组名是否重复
     * @作者： kjz
     * @日期： Created in 2020/4/14 13:25
     */
    public void judgePacketNameRepetition(String packetName,String id) {
        int count = mapper.selectRecordCountByPacketName(packetName,id);
        if(count>0){
           throw new BusinessException("packetName.is.exist");
        }
    }


    /**
     * @方法名：deleteApprovalCardReissuePacketRecordById
     * @描述： 物理删除补卡申请审批分组
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/21 17:05
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteApprovalCardReissuePacketRecordById(String id) {
        Example example = new Example(CardReissuePacketRecord.class);
        example.and().andEqualTo("id", id);
        int result = this.mapper.deleteByPrimaryKey(example);
        if(result>0){
            this.cardReissuePacketApplicantService.deleteApprovalCardReissuePacketApplicantBypacketId(id);
        }
        return result;
    }

    /**
     * @方法名：selectCardReissuePacketRecord
     * @描述： 根据条件查询补卡申请分组(带分页)
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/21 20:56
     */
    public PageInfo<CardReissuePacketRecord> selectCardReissuePacketRecordByCondition(ApprovalPacketRecordVo approvalPacketRecordVo) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();
        approvalPacketRecordVo.setScenicId(scenicId);

        PageHelper.startPage(approvalPacketRecordVo.getNum(),approvalPacketRecordVo.getSize());
        List<CardReissuePacketRecord> list = this.mapper.selectCardReissuePacketRecordByCondition(approvalPacketRecordVo);
        PageInfo<CardReissuePacketRecord> p = new PageInfo<>(list);
        return p;
    }

    /**
     * @方法名：deleteCardReissuePacketRecordByIds
     * @描述： 批量删除补卡审批分组
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 15:46
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteCardReissuePacketRecordByIds(String ids) {
        String[] id = ids.split(",");
        List<String> list = Arrays.asList(id);
        int result = 0;
        for(String packId:list){
            result += this.mapper.deleteCardReissuePacketRecordById(packId);
            this.cardReissuePacketApplicantService.deleteApprovalCardReissuePacketApplicantBypacketId(packId);
        }
        return result;
    }

    /**
     * @方法名：selectCardReissuePacketDetailsById
     * @描述： 根据id查询补卡审批分组的详情
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 17:41
     */
    public PacketRecordDetailsView selectCardReissuePacketDetailsById(String id) {
        CardReissuePacketRecord cardReissuePacketRecord = this.mapper.selectCardReissuePacketById(id);
        List<CardReissuePacketApplicant> applicants = this.cardReissuePacketApplicantService.selectApplicantByPacketId(id);
        PacketRecordDetailsView packetRecordDetailsView = new PacketRecordDetailsView();
        List<UserView> applicantList = new ArrayList<>();
        for(CardReissuePacketApplicant applicant:applicants){
            if(StringUtil.isNotEmpty(applicant.getApplicantId())){
                UserView userView = usersService.selectUserViewById(applicant.getApplicantId());
                applicantList.add(userView);
            }
        }
        UserView approver = usersService.selectUserViewById(cardReissuePacketRecord.getApprovedBy());
        packetRecordDetailsView.setApprover(approver)
                .setApplicantList(applicantList)
                .setCreateTime(cardReissuePacketRecord.getCreateTime())
                .setPacketName(cardReissuePacketRecord.getPacketName());
        return packetRecordDetailsView;
    }
}
