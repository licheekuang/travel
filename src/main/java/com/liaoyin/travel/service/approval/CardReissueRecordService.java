package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.approval.CardReissueRecordMapper;
import com.liaoyin.travel.entity.approval.CardReissueRecord;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import com.liaoyin.travel.vo.approval.CardReissueRecordVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * 补卡审批记录
 *
 * @author kuang.jiazhuo
 * @date 2019-11-23 09:53
 */
@Service
public class CardReissueRecordService extends BaseService<CardReissueRecordMapper, CardReissueRecord> {

    /**
     * @方法名：insertCardReissueRecord
     * @描述： 发起补卡申请
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/23 10:09
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertCardReissueRecord(CardReissueRecordVo cardReissueRecordVo,String approvalRecordId) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        CardReissueRecord cardReissueRecord = new CardReissueRecord()
                .setId(SnowIdUtil.getInstance().nextId())
                .setApprovalRecordId(approvalRecordId)
                .setSupplementaryCardCause(cardReissueRecordVo.getSupplementaryCardCause())
                .setShift(cardReissueRecordVo.getShift())
                .setLackOfCardId(cardReissueRecordVo.getLackOfCardId())
                .setLackOfCardDivisions(cardReissueRecordVo.getLackOfCardDivisions())
                .setClockTimeDisplay(cardReissueRecordVo.getClockTimeDisplay().toString())
                .setClockTime(LocalTime.parse(cardReissueRecordVo.getClockTime()))
                .setImgUrl(cardReissueRecordVo.getApprovalRecordVo().getImgUrl())
                .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                .setCreateTime(LocalDateTime.now())
                //景区id
                .setScenicId(scenicId);
        int result = this.mapper.insert(cardReissueRecord);
        return result;
    }

    /**
     * @方法名：selectCardReissueRecordByApprovalRecordId
     * @描述： 根据审批记录表的id
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/23 14:18
     */
    public CardReissueRecord selectCardReissueRecordByApprovalRecordId(String approvalRecordId) {
        CardReissueRecord cardReissueRecord = this.mapper.selectCardReissueRecordByApprovalRecordId(approvalRecordId);
        return cardReissueRecord;
    }
}
