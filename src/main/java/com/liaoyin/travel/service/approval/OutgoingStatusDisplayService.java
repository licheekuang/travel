package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.approval.OutgoingStatusDisplayMapper;
import com.liaoyin.travel.entity.approval.OutgoingStatusDisplay;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 外出状态显示service
 *
 * @author kuang.jiazhuo
 * @date 2019-11-25 22:28
 */
@Service
public class OutgoingStatusDisplayService extends BaseService<OutgoingStatusDisplayMapper, OutgoingStatusDisplay> {

    /**
     * @方法名：insertOutgoingStatusDisplay
     * @描述： 新增外出状态显示信息
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 22:34
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertOutgoingStatusDisplay(OutgoingStatusDisplay outgoingStatusDisplay) {
        outgoingStatusDisplay.setCreateTime(LocalDateTime.now()).setId(SnowIdUtil.getInstance().nextId());
        int result = this.mapper.insertSelective(outgoingStatusDisplay);
        return result;
    }

    /**
     * @方法名：selectDisplayByDate
     * @描述： 按时间查找需要外出显示的
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/28 17:46
     */
    public List<OutgoingStatusDisplay> selectDisplayByDate(LocalDateTime now) {
        List<OutgoingStatusDisplay> list = this.mapper.selectDisplayByDate(now);
        return list;
    }
}
