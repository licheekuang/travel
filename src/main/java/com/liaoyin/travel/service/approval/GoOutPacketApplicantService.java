package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.UsersMapper;
import com.liaoyin.travel.dao.approval.GoOutPacketApplicantMapper;
import com.liaoyin.travel.entity.approval.CardReissuePacketApplicant;
import com.liaoyin.travel.entity.approval.GoOutPacketApplicant;
import com.liaoyin.travel.entity.attendance.AttendanceUser;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.attendance.AttendanceUserService;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 外出申请-审批分组-请求人
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 19:57
 */
@Service
public class GoOutPacketApplicantService extends BaseService<GoOutPacketApplicantMapper, GoOutPacketApplicant> {

    @Resource
    AttendanceUserService attendanceUserService;

    @Resource
    UsersMapper usersMapper;

    /**
     * 新增或修改【外出申请-审批分组-请求人】
     * @param applicantIdList
     * @param packetId
     * @param sign【1、新增;2、更新】
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdateApplicant(List<String> applicantIdList, String packetId, int sign) {

        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        int result = 0;
        List<GoOutPacketApplicant> goOutPacketApplicantList = new ArrayList<>();
        if(applicantIdList.size()==0){
            throw new BusinessException("the.requester.is.not.selected");
        }
        if(sign==1){
            /** 新增*/
            applicantIdList.forEach(userId ->{
                List<AttendanceUser> attendanceUserList = this.attendanceUserService.selectAttendanceUserByUserId(userId);
                if(attendanceUserList.size()==0){
                    throw new BusinessException("please.join.the.attendance.section.first");
                }
                List<GoOutPacketApplicant> oldGoOutPacketApplicants = this.mapper.selectApplicantByUserId(userId);
                if(oldGoOutPacketApplicants.size()>0){
                    String nikeName = this.usersMapper.selectNiceNameById(userId);
                    throw new BusinessException("applicants.can.only.join.one.group","申请人【"+nikeName+"】已经存在其他审批组中");
                }
                GoOutPacketApplicant goOutPacketApplicant = new GoOutPacketApplicant()
                        .setId(SnowIdUtil.getInstance().nextId()).setPacketId(packetId).setApplicantId(userId)
                        .setCreateTime(LocalDateTime.now()).setIsDelete(VariableConstants.STRING_CONSTANT_0)
                        //景区id
                        .setScenicId(scenicId);
                goOutPacketApplicantList.add(goOutPacketApplicant);
            });
            result = this.mapper.insertList(goOutPacketApplicantList);
        }else{
            /** 更新*/
            //1.先删除
            int removNum = this.deleteGoOutPacketApplicantByPacketId(packetId);
            System.err.println("删除的行数="+removNum);
            if(removNum>0){
                //2.再新增
                result = this.insertOrUpdateApplicant(applicantIdList,packetId,1);
            }
        }
        System.err.println(goOutPacketApplicantList);
        return result;
    }

    /**
     * @方法名：deleteGoOutPacketApplicantByPacketId
     * @描述： 根据外出审批分组的id物理删除【外出-审批分组-申请请求人】
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/21 20:19
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteGoOutPacketApplicantByPacketId(String packetId) {
        Example example = new Example(CardReissuePacketApplicant.class);
        example.and().andEqualTo("packetId", packetId);
        int result = this.mapper.deleteByExample(example);
        return result;
    }

    /**
     * @方法名：selectApplicantByPacketId
     * @描述： 根据分组id查询外出申请请求人
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 20:09
     */
    public List<GoOutPacketApplicant> selectApplicantByPacketId(String packetId) {
        List<GoOutPacketApplicant> list = this.mapper.selectApplicantByPacketId(packetId);
        return list;
    }
}
