package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.approval.GoOutRecordMapper;
import com.liaoyin.travel.entity.approval.GoOutRecord;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.DateUtil;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import com.liaoyin.travel.vo.approval.GoOutRecordVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 *  外出申请
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 20:15
 */
@Service
public class GoOutRecordService extends BaseService<GoOutRecordMapper, GoOutRecord> {

    /**
     * @方法名：insertGoOutRecord
     * @描述： 新增外出申请记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/22 21:17
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertGoOutRecord(GoOutRecordVo goOutRecordVo, String approvalRecordId) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        GoOutRecord goOutRecord = new GoOutRecord()
                .setId(SnowIdUtil.getInstance().nextId())
                .setApprovalRecordId(approvalRecordId)
                .setDepartureTime(DateUtil.strTransitionLocalDateTime(goOutRecordVo.getDepartureTime()))
                .setEndOfOuting(DateUtil.strTransitionLocalDateTime(goOutRecordVo.getEndOfOuting()))
                .setGoOutTime(goOutRecordVo.getGoOutTime())
                .setGoOutFor(goOutRecordVo.getGoOutFor())
                .setCarFare(goOutRecordVo.getCarFare())
                .setImgUrl(goOutRecordVo.getApprovalRecordVo().getImgUrl())
                .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                .setCreateTime(LocalDateTime.now())
                //景区id
                .setScenicId(scenicId);
        int result = this.mapper.insert(goOutRecord);
        return result;
    }

    /**
     * @方法名：selectGoOutRecordByApprovalRecordId
     * @描述： 根据审批记录表的id查看外出申请详情
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/23 14:39
     */
    public GoOutRecord selectGoOutRecordByApprovalRecordId(String approvalRecordId) {
        GoOutRecord goOutRecord = this.mapper.selectGoOutRecordByApprovalRecordId(approvalRecordId);
        return goOutRecord;
    }
}
