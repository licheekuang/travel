package com.liaoyin.travel.service.approval;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.UsersMapper;
import com.liaoyin.travel.dao.approval.ApprovalCardReissuePacketApplicantMapper;
import com.liaoyin.travel.entity.approval.CardReissuePacketApplicant;
import com.liaoyin.travel.entity.attendance.AttendanceUser;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.attendance.AttendanceUserService;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 补卡申请-审批分组-请求人
 *
 * @author kuang.jiazhuo
 * @date 2019-11-21 16:08
 */
@Service
public class CardReissuePacketApplicantService extends BaseService<ApprovalCardReissuePacketApplicantMapper, CardReissuePacketApplicant> {

    @Resource
    AttendanceUserService attendanceUserService;

    @Resource
    UsersMapper usersMapper;

    /**
     * 新增或更新补卡-审批分组-申请请求人
     * @param applicantIdList
     * @param packetId
     * @param sign【1、新增;2、更新】
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdateApplicant(List<String> applicantIdList, String packetId, int sign) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        int result = 0;
        if(applicantIdList.size()==0){
            throw new BusinessException("the.requester.is.not.selected");
        }
        if(sign==1){
            /** 新增*/
            List<CardReissuePacketApplicant> cardReissuePacketApplicantList = new ArrayList<>();
            applicantIdList.forEach(userId ->{
                List<AttendanceUser> attendanceUserList = this.attendanceUserService.selectAttendanceUserByUserId(userId);
                if(attendanceUserList.size()==0){
                    throw new BusinessException("please.join.the.attendance.section.first");
                }
                List<CardReissuePacketApplicant> oldCardReissuePacketApplicants = this.selectApplicantByUserId(userId);
                if(oldCardReissuePacketApplicants.size()>0){
                    String nikeName = this.usersMapper.selectNiceNameById(userId);
                    throw new BusinessException("applicants.can.only.join.one.group","申请人【"+nikeName+"】已经存在其他审批组中");
                }
                CardReissuePacketApplicant cardReissuePacketApplicant = new CardReissuePacketApplicant()
                        .setId(SnowIdUtil.getInstance().nextId()).setPacketId(packetId).setApplicantId(userId)
                        .setCreateTime(LocalDateTime.now()).setIsDelete(VariableConstants.STRING_CONSTANT_0)
                        //景区id
                        .setScenicId(scenicId);
                cardReissuePacketApplicantList.add(cardReissuePacketApplicant);
            });
            result = this.mapper.insertList(cardReissuePacketApplicantList);
        }else{
            /** 更新*/
            //1.先删除
            int removNum = this.deleteApprovalCardReissuePacketApplicantBypacketId(packetId);
            result = this.insertOrUpdateApplicant(applicantIdList,packetId,1);
            /*if(removNum>0){
                //2.再新增
                result = this.insertOrUpdateApplicant(applicantIdList,packetId,1);
            }*/
        }
        return result;
    }

    /**
     * @方法名：selectApplicantByUserId
     * @描述： 查询补卡审批申请人
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/28 16:02
     */
    private List<CardReissuePacketApplicant> selectApplicantByUserId(String userId) {
        List<CardReissuePacketApplicant> list = this.mapper.selectApplicantByUserId(userId);
        return list;
    }

    /**
     * @方法名：deleteApprovalCardReissuePacketApplicantBypacketId
     * @描述： 根据补卡审批分组的id物理删除【补卡-审批分组-申请请求人】
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/21 17:09
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteApprovalCardReissuePacketApplicantBypacketId(String packetId) {
        Example example = new Example(CardReissuePacketApplicant.class);
        example.and().andEqualTo("packetId", packetId);
        int result = this.mapper.deleteByExample(example);
        return result;
    }

    /**
     * @方法名：selectApplicantByPacketId
     * @描述： 根据分组id查询请求人List
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 17:44
     */
    public List<CardReissuePacketApplicant> selectApplicantByPacketId(String packetId) {
        List<CardReissuePacketApplicant> list = this.mapper.selectApplicantByPacketId(packetId);
        return list;
    }
}
