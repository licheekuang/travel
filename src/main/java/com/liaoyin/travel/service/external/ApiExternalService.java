package com.liaoyin.travel.service.external;

import com.alibaba.fastjson.JSONObject;
import com.liaoyin.travel.view.ApiExternal.*;
import com.liaoyin.travel.view.mine.attendance.AttendanceStatisticsDetailedView;
import com.liaoyin.travel.vo.task.InsertTaskVo;
import com.liaoyin.travel.vo.task.SelectTaskListVo;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.DictBasicMapper;
import com.liaoyin.travel.dao.UserTeamMapper;
import com.liaoyin.travel.dao.UsersMapper;
import com.liaoyin.travel.dao.attendance.AttendanceStatisticsMapper;
import com.liaoyin.travel.dao.attendance.EquipmentMapper;
import com.liaoyin.travel.dao.report.EventReportMapper;
import com.liaoyin.travel.dao.task.TaskExceptionDescriptionMapper;
import com.liaoyin.travel.dao.task.TaskMapper;
import com.liaoyin.travel.dao.task.TaskReceivingRecordsMapper;
import com.liaoyin.travel.dao.team.TeamMapper;
import com.liaoyin.travel.dao.team.WorkerMapper;
import com.liaoyin.travel.entity.DictBasic;
import com.liaoyin.travel.entity.UserTeam;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.attendance.Equipment;
import com.liaoyin.travel.entity.report.EventReport;
import com.liaoyin.travel.entity.task.*;
import com.liaoyin.travel.entity.team.Team;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.task.TaskBusinessService;
import com.liaoyin.travel.service.task.TaskLineService;
import com.liaoyin.travel.service.task.TaskReceivingRecordsService;
import com.liaoyin.travel.service.task.TaskService;
import com.liaoyin.travel.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author privatePanda777@163.com
 * @title: ApiExternalService
 * @projectName travel
 * @description: TODO  对外接口的Service
 * @date 2019/8/1210:41
 */
@Service
public class ApiExternalService extends BaseService<TaskMapper,Task> {


    /*//AES加密密钥
    private static final String AES_KEY = "ae2f88c2031f470385a4edb16be8eda5";*/

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskLineService taskLineService;
    @Autowired
    private TaskBusinessService taskBusinessService;
    @Autowired
    private TaskReceivingRecordsService taskReceivingRecordsService;

    @Resource
    private UsersMapper usersMapper;
    @Resource
    private TaskMapper taskMapper;
    @Resource
    private TeamMapper teamMapper;
    @Resource
    private WorkerMapper workerMapper;
    @Resource
    private TaskReceivingRecordsMapper taskReceivingRecordsMapper;
    @Resource
    private AttendanceStatisticsMapper attendanceStatisticsMapper;
    @Resource
    private EquipmentMapper equipmentMapper;
    @Resource
    private DictBasicMapper dictBasicMapper;
    @Resource
    private EventReportMapper eventReportMapper;
    @Resource
    private TaskExceptionDescriptionMapper taskExceptionDescriptionMapper;
    @Resource
    private UserTeamMapper userTeamMapper;

    /**
     * @方法名：issueWarningTask
     * @描述： 选择预警地点周围最近且没有任务的一个员工 去执行此任务 //发布人使用System id 为 01.
     * @作者： 周明智
     * @日期： Created in 2019/8/12 10:45
     */
    public ApiExternalUser issueWarningTask(String taskName,String taskStatement,String taskLat,String taskLog){
        System.out.println("------------------------"+taskLat+","+taskLog);
        String toUserId = null;
        InsertTaskVo insertTaskVo = new InsertTaskVo();
        Task task = new Task();
        String id = UUIDUtil.getUUID();
        task.setTaskName(taskName);
        task.setTaskStatement(taskStatement);
        task.setTaskLat(taskLat);
        task.setTaskLog(taskLog);
        task.setId(id);
        task.setUserId("System");
        task.setTaskLevel((short)2);
        task.setTaskType((short)2);
        task.setTaskCategories((short)2);
        task.setIsSpecify((short)1);
        task.setCreatTime(new Date());
        insertTaskVo.setTask(task);
        if (task.getTaskType() == 2){
            TaskLine taskLine = new TaskLine();
            List<TaskDetails> detailsList = new ArrayList<>();
            TaskDetails details = new TaskDetails();
            details.setId(UUIDUtil.getUUID());
            details.setIsDelete((short)0);
            details.setTaskId(task.getId());
            details.setTaskFlow((short)1);
            details.setTaskOperation((short)1);
            details.setLat(task.getTaskLat());
            details.setLog(task.getTaskLog());
            detailsList.add(details);
            System.out.println(detailsList.toString());
            taskLine.setTaskDetailsList(detailsList);
            String s = taskLineService.insertOrUpdateTaskLine(taskLine);
            task.setLineId(s);
        }

        Map<String, Double> tempMap = new HashMap<>();
        //查询所有没有任务的员工
        List<String> strings = this.taskMapper.selectIdleEmployeeId();
        for (String uid : strings) {
            String s = gpsDataMap.get(uid);
            if (null != s) {
                String[] split = s.split(",");
                double distance = LocationUtils.getDistance(Double.valueOf(taskLat), Double.valueOf(taskLog), Double.valueOf(split[0]), Double.valueOf(split[1]));
                tempMap.put(uid,distance);
            }
        }
        if (null == strings || tempMap.size() == 0){
            //如果没有空闲员工,查询所有员工
            List<Users> usersList = this.usersMapper.selectAll();
            //查询离任务最近的员工撤销当前任务再发布紧急任务
            for (Users u : usersList) {
                if("2".equals(u.getUserType())) {
                    continue;
                }
                String s = gpsDataMap.get(u.getId());
                if (null != s) {
                    String[] split = s.split(",");
                    double distance = LocationUtils.getDistance(Double.valueOf(taskLat), Double.valueOf(taskLog), Double.valueOf(split[0]), Double.valueOf(split[1]));
                    tempMap.put(u.getId(), distance);
                }
            }
            Object minValue = MapUtil.getMinValue(tempMap);
            if (null != minValue){
                List key = (List) (MapUtil.getKey(tempMap, minValue));
                toUserId = (String) key.get(0);
            } else {
                throw new BusinessException("no.idle.users");
            }
            //先撤销其当前任务
            //查询当前用户领取的正在进行中的任务
            Example example = new Example(TaskReceivingRecords.class);
            example.and().andEqualTo("userId",toUserId).andEqualTo("tastStatus",(short)2)
                    .andIsNotNull("taskStartTime").andIsNull("taskEndTime");
            List<TaskReceivingRecords> taskReceivingRecords = this.taskReceivingRecordsMapper.selectByExample(example);
            if (taskReceivingRecords.size()>0){
                for (TaskReceivingRecords record : taskReceivingRecords) {
                    Task t = new Task();
                    t.setId(record.getTaskId());
                    Task t1 = this.taskMapper.selectByPrimaryKey(t);
                    //如果正在执行紧急任务，则不能让他上报事件，直接抛业务异常
                    if(t1.getTaskLevel() == 2){
                        throw new BusinessException("taskLevel.is.two");
                    }
                    record.setTastStatus((short)4);
                    int i = this.taskReceivingRecordsMapper.updateByPrimaryKeySelective(record);
                    System.out.println("修改当前执行任务为已撤销数："+i);
                    tempTaskMap.put(id,record.getId());
                }
            }
            //发布紧急任务
            insertTaskVo.setBusinessId(toUserId);
        } else {
            //根据没有任务的员工查询里任务最近的一个
            for (String uid : strings) {
                String s = gpsDataMap.get(uid);
                if (null != s) {
                    String[] split = s.split(",");
                    double distance = LocationUtils.getDistance(Double.valueOf(taskLat), Double.valueOf(taskLog), Double.valueOf(split[0]), Double.valueOf(split[1]));
                    tempMap.put(uid,distance);
                }
            }
            if (tempMap.size()>0){
                Object minValue = MapUtil.getMinValue(tempMap);
                List<String> key = (List)MapUtil.getKey(tempMap, minValue);
                for (String s : key) {
                    toUserId = s;
                }
                //对里任务点最近的一个发布紧急任务
                insertTaskVo.setBusinessId(toUserId);
            } else {
                return null;
            }
            /*toUserId="39568981b38f4219a66038867667056f";
            insertTaskVo.setBusinessId("39568981b38f4219a66038867667056f");*/
        }
        tempTaskMap.put("userBriefing",id);
        int k = this.taskMapper.insertSelective(insertTaskVo.getTask());
        //添加领取人
        String isSpecify = String.valueOf(task.getIsSpecify());
        if(k >0) {
            //添加任务指定的领取人
            if(task.getIsSpecify() !=null) {
                this.taskBusinessService.insertTaskBusiness(String.valueOf(task.getIsSpecify()), insertTaskVo.getBusinessId(), id);
            }
        }
        //推送任务给指派人
        HashMap<String, String> map = new HashMap<>();
        map.put("fromUserId", "System");
        map.put("toUserId",insertTaskVo.getBusinessId());
        map.put("objectName","RC:TxtMsg");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", "您有新的任务！");
        jsonObject.put("extra", "任务名称:" + insertTaskVo.getTask().getTaskName());
        map.put("content",jsonObject.toJSONString());
        /*map.put("pushContent","您有新的任务");*/
        try {
            /*String s1 = RongUtils.groupManagement(map, "6");*/
            String s = RongUtils.groupManagement(map, "7");
            System.out.println("::::::"+s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Users> usersList = this.usersMapper.selectAll();
        StringBuffer buffer = new StringBuffer();
        for (Users u : usersList) {
            buffer.append(u.getId()+"&");
        }
        System.out.println(buffer.toString());
        HashMap<String, String> map1 = new HashMap<>();
        map1.put("fromUserId", "System");
        map1.put("toUserId",buffer.toString());
        map1.put("objectName","RC:TxtMsg");
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("content", "发生紧急事件,"+taskStatement);
        jsonObject1.put("extra",  insertTaskVo.getTask().getTaskName()+taskStatement);
        map1.put("content",jsonObject1.toJSONString());
        /*map1.put("pushContent","您有新的任务");*/
        try {
            /*String s1 = RongUtils.groupManagement(map, "6");*/
            String s = RongUtils.groupManagement(map1, "7");
            System.out.println("::::::"+s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tempMap=null;
        insertTaskVo = null;
        ApiExternalUser externalUser = null;
        if(toUserId!=null){
            //返回指派人的信息
            Users users = this.usersMapper.selectByPrimaryKey(toUserId);
            List<UserTeam> userTeamList = this.userTeamMapper.selectUserTeam(toUserId, null);
            ArrayList<String> temas = new ArrayList<String>();
            for (UserTeam userTeam : userTeamList) {
                temas.add(userTeam.getTeamId());
            }
            Example example = new Example(Team.class);
            example.and().andIn("id",temas);
            externalUser = new ApiExternalUser();
            externalUser.setId(users.getId());
            externalUser.setAccount(users.getAccount());
            externalUser.setNickName(users.getNickName());
            externalUser.setTeamName(this.teamMapper.selectByExample(example).get(0).getTeamName());
            externalUser.setWorkerName(this.workerMapper.selectByPrimaryKey(users.getWorkId()).getWorkerName());
            externalUser.setTaskId(id);
        }

        return externalUser;
    }

    /**
     * @方法名：issueWarningTaskNew
     * @描述： 新增一键报警，但是取消之前的【当前员工有正在进行的紧急任务时,不能一键报警】
     * @作者： kjz
     * @日期： Created in 2020/4/2 18:42
     */
    @Transactional(rollbackFor = Exception.class)
    public ApiExternalUser issueWarningTaskNew(String taskName,String taskStatement,String taskLat,String taskLog){

        /** 获取登录用户信息*/
        UserInfo activeUser = PartyUtil.getCurrentUserInfo();
        Optional.ofNullable(activeUser).orElseThrow(() -> new BusinessException("not.login"));

        System.out.println("------------------------"+taskLat+","+taskLog);
        String toUserId = null;
        InsertTaskVo insertTaskVo = new InsertTaskVo();
        Task task = new Task();
        String id = UUIDUtil.getUUID();
        task.setTaskName(taskName);
        task.setTaskStatement(taskStatement);
        task.setTaskLat(taskLat);
        task.setTaskLog(taskLog);
        task.setId(id);
        task.setUserId(activeUser.getId());
        task.setTaskLevel((short)2);
        task.setTaskType((short)2);
        task.setTaskCategories((short)2);
        task.setIsSpecify((short)1);
        task.setCreatTime(new Date());
        insertTaskVo.setTask(task);
        if (task.getTaskType() == 2){
            TaskLine taskLine = new TaskLine();
            List<TaskDetails> detailsList = new ArrayList<>();
            TaskDetails details = new TaskDetails();
            details.setId(UUIDUtil.getUUID());
            details.setIsDelete((short)0);
            details.setTaskId(task.getId());
            details.setTaskFlow((short)1);
            details.setTaskOperation((short)1);
            details.setLat(task.getTaskLat());
            details.setLog(task.getTaskLog());
            detailsList.add(details);
            System.out.println(detailsList.toString());
            taskLine.setTaskDetailsList(detailsList);
            String s = taskLineService.insertOrUpdateTaskLine(taskLine);
            task.setLineId(s);
        }

        Map<String, Double> tempMap = new HashMap<>();
        //查询所有没有任务的员工
        List<String> strings = this.taskMapper.selectIdleEmployeeId();
        for (String uid : strings) {
            String s = gpsDataMap.get(uid);
            if (null != s) {
                String[] split = s.split(",");
                double distance = LocationUtils.getDistance(Double.valueOf(taskLat), Double.valueOf(taskLog), Double.valueOf(split[0]), Double.valueOf(split[1]));
                tempMap.put(uid,distance);
            }
        }
        if (null == strings || tempMap.size() == 0){
            //如果没有空闲员工,查询所有员工
            List<Users> usersList = this.usersMapper.selectAll();
            //查询离任务最近的员工撤销当前任务再发布紧急任务
            for (Users u : usersList) {
                if("2".equals(u.getUserType())) {
                    continue;
                }
                String s = gpsDataMap.get(u.getId());
                if (null != s) {
                    String[] split = s.split(",");
                    double distance = LocationUtils.getDistance(Double.valueOf(taskLat), Double.valueOf(taskLog), Double.valueOf(split[0]), Double.valueOf(split[1]));
                    tempMap.put(u.getId(), distance);
                }
            }
            Object minValue = MapUtil.getMinValue(tempMap);
            if (null != minValue){
                List key = (List) (MapUtil.getKey(tempMap, minValue));
                toUserId = (String) key.get(0);
            } else {
                throw new BusinessException("no.idle.users");
            }
            //先撤销其当前任务
            //查询当前用户领取的正在进行中的任务
            Example example = new Example(TaskReceivingRecords.class);
            example.and().andEqualTo("userId",toUserId).andEqualTo("tastStatus",(short)2)
                    .andIsNotNull("taskStartTime").andIsNull("taskEndTime");
            List<TaskReceivingRecords> taskReceivingRecords = this.taskReceivingRecordsMapper.selectByExample(example);
            if (taskReceivingRecords.size()>0){
                for (TaskReceivingRecords record : taskReceivingRecords) {
                    /*Task t = new Task();
                    t.setId(record.getTaskId());
                    Task t1 = this.taskMapper.selectByPrimaryKey(t);
                    //如果正在执行紧急任务，则不能让他上报事件，直接抛业务异常
                    if(t1.getTaskLevel() == 2){
                        throw new BusinessException("taskLevel.is.two");
                    }*/
                    record.setTastStatus((short)4);
                    int i = this.taskReceivingRecordsMapper.updateByPrimaryKeySelective(record);
                    System.out.println("修改当前执行任务为已撤销数："+i);
                    tempTaskMap.put(id,record.getId());
                }
            }
            //发布紧急任务
            insertTaskVo.setBusinessId(toUserId);
        } else {
            //根据没有任务的员工查询里任务最近的一个
            for (String uid : strings) {
                String s = gpsDataMap.get(uid);
                if (null != s) {
                    String[] split = s.split(",");
                    double distance = LocationUtils.getDistance(Double.valueOf(taskLat), Double.valueOf(taskLog), Double.valueOf(split[0]), Double.valueOf(split[1]));
                    tempMap.put(uid,distance);
                }
            }
            if (tempMap.size()>0){
                Object minValue = MapUtil.getMinValue(tempMap);
                List<String> key = (List)MapUtil.getKey(tempMap, minValue);
                for (String s : key) {
                    toUserId = s;
                }
                //对里任务点最近的一个发布紧急任务
                insertTaskVo.setBusinessId(toUserId);
            } else {
                return null;
            }
            /*toUserId="39568981b38f4219a66038867667056f";
            insertTaskVo.setBusinessId("39568981b38f4219a66038867667056f");*/
        }
        tempTaskMap.put("userBriefing",id);
        int k = this.taskMapper.insertSelective(insertTaskVo.getTask());
        //添加领取人
        String isSpecify = String.valueOf(task.getIsSpecify());
        if(k >0) {
            //添加任务指定的领取人
            if(task.getIsSpecify() !=null) {
                this.taskBusinessService.insertTaskBusiness(String.valueOf(task.getIsSpecify()), insertTaskVo.getBusinessId(), id);
            }
        }
        //推送任务给指派人
        HashMap<String, String> map = new HashMap<>();
        map.put("fromUserId", activeUser.getId());
        map.put("toUserId",insertTaskVo.getBusinessId());
        map.put("objectName","RC:TxtMsg");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", "您有新的任务！");
        jsonObject.put("extra", "任务名称:" + insertTaskVo.getTask().getTaskName());
        map.put("content",jsonObject.toJSONString());
        /*map.put("pushContent","您有新的任务");*/
        try {
            /*String s1 = RongUtils.groupManagement(map, "6");*/
            String s = RongUtils.groupManagement(map, "7");
            System.out.println("::::::"+s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Users> usersList = this.usersMapper.selectAll();
        StringBuffer buffer = new StringBuffer();
        for (Users u : usersList) {
            buffer.append(u.getId()+"&");
        }
        System.out.println(buffer.toString());
        HashMap<String, String> map1 = new HashMap<>();
        map1.put("fromUserId", activeUser.getId());
        map1.put("toUserId",buffer.toString());
        map1.put("objectName","RC:TxtMsg");
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("content", "发生紧急事件,"+taskStatement);
        jsonObject1.put("extra",  insertTaskVo.getTask().getTaskName()+taskStatement);
        map1.put("content",jsonObject1.toJSONString());
        /*map1.put("pushContent","您有新的任务");*/
        try {
            /*String s1 = RongUtils.groupManagement(map, "6");*/
            String s = RongUtils.groupManagement(map1, "7");
            System.out.println("::::::"+s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tempMap=null;
        insertTaskVo = null;
        ApiExternalUser externalUser = null;
        if(toUserId!=null){
            //返回指派人的信息
            Users users = this.usersMapper.selectByPrimaryKey(toUserId);
            List<UserTeam> userTeamList = this.userTeamMapper.selectUserTeam(toUserId, null);
            ArrayList<String> temas = new ArrayList<String>();
            for (UserTeam userTeam : userTeamList) {
                temas.add(userTeam.getTeamId());
            }
            Example example = new Example(Team.class);
            example.and().andIn("id",temas);
            externalUser = new ApiExternalUser();
            externalUser.setId(users.getId());
            externalUser.setAccount(users.getAccount());
            externalUser.setNickName(users.getNickName());
            externalUser.setTeamName(this.teamMapper.selectByExample(example).get(0).getTeamName());
            externalUser.setWorkerName(this.workerMapper.selectByPrimaryKey(users.getWorkId()).getWorkerName());
            externalUser.setTaskId(id);
        }
        System.err.println("externalUser="+externalUser);
        return externalUser;
    }


    /**
    　* @description: TODO  获取当日的任务列表
    　* @param []
    　* @return java.util.List<com.liaoyin.travel.entity.task.Task>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/12 10:50
    　*/
    public List<ApiExternalTask> getTaskList(String taskType){
        UserInfo currentUserInfo = PartyUtil.getCurrentUserInfo();
        if(currentUserInfo == null) {
            throw new BusinessException("not.login");
        }
        StringBuffer teamId = new StringBuffer();
        List<UserTeam> usertramList = currentUserInfo.getUserTeamList();
        for(UserTeam u:usertramList) {
            teamId.append(u.getTeamId()+",");
        }
        SelectTaskListVo taskListVo = new SelectTaskListVo();
        //查询类型:前端管理端
        taskListVo.setQurtyType("2");
        //任务查询类别:全部任务
        taskListVo.setTaskQurty("1");
        //任务时间
        taskListVo.setTaskTime(DateUtil.getCurrentyyyyMMdd());
        if(null != taskType) {
            taskListVo.setTaskType(taskType);
        }
        if(!StringUtil.isEmpty(teamId)){
            taskListVo.setTeamId(teamId.toString());
        }
        List<Task> tasks = this.mapper.selectTaskListOnBack(taskListVo);
        for (Task task : tasks) {
            String s = this.mapper.selectNickNameByBackUser(task.getUserId());
            if (s != null){
                task.setNickName(s);
            } else {
                String s1 = this.mapper.selectNickNameByUsers(task.getUserId());
                task.setNickName(s1);
            }
        }
        List<ApiExternalTask> apiExternalTasks = new ArrayList<>();
        if (tasks.size() == 0){
            throw new BusinessException("task.is.null");
        }
        for (Task task : tasks) {
            ApiExternalTask externalTask = new ApiExternalTask();
            externalTask.setCreatTime(task.getCreatTime().getTime());
            externalTask.setNickName(task.getNickName());
            externalTask.setTaskCategories(task.getTaskCategories());
            externalTask.setTaskId(task.getId());
            externalTask.setTaskLevel(task.getTaskLevel());
            externalTask.setTaskType(task.getTaskType());
            if (null != task.getTaskStatus()) {
                externalTask.setTaskStatus(Short.valueOf(task.getTaskStatus()));
            }
            externalTask.setTaskStatement(task.getTaskStatement());
            externalTask.setTaskName(task.getTaskName());
            apiExternalTasks.add(externalTask);
        }
        return apiExternalTasks;
    }


    /**
    　* @description: TODO 获取当日的考勤率及其异常考勤列表
    　* @param []
    　* @return com.liaoyin.travel.view.mine.attendance.AttendanceStatisticsDetailedView
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/12 10:48
    　*/
    public Map<String, Object> getAttendanceStatisticsDetailedList(){
        Example example = new Example(Users.class);
        example.and().andEqualTo("delFlag",(short)0).andEqualTo("isUsing",(short)1);
        List<Users> users = usersMapper.selectByExample(example);
        List<AttendanceStatisticsDetailedView> statisticsDetailedViews = this.attendanceStatisticsMapper.selectAttendanceStatisticsDetailedListByTime(null, new Timestamp(System.currentTimeMillis()).toString());
        List<AttendanceStatisticsDetailedView> tempList = new ArrayList<>();
        List<AttendanceStatisticsDetailedView> list = new ArrayList<>();
        for (AttendanceStatisticsDetailedView view : statisticsDetailedViews) {
            if (Integer.valueOf(view.getWorkType()) == 3 || Integer.valueOf(view.getWorkType()) == 1 || Integer.valueOf(view.getWorkType()) == 1){
                tempList.add(view);
            }
            if (Integer.valueOf(view.getWorkType()) == 3 || Integer.valueOf(view.getWorkType()) == 4 || Integer.valueOf(view.getWorkType()) == 5){
                list.add(view);
            }
        }
        Float percentage = ((float)tempList.size()/(float) users.size())*100;
        tempList=null;
        statisticsDetailedViews=null;
        ArrayList<ApiExternalAttendance> views = new ArrayList<>();
        for (AttendanceStatisticsDetailedView detailedView : list) {
            ApiExternalAttendance view = new ApiExternalAttendance();
            view.setUsersNickName(detailedView.getUsersNickName());
            view.setCheckTime(detailedView.getCheckTime().getTime());
            view.setDetailedTime(detailedView.getDetailedTime().getTime());
            view.setWorkType(detailedView.getWorkType());
            views.add(view);
        }
        Map map = new HashMap<>();
        map.put("percentage",percentage);
        map.put("list",views);
        return map;
    }

    /**
    　* @description: TODO  获取所有保洁人员  -仁义在线  保洁工种ID 485f30c2c0b84551b8d10aa6a86afac7
    　* @param []
    　* @return java.util.List<com.liaoyin.travel.entity.Users>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 10:12
    　*/
    public List<ApiExternalUser> getUsesList(String workerId){
        Example example = new Example(Users.class);
        example.and().andEqualTo("workId",workerId);
        List<Users> users = this.usersMapper.selectByExample(example);
        List<ApiExternalUser> apiExternalUsers = new ArrayList<>();
        for (Users user : users) {
            ApiExternalUser au = new ApiExternalUser();
            au.setId(user.getId());
            au.setNickName(user.getNickName());
            if (null != user.getWorkId()) {au.setWorkerName(this.workerMapper.selectByPrimaryKey(user.getWorkId()).getWorkerName());}
            if (null != user.getTeamId()) {au.setTeamName(this.teamMapper.selectByPrimaryKey(user.getTeamId()).getTeamName());}
            apiExternalUsers.add(au);
        }
        return apiExternalUsers;
    }

    /**
    　* @description: TODO  发布保洁任务   -仁义在线
    　* @param [userId, lat, log, taskName, taskStatement]
    　* @return com.liaoyin.travel.view.ApiExternal.ApiExternalUser
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 10:26
    　*/
    public ApiExternalUser cleaningTask(String userId ,String taskLat, String taskLog, String taskName,String taskStatement){
        String toUserId = userId;
        InsertTaskVo insertTaskVo = new InsertTaskVo();
        Task task = new Task();
        String id = UUIDUtil.getUUID();
        task.setTaskName(taskName);
        task.setTaskStatement(taskStatement);
        task.setTaskLat(taskLat);
        task.setTaskLog(taskLog);
        task.setId(id);
        task.setUserId("System");
        task.setTaskLevel((short)1);
        task.setTaskType((short)2);
        task.setTaskCategories((short)2);
        task.setIsSpecify((short)1);
        task.setCreatTime(new Date());
        task.setBusinessId(toUserId);
        insertTaskVo.setTask(task);
        if (task.getTaskType() == 2){
            TaskLine taskLine = new TaskLine();
            List<TaskDetails> detailsList = new ArrayList<>();
            TaskDetails details = new TaskDetails();
            details.setId(UUIDUtil.getUUID());
            details.setIsDelete((short)0);
            details.setTaskId(task.getId());
            details.setTaskFlow((short)1);
            details.setTaskOperation((short)1);
            details.setLat(task.getTaskLat());
            details.setLog(task.getTaskLog());
            detailsList.add(details);
            System.out.println(detailsList.toString());
            taskLine.setTaskDetailsList(detailsList);
            String s = taskLineService.insertOrUpdateTaskLine(taskLine);
            task.setLineId(s);
        }
        int k = this.taskMapper.insertSelective(insertTaskVo.getTask());
        //添加领取人
        String isSpecify = String.valueOf(task.getIsSpecify());
        if(k >0) {
            //添加任务指定的领取人
            tempTaskMap.put("cleaningTask",task.getId());
            if(task.getIsSpecify() !=null) {
                this.taskBusinessService.insertTaskBusiness(String.valueOf(task.getIsSpecify()), toUserId, id);
            }
        }
        //推送任务给指派人
        HashMap<String, String> map = new HashMap<>();
        map.put("fromUserId", "System");
        map.put("toUserId",toUserId);
        map.put("objectName","RC:TxtMsg");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", "您有新的任务！");
        jsonObject.put("extra", "任务名称:" + insertTaskVo.getTask().getTaskName());
        map.put("content",jsonObject.toJSONString());

        try {
            /*String s1 = RongUtils.groupManagement(map, "6");*/
            map.put("pushContent","您有新的任务");
            String s = RongUtils.groupManagement(map, "7");
            System.out.println(/*s1+*/"::::::"+s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        insertTaskVo = null;
        //返回指派人的信息
        Users users = this.usersMapper.selectByPrimaryKey(toUserId);
        ApiExternalUser externalUser = new ApiExternalUser();
        externalUser.setId(users.getId());
        externalUser.setAccount(users.getAccount());
        externalUser.setNickName(users.getNickName());
        externalUser.setTeamName(this.teamMapper.selectByPrimaryKey(users.getTeamId()).getTeamName());
        externalUser.setWorkerName(this.workerMapper.selectByPrimaryKey(users.getWorkId()).getWorkerName());
        return externalUser;
    }

    /**
    　* @description: TODO  获取任务数据   任务总数  异常任务数  常规任务数   临时任务数
    　* @param []
    　* @return java.util.Map<java.lang.String,java.lang.Integer>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 21:02
    　*/
    public Map<String,Integer> getDataTask(){
        Map<String, Integer> map = new HashMap<>();

        UserInfo user = PartyUtil.getCurrentUserInfo();
        if(user == null){
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = user.getScenicId();

        List<UserTeam> userTeamList = user.getUserTeamList();
        StringBuffer teamIds = new StringBuffer();
        for (UserTeam team : userTeamList) {
            teamIds.append(team.getTeamId()+",");
        }
        List<TaskExceptionDescription> taskExceptionDescriptions = this.taskExceptionDescriptionMapper.selectExceptionTaskList(null,teamIds.toString(),scenicId);
        SelectTaskListVo vo = new SelectTaskListVo();
        vo.setTeamId(teamIds.toString());
        List<Task> tasks = this.taskMapper.selectTaskListByManagement(vo);
        System.out.println(tasks.size());
        int totalTask = tasks.size();
        int totalTaskException = taskExceptionDescriptions.size();
        int regularTask = 0;
        int interimTask = 0;
        for (Task task : tasks) {
            if (task.getTaskCategories() == 1){
                regularTask+=1;
            }else {
                interimTask+=1;
            }
        }
        map.put("totalTask",totalTask);
        map.put("totalTaskException",totalTaskException);
        map.put("regularTask",regularTask);
        map.put("interimTask",interimTask);
        return map;
    }


    /**
    　* @description: TODO  获取所有物资数据  物资总数、物资启用|损坏、物资类别统计 消防栓、车船点、路灯、急救点
    　* @param []
    　* @return java.util.Map<java.lang.String,java.lang.String>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 18:23
    　*/
    public Map<String,Integer> getDataMaterial(){
        Map<String, Integer> map = new HashMap<>();
        Example exa = new Example(DictBasic.class);
        exa.and().andEqualTo("groupCode",3);
        List<DictBasic> dictBasics = dictBasicMapper.selectByExample(exa);
        ArrayList<String> values = new ArrayList<>();
        for (DictBasic basic : dictBasics) {
            values.add(basic.getValue()+"");
        }
        Example example = new Example(Equipment.class);
        example.and().andIn("equipmentType",values).andEqualTo("isDelete",0);
        List<Equipment> equipment = this.equipmentMapper.selectByExample(example);
        if (null != equipment) {
            //物资总数
            int totalMaterials = equipment.size();
            int isUsing = 0;
            int notUsing = 0;
            //消防栓 5
            int fireHydrant = 0;
            //车船点 6
            int CarAndBoatPoint = 0;
            //路灯 7
            int streetLight = 0;
            //急救点 8
            int firstAidPoint = 0;
            for (Equipment eq : equipment) {
                if (eq.getIsUsing() == 0){
                    notUsing += 1;
                }else if (eq.getIsUsing() == 1){
                    isUsing += 1;
                }
                if (eq.getEquipmentType() == 5){
                    fireHydrant += 1;
                } else if (eq.getEquipmentType() == 6){
                    CarAndBoatPoint +=1;
                } else if (eq.getEquipmentType() == 7){
                    streetLight+=1;
                } else if (eq.getEquipmentType() == 8){
                    firstAidPoint+=1;
                }
             }
            map.put("totalMaterials",totalMaterials);
            map.put("isUsing",isUsing);
            map.put("notUsing",notUsing);
            map.put("fireHydrant",fireHydrant);
            map.put("carAndBoatPoint",CarAndBoatPoint);
            map.put("streetLight",streetLight);
            map.put("firstAidPoint",firstAidPoint);
        }
        return map;
    }


    /**
    　* @description: TODO  获取所有用户数据   人员总数 在线人员 男  女
    　* @param []
    　* @return java.util.Map<java.lang.String,java.lang.Integer>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 19:07
    　*/
    public Map<String, Integer> getDataUser(){
        Map<String, Integer> map = new HashMap<>();
        int totalUser = 0;
        int onlineUser = 0;
        int female = 0;
        int male = 0;
        Example userExample = new Example(Users.class);
        userExample.and().andEqualTo("delFlag",0).andEqualTo("isUsing",1);
        List<Users> users = this.usersMapper.selectByExample(userExample);
        totalUser = users.size();
        onlineUser = gpsDataMap.size();
        for (Users user : users) {
            if (user.getSex() == 1){
                male += 1;
            }else {
                female += 1;
            }
        }
        map.put("totalUser",totalUser);
        map.put("onlineUser",onlineUser);
        map.put("female",female);
        map.put("male",male);
        return map;
    }


    /**
    　* @description: TODO  获取事件数据  事件总数   不良事件数   报警事件数   已处理事件数
    　* @param []
    　* @return java.util.Map<java.lang.String,java.lang.Integer>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 19:18
    　*/
    public Map<String,Integer> getDataEventAll(){
        Map<String, Integer> map = new HashMap<>();
        //事件总数
        int totalEvent = 0;
        //不良事件
        int adverseEvent = 0;
        //报警事件
        int alarmEvent = 0;
        //已处理事件
        int processedEvent = 0;
        Example eventExample = new Example(EventReport.class);
        eventExample.and().andEqualTo("isDelete",0);
        List<EventReport> eventReports = this.eventReportMapper.selectByExample(eventExample);
        totalEvent = eventReports.size();
        for (EventReport report : eventReports) {
            if (report.getEventType() == 1){
                adverseEvent+=1;
            } else {
                alarmEvent+=1;
            }
            if (report.getEventProgress() == 2){
                processedEvent+=1;
            }
        }
        map.put("totalEvent",totalEvent);
        map.put("adverseEvent",adverseEvent);
        map.put("alarmEvent",alarmEvent);
        map.put("processedEvent",processedEvent);
        return map;
    }


    /**
    　* @description: TODO  根据年度查询事件  年度总数  年度不良事件数   年度报警事件数  及每个月的总数、不良事件及报警事件数   1.yyyy 2.MM 3.dd
    　* @param [time]
    　* @return java.util.Map<java.lang.String,java.lang.Integer>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 19:50
    　*/
    public Map<String,Object> getDataEventByYear(String time){
        Map<String, Object> map = new HashMap<>();
        Integer totalEventYear = this.eventReportMapper.selectTotalEventByTime(time, "1");
        Integer badEvnetYear = this.eventReportMapper.selectBadEventByTime(time, "1");
        Integer alarmEventYear = this.eventReportMapper.selectAlarmEventByTime(time, "1");
        map.put("totalEventYear", totalEventYear);
        map.put("badEvnetYear", badEvnetYear);
        map.put("alarmEventYear", alarmEventYear);
        ArrayList<ApiExternalEventData> list = new ArrayList<ApiExternalEventData>();
        for (int i = 1;i <= 12;i++){
            Integer totalEventMonth = this.eventReportMapper.selectTotalEventByTime(time+"-"+(i < 10 ? "0"+i+"-00":i+"-00"), "2");
            Integer badEvnetMonth = this.eventReportMapper.selectBadEventByTime(time+"-"+(i < 10 ? "0"+i+"-00":i+"-00"), "2");
            Integer alarmEventMonth = this.eventReportMapper.selectAlarmEventByTime(time+"-"+(i < 10 ? "0"+i+"-00":i+"-00"), "2");
            ApiExternalEventData data = new ApiExternalEventData();
            data.setDate(i+"月");
            data.setTotalEventNum(totalEventMonth.toString());
            data.setBadEvnetNum(badEvnetMonth.toString());
            data.setAlarmEventNum(alarmEventMonth.toString());
            list.add(data);
        }
        map.put("list",list);
        return map;
    }


    /**
    　* @description: TODO  根据月份查询每天 事件总数  不良事件数 报警事件数
    　* @param [time]
    　* @return java.util.Map<java.lang.String,java.lang.Integer>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/17 20:02
    　*/
    public Map<String,Object> getDateEventMonth(String time){
        Map<String, Object> map = new HashMap<>();
        Integer totalEventMonth = this.eventReportMapper.selectTotalEventByTime(time+"-00", "2");
        Integer badEvnetMonth = this.eventReportMapper.selectBadEventByTime(time+"-00", "2");
        Integer alarmEventMonth = this.eventReportMapper.selectAlarmEventByTime(time+"-00", "2");
        map.put("totalEventMonth", totalEventMonth);
        map.put("badEvnetMonth", badEvnetMonth);
        map.put("alarmEventMonth", alarmEventMonth);
        List<ApiExternalEventData> list = new ArrayList<>();
        //获取当前月份的天数
        int days = DateUtil.getDaysByYYMM(time);
        for (int i = 1;i <= days;i++){
            Integer totalEventDay = this.eventReportMapper.selectTotalEventByTime(time+"-"+(i < 10 ? "0"+i:i) , "3");
            Integer badEvnetDay = this.eventReportMapper.selectBadEventByTime(time+"-"+(i < 10 ? "0"+i:i), "3");
            Integer alarmEventDay = this.eventReportMapper.selectAlarmEventByTime(time+"-"+(i < 10 ? "0"+i:i), "3");
            ApiExternalEventData data = new ApiExternalEventData();
            data.setDate(i+"日");
            data.setBadEvnetNum(badEvnetDay.toString());
            data.setAlarmEventNum(alarmEventDay.toString());
            data.setTotalEventNum(totalEventDay.toString());
            list.add(data);
        }
        map.put("list",list);
        return map;
    }

    /**
    　* @description: TODO 获取事件有数据的年份列表
    　* @param []
    　* @return java.util.List<java.lang.String>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/19 11:43
    　*/
    public List<String> getDataYear(){
        List<String> list = this.eventReportMapper.selectDataYear();
        return list;
    }

    /**
    　* @description: TODO  获取事件有数据的月份列表
    　* @param []
    　* @return java.util.List<java.lang.String>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/19 16:26
    　*/
    public List<String> getDataMonth(){
        List<String> list = this.eventReportMapper.selectDataMonth();
        return list;
    }

    /**
    　* @description: TODO  获取年度任务数据   类别
    　* @param [time]
    　* @return java.util.Map<java.lang.String,java.lang.Object>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/19 16:35
    　*/
    public List<ApiExternalTaskData> getDataTaskByYear(String time){
        UserInfo userInfo = PartyUtil.getCurrentUserInfo();
        List<ApiExternalTaskData> list = new ArrayList<>();
        for (int i = 1;i <= 12;i++){
            ApiExternalTaskData taskData = new ApiExternalTaskData();
            taskData.setDate(i+"月");
            Integer OrdinaryTask = this.taskMapper.selectTaskDataByMonth(time + "-" + (i < 10 ? "0" + i + "-00" : i + "-00"),2,userInfo.getId());
            Integer PatrolTask = this.taskMapper.selectTaskDataByMonth(time + "-" + (i < 10 ? "0" + i + "-00" : i + "-00"), 3,userInfo.getId());
            taskData.setOrdinaryTask(OrdinaryTask != null ? OrdinaryTask+"":"0");
            taskData.setPatrolTask(PatrolTask != null ? PatrolTask+"":"0");
            list.add(taskData);
        }
        return list;
    }

    /**
    　* @description: TODO  获取有任务看板数据的年份
    　* @param []
    　* @return java.util.List<java.lang.String>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/19 17:58
    　*/
    public List<String> getDataTaskYear(){
        List<String> list = this.taskMapper.getDataTaskYear();
        return list;
    }
}
