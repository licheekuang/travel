package com.liaoyin.travel.service;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.FunctionMapper;
import com.liaoyin.travel.entity.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FunctionService extends BaseService<FunctionMapper, Function> {
	//@Autowired
	//UserRoleService userRoleService;
	public List<Function> selectFunctionsByUserId(String partymemberId) {
		return mapper.selectFunctionsByUserId(partymemberId);
	}
	/**
	 * 作者：cy
	 * 描述：根据组织ID获取功能权限列表
	 * 日期：2017/11/3 15:22
	 */
	/*public List<Function> selectFunctionsByOrgId(String orgId) {
		//根据组织ID查询角色ID
		List<String> roleIds = userRoleService.selectRoleByOrgId(orgId);

		if(roleIds!=null){//角色ID不为空时根据角色ID获取菜单权限
			return this.mapper.selectMenuByRoleIds(roleIds);
		}else{//角色id为空时根据组织类型获取菜单权限
			return this.mapper.selectFunctionsByOrgId(orgId);
		}
	}*/
	/**
	 * 作者：cy
	 * 描述：根据菜单ID获取静态页面权限
	 * 日期：2018/3/13 11:20
	 */
	public List<Function> getHtmlListByMenu(List<String> menuIds) {
		return mapper.getHtmlListByMenu(menuIds);
	}
	
	
	/**
	 * 
	 * 方   法  名：selectFunctionListByPage
	 * 创建日期：2017年10月17日 11:03:06
	 * 创   建  者：zhou.ning
	 * 描          述：根据用户id查询菜单列表
	 */
	public List<Function> selectFunctionListByPage(String menuId,int page,int size,String dataType,String memtype,String method,String search){
		//return this.mapper.selectEducation(partymemberId);
		PageHelper.startPage(page,size);
		return this.mapper.selectFunctionListByPage(menuId,dataType,memtype,method,search);
	}
	/**
	 * 
	 * 方   法  名：selectFunctionListByPageInMenu
	 * 创建日期：2017年10月17日 11:03:06
	 * 创   建  者：zhou.ning
	 * 描          述：根据用户id查询菜单列表
	 * @param menuId
	 * @return List<Function>
	 */
	public List<Function> selectFunctionListByPageInMenu(String menuId,int page,int size){
		//return this.mapper.selectEducation(partymemberId);
		PageHelper.startPage(page,size);
		return this.mapper.selectFunctionListByPageInMenu(menuId);
	}
	
	/**
	 * 
	 * 方   法  名：selectFunctionMAXfuncCode
	 * 创建日期：2017年10月17日 11:03:06
	 * 创   建  者：li.jing
	 * 描          述：查询功能当前最大code
	 * @param 
	 * @return
	 */
	public String selectFunctionMAXfuncCode(){
		List<String> list = (List<String>) this.mapper.selectFunctionMAXfuncCode();
		String code= "";
		if(list != null && list.size() >0) {
			String c = list.get(0);			
			 if (Integer.parseInt(c) < 9) {
				 code = c.substring(0, c.length() - 1) + (Integer.parseInt(c) + 1) + "";
            } else if (Integer.parseInt(c) < 99) {
            	 code = c.substring(0, c.length() - 2) + (Integer.parseInt(c) + 1) + "";
            } else if (Integer.parseInt(c) < 999) {
            	 code = c.substring(0, c.length() - 3) + (Integer.parseInt(c) + 1) + "";
            }
		}else {
			code="001";
		}
		return code;
	}
	
	/**
	 * 
	 * 方   法  名：selectEducationListByUserId
	 * 创建日期：2017年10月17日 11:03:06
	 * 创   建  者：zhou.ning
	 * 描          述：根据用户id查询菜单列表
	 * @return
	 */
	public void updateFunction(Function fun){
		Function f = this.mapper.selectByPrimaryKey(fun.getId());
		f.setMenuCode(fun.getMenuId());
		f.setFuncName(fun.getFuncName());
		f.setFuncType(fun.getFuncType());
		f.setMethod(fun.getMethod());
		f.setUri(fun.getUri());
		f.setIsUsing(fun.getIsUsing());
		this.mapper.updateByPrimaryKey(f);
	}
	
	/**
	 * 
	 * 方   法  名：selectFunctionListByMenuId
	 * 创建日期：2017年10月17日 11:03:06
	 * 创   建  者：li.jing
	 * 描          述：根据用户id查询菜单列表
	 * @return
	 */
	public List<Function> selectFunctionListByMenuId(String menuId){
		//return this.mapper.selectEducation(partymemberId);
		
		return this.mapper.selectFunctionListByMenuId(menuId);
	}
	
	/**
	 * 
	 * 方   法  名：selectMenuAllThis
	 * 创建日期：2017年12月08日 下午7:59:06
	 * 创   建  者：li.jing
	 * 描          述：设置菜单功能
	 * @param 
	 * @return
	 */
	public void updateMenuAndFun(String id,String funId){
		
		 this.mapper.updateMenuAndFun(id,funId);
	}
}
