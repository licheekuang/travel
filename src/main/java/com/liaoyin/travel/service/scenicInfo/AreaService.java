package com.liaoyin.travel.service.scenicInfo;

import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.scenicInfo.AreaCityMapper;
import com.liaoyin.travel.dao.scenicInfo.AreaDistrictMapper;
import com.liaoyin.travel.dao.scenicInfo.AreaProvinceMapper;
import com.liaoyin.travel.entity.scenicInfo.AreaCity;
import com.liaoyin.travel.entity.scenicInfo.AreaDistrict;
import com.liaoyin.travel.entity.scenicInfo.AreaProvince;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 区域管理service
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-22 13:50
 */
@Service
public class AreaService {

    @Resource
    AreaProvinceMapper areaProvinceMapper;
    @Resource
    AreaDistrictMapper areaDistrictMapper;
    @Resource
    AreaCityMapper areaCityMapper;

    /**
     * @方法名：selectAreaProvince
     * @描述： 查询省级区域
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 14:09
     */
    public List<AreaProvince> selectAreaProvince() {
        UserInfo user = PartyUtil.getCurrentUserInfo();
        if(user == null) {
            throw new BusinessException("not.login");
        }
        System.err.println(user.getScenicId());
        return this.areaProvinceMapper.selectAreaProvince();
    }

    /**
     * @方法名：selectAreaCityByProvinceId
     * @描述： 根据省级编码查询市级区域
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 14:27
     */
    public List<AreaCity> selectAreaCityByProvinceId(String provinceId) {
        return this.areaCityMapper.selectAreaCityByProvinceId(provinceId);
    }

    /**
     * @方法名：selectAreaDistrictByCityId
     * @描述： 根据市级区域id查询区县区域信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 14:36
     */
    public List<AreaDistrict> selectAreaDistrictByCityId(String cityId) {
        return this.areaDistrictMapper.selectAreaDistrictByCityId(cityId);
    }

    /**
     * @方法名：selectAreaCityByDistrictId
     * @描述： 通过区域id查询市级区域信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/23 12:59
     */
    public AreaCity selectAreaCityByDistrictId(String districtId) {
        return this.areaCityMapper.selectAreaCityByDistrictId(districtId);
    }
}
