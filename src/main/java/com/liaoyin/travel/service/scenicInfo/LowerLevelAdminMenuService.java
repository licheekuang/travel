package com.liaoyin.travel.service.scenicInfo;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.scenicInfo.LowerLevelAdminMenuMapper;
import com.liaoyin.travel.entity.scenicInfo.ScenicLowerLevelAdminMenu;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

/**
 * 景区(单位)下级管理员和菜单关联 Service
 *
 * @author kjz
 * @date 2020-04-28 15:48
 */
@Service
public class LowerLevelAdminMenuService extends BaseService<LowerLevelAdminMenuMapper, ScenicLowerLevelAdminMenu> {


    /**
     * @方法名：insertScenicBackUserMenu
     * @描述： 新增单位子管理员和菜单的关联
     * @作者： kjz
     * @日期： Created in 2020/4/28 15:58
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertScenicBackUserMenu(String menuIds, String backUserId) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        Optional.ofNullable(curveUserInfo).orElseThrow(() -> new BusinessException("not.login"));
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();

        //字符串转换为list
        List<String> menuIdList = Arrays.asList(menuIds.split(","));

        List<ScenicLowerLevelAdminMenu> lowerLevelAdminMenuList = new ArrayList<>();
        menuIdList.forEach(menuId->{
            ScenicLowerLevelAdminMenu lowerLevelAdminMenu = new ScenicLowerLevelAdminMenu()
                    .setMenuId(menuId).setBackUserId(backUserId).setScenicId(scenicId)
                    .setId(SnowIdUtil.getInstance().nextId())
                    .setCreateTime(LocalDateTime.now());
            lowerLevelAdminMenuList.add(lowerLevelAdminMenu);
        });

        /** 先删除之前的【单位子管理员和菜单的关联】数据 **/
        this.mapper.deleteLowerLevelAdminMenuByBackUserId(backUserId);

        return this.mapper.insertList(lowerLevelAdminMenuList);
    }

    /**
     * @方法名：selectBackUserThisMenuByUserId
     * @描述： 根据后台用户id查询景区子管理员分配的菜单
     * @作者： kjz
     * @日期： Created in 2020/5/14 18:12
     */
    public Map<String, Object> selectBackUserThisMenuByUserId(String backUserId) {
        Map<String, Object> map = new HashMap<String,Object>();
        List<ScenicLowerLevelAdminMenu> rmList = this.mapper.selectBackUserThisMenuByUserId(backUserId);
        List<String> rmStrList = new ArrayList<String>();
        if(rmList != null && rmList.size() >0) {
            for(ScenicLowerLevelAdminMenu rm:rmList) {
                rmStrList.add(rm.getMenuId());
            }
        }

        map.put("menu",rmStrList);
        return map;
    }
}
