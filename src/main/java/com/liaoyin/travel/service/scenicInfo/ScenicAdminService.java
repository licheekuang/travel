package com.liaoyin.travel.service.scenicInfo;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.scenicInfo.ScenicAdminMapper;
import com.liaoyin.travel.entity.scenicInfo.ScenicAdmin;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 景区管理员service
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-21 15:47
 */
@Service
public class ScenicAdminService extends BaseService<ScenicAdminMapper, ScenicAdmin> {

    /**
     * @方法名：updateScenicAdminByScenicId
     * @描述： 根据景区id更新景区管理员信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 20:04
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateScenicAdminByScenicId(ScenicAdmin scenicAdmin) {
        return this.mapper.updateScenicAdminByScenicId(scenicAdmin);
    }

    /**
     * @方法名：selectScenicAdminByAccount
     * @描述： 根据账户查询未审核的管理员信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 20:35
     */
    public List<ScenicAdmin> selectScenicAdminByAccount(String account) {
        return this.mapper.selectScenicAdminByAccount(account);
    }

    /**
     * @方法名：selectScenicAdminByPhone
     * @描述： 根据联系电话查询未审核的管理员信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 20:43
     */
    public List<ScenicAdmin> selectScenicAdminByPhone(String phone) {
        return this.mapper.selectScenicAdminByPhone(phone);
    }

    /**
     * @方法名：selectScenicAdminByIdentityCard
     * @描述： 根据身份证号查询景区管理员申请信息
     * @作者： kjz
     * @日期： Created in 2020/2/24 17:29
     */
    public List<ScenicAdmin> selectScenicAdminByIdentityCard(String identityCard,String scenicId) {
        return this.mapper.selectScenicAdminByIdentityCard(identityCard,scenicId);
    }
}
