package com.liaoyin.travel.service;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.dao.MenuMapper;
import com.liaoyin.travel.entity.Function;
import com.liaoyin.travel.entity.Menu;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.UUIDUtil;
import com.liaoyin.travel.view.moble.back.MenuView;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class MenuService extends BaseService<MenuMapper, Menu> {
	/*@Autowired
	UserRole userRoleService;*/
	@Autowired
	private FunctionService functionService;

	/**
	 * 方   法  名：selectMenu
	 * 创建日期：2017年8月8日 下午7:59:06
	 * 创   建  者：lijing
	 * 描          述：根据用户——角色——菜单的关联关系查询出菜单列表
	 *
	 * @param partymemberId
	 * @return
	 */
	public List<Menu> selectMenu(String partymemberId) {
		return this.mapper.selectMenu(partymemberId);
	}

	//根据角色ID查询菜单列表
	public List<Menu> selectMenuByRoleId(String roleId) {
		return this.mapper.selectMenuByRoleId(roleId);
	}

	/**
	 * 作者：cy
	 * 描述：根据人员和组织获取角色菜单
	 * 日期：2018/1/17 19:44
	 */
	public List<Menu> selectMenuByUserAndOrg(String userId, String orgId) {
		if (StringUtils.isBlank(userId)) {
			throw new BusinessException("msg.user.userIdisNull");
		}
		return this.mapper.selectMenuByUserAndOrg(userId, orgId);
	}

	/**
	 * 根据用户和组的权限关系查找用户可访问菜单
	 *
	 * @param userId
	 * @return
	 */
	public List<MenuView> selectMenuByUserId(String userId) {
		return this.mapper.selectMenuByUserId(userId);
	}

	/**
	 * @方法名：selectScenicLowerLevelAdminMenuByUserId
	 * @描述： 根据后台用户id查询单位子管理员的菜单
	 * @作者： kjz
	 * @日期： Created in 2020/5/15 10:13
	 */
	public List<MenuView> selectScenicLowerLevelAdminMenuByUserId(String backUserId) {
		return this.mapper.selectScenicLowerLevelAdminMenuByUserId(backUserId);
	}


	/**
	 * 根据组织查找该组织可访问菜单
	 *
	 * @param orgId
	 * @return
	 */
	/*public List<Menu> selectMenuByOrgId(String orgId){
		//根据组织ID查询角色ID
		List<String> roleIds = userRoleService.selectRoleByOrgId(orgId);

		if(roleIds!=null){//角色ID不为空时根据角色ID获取菜单权限
			return this.mapper.selectMenuByRoleIds(roleIds);
		}else{//角色id为空时根据组织类型获取菜单权限
			return this.mapper.selectMenuByOrgId(orgId);
		}
	}*/
	public List<Menu> selectMenuByOrgIdOnlyMenuId(String orgId, String userId) {
		//返回map
		Map map = new HashMap();
		//根据组织ID查询角色ID
		List<Menu> menuList = this.selectMenuByUserAndOrg(orgId, userId);
		List<String> rmStrList = new ArrayList<String>();
		for (Menu item : menuList
		) {
			rmStrList.add(item.getId());
		}
		map.put("menu", rmStrList);
		return menuList;
	}

	/**
	 * 方   法  名：selectMenu
	 * 创建日期：2017年8月8日 下午7:59:06
	 * 创   建  者：lijing
	 * 描          述：根据用户——角色——菜单的关联关系查询出菜单列表
	 *
	 * @return
	 */
	public List<MenuView> selectMenuAll() {
		return this.mapper.selectMenuAll();
	}

	public List<MenuView> selectMenuByRoleIdIsAdd(String id) {
		return this.mapper.selectMenuByRoleIdIsAdd(id);
	}

	/**
	 * 方   法  名：insertMenu
	 * 创建日期：2017年8月8日 下午7:59:06
	 * 创   建  者：li.jing
	 * 描          述：新增菜单
	 *
	 * @param menu
	 * @return
	 */
	public void insertMenu(Menu menu) {
		menu.setCreateTime(new Date());
		menu.setIsDelete((short) 0);
		menu.setId(UUIDUtil.getUUID());
		/*if(menu.getUrl()!= null && !"".equals(menu.getUrl())) {
			menu.setUrl(CommonConstant.FRONT_ADDRESS+menu.getUrl());
		}*/
		menu.setMenuName(menu.getMenuName());
		this.mapper.insert(menu);
	}

	/**
	 * 方   法  名：selectMenuListByPage
	 * 创建日期：2017年8月8日 下午7:59:06
	 * 创   建  者：lijing
	 * 描          述：后台查询菜单(带分页)
	 *
	 * @return
	 */
	public List<Menu> selectMenuListByPage(String id, Integer num, String size) {
		if (id == null || "".equals(id)) {
			if (size != null && !"".equals(size)) {
				PageHelper.startPage(num, Integer.valueOf(size));
			}
		}
		List<Menu> list = this.mapper.selectMenuListByPage(id);
		/*if(list != null && list.size() >0) {
			for(Menu m :list) {
				if(m.getIcon() != null && !"".equals(m.getIcon()) &&m.getIcon().length() >25) {
					m.setFullFilePath(CommonConstant.FILE_SERVER+FileuploadUtil.getFileupload(m.getIcon()).getFilePath());
				}
			}
		}*/
		return list;
	}

	/**
	 * 方   法  名：selectMenuById
	 * 创建日期：2017年12月8日 下午7:59:06
	 * 创   建  者：li.jing
	 * 描          述：菜单id查询
	 *
	 * @return
	 */
	public Menu selectMenuById(String id) {
		Menu menu = this.mapper.selectMenuById(id);
		if (menu == null) {
			throw new BusinessException("msg.Menu.isNull");
		}
		/*if(menu.getIcon() != null && !"".equals(menu.getIcon())) {
			menu.setFileupload(FileuploadUtil.getFileupload(menu.getIcon()));
		}*/

		return menu;
	}

	/**
	 * 方   法  名：updateMenuThis
	 * 创建日期：2017年12月8日 下午7:59:06
	 * 创   建  者：li.jing
	 * 描          述：修改菜单
	 *
	 * @param menu
	 * @return
	 */
	public void updateMenuThis(Menu menu) {

		if (menu.getId() == null) {
			throw new BusinessException("id.is.null");
		}
		this.mapper.updateByPrimaryKeySelective(menu);
	}

	/**
	 * 方   法  名：deleteMenuByThis
	 * 创建日期：2017年12月8日 下午7:59:06
	 * 创   建  者：li.jing
	 * 描          述：删除菜单--删除后要同步删除菜单与角色的关联表项
	 *
	 * @return
	 */
	public void deleteMenuByThis(String id, String deleteUserId) {
		if (id == null) {
			throw new BusinessException("is.is.null", "id为空");
		}
		Menu m = this.mapper.selectByPrimaryKey(id);
		m.setDeleteTime(new Date());
		m.setDeleteUserId(deleteUserId);
		m.setIsDelete((short) 1);
		//删除关联表中的数据
		this.mapper.updateByPrimaryKey(m);
	}

	/**
	 * 方   法  名：selectMenuMAXfuncCode
	 * 创建日期：2017年8月8日 下午7:59:06
	 * 创   建  者：lijing
	 * 描          述：生成菜单全编码
	 *
	 * @return
	 */
	public String selectMenuMAXfuncCode(String id, String level) {
		String orgCode = "";
		if (id != null && !"".equals(id)) {
			Menu menu1 = this.mapper.selectByPrimaryKey(id);
			if (menu1 != null) {
				orgCode = this.mapper.selectByPrimaryKey(id).getMenuCode();
			}
			if (orgCode.length() >= 3) {
				Menu menu = new Menu();
				/*if(orgCode.length() > 3) {
					menu.setMenuCode(orgCode.substring(0, orgCode.length()-3));
				}else {
					menu.setMenuCode(orgCode);
				}*/
				//001
				menu.setMenuCode(orgCode);
				menu.setLevel(Integer.valueOf(level));
				List<Menu> directLevelList = this.mapper.selectDirectLevelOrgsNoSearchNotOnlyManager(menu);
				if (directLevelList == null || (directLevelList != null && directLevelList.size() < 1)) {
					orgCode = menu.getMenuCode() + "001";
				} else {
					String lastOrgCode = directLevelList.get(directLevelList.size() - 1).getMenuCode();
					String lastStr = lastOrgCode.substring(lastOrgCode.length() - 1);
					String last2Str = lastOrgCode.substring(lastOrgCode.length() - 2);
					String last3Str = lastOrgCode.substring(lastOrgCode.length() - 3);
					if (Integer.parseInt(lastStr) < 9) {
						orgCode = lastOrgCode.substring(0, lastOrgCode.length() - 1) + (Integer.parseInt(lastStr) + 1) + "";
					} else if (Integer.parseInt(last2Str) < 99) {
						orgCode = lastOrgCode.substring(0, lastOrgCode.length() - 2) + (Integer.parseInt(last2Str) + 1) + "";
					} else if (Integer.parseInt(last3Str) < 999) {
						orgCode = lastOrgCode.substring(0, lastOrgCode.length() - 3) + (Integer.parseInt(last3Str) + 1) + "";
					}
				}
			} else {
				List<String> maxList = this.mapper.selectMenuMaxById(level);
				String lastOrgCode = maxList.get(0);
				if (lastOrgCode != null && !"".equals(lastOrgCode)) {
					if (Integer.parseInt(lastOrgCode) < 9) {
						orgCode = lastOrgCode.substring(0, lastOrgCode.length() - 1) + (Integer.parseInt(lastOrgCode) + 1) + "";
					} else if (Integer.parseInt(lastOrgCode) < 99) {
						orgCode = lastOrgCode.substring(0, lastOrgCode.length() - 2) + (Integer.parseInt(lastOrgCode) + 1) + "";
					} else if (Integer.parseInt(lastOrgCode) < 999) {
						orgCode = (Integer.valueOf(lastOrgCode) + 1) + "";
					}
				} else {
					orgCode = "001";
				}

			}
		} else {
			List<String> maxList = this.mapper.selectMenuMaxById(level);
			if (maxList != null && maxList.size() > 0) {
				String lastOrgCode = maxList.get(0);
				if (lastOrgCode != null && !"".equals(lastOrgCode)) {
					if (lastOrgCode.length() > 2) {
						if (Integer.parseInt(lastOrgCode) < 9) {
							orgCode = lastOrgCode.substring(0, lastOrgCode.length() - 1) + (Integer.parseInt(lastOrgCode) + 1) + "";
						} else if (Integer.parseInt(lastOrgCode) < 99) {
							orgCode = lastOrgCode.substring(0, lastOrgCode.length() - 2) + (Integer.parseInt(lastOrgCode) + 1) + "";
						} else if (Integer.parseInt(lastOrgCode) < 999) {
							orgCode = (Integer.valueOf(lastOrgCode) + 1) + "";
						}
					} else {
						orgCode = Integer.valueOf(lastOrgCode) + 1 + "";
					}
				} else {
					orgCode = "0";
				}
			} else {
				orgCode = "0";
			}

		}

		return orgCode;
	}

	/**
	 * 方   法  名：selectMenuAllThis
	 * 创建日期：2017年8月8日 下午7:59:06
	 * 创   建  者：lijing
	 * 描          述：获取菜单所有--带返回下级菜单数量
	 *
	 * @return
	 */
	public List<Menu> selectMenuAllThis(String id) {

		return this.mapper.selectMenuAllThis(id);
	}


	/**
	 * @方法名：selectMenuAllThisMenu
	 * @描述： 获取菜单所有---带返回下级菜单数量--不传id
	 * @作者： li.jing
	 * @日期： Created in 2017年12月12日 下午7:59:06
	 */
	public Map<String, Object> selectMenuAllThisMenu(String orgId) {
		List<Map<String, Object>> allList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		//获取菜单所有---带返回下级菜单数量
		List<Menu> mlist = this.mapper.selectMenuAllAllThisMenu();
		//根据菜单ID查询功能列表
		List<Function> funcList = this.functionService.selectFunctionListByMenuId("");

		if (mlist != null && mlist.size() > 0) {
			List<Menu> zList = new ArrayList<Menu>();
			List<Menu> oneList = new ArrayList<Menu>();
			List<Menu> twoList = new ArrayList<Menu>();
			List<Menu> threeList = new ArrayList<Menu>();
			List<Menu> fourList = new ArrayList<Menu>();
			Map<String, Object> mp = new HashMap<String, Object>();
			for (Menu m : mlist) {
				if (m.getMenuCode().length() == 1) {
					zList.add(m);
				} else if (m.getMenuCode().length() == 3) {
					oneList.add(m);
				} else if (m.getMenuCode().length() == 6) {
					twoList.add(m);
				} else if (m.getMenuCode().length() == 9) {
					threeList.add(m);
				} else if (m.getMenuCode().length() == 12) {
					fourList.add(m);
				}
			}
			mp.put("m1", zList);
			mp.put("m2", oneList);
			mp.put("m3", twoList);
			mp.put("m4", threeList);
			mp.put("m5", fourList);
			allList.add(mp);
			zList = null;
			oneList = null;
			twoList = null;
			threeList = null;
			fourList = null;
		}
		map.put("menu", allList);
		map.put("func", funcList);

		return map;
	}

	/**
	 * @方法名：selectMenuAllThisScenicLowerLevelAdminMenu
	 * @描述： 获取单位子管理员的所有菜单---带返回下级菜单数量
	 * @作者： kjz
	 * @日期： Created in 2020/5/14 17:49
	 */
	public Map<String, Object> selectMenuAllThisScenicLowerLevelAdminMenu() {
		List<Map<String, Object>> allList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		//获取菜单所有---带返回下级菜单数量
		List<Menu> mlist = this.mapper.selectMenuAllThisScenicLowerLevelAdminMenu();

		if (mlist != null && mlist.size() > 0) {
			List<Menu> zList = new ArrayList<Menu>();
			List<Menu> oneList = new ArrayList<Menu>();
			List<Menu> twoList = new ArrayList<Menu>();
			List<Menu> threeList = new ArrayList<Menu>();
			List<Menu> fourList = new ArrayList<Menu>();
			Map<String, Object> mp = new HashMap<String, Object>();
			for (Menu m : mlist) {
				if (m.getMenuCode().length() == 1) {
					zList.add(m);
				} else if (m.getMenuCode().length() == 3) {
					oneList.add(m);
				} else if (m.getMenuCode().length() == 6) {
					twoList.add(m);
				} else if (m.getMenuCode().length() == 9) {
					threeList.add(m);
				} else if (m.getMenuCode().length() == 12) {
					fourList.add(m);
				}
			}
			mp.put("m1", zList);
			mp.put("m2", oneList);
			mp.put("m3", twoList);
			mp.put("m4", threeList);
			mp.put("m5", fourList);
			allList.add(mp);
			zList = null;
			oneList = null;
			twoList = null;
			threeList = null;
			fourList = null;
		}
		map.put("menu", allList);

		return map;
	}

	/**
	 * 方   法  名：selectMenuAllById
	 * 创建日期：2017年12月13日 下午7:59:06
	 * 创   建  者：li.jing
	 * 描          述：获取菜单所有
	 *
	 * @return
	 */
	public List<MenuView> selectMenuAllById(String id) {

		return this.mapper.selectMenuAllById(id);
	}


	/**
	 * 方   法  名：selectMenuListByPage
	 * 创建日期：2017年8月8日 下午7:59:06
	 * 创   建  者：lijing
	 * 描          述：后台查询菜单(带分页)
	 *
	 * @return
	 */
	public List<Menu> selectMenuListByPageByThis(String id, Integer num, String size, String dataType, String search) {
		if (id == null || "".equals(id)) {
			if (size != null && !"".equals(size)) {
				PageHelper.startPage(num, Integer.valueOf(size));
			}
		}
		if (search != null && !"".equals(search)) {
			search = search.trim();
		}
		List<Menu> list = this.mapper.selectMenuListByPageByThis(id, dataType, search);
		/*if(list != null && list.size() >0) {
			for(Menu m :list) {
				if(m.getIcon() != null && !"".equals(m.getIcon()) &&m.getIcon().length() >25) {
					m.setFullFilePath(CommonConstant.FILE_SERVER+FileuploadUtil.getFileupload(m.getIcon()).getFilePath());
				}
			}
		}*/
		return list;
	}

	/**
	 * 作者：cy
	 * 描述：获取移动端免登陆菜单
	 * 日期：2018/3/26 19:45
	 */
	public List<Menu> getLogoutAppMenu() {
		List<Menu> menuList = this.mapper.getLogoutAppMenu();
		return menuList;
	}

	/**
	 * @方法名：selectMenuListByRoleId
	 * @描述： 根据角色查询角色权限
	 * @作者： lijing
	 * @日期： 2018年8月2日
	 */
	public List<Menu> selectMenuListByRoleId(String roleId) {
		List<Menu> list = null;
		if (roleId != null && !"".equals(roleId)) {
			list = this.mapper.selectMenuListByRoleId(roleId);
			if (list != null && list.size() > 0) {
				for (Menu m : list) {
					if (m.getUrl() != null && !"".equals(m.getUrl())) {
						m.setUrl(CommonConstant.FRONT_ADDRESS + m.getUrl());
					}
				}
			}

		}
		return list;
	}

	/**
	 * @方法名：updateScenicRoleThisMenu
	 * @描述： 根据菜单id字符串更新景区子管理员可以分配的菜单
	 * @作者： kjz
	 * @日期： Created in 2020/5/14 16:09
	 */
	@Transactional(rollbackFor = Exception.class)
	public int updateScenicRoleThisMenu(String menuIds) {
		//首先执行都设置为不关联
		this.mapper.updateScenicRoleNoSubordinateByAll();
		List<String> menuIdList = Arrays.asList(menuIds.split(","));
		int result = this.mapper.updateScenicRoleThisMenu(menuIdList);
		return result;
	}

	/**
	 * @方法名：selectScenicRoleThisMenu
	 * @描述： 查询景区子管理员可以进行分配的菜单
	 * @作者： kjz
	 * @日期： Created in 2020/5/14 16:40
	 */
	public Map<String, Object> selectScenicRoleThisMenu() {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Menu> rmList = this.mapper.selectMenuBySubordinateEqualOne();
		List<String> rmStrList = new ArrayList<String>();
		if (rmList != null && rmList.size() > 0) {
			for (Menu rm : rmList) {
				rmStrList.add(rm.getId());
			}
		}
		map.put("menu", rmStrList);
		return map;
	}

	public static void main(String[] args) {
		String s = "http://192.168.0.144:8020/backweb/";
		String st = "http://192.168.0.144:8020/backweb/message/personnel.html";
		System.out.println(s.indexOf(st));
		if (s.indexOf(st) == -1) {
			System.out.println(st.substring(s.length(), st.length()));
		}
	}

}


