package com.liaoyin.travel.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.liaoyin.travel.base.api.JsonRestResponse;
import com.liaoyin.travel.base.api.RestUtil;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.SmsCodeMapper;
import com.liaoyin.travel.entity.SmsCode;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.thread.SmsThread;

import java.util.Date;

/**
 * @项目名：
 * @作者：zhou.ning
 * @描述：用户登录注册业务类
 * @日期：Created in 2018/6/8 14:55
 */
@Service
public class SmsCodeService extends BaseService<SmsCodeMapper, SmsCode> {

    @Value("${project.sms.sms-expireTime}")
    Integer sms_expireTime;
//   /**
//    * @方法名：selectUsersByPage
//    * @描述： 分页查询
//    * @作者： zhou.ning
//    * @日期： Created in 2018/6/8 16:53
//    */
//    public PageInfo<Users>  selectUsersByPage(Users users){
//        PageHelper.startPage(users.getNum(),users.getSize(), users.getOrderby()==null?null:null);
//        List<Users> list = this.mapper.selectUsersByPage(users);
//        PageInfo<Users> p = new PageInfo<>(list);
//        return p;
//    }

    /**
     * @方法名：insertSmsCode
     * @描述： 新增
     * @作者： zhou.ning
     * @日期： Created in 2018/6/11 10:27
     */
    public void insertSmsCode(String mobilePhone, String functionCode)
    {
        if(mobilePhone==null){
            throw new BusinessException("common.paramIsNull");
        }
        //根据手机号和功能编码查询验证码，如果存在则更新，否则新增
        SmsCode sc = this.mapper.selectByPhoneAndFunction(mobilePhone, functionCode);
        SmsCode smsCode = new SmsCode();
        Date d = new Date();
        //2分钟后过期
        d.setTime(d.getTime() + this.sms_expireTime * 1000);
        smsCode.setExpireTime(d);
        //生成验证码
        smsCode.setSmsCode("123456");
//        smsCode.setSmsCode(StringUtil.generateSmsCode(true,6));

        //发送验证码
        Thread thread = new Thread(new SmsThread(mobilePhone, smsCode.getSmsCode()));
        thread.start();
        //发送成功则写入数据库
        if(sc==null) {
            smsCode.setFunctionCode(functionCode);
            smsCode.setMobilePhone(mobilePhone);
            this.mapper.insert(smsCode);
        }else {
            smsCode.setId(sc.getId());
            this.mapper.updateByPrimaryKeySelective(smsCode);
        }
    }
//    /**
//     * @方法名：updateusers
//     * @描述： 修改
//     * @作者： zhou.ning
//     * @日期： Created in 2018/6/11 10:27
//     */
//    public void updateusers(Users users)
//    {
//        if(users==null || users.getId()==null){
//            throw new BusinessException("common.idIsNull");
//        }
//        this.mapper.updateByPrimaryKeySelective(users);
//    }
//    /**
//     * @方法名：deleteusers
//     * @描述： 根据主键逻辑删除
//     * @作者： zhou.ning
//     * @日期： Created in 2018/6/11 10:27
//     */
//    public void deleteusers(String id)
//    {
//        if(StringUtil.isEmpty(id)){
//            throw new BusinessException("common.idIsNull");
//        }
//        Users users = new users();
//        users.setId(id);
//        users.setDel("1");
//        this.mapper.updateByPrimaryKeySelective(users);
//    }
//    /**
//     * @方法名：deleteusers
//     * @描述： 根据主键物理删除
//     * @作者： zhou.ning
//     * @日期： Created in 2018/6/11 10:27
//     */
//    public void deleteusersPhysical(Users users)
//    {
//        if(users==null || users.getId()==null){
//            throw new BusinessException("common.idIsNull");
//        }
//        this.mapper.delete(users);
//    }
    @GetMapping(value = "/tokenValidate")
    public JsonRestResponse tokenValidate() {

        return RestUtil.createResponse();
    }
}
