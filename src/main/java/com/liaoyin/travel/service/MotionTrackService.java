package com.liaoyin.travel.service;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.MotionTrackMapper;
import com.liaoyin.travel.entity.MotionTrack;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * @author privatePanda@163.com
 * @title: MotionTrackService
 * @projectName travel
 * @description: TODO  用户轨迹的Service
 * @date 2019/9/2710:49
 */
@Service
public class MotionTrackService extends BaseService<MotionTrackMapper, MotionTrack> {

    /**
     * @方法名：insertUserLatAndLog
     * @描述： 新增一条用户运动轨迹
     * @作者： 周明智
     * @日期： Created in 2019/9/27 15:40
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertUserLatAndLog(String userId,String lat,String log){
        LocalDateTime time=LocalDateTime.now();
        System.out.println(time);
        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.err.println("新增运动轨迹----"+ dtf2.format(LocalDateTime.now()));
        //获取当前用户登录信息
        UserInfo activeUser = PartyUtil.getCurrentUserInfo();
        if(activeUser == null) {
            throw new BusinessException("not.login");
        }
        int i = 0;
        String id = UUIDUtil.getUUID();
        MotionTrack motionTrack = new MotionTrack()
                .setId(UUIDUtil.getUUID())
                .setUserId(userId)
                .setDelFlag((short)0)
                .setCreateTime(new Date())
                .setScenicId(activeUser.getScenicId());
        //这两个命令不太规范，但是改了之后移动端也要改，为了避免引起bug，就不做处理
        motionTrack.setuLat(lat);
        motionTrack.setuLog(log);
        i = this.mapper.insertSelective(motionTrack);
        return i;
    }

    /**
     * @方法名：deleteMotionTrack
     * @描述： 删除可多选删除 根据运动轨迹id
     * @作者： 周明智
     * @日期： Created in 2019/9/27 15:39
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteMotionTrack(String ids){
        String[] split = ids.split(",");
        int i = 0;
        for (String s : split) {
            MotionTrack track = new MotionTrack();
            track.setId(s);
            track.setDelFlag((short)1);
            i = this.mapper.updateByPrimaryKeySelective(track);
            track = null;
        }
        return i;
    }

    /**
     * @方法名：selectMotionTrackByUserId
     * @描述： 根据用户id 获取最近半小时的运动轨迹
     * @作者： 周明智
     * @日期： Created in 2020/4/23 15:36
     */
    public List<MotionTrack> selectMotionTrackByUserId(String userId) {
        List<MotionTrack> motionTracks = this.mapper.selectMotionTrackByUserId(userId);
        return motionTracks;
    }


    /**
     * @方法名：selectMotionTrackByUserIdAndTime
     * @描述： 根据用户id查询时间区间的运动轨迹
     * @作者： 周明智
     * @日期： Created in 2019/9/27 16:13
     */
    public List<MotionTrack> selectMotionTrackByUserIdAndTime(String userId,String startTime,String endTime){
        List<MotionTrack> motionTracks = this.mapper.selectMotionTrackByUserIdAndTime(userId, startTime, endTime);
        return motionTracks;
    }

    /**
     * @方法名：deleteMotionTrackByUserIds
     * @描述： 根据用户id（物理）删除用户的运动轨迹
     * @作者： kjz
     * @日期： Created in 2020/4/24 13:57
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteMotionTrackByUserIds(String[] userIds) {
        return this.mapper.deleteMotionTrackByUserIds(userIds);
    }

    /**
     * @方法名：selectMotionTrackLastTimeByUserID
     * @描述： 根据用户id查询最近的一条运动轨迹
     * @作者： kjz
     * @日期： Created in 2020/4/24 16:49
     */
    public MotionTrack selectMotionTrackLastTimeByUserID(String userId) {
        return this.mapper.selectMotionTrackLastTimeByUserID(userId);
    }
}
