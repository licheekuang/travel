package com.liaoyin.travel.service;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.UserTeamMapper;
import com.liaoyin.travel.entity.UserTeam;
import com.liaoyin.travel.util.SnowIdUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @项目名：
 * @作者：lijing
 * @描述：员工所属部门
 * @日期：Created in 2018/6/20 14:55
 */
@Service
public class UserTeamService extends BaseService<UserTeamMapper, UserTeam> {

	/***
	 * 
	     * @方法名：selectUserTeam
	     * @描述： 根据用户id或部门id查询部门团队信息
	     * @作者： lijing
	     * @日期： 2019年8月19日
	 */
	public List<UserTeam> selectUserTeam(String userId,String teamId){
	
		List<UserTeam> list = this.mapper.selectUserTeam(userId, teamId);
		return list;
	}
	
   
	/****
	 * 
	     * @方法名：insertOrUpdate
	     * @描述： 新增或修改
	     * @作者： lijing
	     * @日期： 2019年8月19日
	 */
	@Transactional(rollbackFor = Exception.class)
	public void insertOrUpdate(String userId,List<UserTeam> list,String originalDirectorId) {
		
		deleteUserTeamByIds(userId);
		list.stream().forEach((u) ->{
			/*if(originalDirectorId!=null){
				//把原来的经理更新为普通员工
				String teamId = u.getTeamId();
				this.mapper.updateAssumeOfficeActAsTwoByTeamIdAndUserId(teamId,originalDirectorId);
			}*/
			u.setId(SnowIdUtil.getInstance().nextId())
					.setUserId(userId)
					.setCreateTime(LocalDateTime.now());
		});
		this.mapper.insertList(list);
	}
	
	/***
	 * 
	     * @方法名：deleteUserTeamByIds
	     * @描述： 根据用户id删除
	     * @作者： lijing
	     * @日期： 2019年8月19日
	 */
	@Transactional(rollbackFor = Exception.class)
	public void deleteUserTeamByIds(String userId) {
		
		this.mapper.deleteUserTeamByIds(userId);
	}

	/**
	 * @方法名：insertOrUpdateUserTeam
	 * @描述： 新增或修改【用户-部门】关联
	 * @作者： kuang.jiazhuo
	 * @日期： Created in 2019/11/29 15:03
	 */
	@Transactional(rollbackFor = Exception.class)
	public int insertOrUpdateUserTeam(String userId, Short assumeOffice, String teamId) {
		deleteUserTeamByIds(userId);
		UserTeam userTeam = new UserTeam()
				.setId(SnowIdUtil.getInstance().nextId())
				.setTeamId(teamId)
				.setAssumeOffice(assumeOffice)
				.setUserId(userId)
				.setCreateTime(LocalDateTime.now());
		int result = this.mapper.insertSelective(userTeam);
		return result;
	}

	/**
	 * @方法名：selectBranchManagerIdByStaffId
	 * @描述： 根据员工id查询部门经理的id
	 * @作者： kuang.jiazhuo
	 * @日期： Created in 2019/12/2 14:52
	 */
	@Transactional(rollbackFor = Exception.class)
    public String selectBranchManagerIdByStaffId(String userId) {
		String branchManagerId = this.mapper.selectBranchManagerIdByStaffId(userId);
		return branchManagerId;
    }

    /**
     * @方法名：selectDirectorIdByTeamId
     * @描述： 根据部门id查询该部门的经理
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/12/3 14:38
     */
	public List<String> selectDirectorIdByTeamId(String teamId) {
        List<String> userIds = this.mapper.selectDirectorIdByTeamId(teamId);
		return userIds;
	}

	/**
	 * 批量删除部门-用户关系
	 * @param teamIds
	 * @return
	 */
	public int deleteUserTeamByTeamIds(List<String> teamIds) {
		return this.mapper.deleteUserTeamByTeamIds(teamIds);
	}

	/**
	 * 判断部门是否已经关联员工
	 * @param teamIds
	 * @return
	 */
	public List<String> judgeTeamIsCorrelationStaff(List<String> teamIds){
		return this.mapper.judgeTeamIsCorrelationStaff(teamIds);
	}


}
