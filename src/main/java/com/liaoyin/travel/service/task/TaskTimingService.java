package com.liaoyin.travel.service.task;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.business.util.TaskUtil;
import com.liaoyin.travel.dao.task.TaskTimingMapper;
import com.liaoyin.travel.entity.task.TaskTiming;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 定时任务参数 Service
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-25 17:18
 */
@Service
public class TaskTimingService extends BaseService<TaskTimingMapper, TaskTiming> {

    /**
     * @方法名：insertOrUpdateTaskTiming
     * @描述： 新增或更新定时任务时间
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/25 17:27
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertOrUpdateTaskTiming(TaskTiming taskTiming) {
        String pubWeeks = taskTiming.getPubWeeks();
        String[] pubWeekArray = pubWeeks.split(",");
        List<String> list = Arrays.asList(pubWeekArray);
        if(taskTiming.getId()==null){
            //新增
            for(String pubWeek:list){
                //"星期"有效性验证
                if(pubWeek.split(":").length>0){
                    taskTiming.setIsDelete(VariableConstants.STRING_CONSTANT_0);
                    taskTiming.setPubWeek(pubWeek);
                    taskTiming.setId(UUIDUtil.getUUID());
                    taskTiming.setCreateTime(new Date());
                    this.mapper.insert(taskTiming);
                }else{
                    throw new BusinessException("pubWeek.is.mistake");
                }
            }
        }else{
            //更新
            for(String pubWeek:list){
                //"星期"有效性验证
                if(TaskUtil.IspubWeekValueRange(pubWeek)){
                    this.mapper.updateByPrimaryKey(taskTiming);
                }
            }
        }
    }

    /**
     * @author 王海洋
     * @methodName: 移动端定时任务
     * @methodDesc:
     * @description:
     * @param:
     * @return
     * @create 2019-12-05 11:32
     **/
    @Transactional(rollbackFor = Exception.class)
    public void insertOrUpdateTaskTimingMobile(TaskTiming taskTiming) {
        if(taskTiming.getId()==null){
            //新增
            taskTiming.setIsDelete(VariableConstants.STRING_CONSTANT_0);
            taskTiming.setId(UUIDUtil.getUUID());
            taskTiming.setCreateTime(new Date());
            this.mapper.insert(taskTiming);
        }else{
            //更新
            this.mapper.updateByPrimaryKey(taskTiming);
        }
    }

    public Map<String, TaskTiming> selectTaskTimingbyTimingTaskParameterId(String timingTaskParameterId) {
        return this.mapper.selectTaskTimingbyTimingTaskParameterId(timingTaskParameterId);
    }

    /**
     * @方法名：selectTaskTimingByTaskId
     * @描述： 根据定时任务参数id查询定时任务时间
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/28 21:05
     */
    public List<TaskTiming> selectTaskTimingByTaskId(String timedTaskId,String scenicId) {
        List<TaskTiming> list = this.mapper.selectTaskTimingByTaskId(timedTaskId,scenicId);
        return list;
    }
}
