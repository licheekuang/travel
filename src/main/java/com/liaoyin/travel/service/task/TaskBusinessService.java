package com.liaoyin.travel.service.task;

import com.liaoyin.travel.vo.task.TaskIdAndUserIdVo;
import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.task.TaskBusinessMapper;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.task.TaskBusiness;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.UsersService;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @项目名：旅投
 * @作者：lijing
 * @描述：任务指定的领取人
 * @日期：Created in 2018/6/20 14:55
 */
@Service
public class TaskBusinessService extends BaseService<TaskBusinessMapper, TaskBusiness> {
   
	@Autowired
	private UsersService usersService;
	@Autowired
	private TaskReceivingRecordsService taskReceivingRecordsService;
	
	
	
	/***
	 * 
	     * @方法名：selectTaskBusinessList
	     * @描述： 根据任务id查询指定的领取人
	     * @作者： lijing
	     * @日期： 2019年7月25日
	 */
	public List<TaskBusiness> selectTaskBusinessList(String taskId){
		
		List<TaskBusiness> list= this.mapper.selectTaskBusinessList(taskId);
		return list;
	}
	
	/****
	 * 
	     * @方法名：getTaskBusinessList
	     * @描述： 获取指定任务的领取人
	     * @作者： lijing
	     * @日期： 2019年8月21日
	 */
	public List<String> getTaskBusinessList(String taskId){
		
		List<TaskBusiness> taskList=selectTaskBusinessList(taskId);
		List<String> list = new ArrayList<>();
		if(taskList !=null && taskList.size() >0) {
		 taskList.stream().forEach((t)->{
				if(t.getIsSpecify() !=null ) {
					if(t.getIsSpecify().intValue() == 1) {
						//指定人员
						list.add(t.getBusinessId());
					}else if(t.getIsSpecify().intValue() == 2) {
						//指定部门
						//1:查询部门下的所有员工
						List<String> userList = this.usersService.selectAllTheTreamUserId(t.getBusinessId());
						list.addAll(userList);
					}else if(t.getIsSpecify().intValue() == 3) {
						//指定工种
						List<String> userList = this.usersService.selectUsersWorkList(t.getBusinessId());
						list.addAll(userList);
					}
				}
			});
		}		
		return list.stream().distinct().collect(Collectors.toList());
	}

	/*****
	 * 
	     * @方法名：insertTaskBusiness
	     * @描述： 新增任务指定的领取人
	     * @作者： lijing
	     * @日期： 2019年7月25日
	 */
	@Transactional(rollbackFor=BusinessException.class)
	public void insertTaskBusiness(String isSpecify,String businessId,String taskId) {
		List<TaskBusiness> list = new ArrayList<TaskBusiness>();
		TaskBusiness t = null;
		if(businessId ==null) {
			throw new BusinessException("businessId.is.null","任务指定为空");
		}
		String[] id = businessId.split(",");
		if(id == null || id.length == 0) {
			throw new BusinessException("businessId.is.null","任务指定为空");
		}
		for(String s:id) {
			t = new TaskBusiness();
			t.setBusinessId(s);
			t.setCreatTime(new Date());
			t.setId(UUIDUtil.getUUID());
			t.setTaskId(taskId);
			t.setIsSpecify(Short.valueOf(isSpecify));
			list.add(t);
			t = null;
		}
		List<String> userIdList = new ArrayList<String>();
		//this.taskReceivingRecordsService.insertTaskReceivingRecords(insertTaskVo.getUserList(), id);
		if(VariableConstants.STRING_CONSTANT_1.equals(isSpecify)){
			userIdList.clear();
			//指定人员
			for(String s:id) {
				if(s !=null && !"".equals(s)) {
					userIdList.add(s);
				}
			}
			//添加用户领取记录
			this.taskReceivingRecordsService.insertTaskReceivingRecords(userIdList,taskId);
			
		}else if(VariableConstants.STRING_CONSTANT_2.equals(isSpecify)) {
			//指定部门
			//1:查询部门下的所有人
			List<Users> userList = this.usersService.selectselectUsersListBybusinessIds("1", id);
			if(userList!=null && userList.size() >0) {
				userList.stream().forEach((u) ->{
					if(u.getId() !=null && !"".equals(u.getId())) {
						userIdList.add(u.getId());
					}
				});
				//添加用户领取记录
				this.taskReceivingRecordsService.insertTaskReceivingRecords(userIdList,taskId);
			}
		}else {
			//指定工种
			//1:查询部门下的所有人
			List<Users> userList = this.usersService.selectselectUsersListBybusinessIds("2", id);
			if(userList!=null && userList.size() >0) {
				userList.stream().forEach((u) ->{
					if(u.getId() !=null && !"".equals(u.getId())) {
						userIdList.add(u.getId());
					}
				});
				//添加用户领取记录
				this.taskReceivingRecordsService.insertTaskReceivingRecords(userIdList,taskId);
			}
		}
		this.mapper.insertList(list);
	}
	
	/******
	 * 
	     * @方法名：selectTaskBusinessId
	     * @描述： 查询任务领取人集合
	     * @作者： lijing
	     * @日期： 2019年7月25日
	 */
	public String selectTaskBusinessId(String taskId) {
		List<TaskBusiness> list = selectTaskBusinessList(taskId);
		StringBuffer businessId = new StringBuffer();
		if(list !=null &&list.size() >0) {
			list.stream().forEach((t) ->{
				if(t.getBusinessId() !=null && !"".equals(t.getBusinessId())) {
					businessId.append(t.getBusinessId()+",");
				}
			});
		}
		return businessId.toString();
	}

	/**
	 * @方法名：selectUserIdByBusinessIds
	 * @描述： 通过是否指定人员和businessIds获取到用户的id
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/11/12 21:38
	 */
	public List<String> selectUserIdByBusinessIds(String isSpecify, String businessIds) {
		if(businessIds ==null) {
			throw new BusinessException("businessId.is.null","任务指定为空");
		}
		String[] id = businessIds.split(",");
		if(id == null || id.length == 0) {
			throw new BusinessException("businessId.is.null","任务指定为空");
		}
		List<String> userIdList = new ArrayList<String>();
		if(VariableConstants.STRING_CONSTANT_1.equals(isSpecify)){
			userIdList.clear();
			//指定人员
			for(String s:id) {
				if(s !=null && !"".equals(s)) {
					/** 没有被删除 且 没有被禁用的才发任务 */
					Users users = this.usersService.selectUsersById(s);
					if(users!=null && users.getIsUsing()==1){
						userIdList.add(s);
					}
				}
			}
		}else if(VariableConstants.STRING_CONSTANT_2.equals(isSpecify)) {
			//指定部门
			//1:查询部门下的所有人
			List<Users> userList = this.usersService.selectselectUsersListBybusinessIds("1", id);
			if(userList!=null && userList.size() >0) {
				userList.stream().forEach((u) ->{
					if(u.getId() !=null && !"".equals(u.getId())) {
						userIdList.add(u.getId());
					}
				});
			}
		}else {
			//指定工种
			//1:查询工种下的所有人
			List<Users> userList = this.usersService.selectselectUsersListBybusinessIds("2", id);
			if(userList!=null && userList.size() >0) {
				userList.stream().forEach((u) ->{
					if(u.getId() !=null && !"".equals(u.getId())) {
						userIdList.add(u.getId());
					}
				});
			}
		}
		return userIdList;
	}

	/**
	 * @方法名：insertTaskBusinessByUserIdsAndTaskIds
	 * @描述： 新增指定任务领取人
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/11/12 22:07
	 */
	@Transactional(rollbackFor=BusinessException.class)
	public void insertTaskBusinessByUserIdsAndTaskIds(List<TaskIdAndUserIdVo> taskIdAndUserIdVoList,String isSpecify) {
		taskIdAndUserIdVoList.forEach(taskIdAndUserIdVo -> {
			String taskId = taskIdAndUserIdVo.getTaskId();
			String userId = taskIdAndUserIdVo.getUserId();
			String taskManagerId = taskIdAndUserIdVo.getTaskMangerId();
			String releaseWay = taskIdAndUserIdVo.getReleaseWay();
			String scenicId = taskIdAndUserIdVo.getScenicId();
			this.taskReceivingRecordsService.insertTaskReceivingRecordsByUserIdAndTaskId(taskId,userId,taskManagerId,releaseWay,scenicId);

			TaskBusiness taskBusiness = new TaskBusiness();
			taskBusiness.setTaskId(taskId);
			taskBusiness.setBusinessId(userId);
			taskBusiness.setCreatTime(new Date());
			taskBusiness.setId(UUIDUtil.getUUID());
			taskBusiness.setIsSpecify(Short.valueOf(isSpecify));
			this.mapper.insert(taskBusiness);
		});
	}
}
