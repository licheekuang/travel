package com.liaoyin.travel.service.task;


import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.task.TaskDetailsMapper;
import com.liaoyin.travel.entity.task.TaskDetails;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @项目名：旅投
 * @作者：lijing
 * @描述：任务详情
 * @日期：Created in 2018/6/20 14:55
 */
@Service
public class TaskDetailsService extends BaseService<TaskDetailsMapper, TaskDetails> {
   
	/*****
	 * 
	     * @方法名：selectTaskDetailsListByTaskId
	     * @描述： 根据任务id查询任务详情
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	public List<TaskDetails> selectTaskDetailsListByTaskId(String taskId,Integer queryType){
		
		List<TaskDetails> list = this.mapper.selectTaskDetailsListByTaskId(taskId,queryType);
		return list;
	}
	
	
	/*****
	 * 
	     * @方法名：insertTaskDetails
	     * @描述： 新增任务详情
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	public void  insertTaskDetails(List<TaskDetails> list,String id) {
		UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
		if(curveUserInfo == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = curveUserInfo.getScenicId();
		
		List<TaskDetails> collect = list.stream().map((t) ->{
			t.setCreatTime(new Date());
			t.setId(UUIDUtil.getUUID());
			t.setTaskId(id);
			t.setIsDelete((short)0);
			t.setScenicId(scenicId);
			return t;
		}).collect(Collectors.toList());
		
		this.mapper.insertList(collect);
		collect=null;
		list= null;
	}

	/**
	 * @方法名：insertTimingTaskDetails
	 * @描述： 通过定时任务新增详情
	 * @作者： kjz
	 * @日期： Created in 2020/3/19 17:30
	 */
	public void  insertTimingTaskDetails(List<TaskDetails> list,String id,String scenicId) {

		List<TaskDetails> collect = list.stream().map((t) ->{
			t.setCreatTime(new Date());
			t.setId(UUIDUtil.getUUID());
			t.setTaskId(id);
			t.setIsDelete((short)0);
			t.setScenicId(scenicId);
			return t;
		}).collect(Collectors.toList());

		this.mapper.insertList(collect);
		collect=null;
		list= null;
	}
	
	/*****
	 * 
	     * @方法名：selectMaxTaskDetailsTaskFlow
	     * @描述： 查询任务最大的流程
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	public Integer selectMaxTaskDetailsTaskFlow(String taskId,Integer queryType) {
		List<TaskDetails> taskDetails = this.mapper.selectTaskDetailsListByTaskId(taskId,queryType);
		int size = taskDetails.size();

		Integer  k = size;
		return k;
	}

	/**
	　* @description: TODO  根据任务ID查询任务详情及任务路线
	　* @param [taskId]
	　* @return com.liaoyin.travel.entity.task.TaskDetails
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/7/26 13:06
	　*/
	public TaskDetails selectTaskDetailByTid(String taskId){
		return this.mapper.selectTaskDetailByTid(taskId);
	}
	
}
