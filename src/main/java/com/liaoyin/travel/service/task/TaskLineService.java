package com.liaoyin.travel.service.task;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.task.TaskLineMapper;
import com.liaoyin.travel.entity.task.TaskCompletionRecord;
import com.liaoyin.travel.entity.task.TaskDetails;
import com.liaoyin.travel.entity.task.TaskLine;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


/**
 * @项目名：旅投
 * @作者：lijing
 * @描述：任务线路
 * @日期：Created in 2019/7/25 14:55
 */
@Service
public class TaskLineService extends BaseService<TaskLineMapper, TaskLine> {
    
	@Autowired
	private TaskDetailsService taskDetailsService;
	@Autowired
	private TaskCompletionRecordService taskCompletionRecordService;
	
	/****
	 * 
	     * @方法名：insertOrUpdateTaskLine
	     * @描述： 新增或修改
	     * @作者： lijing
	     * @日期： 2019年7月25日
	 */
	@Transactional(rollbackFor = Exception.class)
	public String insertOrUpdateTaskLine(TaskLine taskLine) {

		UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
		if(curveUserInfo == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = curveUserInfo.getScenicId();

		//线路名称
		String lineName = taskLine.getLineName();
		//id
		String id = taskLine.getId();
		if(id!=null && !"".equals(id)) {
			/** 更新*/
			//判断线路名称不能重复
			judgementLineNameRepetition(lineName,id);
			return taskLine.getId();
		}else {
			/** 新增*/
			//判断线路名称不能重复
			judgementLineNameRepetition(lineName,null);

			if(taskLine.getTaskDetailsList()== null || taskLine.getTaskDetailsList().size() ==0) {
				throw new BusinessException("TaskDetailsList.is.null","线路为空");
			}
			id = UUIDUtil.getUUID();
			taskLine.setCreatTime(LocalDateTime.now())
				.setId(id).setIsDelete((short)0)
				.setScenicId(scenicId);
			System.err.println("taskLine="+taskLine);
			this.mapper.insertSelective(taskLine);
			this.taskDetailsService.insertTaskDetails(taskLine.getTaskDetailsList(), id);
			return id;
		}
	}

	/**
	 * @方法名：judgementLineNameRepetition
	 * @描述： 判断线路名称不能重复，若重复抛出异常
	 * @作者： kjz
	 * @日期： Created in 2020/4/6 13:32
	 */
	public void judgementLineNameRepetition(String lineName, String id) {
		Integer count = this.mapper.selectCountByLineName(lineName,id);
		if(count>0){
			throw new BusinessException("lineName.is.repeat");
		}
	}

	/**
	 * @方法名：insertTimingTaskLine
	 * @描述： 通过定时任务新增线路任务
	 * @作者： kjz
	 * @日期： Created in 2020/3/19 17:10
	 */
	@Transactional(rollbackFor = Exception.class)
	public String insertTimingTaskLine(TaskLine taskLine) {

		if(taskLine.getId() !=null && !"".equals(taskLine.getId())) {

			return taskLine.getId();
		}else {
			if(taskLine.getTaskDetailsList()== null || taskLine.getTaskDetailsList().size() ==0) {
				throw new BusinessException("TaskDetailsList.is.null","线路为空");
			}
			String id = UUIDUtil.getUUID();
			taskLine.setCreatTime(LocalDateTime.now());
			taskLine.setId(id);
			taskLine.setIsDelete((short)0);
			this.mapper.insertSelective(taskLine);
			this.taskDetailsService.insertTimingTaskDetails(taskLine.getTaskDetailsList(), id,taskLine.getScenicId());
			return id;
		}
	}
	
	/****
	 * 
	     * @方法名：selectTaskDetailsListByLineId
	     * @描述： 
	     * @作者： lijing
	     * @日期： 2019年7月25日
	 */
	public List<TaskDetails> selectTaskDetailsListByLineId(String lineId,String userId,Integer queryType){
		
		List<TaskDetails> list = this.taskDetailsService.selectTaskDetailsListByTaskId(lineId,queryType);
		if(list !=null && list.size() >0) {
			TaskCompletionRecord tr = null;
			for(TaskDetails t:list) {
				tr = this.taskCompletionRecordService.selectTaskCompletionRecordByTaskIdAndDetailsIdAndUserId(lineId, t.getId(), userId);
				if(tr !=null) {
					t.setTaskCompletionRecord(tr);
					tr = null;
				}else {
					break;
				}
			}
		}
		return list;
	} 
	
	/***
	 * 
	     * @方法名：selectTaskLineBackList
	     * @描述： 查询任务线路列表
	     * @作者： lijing
	     * @日期： 2019年7月26日
	 */
	public List<TaskLine> selectTaskLineBackList(Integer num,Integer size,String linekName){
		UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
		if(curveUserInfo == null) {
			throw new BusinessException("not.login");
		}
		/** 景区id */
		String scenicId = curveUserInfo.getScenicId();

		PageHelper.startPage(num,size);
		List<TaskLine> list = this.mapper.selectTaskLineBackList(linekName,scenicId);
		return list;
	}
	
	/***
	 * 
	     * @方法名：selectTaskLineByIdOnBack
	     * @描述： 根据id查询任务线路详情-后台
	     * @作者： lijing
	     * @日期： 2019年7月26日
	 */
	public TaskLine selectTaskLineByIdOnBack(String id ,Integer queryType) {
		
		if(id == null) {
			throw new BusinessException("id.si.null");
		}
		TaskLine taskLine = this.mapper.selectByPrimaryKey(id);
		if(taskLine !=null ) {
			List<TaskDetails> list= this.taskDetailsService.selectTaskDetailsListByTaskId(taskLine.getId(),queryType);
			taskLine.setTaskDetailsList(list);
			list = null;
		}
		return taskLine;
		
	}
	
	/***
	 * 
	     * @方法名：deleteTaskLineByIdOnBack
	     * @描述： 根据id删除任务线路-后台
	     * @作者： lijing
	     * @日期： 2019年7月26日
	 */
	@Transactional(rollbackFor = Exception.class)
	public int deleteTaskLineByIdOnBack(String ids) {
		
		if(ids == null) {
			throw new BusinessException("id.si.null");
		}
		String[] id = ids.split(",");
		return this.mapper.deleteTaskLineByIdOnBack(id);
	}
	
}
