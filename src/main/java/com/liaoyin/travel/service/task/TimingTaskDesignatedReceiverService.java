package com.liaoyin.travel.service.task;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.dao.task.TaskMapper;
import com.liaoyin.travel.dao.task.TimingTaskDesignatedReceiverMapper;
import com.liaoyin.travel.entity.task.Task;
import com.liaoyin.travel.entity.task.TimingTaskDesignatedReceiver;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 定时任务指定领取人 Service
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-28 19:55
 */
@Service
public class TimingTaskDesignatedReceiverService extends BaseService<TimingTaskDesignatedReceiverMapper, TimingTaskDesignatedReceiver> {

    /**
     * @方法名：insertTimingTaskDesignatedReceiver
     * @描述： 新增定时任务指定领取人
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/28 19:58
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertTimingTaskDesignatedReceiver(TimingTaskDesignatedReceiver timingTaskDesignatedReceiver) {
        timingTaskDesignatedReceiver.setId(UUIDUtil.getUUID());
        timingTaskDesignatedReceiver.setCreateTime(new Date());
        timingTaskDesignatedReceiver.setIsDelete(VariableConstants.STRING_CONSTANT_0);
        this.mapper.insert(timingTaskDesignatedReceiver);
    }
}
