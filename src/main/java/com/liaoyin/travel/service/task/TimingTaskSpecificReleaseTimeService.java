package com.liaoyin.travel.service.task;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.task.TimingTaskSpecificReleaseTimeMapper;
import com.liaoyin.travel.entity.task.TimingTaskSpecificReleaseTime;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 定时任务的具体发布时间记录
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-26 15:40
 */
@Service
public class TimingTaskSpecificReleaseTimeService extends BaseService<TimingTaskSpecificReleaseTimeMapper, TimingTaskSpecificReleaseTime> {

    /**
     * @方法名：insertTimingTaskSpecificReleaseTime
     * @描述： 新增定时任务的发布记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/26 18:09
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertTimingTaskSpecificReleaseTime(TimingTaskSpecificReleaseTime timingTaskSpecificReleaseTime) {
        timingTaskSpecificReleaseTime
                .setId( UUIDUtil.getUUID())
                .setIsDelete(VariableConstants.STRING_CONSTANT_0)
                .setCreateTime(new Date())
                .setPostStatus(VariableConstants.STRING_CONSTANT_1);
        this.mapper.insert(timingTaskSpecificReleaseTime);
    }

    /**
     * @方法名：selectTimingTaskSpecificReleaseTimeByTimedTaskId
     * @描述： 后台-查询定时任务具体发布时间记录
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/29 10:36
     */
    public PageInfo<TimingTaskSpecificReleaseTime> selectTimingTaskSpecificReleaseTimeByTimedTaskId(String timedTaskId,Integer num,Integer size) {
        UserInfo curveUserInfo = PartyUtil.getCurrentUserInfo();
        if(curveUserInfo == null) {
            throw new BusinessException("not.login");
        }
        /** 景区id */
        String scenicId = curveUserInfo.getScenicId();
        PageHelper.startPage(num,size);
        List<TimingTaskSpecificReleaseTime> list =this.mapper.selectTimingTaskSpecificReleaseTimeByTimedTaskId(timedTaskId,scenicId);
        PageInfo<TimingTaskSpecificReleaseTime> p = new PageInfo<>(list);
        return p;
    }
}
