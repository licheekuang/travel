package com.liaoyin.travel.service.task;

import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.dao.task.TaskCompletionRecordMapper;
import com.liaoyin.travel.dao.task.TaskLineMapper;
import com.liaoyin.travel.dao.task.TaskReceivingRecordsMapper;
import com.liaoyin.travel.entity.attendance.Equipment;
import com.liaoyin.travel.entity.task.TaskCompletionRecord;
import com.liaoyin.travel.entity.task.TaskDetails;
import com.liaoyin.travel.entity.task.TaskReceivingRecords;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.service.attendance.EquipmentService;
import com.liaoyin.travel.util.LocationUtils;
import com.liaoyin.travel.util.ParamUtil;
import com.liaoyin.travel.util.PartyUtil;
import com.liaoyin.travel.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名：
 * @作者：lijing
 * @描述：轮播图
 * @日期：Created in 2018/6/20 14:55
 */
@Service
public class TaskCompletionRecordService extends BaseService<TaskCompletionRecordMapper, TaskCompletionRecord> {
    
	
	@Autowired
	private TaskDetailsService taskDetailsService;
	@Autowired
	private TaskReceivingRecordsService taskReceivingRecordsService;
	@Autowired
	private EquipmentService equipmentService;

	@Resource
	private TaskReceivingRecordsMapper taskReceivingRecordsMapper;
	@Resource
	private TaskLineMapper taskLineMapper;


	/***
	 * 
	     * @方法名：selectTaskCompletionRecordByTaskIdAndDetailsIdAndUserId
	     * @描述： 根据任务id，详情id，用户id 查询任务完成情况
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	public TaskCompletionRecord selectTaskCompletionRecordByTaskIdAndDetailsIdAndUserId(String taskId,String detailsId,String userId) {		
		
		TaskCompletionRecord tr = this.mapper.selectTaskCompletionRecordByTaskIdAndDetailsIdAndUserId(taskId, detailsId, userId);
		return tr;
	}
	
	/****
	 * 
	     * @方法名：judgeCurrentTask
	     * @描述： 判断当前任务是否允许签到
	     * @作者： lijing
	     * @日期： 2019年7月16日
	 */
	public Map<String, Object> judgeCurrentTask(String taskDetailsId,
			String lat,String log,String equipmentId){
		/*LocationUtils.bd_encrypt(Double.valueOf(log),Double.valueOf(lat));
		lat = LocationUtils.bd_lat + "";
		log = LocationUtils.bd_log + "";*/
		Map<String, Object> map = new HashMap<>();
		TaskDetails td = this.taskDetailsService.selectById(taskDetailsId);
		if(td == null) {
			throw new BusinessException("task.is.null");
		}
		if(td.getTaskOperation()!=null && td.getTaskOperation().intValue()==1) {
			//签到 
			if(lat==null) {
				throw new BusinessException("lat.is.null");
			}
			if(log==null) {
				throw new BusinessException("log.is.null");
			}
			//获取允许的偏差距离
			Double allowableDeviationDistance = Double.valueOf(ParamUtil.getValue("allowableDeviationDistance"));
			System.out.println(allowableDeviationDistance.toString());
			Equipment eq;
			/*if(eq == null) {
				throw new BusinessException("equipment.is.null");
			}*/
			Double disparity;
			if (td.getLat() != null && !"".equals(td.getLat())){
				disparity = LocationUtils.getDistance(Double.valueOf(td.getLat()), Double.valueOf(td.getLog()), Double.valueOf(lat), Double.valueOf(log));
			} else {
				//查询打卡设备信息
				eq = this.equipmentService.selectEquipmentById(equipmentId);
				System.out.println(eq.toString());
				disparity = LocationUtils.getDistance(Double.valueOf(eq.getLat()), Double.valueOf(eq.getLog()), Double.valueOf(lat), Double.valueOf(log));
			}
			/*if(Math.abs(disparity) > allowableDeviationDistance) {
				throw new BusinessException("allowableDeviationDistance.is.erreo");
			}*/
			System.out.println(Math.abs(disparity)+":::::"+allowableDeviationDistance);
			map.put("disparity", Math.abs(disparity));
			/*map.put("disparity", 1.0);*/
			map.put("allowableDeviationDistance", allowableDeviationDistance);
			eq=null;
			td=null;
		}
		System.out.println("map="+map.toString());
		return map;
	}
	
	
	/****
	 * 
	     * @方法名：insertTaskCompletionRecord
	     * @描述： 新增任务完成
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	public void insertTaskCompletionRecord(TaskCompletionRecord taskCompletionRecord) {
		
		UserInfo user = PartyUtil.getCurrentUserInfo();
		if(user == null) {
			throw new BusinessException("not.login");
		}

		/** 景区id */
		String scenicId = user.getScenicId();

		System.err.println("taskCompletionRecord="+taskCompletionRecord);

		//判断是否在允许的范围
		if(taskCompletionRecord.getTaskDetailsId()==null){
			throw new BusinessException("","任务不存在");
		}
		Map<String, Object> map = judgeCurrentTask(taskCompletionRecord.getTaskDetailsId(), taskCompletionRecord.getLat(), taskCompletionRecord.getLog(),taskCompletionRecord.getEquipmentId());
		if(map != null && map.size() >0) {
			Double disparity = (Double) map.get("disparity");
			Double allowableDeviationDistance =(Double)map.get("allowableDeviationDistance");
			if(disparity > allowableDeviationDistance) {
				throw new BusinessException("allowableDeviationDistance.is.erreo");
			}
			taskCompletionRecord.setDisparity(disparity);
			map = null;
		}
		TaskCompletionRecord t = selectTaskCompletionRecordByTaskIdAndDetailsIdAndUserId(taskCompletionRecord.getTaskId(), taskCompletionRecord.getTaskDetailsId(), user.getId());
		if(t !=null) {
			t = null;
			throw new BusinessException("taskFlow.is.success");
		}
		taskCompletionRecord.setCreatTime(new Date());
		taskCompletionRecord.setId(UUIDUtil.getUUID());
		taskCompletionRecord.setUserId(user.getId());
		taskCompletionRecord.setScenicId(scenicId);
		this.mapper.insertSelective(taskCompletionRecord);
		Integer k = this.taskDetailsService.selectMaxTaskDetailsTaskFlow(taskCompletionRecord.getTaskId(),taskCompletionRecord.getTaskType() == 2 ? 0:1);
		if(k == taskCompletionRecord.getTaskFlow().intValue()) {
			//已经完成
			List<TaskReceivingRecords> taskReceivingRecords = this.taskReceivingRecordsService.selectTaskReceivingRecordsByTaskId(taskCompletionRecord.getTaskReceivingRecordsId(), null, null);
			for (TaskReceivingRecords record : taskReceivingRecords) {
				record.setTaskEndTime(new Date());
				/*record.setTastStatus((short)5);*/
				this.taskReceivingRecordsService.updateTaskReceivingRecords(record);
			}
			/*TaskReceivingRecords tr = new TaskReceivingRecords();
			tr.setId();
			*//*tr.setTastStatus((short)5);*//*
			tr.setTaskEndTime(new Date());*/

			System.out.println("任务已完成");
			this.taskLineMapper.deleteByExample(taskCompletionRecord.getLineId());
			//tr=null;
		}
		String s = tempTaskMap.get(taskCompletionRecord.getTaskId());
		if (null != s){
			TaskReceivingRecords records = new TaskReceivingRecords();
			records.setTastStatus((short)2);
			records.setId(s);
			this.taskReceivingRecordsMapper.updateByPrimaryKeySelective(records);
		}
	}
}
