package com.liaoyin.travel.service;

import com.github.pagehelper.PageHelper;
import com.liaoyin.travel.base.service.BaseService;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.dao.BannerMapper;
import com.liaoyin.travel.entity.Banner;
import com.liaoyin.travel.entity.FileUpload;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.util.FileUploadUtil;
import com.liaoyin.travel.util.JsonUtil;
import com.liaoyin.travel.util.RedisUtil;
import com.liaoyin.travel.util.UUIDUtil;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @项目名：
 * @作者：lijing
 * @描述：轮播图
 * @日期：Created in 2018/6/20 14:55
 */
@Service
public class BannerService extends BaseService<BannerMapper, Banner> {
    /**
     * @方法名：insertBanner
     * @描述： 新增
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
    public void insertBanner(Banner banner) {
      if(banner.getId() != null && !"".equals(banner.getId())) {
    	  if(banner.getListFileupload() != null && banner.getListFileupload().size() >0) {
        	FileUploadUtil.saveFileUploadList(banner.getListFileupload(), banner.getId());
          }
    	  if(banner.getBannerUrlType().intValue()!=6) {
    		  banner.setBannerUrl(""); 
    	  }
    	  this.mapper.updateByPrimaryKeySelective(banner);
    	  RedisUtil.del("selectBannerList");
    	  selectBannerListRedis("");
      }else {
    	  String id = UUIDUtil.getUUID();
    	  banner.setId(id);
    	  if(banner.getListFileupload() != null && banner.getListFileupload().size() >0) {
          	FileUploadUtil.saveFileUploadList(banner.getListFileupload(), id);
            }
    	  this.mapper.insertSelective(banner);
    	  RedisUtil.del("selectBannerList");
    	  selectBannerListRedis("");
      }
    }
    
    /**
     * @方法名：selectBannerList
     * @描述： 获取banner列表
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
    public List<Banner> selectBannerList (Integer num,Integer size,String bannertype,String bannerDesc){
    	PageHelper.startPage(num,size);
    	List<Banner> list = this.mapper.selectBannerList(bannertype,bannerDesc);
    	return list;
    }
    
    /**
     * @方法名：selectBannerById
     * @描述： 根据id查询banner
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
    public Banner selectBannerById(String id) {
    	if(id == null) {
    		throw new BusinessException("id is null","id为空");
    	}
    	Banner b = this.mapper.selectByPrimaryKey(id);
    	if(b != null && !"".equals(b)) {
    		//获取图片
    		List<FileUpload> listFileUpload = FileUploadUtil.getFileUploadList(b.getId());
    		if(listFileUpload!= null && listFileUpload.size() >0) {
    			for(FileUpload f :listFileUpload) {
    				if(f != null && !"".equals(f)) {
    					b.setPic(CommonConstant.FILE_SERVER+f.getFilePath());
    					break;
    				}
    			}
    			b.setListFileupload(listFileUpload);
    		}
    	}
    	return b;
    }
    /**
     * @方法名：insertOrUpdateBanner
     * @描述： 新增或修改banner
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
    public void insertOrUpdateBanner (Banner banner) {
    	//保存图片
    	if(banner.getId() != null && !"".equals(banner.getId())) {
      	  
    		if(banner.getListFileupload() != null && banner.getListFileupload().size() >0) {
            	FileUploadUtil.saveFileUploadList(banner.getListFileupload(), banner.getId());
              }
      	  this.mapper.updateByPrimaryKeySelective(banner);
      	  RedisUtil.del("selectBannerList");
    	  selectBannerListRedis("");
        }else {
      	  String id = UUIDUtil.getUUID();
      	  banner.setId(id);
      	if(banner.getListFileupload() != null && banner.getListFileupload().size() >0) {
        	FileUploadUtil.saveFileUploadList(banner.getListFileupload(), id);
          }
      	  this.mapper.insertSelective(banner);
      	  RedisUtil.del("selectBannerList");
    	  selectBannerListRedis("");
        }
    }
    /**
     * @方法名：deleteBannerByIds
     * @描述： 根据id删除banner
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
    public void deleteBannerByIds(String ids) {
    	if(ids == null) {
    		throw new BusinessException("id is null","id为空");
    	}
    	String[] id = ids.split(",");
    	this.mapper.deleteBannerByIds(id);
    	 RedisUtil.del("selectBannerList");
    	 selectBannerListRedis("");
    }
    
    
    /******************************app端************************************/
    
    public List<Banner> selectBannerListRedis(String bannertype){
    	List<Banner> list = null;
    	if(RedisUtil.isExistence("selectBannerList")) {
    		try {
				list = JsonUtil.getBeanList(RedisUtil.get("selectBannerList", String.class),Banner.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}else {
    		list = this.mapper.selectBannerList("","");
    	}
    	List<Banner> retList = new ArrayList<Banner>();
    	if(list != null && list.size() >0) {
    		if(bannertype!= null&&!"".equals(bannertype)) {
        		for(Banner b :list) {
        			if(bannertype.equals(b.getBannerType())) {
        				retList.add(b);
        			}
        		}
        	}else {
        		retList.addAll(list);
        	}
    		RedisUtil.set("selectBannerList", JsonUtil.getJsonString(list));
    	}
    	
    	
    	
    	return retList;
    }
}
