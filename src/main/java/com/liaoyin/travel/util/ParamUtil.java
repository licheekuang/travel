package com.liaoyin.travel.util;

import com.google.common.collect.Maps;
import com.liaoyin.travel.entity.GlobalSystemParam;
import com.liaoyin.travel.service.GlobalSystemParamService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/*
 * 系统参数工具类
 */
@Component
public class ParamUtil {

   
    private static GlobalSystemParamService globalSystemParamService;
    private static Map<String, String> globalParamMap;
    private static Map<String, String> globalParamOnIdMap;
  
    @Autowired
    public void setGlobalSystemParamService(GlobalSystemParamService globalSystemParamService) {
        ParamUtil.globalSystemParamService = globalSystemParamService;
    }

    public static void getGlobalSystemParamListAll() {
        globalParamMap = Maps.newHashMap();
        globalParamOnIdMap = Maps.newHashMap();
        List<GlobalSystemParam> globalParams = globalSystemParamService.selectListAll();
        for (GlobalSystemParam globalParam : globalParams) {
            globalParamMap.put(globalParam.getParamCode(), globalParam.getParamValue());
            globalParamOnIdMap.put(globalParam.getParamCode(), globalParam.getId()+"//"+globalParam.getConditionParam());
        }
    }

    public static String getValue(String code) {
    	if(globalParamMap == null ) {
    		getGlobalSystemParamListAll();
    	}
        return globalParamMap.get(code);
    }
    
    public static String getId(String code) {
    	if(globalParamOnIdMap == null ) {
    		getGlobalSystemParamListAll();
    	}
        return globalParamOnIdMap.get(code);
    }

}
