package com.liaoyin.travel.util;


import com.liaoyin.travel.exception.BusinessException;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 对象转换工具类
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-03 15:41
 */
public class Trans2BO {
    /**
     *   类转BO
     * @param obj  对象
     * @param clzz 要转换的对象
     * @param <T>
     * @return
     * @throws Exception
     */
    public static   <T> T trans2BO(Object obj,Class<T> clzz) {
        try {
            T obj1 = clzz.newInstance();
            Field[] fields2 = clzz.getDeclaredFields();
            Class<?> clazz1 = obj.getClass();
            for (; clazz1 != Object.class; clazz1 = clazz1.getSuperclass()) {
                Field[] fields1 = clazz1.getDeclaredFields();
                for (Field f1 : fields1) {
                    if("serialVersionUID".equals(f1.getName())){
                        continue;
                    }
                    for (Field f2 : fields2) {
                        if(f1.getName().equals(f2.getName())){
                            Object[] obj2 = new Object[1];
                            obj2[0] = invokeGetMethod(obj ,f1.getName(),null);
                            invokeSetMethod(obj1, f2.getName(), obj2);
                        }
                    }
                }
            }
            return obj1;
        }catch (Exception e){
            throw new BusinessException("转换错误");
        }

    }



    /**
     * list   BO转换
     * @param objs
     * @param clzz
     * @param <T>
     * @return
     */
    public static <T> List<T> trans2BoList(List<Object>  objs,Class<T> clzz){
        List<T> obj=new ArrayList<>();
        objs= (List<Object>) objs.get(0);
        for(int i=0;i<objs.size();i++){
            T t = trans2BO(objs.get(i), clzz);
            obj.add(t);
        }
        return obj;
    }



    public static Object invokeGetMethod(Object obj, String fieldName, Object[] args)
    {
        String methodName = fieldName.substring(0, 1).toUpperCase()+ fieldName.substring(1);
        Method method = null;
        for(Class<?> clazz = obj.getClass() ; clazz != Object.class ; clazz = clazz.getSuperclass()) {
            try {
                method = clazz.getDeclaredMethod("get" + methodName);
                return method.invoke(obj);
            } catch (Exception e) {

            }
        }
        return "";
//        try
//        {
//            method = Class.forName(clazz.getClass().getName()).getDeclaredMethod();
//            return method.invoke(clazz);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            return "";
//        }
    }
    public  static Object invokeSetMethod(Object clazz, String fieldName, Object[] args)
    {
        String methodName = fieldName.substring(0, 1).toUpperCase()+ fieldName.substring(1);
        Method method = null;
        try
        {
            Class[] parameterTypes = new Class[1];
            Class c = Class.forName(clazz.getClass().getName());
            Field field = c.getDeclaredField(fieldName);
            parameterTypes[0] = field.getType();
            method = c.getDeclaredMethod("set" + methodName,parameterTypes);
            return method.invoke(clazz,args);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
