package com.liaoyin.travel.util;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.liaoyin.travel.constant.CommonParamter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @项目名：
 * @作者：lijing
 * @描述：短信工具类
 * @日期：Created in 2018/12/12 15:22
 */
public class SmsUtil {

    //随机数
    private static final String NONCE="123456";
   
    /***
     * 
         * @方法名：sendCode
         * @描述： 发送验证码模板
         * @作者： lijing
         * @日期： 2018年12月14日
     */
    public static void sendCode(String phone,String message) throws ClientProtocolException, IOException {
    	DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(CommonParamter.SERVER_URL);
        String curTime = String.valueOf((new Date()).getTime() / 1000L);
        String checkSum = CheckSumBuilder.getCheckSum(CommonParamter.APP_SECRET, NONCE, curTime);
        // 设置请求的header
        httpPost.addHeader("AppKey", CommonParamter.APP_KEY);
        httpPost.addHeader("Nonce", NONCE);
        httpPost.addHeader("CurTime", curTime);
        httpPost.addHeader("CheckSum", checkSum);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

        // 设置请求的的参数，requestBody参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("templateid", CommonParamter.TEMPLATEID));
        nvps.add(new BasicNameValuePair("mobile", phone));
        nvps.add(new BasicNameValuePair("authCode", message));

        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

        // 执行请求
        HttpResponse response = httpClient.execute(httpPost);
       // System.out.println(EntityUtils.toString(response.getEntity(), "utf-8"));
    }

    /***
     * 
     * @throws IOException 
     * @throws Exception 
     * @方法名：sendNotificationAndOperatingSMS
     * @描述： 发送通知类和运营类短信
     * @作者： lijing
     * @日期： 2018年12月14日
     */
    //参数待定
    public static void sendNotificationAndOperatingSMS(String phone,String message) throws Exception {
    	
    	DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(CommonParamter.sendtemplate);
        String curTime = String.valueOf((new Date()).getTime() / 1000L);
        /*
         * 参考计算CheckSum的java代码，在上述文档的参数列表中，有CheckSum的计算文档示例
         */
        String checkSum = CheckSumBuilder.getCheckSum(CommonParamter.APP_SECRET, NONCE, curTime);

        // 设置请求的header
        httpPost.addHeader("AppKey", CommonParamter.APP_KEY);
        httpPost.addHeader("Nonce", NONCE);
        httpPost.addHeader("CurTime", curTime);
        httpPost.addHeader("CheckSum", checkSum);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

        // 设置请求的的参数，requestBody参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        /*
         * 1.如果是模板短信，请注意参数mobile是有s的，详细参数配置请参考“发送模板短信文档”
         * 2.参数格式是jsonArray的格式，例如 "['13888888888','13666666666']"
         * 3.params是根据你模板里面有几个参数，那里面的参数也是jsonArray格式
         */
        nvps.add(new BasicNameValuePair("templateid",CommonParamter.SENDTEMPLATEID));
        nvps.add(new BasicNameValuePair("mobiles", "["+phone+"]"));
        nvps.add(new BasicNameValuePair("params", "["+message+"]"));

        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

        // 执行请求
        HttpResponse response = httpClient.execute(httpPost);
        /*
         * 1.打印执行结果，打印结果一般会200、315、403、404、413、414、500
         * 2.具体的code有问题的可以参考官网的Code状态表
         */
        System.out.println(EntityUtils.toString(response.getEntity(), "utf-8"));

    	
    }
    
    
    public static void main(String[] args) {
    	try {
    		sendNotificationAndOperatingSMS("13290001943", "123456");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
