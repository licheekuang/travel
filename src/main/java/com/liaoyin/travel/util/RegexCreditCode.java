package com.liaoyin.travel.util;

import java.util.HashMap;
import java.util.Map;

/******
 * 社会信用代码证 检验
 * @author Administrator
 *
 */
public class RegexCreditCode {

	private static String isCreditCode = "1";
	private static String error_CreditCode = "2";
	private static String error_CreditCode_min = "社会信用代码不足18位，请核对后再输！";
	private static String error_CreditCode_max = "社会信用代码大于18位，请核对后再输！";
	private static String error_CreditCode_empty ="社会信用代码不能为空！";
    private static Map<String,Integer> datas = null;
    private static char[] pre17s;
    private static int[] power = {1,3,9,27,19,26,16,17,20,29,25,13,8,24,10,30,28};
    private final static String regex = "^([159Y]{1})([1239]{1})([0-9ABCDEFGHJKLMNPQRTUWXY]{6})([0-9ABCDEFGHJKLMNPQRTUWXY]{9})([0-90-9ABCDEFGHJKLMNPQRTUWXY])$";
	// 社会统一信用代码不含（I、O、S、V、Z） 等字母
    private static char[] code = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','T','U','W','X','Y'};
	
	public static Boolean isLicense18(String creditCode) {
		creditCode.trim();
		if(creditCode == null || "".equals(creditCode) || creditCode.length() < 18) {
			return false;
		}
		//正则验证
		if (!creditCode.matches(regex)) {
			return false;
		}
		if(datas == null) {
			initDatas(code.length);
		}
        pre17(creditCode);
    	String str = isCreditCode(creditCode);
    	if("1".equals(str)) {
    		return true;
    	}else {
    		return false;
    	}
    }
   
	 /**
     * 判断是否是一个有效的社会信用代码
     * @param creditCode
     * @return
     */
   public static String isCreditCode(String creditCode){
        if("".equals(creditCode)||" ".equals(creditCode)){
            //System.out.println(error_CreditCode_empty);
            return error_CreditCode_empty;
        }else if(creditCode.length()<18){
            System.out.println(error_CreditCode_min);
            return  error_CreditCode_min;
        }else if(creditCode.length()>18){
            System.out.println(error_CreditCode_max);
            return  error_CreditCode_max;
        }else{
            int sum =sum(creditCode);
            int temp = sum%31;
            temp = temp==0?31:temp;
            return creditCode.substring(17,18).equals(code[31-temp]+"")?isCreditCode:error_CreditCode;
        }
    }
	
   
   /**
    * @param chars
    * @return
    */
   private static int sum(String creditCode){
       int sum = 0;
       String pre17 = creditCode.substring(0,17);
       pre17s = pre17.toCharArray();
       for(int i=0;i<pre17s.length;i++){
           int code = datas.get(pre17s[i]+"");
           sum+=power[i]*code;
       }
       pre17s=null;
       return sum;

   }
   
   /**
    * 获取前17位字符
    * @param creditCode
    */
   static  void  pre17(String creditCode){
       String pre17 = creditCode.substring(0,17);
       pre17s = pre17.toCharArray();
   }

   /**
    * 初始化数据
    * @param count
    */
   public static void  initDatas(int count){
	   datas = new HashMap<>();
       for(int i=0;i<code.length;i++){
           datas.put(code[i]+"",i);
       }
   }
   
   public static void main(String[] args) {
      // initDatas(code.length);
       //pre17("112345678911111111");
	   boolean b = isLicense18("9****thfghdfgdfgdfdfjoieogi的价格商家的反倒是开了房间发的斯蒂芬 ");
	   System.out.println(b);
   }
}
