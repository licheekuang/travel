package com.liaoyin.travel.util;


import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @项目名：
 * @类名称：${CLASS_NAME}
 * @类描述：MD5工具类
 * @创建人：john
 * @时间：2018年01月04日 17:11
 */
@SuppressWarnings("restriction")
public class MD5Util {

    public static String EncoderByMd5(String str){
        String newStr = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            BASE64Encoder base64Encoder = new BASE64Encoder();
            newStr = base64Encoder.encode(md5.digest(str.getBytes("utf-8")));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return  newStr;
    }

    public static  String createPassword(String pwd, String salt){
        pwd = pwd.substring(0,3) + salt + pwd.substring(1,pwd.length());
        pwd = MD5Util.EncoderByMd5(pwd);
        return  pwd;
    }

    /**
     * @方法名：Base64Encode
     * @描述： 使用Base64进行加密
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 9:35
     */
     public static String Base64Encode(String res) {
        return Base64.encode(res.getBytes());
     }

     /**
      * @方法名：Base64Decode
      * @描述： 使用Base64进行解密
      * @作者： Kuang.JiaZhuo
      * @日期： Created in 2020/2/22 9:36
      */
      public static String Base64Decode(String res) {
         return new String(Base64.decode(res));
      }

    /**
     * @方法名：getMD5String
     * @描述： Md5加密(不可逆)
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 9:31
     */
    public static String getMD5String(String str) {
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(str.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8位字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            //一个byte是八位二进制，也就是2位十六进制字符（2的8次方等于16的2次方）
            return new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @方法名：MD5Upper
     * @描述： MD5加密(大写)
     * @作者： kjz
     * @日期： Created in 2020/3/31 15:40
     */
    public static String MD5Upper(String s) {
        char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F' };
        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(getMD5String("123456"));
        System.out.println(Base64Decode("MTIzNDU2"));
    }

}
