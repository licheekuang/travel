package com.liaoyin.travel.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.gate.JwtTokenUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.RandomAccessFile;

@Component
public class PartyUtil {

	private static HttpServletRequest request;

	private static HttpServletResponse response;

	//private static JwtUtil jwtUtil;

	public static HttpServletResponse getResponse() {
		return response;
	}

	@Autowired
	public void setResponse(HttpServletResponse response) {
		PartyUtil.response = response;
	}

	public static HttpServletRequest getRequest() {
		return request;
	}

	@Autowired
	public void setRequest(HttpServletRequest request) {
		PartyUtil.request = request;
	}
	//获取当前userInfo
	public static UserInfo getCurrentUserInfo() {
		UserInfo userInfo = null;
		/*userInfo = new UserInfo();
		userInfo.setId("1");
		userInfo.setPassword("pwd");
		userInfo.setNickName("昵称");
		userInfo.setAccount("admin");
		userInfo.setUserType("1");
		userInfo.setHeadPicUrl("1");*/
		//userInfo.setIsInvite(false);
		// 从header里获取token
		//TODO 默认一个userInfo
		String authToken = request.getHeader("access-token");
		System.out.println("authToken="+authToken);
		if (StringUtils.isNotBlank(authToken)) {
			try {
				userInfo = JsonUtil.getSingleBean(RedisUtil.get(JwtTokenUtil.getRedisKeyFromToken(authToken),String.class),UserInfo.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return userInfo;
	}
	public static String getCurrentToken(){
		return request.getHeader("access-token");
	}

	public static String getFileSize(long size) {
		// 如果字节数少于1024，则直接以B为单位，否则先除于1024，后3位因太少无意义
		if (size < 1024) {
			return String.valueOf(size) + "B";
		} else {
			size = size / 1024;
		}
		// 如果原字节数除于1024之后，少于1024，则可以直接以KB作为单位
		// 因为还没有到达要使用另一个单位的时候
		// 接下去以此类推
		if (size < 1024) {
			return String.valueOf(size) + "KB";
		} else {
			size = size / 1024;
		}
		if (size < 1024) {
			// 因为如果以MB为单位的话，要保留最后1位小数，
			// 因此，把此数乘以100之后再取余
			size = size * 100;
			return String.valueOf((size / 100)) + "." + String.valueOf((size % 100)) + "MB";
		} else {
			// 否则如果要以GB为单位的，先除于1024再作同样的处理
			size = size * 100 / 1024;
			return String.valueOf((size / 100)) + "." + String.valueOf((size % 100)) + "GB";
		}
	}

	public static int getFileSizeKb(long size){
		// 如果字节数少于1024，则直接以B为单位，否则先除于1024，后3位因太少无意义
//		if (size < 1024) {
//			size = size * 100 / 1024;
//			return new Long(String.valueOf((size / 100)) + "." + String.valueOf((size % 100))).intValue();
//		} else {
			size = size / 1024;
//		}

		return new Long(size).intValue();
	}

	public static void generateDictJson() throws Exception {
		RandomAccessFile raf=new RandomAccessFile("d:/temp/test.js", "rw");
	}

	public static <T> T ifNull(T o1,T o2){
		if(o1 instanceof String){
			return StringUtils.isBlank((String)o1)?o2:o1;
		}else{
			return o1==null?o2:o1;
		}
	}

}
