package com.liaoyin.travel.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @项目名：
 * @作者：lijing
 * @描述：日期工具类
 * @日期：Created in 2018/6/8 15:05
 */
public class DateUtil {
	
	private static final SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat sdf_date=new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat sdf_date_currentTime=new SimpleDateFormat("yyyyMMddHHmmss");
	private static final SimpleDateFormat sdf_date_StringTime=new SimpleDateFormat("yyyy年MM月dd日");
	private static final SimpleDateFormat sdf_date_StringTime_now=new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat df = new SimpleDateFormat("HH:mm");//设置日期格式 
	
    private static final String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
	

	/**
	 * 获取当前时间 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getCurrentTime4Str(){
		String data = "";
		synchronized(sdf) {
			data = sdf.format(Calendar.getInstance().getTime());
		}
		return data;
	}
	/**
	 * 
	     * @方法名：returenStringTime
	     * @描述： 将时间转化为yyyy年MM月dd日格式
	     * @作者： lijing
	     * @日期： 2018年11月15日
	 */
	public static String returenStringTime(Date date) {
		String data = "";
		synchronized (sdf_date_StringTime) {
			data = sdf_date_StringTime.format(date);
		}
		return data;
	}
	
	/**
	 * 获取当前时间毫秒值
	 * @return
	 */
	public static String getcurrentTimeMillis(){
		return System.currentTimeMillis()+"";
	}
    /**
     * 获取当前时间秒数
     * @return
     */
    public static String getcurrentSecondsString(){
        long currentTimeMillis = System.currentTimeMillis();
        long l = currentTimeMillis/1000;
        String ls = Long.toString(l);
        return ls;
	}
	/**
	 * 获取当前时间秒数
	 * @return  int
	 */
	public static Integer getCurrentSecondsInt(){
		long currentTimeMillis = System.currentTimeMillis();
		long l = currentTimeMillis/1000;
		int i = Integer.parseInt(l + "");
		return i;
	}
	/**
	 * 将字符串时间转换为秒值
	 * @return
	 */
	public static long formateConverLong(String fmt){
		synchronized(sdf) {
			try {
				Date parse = sdf.parse(fmt);
				long l = parse.getTime()/1000;
				return  l;

			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return  0;
	}

	/**
	 * 将时间  yyyy-MM-dd HH:mm:ss 转为Date
	 * @param time
	 * @return
	 */
	public static Date formatyyyyMMddHHmmss(String time){
		synchronized(sdf) {
			try {
				Date parse = sdf.parse(time);
				return  parse;
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return  null;
	}

	/**
	 * 将时间   毫秒值  转换为 yyyy-MM-dd HH:mm:ss
	 * @param time
	 * @return
	 */
	public static String formatTimeMillis(String time){
		String format = "";
		synchronized (sdf) {
			format = sdf.format(Long.parseLong(time));
		}
		return format;
	}
	/**
	 * 获取当前时间 yyyy--mmdd
	 * @return
	 */
	public static String getCurrentyyyymmdd(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(Calendar.getInstance().getTime());
	}
	
	/**
	 * 获取当前时间 yyyy--mmdd
	 * @return
	 */
	public static String getCurrentyyyymmddNow(){
		
		return sdf_date_currentTime.format(Calendar.getInstance().getTime());
	}
	/**
	 * 获取当前时间  yyyy-MM-dd
	 * @return
	 */
	public static String getCurrentyyyyMMdd(){
		String data = "";
		synchronized (sdf_date) {
			data = sdf_date.format(Calendar.getInstance().getTime());
		}
		return data;
	}
	
	/**
	 *将时间转化为  yyyy-MM-dd
	 * @return
	 */
	public static Date formatyyyyMMdd(String time){
		synchronized (sdf_date) {
			try {
				Date parse = sdf_date.parse(time);
				return  parse;
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 获取当前时间 yyyymmddHHmmss
	 * @return
	 */
	public static String getCurrentyyyymmddHHmmss(){
		String data = "";
		synchronized (sdf_date_currentTime) {
			data = sdf_date_currentTime.format(Calendar.getInstance().getTime());
		}
		return data;
	}
	/**
	 * 获取当前时间 yyyymmddHHmmss
	 * @return
	 */
	public static String formatyyyyMMddHHmmss(Date date){
		String data = "";
		synchronized (sdf) {
			data = sdf.format(date);
		}
		return data;
	}
	
	
	 /**
     * 取得指定分钟数后的时间
     * 
     * @param date
     *            基准时间
     * @param minuteAmount
     *            指定分钟数，允许为负数
     * @return 指定分钟数后的时间
     */
    public static Date addMinute(Date date, int minuteAmount) {
        if (date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minuteAmount);
        return calendar.getTime();
    }
    /****
     * 
         * @方法名：determinationAgeUser
         * @描述：  判断两个时间相差的年
         * @作者： lijing
         * @日期： 2018年11月28日
     */
   public static Integer determinationAgeUser(String toDate) {
	   int k = 0;
	   Date date =formatyyyyMMdd(toDate);
	   int toYear = date.getYear();
	   int nowYear = new Date().getYear();
	   return nowYear-toYear;
   }
   /*****
    *  
        * @方法名：validDate
        * @描述： 根据身份证判断用户年龄
        * @作者： lijing
        * @日期： 2018年11月28日
    */
   public static Integer validDate(String card){
	   int k = 0;
	   	String birth = card.length() == 15 ? "19" + card.substring(6, 12) : card.substring(6, 14);
        if(birth !=null && !"".equals(birth)) {
        	 SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd"); 
        	 formatter.setLenient(false);
        	 Date newDate = null;
        	 try {
				newDate= formatter.parse(birth);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
        	if(newDate != null) {
				k = determinationAgeUser(sdf_date.format(newDate));
			}
        } 
        return k;
   } 
    
   /**
	 * 获取当前日期是星期几<br>
	 * 
	 * @param date
	 * @return 当前日期是星期几
	 */
	public static String getWeekOfDate(Date date) {
		String[] weekDays = { "0", "1", "2", "3", "4", "5", "6" };
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0) {
			w = 0;
		}
		return weekDays[w];
	}
	
	/**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     * 
     * @param nowTime 当前时间
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     * @author jqlin
     */
    public static boolean isEffectiveDate(Date startTime, Date endTime) {
    	Date nowTime = new Date();
    	if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }

    /**
    　* @description: TODO  判断是否在目标时间之前或等于目标时间
    　* @param [checkTime]  需要判断的目标时间
    　* @return boolean
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/30 15:45
    　*/
    public static boolean isBeforeDate(Date time,Date checkTime){
		if (time.getTime() == checkTime.getTime()){
			return true;
		}
		Calendar date = Calendar.getInstance();
		date.setTime(time);
		Calendar check = Calendar.getInstance();
		check.setTime(checkTime);
		if(date.before(check)){
			return true;
		}else {
			return false;
		}
	}

	//获取当前毫秒数并随机截取5位
    public static String randomInterceptionMillisecond(Integer num) {
    	String millisecond = System.currentTimeMillis()+"";
    	if(num > millisecond.length()) {
    		num = millisecond.length();
    	}
    	return millisecond.substring(millisecond.length()-num, millisecond.length());
    }
    
    /**
     * 比较两日期对象的大小, 忽略秒, 只精确到分钟.
     * 
     * @param date
     *            日期对象1, 如果为 <code>null</code> 会以当前时间的日期对象代替
     * @param anotherDate
     *            日期对象2, 如果为 <code>null</code> 会以当前时间的日期对象代替
     * @return 如果日期对象1大于日期对象2, 则返回大于0的数; 反之返回小于0的数; 如果两日期对象相等, 则返回0.
     */
    public static int compareIgnoreSecond(Date date, Date anotherDate) {
        if (date == null) {
            date = new Date();
        }

        if (anotherDate == null) {
            anotherDate = new Date();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        date = cal.getTime();

        cal.setTime(anotherDate);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        anotherDate = cal.getTime();

        return date.compareTo(anotherDate);
    }



    //判断时间是否在指定时间的显示分钟范围内
    public static Boolean isBelong(String startTime,String closeTime){ 
    	Date now =null; 
    	Date beginTime = null; 
    	Date endTime = null; 
    	try { 
    		now = df.parse(df.format(new Date())); 
    		beginTime = df.parse(startTime); 
    		endTime = df.parse(closeTime); 
    	} catch (Exception e) { 
    		e.printStackTrace(); 
    	} 
    	boolean flag = belongCalendar(now,beginTime, endTime); 
    	now =null; 
    	beginTime = null; 
    	endTime = null; 
    	return 	flag;	

    }

	/**
	　* @description: TODO  判断时间time是否在checktime之前或等于
	　* @param [time, checkTime]
	　* @return java.lang.Boolean
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/8/30 15:52
	　*/
    public static  Boolean isBeforeHHmmss(Date time,Date checkTime){
		Date now =null;
		try {
			now = df.parse(df.format(time));
		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean flag = isBeforeDate(now, checkTime);
		now = null;
		return flag;
	}

    
  //判断时间是否在指定时间的显示分钟范围内
    public static Boolean isBelongHHmmss(Date checkTime,Date startTime,Date closeTime){ 
    	Date now =null; 
    	//Date beginTime = null; 
    	//Date endTime = null; 
    	try { 
    		now = df.parse(df.format(checkTime)); 
    		//beginTime = df.parse(startTime); 
    		//endTime = df.parse(closeTime); 
    	} catch (Exception e) { 
    		e.printStackTrace(); 
    	} 
    	boolean flag = belongCalendar(now,startTime, closeTime); 
    	now =null; 
    	//beginTime = null; 
    	//endTime = null; 
    	return 	flag;	

    }
    
    /**
     * 判断时间是否在时间段内
     * @param nowTime
     * @param beginTime
     * @param endTime
     * @return
     */ public static boolean belongCalendar(Date nowTime, Date beginTime, Date endTime) { 
    	 Calendar date = Calendar.getInstance(); 
    	 date.setTime(nowTime); 
    	 Calendar begin = Calendar.getInstance(); 
    	 begin.setTime(beginTime); 
    	 Calendar end = Calendar.getInstance(); 
    	 end.setTime(endTime); 
    	 if (date.after(begin) && date.before(end)) { 
    		 return true; 
    	 } else { 
    		 return false;
    	} 
    }

     /**
      * 获取当前日期是星期几
      * @param
      * @return
      */
     public static Integer getWeekDay() {
         // 再转换为时间
         Date date = new Date();
         Calendar c = Calendar.getInstance();
         c.setTime(date);
         int hour=c.get(Calendar.DAY_OF_WEEK);
         if(hour==1){
             return 7;
         }else{
             return hour-1;
         }
         // hour中存的就是星期几了，其范围 1~7
         // 1=星期日 7=星期六，其他类推
     }

     /**
      * 
      * @param strDate
      *            输入日期
      * @param dayNum
      *            相隔天数 正整数表示前推 ，负整数表示后推
      * @return 日期字符串
      * @throws ParseException
      */
     public static String getDifferDate(String strDate, int dayNum)
             throws ParseException {
    	 Date date = null;
         if(strDate != null && !"".equals(strDate)) {
        	 date = sdf.parse(strDate);
         }else {
        	 date = new Date();
         }
         Calendar c = Calendar.getInstance();
         c.setTime(date);
         c.add(Calendar.DATE, dayNum);
         String sqldate = sdf_date.format(c.getTime());
         return sqldate;
     }
     
     /**
      * 获取上周的指定星期的日期。
      * 
      * @param dayOfWeek
      *            星期几，取值范围是 {@link Calendar#MONDAY} - {@link Calendar#SUNDAY}
      */
     public static String getDateOfPreviousWeek(int dayOfWeek) {
         if (dayOfWeek > 7 || dayOfWeek < 1) {
             throw new IllegalArgumentException("参数必须是1-7之间的数字");
         }

         return getDateOfRange(dayOfWeek, -7);
     }
     
     private static String getDateOfRange(int dayOfWeek, int dayOfRange) {
         Calendar cal = Calendar.getInstance();
         cal.set(Calendar.DAY_OF_WEEK, dayOfWeek+1);
         cal.set(Calendar.DATE, cal.get(Calendar.DATE) + dayOfRange);
         cal.set(Calendar.HOUR_OF_DAY, 0);
         cal.set(Calendar.MINUTE, 0);
         cal.set(Calendar.SECOND, 0);
         cal.set(Calendar.MILLISECOND, 0);
         return (String) sdf.format(cal.getTime());
     }
     
     
     //计算两个时间之间的差
     public static Long getDatePoor(Date endDate, Date nowDate,String type) {
    	 
    	    long nd = 1000 * 24 * 60 * 60;
    	    long nh = 1000 * 60 * 60;
    	    long nm = 1000 * 60;
    	    long time = 0;
    	    // long ns = 1000;
    	    // 获得两个时间的毫秒时间差异
    	    long diff = endDate.getTime() - nowDate.getTime();
    	    if("1".equals(type)) {
    	    	// 计算差多少天
    	    	time = diff / nd;
    	    }else if("2".equals(type)) {
    	    	// 计算差多少小时
    	    	time = diff/ nh;
    	    }else if("3".equals(type)) {
    	    	// 计算差多少分钟
    	    	time = diff / nm;
    	    }else if("4".equals(type)) {
    	    	// 计算差多少秒//输出结果
    	    	time = diff/1000;
    	    }
    	    return time;
    	}

    public static Date parseDate(String dateStr, String fmt) throws ParseException {
		return new SimpleDateFormat(fmt).parse(dateStr);
    }
    public static String format(Date date,String fmt){
		return new SimpleDateFormat(fmt).format(date);
	}
    
    public static String getWeekCHN(Date date) {
    	String week = getWeekOfDate(date);
    	return weekDays[Integer.valueOf(week)];
    }

    /**
     * @方法名：getWeekOfDateStr
     * @描述： 传入时间返回星期几(字符串)
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/10/25 16:25
     */
	public static String getWeekOfDateStr(Date date) {
		String[] weekDays = { "星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0){
            w = 0;
        }

		return weekDays[w];
	}

	/**
	 * @方法名：dateShiftHhmm
	 * @描述： 把时间转换为【HH:mm】
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/10/26 17:52
	 */
	public static String dateShiftHhmm(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		String formatStr =formatter.format(date);
		return formatStr;
	}

	/**
	 * @方法名：getDaysByYYMM
	 * @描述： 根据传入的年月字符串【yyyy-MM】返回该月的天数
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/11/5 17:46
	 */
	public static  int getDaysByYYMM(String yearMonth){
		String[] timeStr = yearMonth.split("-");
		int year = Integer.valueOf(timeStr[0]);
		int month = Integer.valueOf(timeStr[1]);
		int days = 0;
		switch (month) {
			case 1: case 3: case 5: case 7: case 8: case 10: case 12 : days = 31; break;
			case 4: case 6: case 9: case 11: days = 30; break;
			case 2: days = year % 4 == 0 ? 29 : 28;
			default : break;
		}
		return days;
	}

	/**
	 * @方法名：strToDateYMDHMS
	 * @描述： 传入字符串返回Date【yyyy-MM-dd HH:mm:ss】
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/11/20 15:37
	 */
	public static Date strToDateYMDHMS(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = sdf.parse(str);
		} catch (ParseException e) {
		}
		return date;
	}

	/**
	 * @方法名：strTransitionLocalDateTime
	 * @描述： String转换为LocalDateTime
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/11/22 21:27
	 */
	public static LocalDateTime strTransitionLocalDateTime(String dateStr){
		LocalDateTime dateTime = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		return dateTime;
	}

	/**
	 * @方法名：localDateSwitchString
	 * @描述： LocalDate 转为 yyyy-MM-dd 字符串
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/11/23 0:15
	 */
	public static String localDateSwitchString(LocalDate date) {
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String dateStr = date.format(fmt);
		return dateStr;
	}


	/**
	 * @方法名：numberSwitchWhatDayStr
	 * @描述： 数字转星期
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/11/23 0:21
	 */
	public static String numberSwitchWhatDayStr(Integer whatDay) {
		String[] weekDays = { "星期一", "星期二", "星期三", "星期四", "星期五", "星期六","星期日"};
		String result = "";
		whatDay = whatDay-1;
		for(int i=0;i<weekDays.length;i++){
			if(whatDay==i){
				result = weekDays[i];
			}
		}
		return result;
	}

    /**
     * 计算LocalTime之间的小时差
     * @param startTime 小的时间
     * @param endTime 大的时间
     * @param digits 保留的小数位,不要小数位可传0(4舍5入,比如13.6小时就会返回14小时)
     * @return
     */
    public static String calculateLocalTimeHoursDifference(LocalTime startTime,LocalTime endTime,int digits){
        double mistiming  = (double) Duration.between(startTime,endTime).getSeconds()/3600;
        String some = "0";
        if(digits>0){
            some += ".";
            for(int i =0;i<digits;i++){
                some += "0";
            }
        }
        DecimalFormat df = new DecimalFormat(some);
        String result = df.format(mistiming);
        return result;
    }

    /**
     * 从小到大返回两个LocalDate之间所有的时间
     * @param startDate 小的时间
     * @param endDate 大的时间
     * @return
     */
    public static List<LocalDate> getDescDateList(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> result = new ArrayList<>();
        LocalDate tempDate = null;//用户存储中间变量
        Long num = endDate.toEpochDay() - startDate.toEpochDay();
        //int num = endDate.compareTo(startDate);//不能使用这个方法进行比较，可以试一下7月30号 到8月30的比较结果
        for (int i = 0; i <= num; i++) {
            tempDate = endDate;
            if (tempDate.toEpochDay() - startDate.toEpochDay() >= 0) {
                result.add(endDate);
                endDate = tempDate.plusDays(-1);
            }
        }
        System.out.println(result);
        return result;
    }

    /**
     * 从大到小返回两个LocalDate之间所有的时间
     * @param startDate 小的时间
     * @param endDate 大的时间
     * @return
     */
    public static List<LocalDate> getAscDateList(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> result = new ArrayList<>();
        LocalDate tempDate = null;//用户存储中间变量
        Long num = endDate.toEpochDay() - startDate.toEpochDay();
        for (int i = 0; i <= num; i++) {
            tempDate = startDate;
            if (tempDate.toEpochDay() - endDate.toEpochDay() <= 0) {
                result.add(startDate);
                startDate = tempDate.plusDays(1);
            }
        }
        System.out.println(result);
        return result;
    }

    /**
     * 计算LocalDate的天数差
     *
     * @param startDate 小的日期
     * @param endDate   大的日期
     * @return
     */
    public static int calculateLocalDateDaysDifference(LocalDate startDate, LocalDate endDate) {
        Period next = Period.between(startDate, endDate);
        int result = next.getDays();
        return result;
    }

    /**
     * @方法名：getOutgoingDisplayDate
     * @描述： 外出时间组装显示
     * @作者： kuang.jiazhuo
     * @日期： Created in 2019/11/25 21:57
     */
	public static String getOutGoingDisplayDate(LocalDateTime startTime,LocalDateTime endTime){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");;
		String startdDateTime = startTime.format(formatter);
		String endTDateTime = endTime.format(formatter);
		String result = startdDateTime + " 至 " +endTDateTime;
		return result;
	}


	public static String getWeekOfDateNum(Date date) {
		String[] weekDays = {"7", "1", "2", "3", "4", "5", "6"};
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0){
			w = 0;
		}
		return weekDays[w];
	}

	/**
	 * @方法名：judgeOutOfTimeIsGteThatDay
	 * @描述： 判断外出日期是否大于等于当天
	 * @作者： kuang.jiazhuo
	 * @日期： Created in 2019/11/25 22:10
	 */
	public static boolean judgeOutOfTimeIsGteThatDay(LocalDate absenceStartDate) {
		LocalDate thatDay = LocalDate.now();
		if(absenceStartDate.equals(thatDay) || absenceStartDate.isAfter(thatDay)){
			return true;
		}
		return false;
	}

	/**
	 * @方法名：judgeDateIsGteThatDay
	 * @描述： 判断传入的日期字符串是否大于等于当天
	 * @作者： kjz
	 * @日期： Created in 2020/4/20 16:29
	 */
	public static boolean judgeDateStrIsGteThatDay(String date) {
		LocalDate introductionDate = LocalDate.parse(date);
		LocalDate thatDay = LocalDate.now();
		if(introductionDate.equals(thatDay) || introductionDate.isAfter(thatDay)){
			return true;
		}
		return false;
	}

	/**
	 * @方法名：getFormatterYMDHM
	 * @描述： 格式化LocalDateTime 为 yyyy-MM-dd HH:mm
	 * @作者： kuang.jiazhuo
	 * @日期： Created in 2019/11/26 17:27
	 */
	public static String getFormatterYMDHM(LocalDateTime localDateTime){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String result = localDateTime.format(formatter);
		return result;
	}

	/**
	 * @方法名：computedTimeCheckFormat
	 * @描述： 格式化LocalDateTime的时间差
	 * @作者： kuang.jiazhuo
	 * @日期： Created in 2019/11/27 19:58
	 */
	public static String computedTimeCheckFormat (LocalDateTime startTime,LocalDateTime endTime) {
		//秒
		long seconds = ChronoUnit.SECONDS.between(startTime, endTime);
		//转换天数
		long days = seconds / 86400;
		//剩余秒数
		seconds = seconds % 86400;
		//转换小时数
		long hours = seconds / 3600;
		//剩余秒数
		seconds = seconds % 3600;
		//转换分钟
		long minutes = seconds / 60;
		//剩余秒数
		seconds = seconds % 60;
		String result = "";
		if(0 < days){
			result += days+"天";
		}
		if(0 < hours){
			result += hours+"小时";
		}
		if(0 < minutes){
			result += minutes+"分";
		}
		if(0 < seconds){
			result += seconds+"秒";
		}else{
			result += "钟";
		}
		return result;
	}

	/**
	 * @方法名：getYesterdayStrByDateStr
	 * @描述： 通过日期字符串得到昨天的日期
	 * @作者： kjz
	 * @日期： Created in 2020/4/20 19:37
	 */
	public static String getYesterdayStrByDateStr(String date) {
		LocalDate localDate = LocalDate.parse(date);
		return localDate.minusDays(1).toString();
	}


	/**
	 * @方法名：dateStr2Week
	 * @描述： 传入日期字符串转换为星期X
	 * @作者： kjz
	 * @日期： Created in 2020/4/27 18:02
	 */
	public static String dateStr2Week(String date,String formatter){
		DateTimeFormatter df = DateTimeFormatter.ofPattern(formatter);
		LocalDate localDate = LocalDate.parse(date,df);
		String result = "";
		switch (localDate.getDayOfWeek()){
			case MONDAY: result="星期一";break;
			case TUESDAY: result="星期二";break;
			case WEDNESDAY: result="星期三";break;
			case THURSDAY: result="星期四";break;
			case FRIDAY: result="星期五";break;
			case SATURDAY: result="星期六";break;
			case SUNDAY: result="星期日";break;
		}
		return result;
	}
}
