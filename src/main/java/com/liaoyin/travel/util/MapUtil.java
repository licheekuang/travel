package com.liaoyin.travel.util;

import java.util.*;

/**
 * @author privatePanda777@163.com
 * @title: MapUtil
 * @projectName travel
 * @description: TODO  map的Util
 * @date 2019/8/1216:08
 */
public class MapUtil {


    /**
    　* @description: TODO 求Map<K,V>中Key(键)的最小值
    　* @param [map]
    　* @return java.lang.Object
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/12 16:11
    　*/
    public static Object getMinKey(Map map) {
        if (map == null){ return null;}
        Set<Integer> set = map.keySet();
        Object[] obj = set.toArray();
        Arrays.sort(obj);
        return obj[0];
    }

    /**
    　* @description: TODO 求Map<K,V>中Value(值)的最小值
    　* @param [map]
    　* @return java.lang.Object
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/12 16:12
    　*/
    public static Object getMinValue(Map map) {
        if (map == null){ return null;}
        Collection<Integer> c = map.values();
        Object[] obj = c.toArray();
        Arrays.sort(obj);
        if (obj.length>0) {
            return obj[0];
        }else {
            return null;
        }
    }


    /**
    　* @description: TODO  根据map的值获取Key
    　* @param [map, value]
    　* @return java.lang.Object
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/8/12 16:10
    　*/
    public static Object getKey(Map map, Object value){
        List<Object> keyList = new ArrayList<>();
        for(Object key: map.keySet()){
            if(map.get(key).equals(value)){
                keyList.add(key);
            }
        }
        return keyList;
    }

    /**
     * @方法名：getKeyOrNullList
     * @描述： 传入Map<String, List<Object>>获取第一个Key
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/19 16:04
     */
    public static String getKeyOrNullList(Map<String, List<Object>> map) {
        String obj = null;
        for (Map.Entry<String, List<Object>> entry : map.entrySet()) {
            obj = entry.getKey();
            if (obj != null) {
                break;
            }
        }
        return  obj;
    }


    /**
     * @方法名：getFirstOrNullList
     * @描述： 传入Map<String, List<Object>>获取第一个value
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/19 16:05
     */
    public static Object getFirstOrNullList(Map<String, List<Object>> map) {
        Object obj = null;
        for (Map.Entry<String, List<Object>> entry : map.entrySet()) {
            obj = entry.getValue();
            if (obj != null) {
                break;
            }
        }
        return  obj;
    }

}
