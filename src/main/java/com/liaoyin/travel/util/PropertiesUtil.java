package com.liaoyin.travel.util;

import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class PropertiesUtil {
    private static Properties prop;

    private PropertiesUtil() {
    }

    public static void init(Properties prop) {
        PropertiesUtil.prop = prop;
    }

    public static String get(String key) {
        return prop.getProperty(key);
    }

    public static String get(String key, String... args) {
		String value = prop.getProperty(key);
        System.err.println("key="+key);
        System.err.println("args="+args);
        System.err.println("value="+value);
		if (null != args && args.length > 0) {
			/*for (int i = 0; i < args.length; i++) {
				value = value.replace("{" + i + "}", args[i]);
			}*/
            value = args2String(args);
		}
		return value;
	}

    /**
     * @方法名：args2String
     * @描述： 把可变参数转换为String
     * @作者： kjz
     * @日期： Created in 2020/2/27 21:10
     */
    public static String args2String(String... args) {
        String result = Arrays.toString(args);
        return result.substring(1,result.length()-1);
    }

    public static Set<Entry<Object, Object>> getKeyVal() {
        return prop.entrySet();
    }

	public static Set<String> getKeys() {
		return prop.stringPropertyNames();
	}
}
