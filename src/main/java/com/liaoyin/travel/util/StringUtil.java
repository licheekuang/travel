package com.liaoyin.travel.util;

import java.util.List;

/**
 * @项目名：
 * @作者：lijing
 * @描述：字符串工具类
 * @日期：Created in 2018/6/8 15:22
 */
public final class StringUtil {

    private StringUtil(){

    }
    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for(int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }
    /**
     * 判断字符串是否为空
     * @param str 待验证的字符串
     * @return true 为空  false 不为空
     */
    public static boolean isNotEmpty( String str ){
        return (str != null && !str.isEmpty());
    }


    public static boolean isEmpty(Object str) {
        return (str == null || str.toString().isEmpty() || "null".equalsIgnoreCase(str.toString()));
    }
    /**
     * 创建指定数量的随机字符串
     * @param numberFlag 是否是数字
     * @param length
     * @return
     */
    public static String generateSmsCode(boolean numberFlag, int length){
        String retStr = "";
        String strTable = numberFlag ? "1234567890" : "1234567890abcdefghijkmnpqrstuvwxyz";
        int len = strTable.length();
        boolean bDone = true;
        do {
            retStr = "";
            int count = 0;
            for (int i = 0; i < length; i++) {
                double dblR = Math.random() * len;
                int intR = (int) Math.floor(dblR);
                char c = strTable.charAt(intR);
                if (('0' <= c) && (c <= '9')) {
                    count++;
                }
                retStr += strTable.charAt(intR);
            }
            if (count >= 2) {
                bDone = false;
            }
        } while (bDone);
        return retStr;
    }

    /**
     * @方法名：trAry2String
     * @描述： 字符串数组转换为以,分割的字符串
     * @作者： kjz
     * @日期： Created in 2020/3/7 19:51
     */
    public static String strAry2String(String[] ary){
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < ary.length; i++){
            sb. append(ary[i].trim()+",");

        }
        return sb.toString();
    }

    /**
     * @方法名：strList2String
     * @描述： 字符串集合转String
     * @作者： kjz
     * @日期： Created in 2020/3/7 19:58
     */
    public static String strList2String(List<String> list){
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < list.size(); i++){
            sb. append(list.get(i).trim()+",");

        }
        return sb.toString();
    }
}
