package com.liaoyin.travel.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.dao.FileUploadMapper;
import com.liaoyin.travel.entity.FileUpload;
import com.liaoyin.travel.exception.BusinessException;

import java.util.List;

@Component
public class FileUploadUtil {

    /**
     * 服务器访问地址
     */
    @Value("${gate.file.server}")
    private String serverUrl;

    @Autowired
    private static FileUploadMapper fileUploadMapper;

    @Autowired
    public void setFileUploadMapper(FileUploadMapper fileUploadMapper){
        FileUploadUtil.fileUploadMapper = fileUploadMapper;
    }

    /**
     * 通过BusinessCode获取文件对象
     * @return
     */
    public static FileUpload getFileUpload(String BusinessCode){
        FileUpload fileUpload = null;
        if(StringUtils.isNoneEmpty(BusinessCode)){
            fileUpload = fileUploadMapper.selectByBusinessCode(BusinessCode);
            if(fileUpload!=null) {
                fileUpload.setUrl(CommonConstant.FILE_SERVER+fileUpload.getFilePath());
            }
        }
        return fileUpload;
    }

    /**
     * 通过BusinessCode获取文件对象列表
     * @param BusinessCode
     * @return
     */
    public static List<FileUpload> getFileUploadList(String BusinessCode){
        List<FileUpload> fileUploads = null;
        if(StringUtils.isNoneEmpty(BusinessCode)){
            fileUploads = fileUploadMapper.selectListByBusinessCode(BusinessCode);
            if(fileUploads!=null&&fileUploads.size()>0){
                for (FileUpload f:fileUploads) {
                    System.err.println("CommonConstant.FILE_SERVER="+CommonConstant.FILE_SERVER);
                    f.setUrl(CommonConstant.FILE_SERVER+f.getFilePath());
                }
            }
        }
        return fileUploads;
    }


    /**
     * 保存文件,返回BusinessCode
     * @param fileUpload
     * @return
     */
    public static String saveFileUpload(FileUpload fileUpload,String businessCode){
       // String businessCode = "";
    	if(businessCode== null) {
    		throw new BusinessException("businessId is null","业务id为空");
    	}
        if(fileUpload!=null&&StringUtils.isNotEmpty(fileUpload.getFilePath())){
            if(StringUtils.isNoneEmpty(businessCode)){
                fileUploadMapper.deleteByBusinessCode(businessCode);
            }
           // businessCode = UUIDUtil.getUUID();
            fileUpload.setBusinessId(businessCode);
            fileUpload.setId(UUIDUtil.getUUID());
            fileUploadMapper.insertSelective(fileUpload);
        }
        return businessCode;
    }
    
    
    /**
     * 保存文件,自动生成code,返回BusinessCode
     * @param fileUpload
     * @return
     */
    public static String saveFileUploadCode(FileUpload fileUpload){
        String businessCode = "";
        if(fileUpload!=null&&StringUtils.isNotEmpty(fileUpload.getFilePath())){
            if(StringUtils.isNoneEmpty(businessCode)){
                fileUploadMapper.deleteByBusinessCode(businessCode);
            }
            businessCode = UUIDUtil.getUUID();
            fileUpload.setBusinessId(businessCode);
            fileUpload.setId(UUIDUtil.getUUID());
            fileUploadMapper.insertSelective(fileUpload);
        }
        return businessCode;
    }

    /**
     * 保存文件列表,返回BusinessCode
     * @param fileUploads
     * @return
     */
    public static String saveFileUploadList(List<FileUpload> fileUploads,String businessCode){
        if(fileUploads!=null&&fileUploads.size()>0){
            //删除文件
            if(StringUtils.isNoneEmpty(businessCode)){
                fileUploadMapper.deleteByBusinessCode(businessCode);
            }else {
            	businessCode=UUIDUtil.getUUID();
            }
            //保存
            for (FileUpload f:fileUploads) {
                f.setBusinessId(businessCode);
            }
            fileUploadMapper.inseretListFileUpload(fileUploads);
        }
        return businessCode;
    }


}
