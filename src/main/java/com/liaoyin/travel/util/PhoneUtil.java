package com.liaoyin.travel.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneUtil {

	/***
	 * 
     * @方法名：isPhone
     * @描述： 判断是否是手机号
     * @作者： lijing
     * @日期： 2018年12月14日
	 */
	public static String isPhone(String phone) {
		if(phone == null) {
			return null;
		}
		String phoneNew = subStringPhone(phone);
	    String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
	    if (phoneNew.length() != 11) {
	        return null;
	    } else {
	        Pattern p = Pattern.compile(regex);
	        Matcher m = p.matcher(phoneNew);
	        boolean isMatch = m.matches();
	        if(isMatch) {
	        	return phoneNew;
	        }
	        return null;
	    }
	}
	/***
	 * 
	     * @方法名：filteringMobilePhoneNumbers
	     * @描述： 批量过滤手机号码
	     * @作者： lijing
	     * @日期： 2018年12月14日
	 */
	public static List<String> filteringMobilePhoneNumbers(List<String> phoneList){
		
		List<String> list = new ArrayList<String>();
		if(phoneList!=null && phoneList.size() >0) {
			String s = null;
			for(String phone:phoneList) {
				s=isPhone(phone);
				if(s != null && !"".equals(s)) {
					list.add(s);
					s = null;
				}
			}
		}
		return list;
	}
	
	
	/****
	 * 
	     * @方法名：subStringPhone
	     * @描述： 去除手机号码的前缀 +86
	     * @作者： lijing
	     * @日期： 2018年12月14日
	 */
	public static String subStringPhone(String phone) {
		if(phone == null) {
			return "";
		}
		String phoneNew = phone.replace(" ", "").replace("-", "").trim();		
		if(phoneNew.length() >11) {
			if(phoneNew.indexOf("+")>=0) {
				phoneNew = phone.substring(phoneNew.length()-11, phoneNew.length());
			}
		}
		return phoneNew;
	}
	
	public static void main(String[] args) {
		System.out.println(isPhone("+86 151       2328 -1943"));
	}
}
