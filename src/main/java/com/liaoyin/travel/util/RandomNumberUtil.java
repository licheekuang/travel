package com.liaoyin.travel.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 随机数工具累
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 18:08
 */
public class RandomNumberUtil {

    /**
     * @方法名：getApprovalNumber
     * @描述： 生成唯一的审批编号
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/22 18:10
     */
    public static String getApprovalNumber(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        String newDate=sdf.format(new Date());
        String result="";
        Random random=new Random();
        for(int i=0;i<3;i++){
            result+=random.nextInt(10);
        }
        return newDate+result;
    }
}
