package com.liaoyin.travel.util;

import java.math.BigDecimal;

import static java.lang.Math.sqrt;

/************
 * 通过经纬度获取距离(单位：米)
 * @author Administrator
 *
 */
public class LocationUtils {

	private static double EARTH_RADIUS = 6378.137; 
	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}
	public static double bd_lat;
	public static double bd_log;
	private static double x_PI = 3.14159265358979324 * 3000.0 / 180.0;
	private static double PI = 3.1415926535897932384626;
 
	/**
	 * 通过经纬度获取距离(单位：米)
	 * 
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return 距离
	 */
	public static double getDistance(double lat1, double lng1, double lat2,
			double lng2) {
		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double a = radLat1 - radLat2;
		double b = rad(lng1) - rad(lng2);
		double s = 2 * Math.asin(sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * EARTH_RADIUS;
		s = Math.round(s * 10000d) / 10000d;
		s = s * 1000;
		return s;
	}

	static double dataDigit(int digit,double in){
		return new BigDecimal(in).setScale(6,   BigDecimal.ROUND_HALF_UP).doubleValue();

	}


	public static void bd_encrypt(Double log,Double lat)
	{
		double x = log, y = lat;
		double z = sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_PI);
		double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x *  x_PI);
		//return new Point(dataDigit(6,z * Math.cos(theta) + 0.0065),dataDigit(6,z * Math.sin(theta) + 0.006));

		bd_lat = dataDigit(6,z * Math.sin(theta) + 0.006);
		bd_log = dataDigit(6,z * Math.cos(theta) + 0.0065);
	}


	/**
	 * 根据经纬度得到距离
	 * @param currentLongitude 当前传入的经度
	 * @param currentLatitude 当前传入的纬度
	 * @param eventLog 经度
	 * @param eventLat 纬度
	 * @return
	 */
	public static double getDisparityByLogAndLat(String currentLongitude,String currentLatitude,String eventLog,String eventLat){
		double disparity = getDistance(Double.valueOf(currentLongitude), Double.valueOf(currentLatitude),
				Double.valueOf(eventLog), Double.valueOf(eventLat));
		return Math.abs(disparity);
	}

 
	public static void main(String[] args) {
		double distance = getDistance(34.2675560000, 108.9534750000,
				34.2675560000, 8);
		System.out.println("距离" + distance + "米");
	}
}
