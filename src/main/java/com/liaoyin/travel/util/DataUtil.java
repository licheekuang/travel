package com.liaoyin.travel.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class DataUtil {

	
	/****
	 * 
	     * @方法名：getDoubleValue
	     * @描述： 保留两位小数
	     * @作者： lijing
	     * @日期： 2019年3月7日
	 */
	public static  Double getDoubleValue(double d) {
		//d=8.7656757575685676765-1;
		BigDecimal b = new BigDecimal(d);
		d = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(); 
		return d;
	}

	/**
	 * 把小数转换为百分比字符串
	 * @param d 小数
	 * @param IntegerDigits  小数点前保留几位
	 * @param FractionDigits 小数点后保留几位
	 * @return
	 */
	public static String getPercentFormat(double d,int IntegerDigits,int FractionDigits) {
		NumberFormat nf = java.text.NumberFormat.getPercentInstance();
		nf.setMaximumIntegerDigits(IntegerDigits);//小数点前保留几位
		nf.setMinimumFractionDigits(FractionDigits);// 小数点后保留几位
		String str = nf.format(d);
		return str;
	}

}
