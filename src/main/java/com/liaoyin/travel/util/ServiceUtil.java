package com.liaoyin.travel.util;

import com.liaoyin.travel.base.constant.VariableConstants;
import com.liaoyin.travel.exception.BusinessException;
import com.liaoyin.travel.vo.scenic.PageInfoParameter;

import java.util.regex.Pattern;

/**
 * 业务处理工具类
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-01 19:50
 */
public class ServiceUtil {

    /**
     * @方法名：verifyUnitTypeValidity
     * @描述： 验证单位类型的数据有效性
     * @作者： kjz
     * @日期： Created in 2020/3/1 19:57
     */
    public static void verifyUnitTypeValidity(String unitType) {
        if(!VariableConstants.STRING_CONSTANT_1.equals(unitType) && !VariableConstants.STRING_CONSTANT_2.equals(unitType)){
            throw new BusinessException("unitType.is.error");
        }
    }

    /**
     * @方法名：verifyDateByTourist
     * @描述： 验证查询游客信息的时间格式是否正确
     * @作者： kjz
     * @日期： Created in 2020/3/2 19:37
     */
    public static void verifyDateByTourist(String beginTime, int i) {
        //时间验证，考虑闰年
        String rexp1 = "((\\d{2}(([02468][048])|([13579][26]))[\\-]((((0?[13578])|(1[02]))[\\-]((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-]((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-]((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-]((((0?[13578])|(1[02]))[\\-]((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-]((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-]((0?[1-9])|(1[0-9])|(2[0-8]))))))";

        String[] date = beginTime.split("-");
        boolean result = true;
        if(date.length==3){
            if(4 == date[0].length() && date[1].length()==2 && date[2].length()==2){
                result = Pattern.matches(rexp1, beginTime);
            }else{
                result = false;
            }
        }else{
            result = false;
        }

        if(!result){
            if(1==i){
                throw new BusinessException("date.format.error","开始搜索的时间传值错误");
            }else if(2==i){
                throw new BusinessException("date.format.error","结束搜索的时间传值错误");
            }
        }
    }

    /**
     * @方法名：verifyHealthState
     * @描述： 验证健康状态传值是否正确，错误则抛出异常
     * @作者： kjz
     * @日期： Created in 2020/3/2 20:12
     */
    public static void verifyHealthState(String healthState) {
        if(!"002001".equals(healthState) && !"002002".equals(healthState)){
            throw new BusinessException("healthState.is.error");
        }
    }

    /**
     * @方法名：createPageInfo
     * @描述： 手动组装生成PageInfo
     * @作者： kjz
     * @日期： Created in 2020/3/2 22:39
     */
    public static PageInfoParameter createPageInfo(Integer total, Integer pageNum, Integer pageSize) {

        //总页数
        int pages = total/pageSize;
        if(total % pageSize > 0){
            pages += 1;
        }
        //是否有下一页
        boolean hasNextPage = pageNum < pages ? true:false;
        //是否有前一页
        boolean hasPreviousPage = pageNum == 1 ? false:true;
        //是否为第一页
        boolean isFirstPage = pageNum == 1 ? true:false;
        //是否为最后一页
        boolean isLastPage = pageNum == pages ? true:false;
        //前一页
        int prePage = pageNum == 1 ? 0 : pageNum-1;
        //下一页
        int nextPage = pageNum==pages ? 0 : pageNum+1;
        //当前页的数量
        int size = 0;
        if(total<pageSize){
            size = total;
        }else if(total>pageSize){
            if(pageNum==pages){
                size = total % pageSize;
            }else if(pageNum<pages){
                size = pageSize;
            }
        }
        //导航页码数
        int[] navigatepageNums = new int[pages];
        for (int i = 0; i < pages; i++) {
            navigatepageNums[i] = i+1;
        }

        PageInfoParameter pageInfoParameter = new PageInfoParameter()
                .setPageNum(pageNum).setPageSize(pageSize).setSize(size)
                .setTotal(total).setPages(pages).setPrePage(prePage)
                .setNextPage(nextPage).setHasPreviousPage(hasPreviousPage)
                .setHasNextPage(hasNextPage).setFirstPage(isFirstPage)
                .setLastPage(isLastPage).setNavigatepageNums(navigatepageNums)
                .setNavigateFirstPage(1).setNavigateLastPage(pages);

        return pageInfoParameter;

    }
}
