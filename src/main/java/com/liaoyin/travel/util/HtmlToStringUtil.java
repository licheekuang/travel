package com.liaoyin.travel.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlToStringUtil {

	public static String delHTMLTag(String htmlStr){ 
		
        String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
         
        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
        Matcher m_script=p_script.matcher(htmlStr); 
        htmlStr=m_script.replaceAll(""); //过滤script标签 
         
        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
        Matcher m_style=p_style.matcher(htmlStr); 
        htmlStr=m_style.replaceAll(""); //过滤style标签 
         
        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
        Matcher m_html=p_html.matcher(htmlStr); 
        htmlStr=m_html.replaceAll(""); //过滤html标签 

        return htmlStr.trim().replace("&nbsp;", ""); //返回文本字符串 
    } 

	public static void main(String[] args) {
		String html = "<p>&nbsp;&nbsp;ffffff<img src='http://upload.zsjz888.com/goodsfile/20150622/7a33f25a-cafa-4f37-ac27-df1cb263055b.jpg' alt='' /></p><img src='http://upload.zsjz888.com/goodsfile/20150622/7a33f25a-cafa-4f37-ac27-df1cb263055b.jpg' alt='' /><p>&nbsp;</p><p>考上一本，上个好大学，是无数考生和家长的愿望。那么，在考分上线的情况下，如何合理地选择志愿学校呢？本文将针对高考考生的需求，对中国100多所好大学进行一个系统的划分和介绍，帮助考生找到最适合自己的学校。目前，社会上判断好大学的标准，有两个常见的误区。</p><p>&nbsp;</p><p><strong>误区一：凭借口碑选择学校</strong></p><p>. 根据口碑选择大学只适用于极少数的情况，因为除了清华、北大等极少数公认的顶级大学，绝大部分学校都不具备超凡脱俗的实力，所谓的口碑实际上是来自于部分人群的更熟悉和更了解。人们由于自身经历和生活中接受到的一些零散的信息，对有些大学比较熟悉以及产生了一些或好或坏的印象，而对另外99%的学校则是完全陌生的，在这样的基础上形成的意见具有很强的片面性和主观性。即便是高等教育领域的工作者，也都会受限于个人的经历和视野，很难对大部分学校的情况做出全面的判断。依靠口碑推荐的方式来选择学校，具有很强的不确定性，风险也很高。</p>";
		  System.out.println(delHTMLTag(html));
	}
}
