package com.liaoyin.travel.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 调用第三方接口工具类
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-02 21:35
 */
public class RestTemplateUtil {
    private static class SingletonRestTemplate {
        static final RestTemplate INSTANCE = new RestTemplate();
    }

    private RestTemplateUtil() {
    }


    public static RestTemplate getInstance() {
        return SingletonRestTemplate.INSTANCE;
    }

    /**
     * post 请求
     * @param url 请求路径
     * @param data body数据
     * @param token JWT所需的Token，不需要的可去掉
     * @return
     */
    public static String post(String url, String data, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Encoding", "UTF-8");
        headers.add("Content-Type", "application/json; charset=UTF-8");
        if (token != null) {
            headers.add("Authorization", token);
        }
        HttpEntity<String> requestEntity = new HttpEntity<>(data, headers);
        return RestTemplateUtil.getInstance().postForObject(url, requestEntity, String.class);
    }

    /**
     * get 请求
     * @param url 请求路径
     * @param token JWT所需的Token，不需要的可去掉
     * @return
     */
    public static  String get(String url, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Encoding", "UTF-8");
        headers.add("Content-Type", "application/json; charset=UTF-8");
        if (token != null) {
            headers.add("Authorization", token);
        }
        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = RestTemplateUtil.getInstance().exchange(url, HttpMethod.GET, requestEntity, String.class);
        String responseBody = response.getBody();
        return responseBody;
    }

    /**
     * 发送get请求获取到结果
     * @param url
     * @param params
     * @return
     */
    public static String getRequest(String url, Map<String, String> params){
        List<String> paramsList = new ArrayList<>();
        if(params!=null && !params.isEmpty()){
            for (Map.Entry<String, String> entry : params.entrySet()) {
                paramsList.add(entry.getKey()+"="+entry.getValue());
            }
            for (int i = 0; i < paramsList.size(); i++) {
                if(i==0){
                    url += "?"+ paramsList.get(i);
                }else{
                    url += "&" + paramsList.get(i);
                }
            }
        }
        System.err.println("url="+url);
        return get(url,null);
    }
}


