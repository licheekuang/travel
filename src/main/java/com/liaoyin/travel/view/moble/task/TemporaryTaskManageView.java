package com.liaoyin.travel.view.moble.task;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 临时任务管理显示(已废弃)
 *
 * @author kuang.jiazhuo
 * @date 2019-12-04 15:20
 */
@Data
public class TemporaryTaskManageView {

    @ApiModelProperty("任务id")
    private String id;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务发布人id")
    private String issuerID;

    @ApiModelProperty("任务领取人id(以','隔开)")
    private String receiverIds;

    @ApiModelProperty("任务领取人名字(以','隔开)")
    private String receivers;

    @ApiModelProperty("任务类型【字典 taskType】")
    private Short taskType;

    @ApiModelProperty("任务类型Display")
    public String getTaskTypeDisplay(){
       return taskType!=null? DictUtil.getDisplay("taskType",taskType.toString()):null;
    }

    @ApiModelProperty("任务级别【字典 taskLevel】")
    private Short taskLevel;

    @ApiModelProperty("任务级别Display")
    public String getTaskLevelDisplay(){
        return taskLevel!=null? DictUtil.getDisplay("taskLevel",taskLevel.toString()):null;
    }

    @ApiModelProperty("任务类别【字典 taskCategories】")
    private Short taskCategories;

    @ApiModelProperty("任务类别Display")
    public String getTaskCategoriesDisplay(){
        return taskCategories!=null? DictUtil.getDisplay("taskCategories",taskCategories.toString()):null;
    }

    @ApiModelProperty("任务说明")
    private String taskStatement;

    @ApiModelProperty("线路id")
    private String lineId;

    @ApiModelProperty("普通任务的经度")
    private String taskLog;

    @ApiModelProperty("普通任务的纬度")
    private String taskLat;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("任务截止时间")
    private LocalDateTime taskEndTime;

    @ApiModelProperty("对应事件ID，针对灾害发布事件任务时必填")
    private String eventId;

    @ApiModelProperty("用于临时任务管理的关联(定时任务不传此参数)")
    private String taskManagerId;

    @ApiModelProperty("发布方式(1:定时发布;2:临时发布)")
    private String releaseWay;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("发布时间")
    private LocalDateTime creatTime;

}
