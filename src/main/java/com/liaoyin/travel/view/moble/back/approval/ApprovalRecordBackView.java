package com.liaoyin.travel.view.moble.back.approval;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 后台查询审批列表返回类
 *
 * @author kuang.jiazhuo
 * @date 2019-11-26 09:19
 */
@Data
@Accessors(chain = true)
public class ApprovalRecordBackView {

    private String id;

    @ApiModelProperty("审批编号")
    private String approvalNumber;

    @ApiModelProperty("审批标题")
    private String approvalTitle;

    @ApiModelProperty("审批类型 Display")
    private String approvalTypeDiplay;

    @ApiModelProperty("审批发起人姓名")
    private String initiatorName;

    @ApiModelProperty("审批的发起时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime initiationTime;

    @ApiModelProperty("审批的完成时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;

    @ApiModelProperty("审批的结果 Display")
    private String approvalStatusDisplay;

    @ApiModelProperty("审批耗时")
    private String approvalConsuming;

    @ApiModelProperty("审批人姓名")
    private String approverNmae;

    @ApiModelProperty("创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

}
