package com.liaoyin.travel.view.moble.attendance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 打卡设置信息
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 14:28
 */
@Data
public class ClockSettingsView {

    @ApiModelProperty("当前员工要打卡的经度")
    private String longitude;

    @ApiModelProperty("当前员工要打卡的经度")
    private String latitude;

    @ApiModelProperty("考勤范围(允许偏差的距离)")
    private Double attendanceRange;
}
