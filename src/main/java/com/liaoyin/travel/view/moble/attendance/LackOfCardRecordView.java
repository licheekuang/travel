package com.liaoyin.travel.view.moble.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 缺卡记录
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 23:35
 */
@Data
@Accessors(chain = true)
public class LackOfCardRecordView {

    @ApiModelProperty("缺卡班次显示")
    private String lackOfCardDivisions;

    @ApiModelProperty("缺卡id")
    private String lackOfCardId;

    @ApiModelProperty("缺卡班次标识(1:上班;2:下班)")
    private String shift;

    @ApiModelProperty("缺卡时间Display")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime clockTimeDisplay;

    @ApiModelProperty("缺卡的时间")
    private String clockTime;
}
