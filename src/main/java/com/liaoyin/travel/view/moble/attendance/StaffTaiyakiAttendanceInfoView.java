package com.liaoyin.travel.view.moble.attendance;

import com.liaoyin.travel.entity.attendance.AttendanceRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 员工当天的考勤情况
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-21 23:40
 */
@Data
public class StaffTaiyakiAttendanceInfoView {

    @ApiModelProperty("打卡次数")
    private Integer clockFrequency;

    @ApiModelProperty("当天打卡明细信息")
    private AttendanceRecord attendanceRecord;

    @ApiModelProperty("考勤打卡的设置信息")
    private ClockSettingsView clockSettingsView;
}
