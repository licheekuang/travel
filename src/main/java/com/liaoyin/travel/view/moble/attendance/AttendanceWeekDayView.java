package com.liaoyin.travel.view.moble.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 员工查询自己的考勤工作日
 *
 * @author kuang.jiazhuo
 * @date 2019-11-19 09:46
 */
@Data
@ApiModel(value="考勤设置的工作日")
public class AttendanceWeekDayView {

    @ApiModelProperty("星期X【1、2、3、4、5、6、7】")
    private Integer weekDay;

    @ApiModelProperty("星期X Display")
    public String getWeekDayDisplay(){
        String weekDayStr = "";
        switch (weekDay) {
            case 1: weekDayStr = "周一";break;
            case 2: weekDayStr = "周二";break;
            case 3: weekDayStr = "周三";break;
            case 4: weekDayStr = "周四";break;
            case 5: weekDayStr = "周五";break;
            case 6: weekDayStr = "周六";break;
            case 7: weekDayStr = "周日";break;
            default : break;
        }
        return weekDayStr;
    }

    @ApiModelProperty("是否考勤工作日")
    private boolean isAttendance;

    @ApiModelProperty("是否考勤工作日 Display")
    public String getIsAttendanceDisplay(){
        if(isAttendance){
            return "考勤日";
        }
        return "休息日";
    }
}
