package com.liaoyin.travel.view.moble.task;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import com.liaoyin.travel.util.StringUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;



import java.util.Date;
import java.util.List;

/**
 * 定时任务后台列表参数显示
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-28 15:25
 */
@Data
public class TimedTaskBackView {

    @ApiModelProperty("定时任务参数id")
    private String timedTaskId;

    @ApiModelProperty("业务id【可关联 用户 部门 工种 的id】")
    private String businessId;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务类型【字典 taskType】")
    private Short taskType;

    @ApiModelProperty(value = "任务类型Display")
    public String getTaskTypeDisplay() {
        if (this.taskType != null) {
            return DictUtil.getDisplay("taskType", this.taskType.toString());
        }
        return null;
    }

    @ApiModelProperty("任务级别【字典 taskLevel】")
    private Short taskLevel;

    @ApiModelProperty(value = "任务级别Display")
    public String getTaskLevelDisplay() {
        if (this.taskType != null) {
            return DictUtil.getDisplay("taskLevel", this.taskLevel.toString());
        }
        return null;
    }

    @ApiModelProperty("任务类别【字典 taskCategories】")
    @Column(name = "task_categories")
    private Short taskCategories;

    @ApiModelProperty(value = "任务类别Display")
    public String getTaskCategoriesDisplay() {
        if (this.taskCategories != null) {
            return DictUtil.getDisplay("taskCategories", this.taskCategories.toString());
        }
        return null;
    }

    @ApiModelProperty("启用状态(0:不启用;1:启用)")
    private String enabled;

    @ApiModelProperty("启用状态Display")
    public String getEnabledDisplay(){
        if(enabled!=null){
            return enabled.equals("0")?"禁用":"启用";
        }
        return null;
    }

    @ApiModelProperty("任务说明")
    private String taskStatement;

    @ApiModelProperty("任务发布人id")
    private String userId;

    @ApiModelProperty("任务发布人")
    private String publisher;

    @ApiModelProperty("定时任务创建时间")
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;

    @ApiModelProperty("是否指定人员【1：指定人员 2：指定部门 3：指定工种】")
    private Short isSpecify;

    @ApiModelProperty("线路id")
    private String lineId;

    @ApiModelProperty("纬度")
    private String taskLat;

    @ApiModelProperty("经度")
    private String taskLog;

    @ApiModelProperty("任务截止时间(我不知道要不要这个字段)")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date taskEndTime;

    @ApiModelProperty("定时任务发布的时间(HH:mm)")
    private String pubTime;

    @ApiModelProperty("星期Str")
    private String pubWeekStr;

    @ApiModelProperty("定时任务选择的星期X")
    private List<RegularWeekView> regularWeekViewList;

    @ApiModelProperty("头像id")
    private String headPicId;

    @ApiModelProperty("头像的url地址")
    private String headPicUrl;

    @ApiModelProperty("任务接收人列表")
    private String receivers;

    @ApiModelProperty("发布类型")
    private String publishType;

    @ApiModelProperty("发布时间")
    public String getPublishTime(){
        if(StringUtil.isEmpty(this.pubTime)){
            return null;
        }
        return this.pubTime;
    }

}
