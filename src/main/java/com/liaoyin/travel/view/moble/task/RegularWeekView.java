package com.liaoyin.travel.view.moble.task;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 定时任务的星期状态
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-28 15:50
 */
@Data
@ApiModel(value="定时任务的星期状态")
public class RegularWeekView {

    @ApiModelProperty("星期X")
    private String weekDay;

    @ApiModelProperty("选择状态(0:未选择;1:已选择)")
    private String selectionState;
}
