package com.liaoyin.travel.view.moble.approval;

import com.liaoyin.travel.view.user.UserView;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 后台-审批可见范围
 *
 * @author kuang.jiazhuo
 * @date 2019-11-23 15:41
 */
@Data
@Accessors(chain = true)
public class ApprovalIsVisibleBackView {

    @ApiModelProperty("审批可见范围的id")
    private String approvalVisualRangeId;

    @ApiModelProperty("审批类型(1:补卡;2:请假;3:外出)")
    private String approvalType;

    @ApiModelProperty("审批类型 Display")
    private String approvalTypeDisplay;

    @ApiModelProperty("可见范围(1:全部可见;2:部分可见)")
    private String isVisible;

    @ApiModelProperty("可见范围 Display")
    private String isVisibleDisplay;

    @ApiModelProperty("是否启用(0:否;1:是)")
    private String isUsing;

    @ApiModelProperty("部分可见时的可见人员信息")
    private List<UserView> visibleUsers;

    @ApiModelProperty("是否启用 Display")
    public String getIsUsingDisplay(){
        if("1".equals(isUsing)){
            return "启用";
        }
        return "禁用";
    }
}
