package com.liaoyin.travel.view.moble.approval;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.view.user.UserView;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 审批分组详情
 *
 * @author kuang.jiazhuo
 * @date 2019-11-25 17:33
 */
@Data
@Accessors(chain = true)
public class PacketRecordDetailsView {

    @ApiModelProperty("分组名称")
    private String packetName;

    @ApiModelProperty("审批人")
    private UserView approver;

    @ApiModelProperty("申请人")
    private List<UserView> applicantList;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
}
