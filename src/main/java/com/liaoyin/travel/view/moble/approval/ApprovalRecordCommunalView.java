package com.liaoyin.travel.view.moble.approval;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 审批列表显示类(员工端管理端通用)
 *
 * @author kuang.jiazhuo
 * @date 2019-11-22 22:37
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApprovalRecordCommunalView {

    @ApiModelProperty("ID")
    private String approvalRecordId;

    @ApiModelProperty("审批类型(1:补卡,2:请假,3:外出 )")
    private String approvalType;

    @ApiModelProperty("审批类型 Display")
    public String getApprovalTypeDisplay(){
        if(approvalType!=null){
            return DictUtil.getDisplay("approvalType",approvalType);
        }
        return null;
    }

    @ApiModelProperty("审批编号")
    private String approvalNumber;

    @ApiModelProperty("审批标题")
    private String approvalTitle;

    @ApiModelProperty("创建时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("审批耗时")
    private String approvalConsuming;

    @ApiModelProperty("审批状态")
    private String approvalStatus;

    @ApiModelProperty("审批状态 Display")
    public String getApprovalStatusDisplay(){
        if(approvalStatus!=null){
            return DictUtil.getDisplay("approvalStatus",approvalStatus);
        }
        return null;
    }


    /*********************************************  请假 ********************************************************/

    @ApiModelProperty("请假开始时间")
    private String leaveStartTimeAndroid;

    @ApiModelProperty("请假结束时间")
    private String leaveEndTimeAndroid;

    /*********************************************  外出 ********************************************************/

    @ApiModelProperty("外出开始时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;

    @ApiModelProperty("外出结束时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endOfOuting;

    @ApiModelProperty("外出时长")
    private String goOutTime;

    /*********************************************  补卡 ********************************************************/

    @ApiModelProperty("补卡事由")
    private String supplementaryCardCause;

    @ApiModelProperty("需要补卡时间显示")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private String  clockTimeDisplay;

}
