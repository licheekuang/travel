package com.liaoyin.travel.view.moble.approval;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.view.user.UserView;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 审批详情
 *
 * @author kuang.jiazhuo
 * @date 2019-11-23 17:05
 */
@Data
@Accessors(chain = true)
public class ApprovalDetailsView {

    /**************************   审批公用参数   ******************************/

    @ApiModelProperty("ID")
    private String approvalRecordId;

    @ApiModelProperty("审批编号")
    private String approvalNumber;

    @ApiModelProperty("审批标题")
    private String approvalTitle;

    @ApiModelProperty("审批类型")
    private String approvalType;

    @ApiModelProperty("审批类型 Display")
    public String getApprovalTypeDisplay(){
        if(approvalType!=null){
            return DictUtil.getDisplay("approvalType",approvalStatus);
        }
        return null;
    }

    @ApiModelProperty("审批发起人")
    private UserView initiator;

    @ApiModelProperty("审批发起人的id")
    private String initiatorId;

    @ApiModelProperty("审批处理人")
    private UserView approver;

    @ApiModelProperty("审批处理人的id")
    private String approverId;

    @ApiModelProperty("审批的发起时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime initiationTime;

    @ApiModelProperty("审批的完成时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;

    @ApiModelProperty("审批的结果(状态)")
    private String approvalStatus;

    @ApiModelProperty("审批的结果(状态) Display")
    public String getApprovalStatusDisplay(){
        if(approvalStatus!=null){
            return DictUtil.getDisplay("approvalStatus",approvalStatus);
        }
        return null;
    }

    @ApiModelProperty("审批耗时")
    private String approvalConsuming;


    @ApiModelProperty("图片地址")
    private String imgUrl;

    @ApiModelProperty("事由")
    private String reasons;

    @ApiModelProperty("时长(请假和外出有此字段)")
    private String duration;

    /**************************   补卡   ******************************/

    @ApiModelProperty("补卡班次显示")
    private String lackOfCardDivisions;

    @ApiModelProperty("缺卡时间显示")
    private String  clockTimeDisplay;

    @ApiModelProperty("缺卡班次标识(1:上班;2:下班)")
    private String shift;

    @ApiModelProperty("缺卡id(考勤记录表id)")
    private String attendanceRecordId;

   /**************************   请假   ******************************/

    @ApiModelProperty("请假开始时间String")
    private String leaveStartTimeAndroid;

    @ApiModelProperty("请假结束时间String")
    private String leaveEndTimeAndroid;

    @ApiModelProperty("请假附件的Url")
    private String accessoryUrl;

    /**************************   外出   ******************************/

    @ApiModelProperty("外出开始时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;

    @ApiModelProperty("外出结束时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endOfOuting;

    @Column(name = "car_fare")
    @ApiModelProperty("交通费金额")
    private BigDecimal carFare;



}
