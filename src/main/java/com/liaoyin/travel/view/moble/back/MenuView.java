package com.liaoyin.travel.view.moble.back;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @program: travel
 * @Description:
 * @author: rzy
 * @create: 2018-11-29 09:52
 **/
@Getter
@Setter
@ApiModel("权限菜单")
public class MenuView {
    @ApiModelProperty("菜单编码，与页面DOM元素的MENUMODEL属性进行匹配")
    private String id;
    @ApiModelProperty("菜单编码，与页面DOM元素的MENUMODEL属性进行匹配")
    private String code;
    private String type;
    @ApiModelProperty("菜单路径")
    private String url;
    @ApiModelProperty("菜单名称")
    private String name;
    @ApiModelProperty("父级菜单ID")
    private String parentId;
    @ApiModelProperty("菜单层级")
    private String level;
    @ApiModelProperty("菜单ICON")
    private String icon;
    @ApiModelProperty("背景色")
    private String background;
    @ApiModelProperty("是否添加菜单(1是,0否)")
    private String isAdd;
    public String getType(){
        return "menu";
    }

}
