package com.liaoyin.travel.view.moble.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalTime;
import java.util.List;

/**
 * 当前员工的考勤设置 view
 * 陈鹏做外出申请和请假申请需要做判断的参数
 * @author Kuang.JiaZhuo
 * @date 2019-11-23 23:05
 */
@Data
@Accessors(chain = true)
public class AttendanceSetView {

    @ApiModelProperty("上班打卡时间")
    @JSONField(format="HH:mm")
    private LocalTime clockInTime;

    @ApiModelProperty("下班打卡时间")
    @JSONField(format="HH:mm")
    private LocalTime clockOutTime;

    @ApiModelProperty("当前员工的考勤工作日设置")
    private List<AttendanceWeekDayView> weekDays;
}
