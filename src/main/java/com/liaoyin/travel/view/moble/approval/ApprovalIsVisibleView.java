package com.liaoyin.travel.view.moble.approval;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 审批可见范围
 *
 * @author kuang.jiazhuo
 * @date 2019-11-23 12:05
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("审批可见范围")
public class ApprovalIsVisibleView {

    @ApiModelProperty("审批类型(1:补卡;2:请假;3:外出)")
    private String approvalType;

    @ApiModelProperty("是否可见")
    private boolean isVisible;
}
