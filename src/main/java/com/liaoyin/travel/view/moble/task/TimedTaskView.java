package com.liaoyin.travel.view.moble.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

/**
 * 定时任务View
 *
 * @author Kuang.JiaZhuo
 * @date 2019-10-27 17:10
 */
@Data
public class TimedTaskView {

    @ApiModelProperty("定时参数id")
    private String timedTaskId;

    @ApiModelProperty("业务id【可关联 用户 部门 工种 的id】")
    private String businessId;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务类型【字典 taskType】")
    private Short taskType;

    @ApiModelProperty("任务级别【字典 taskLevel】")
    private Short taskLevel;

    @ApiModelProperty("任务类别【字典 taskCategories】")
    private Short taskCategories;

    @ApiModelProperty("任务说明")
    private String taskStatement;

    @ApiModelProperty("任务发布人id")
    private String userId;

    @ApiModelProperty("任务发布人")
    private String publisher;

    @ApiModelProperty("是否指定人员【0：所有人  1：指定人员 2：指定部门 3：指定工种】")
    private Short isSpecify;

    @ApiModelProperty("线路id")
    private String lineId;

    @ApiModelProperty("纬度")
    private String taskLat;

    @ApiModelProperty("经度")
    private String taskLog;

    private Date taskEndTime;

    @ApiModelProperty("发布的星期")
    private String pubWeek;

    @ApiModelProperty("发布的时间")
    private String pubTime;

    private Short publishType;

    @ApiModelProperty("单位id")
    private String scenicId;
}
