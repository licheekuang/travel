package com.liaoyin.travel.view.travelMobileControlSystem;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 考勤信息
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-31 10:57
 */
@Data
@Accessors(chain = true)
public class AttendanceView {

    /**
     * 考勤管理表id
     */
    @JSONField(serialize = false)
    private String attendanceId;

    /**
     * 该考勤信息命名(考勤名称)
     */
    private String name;

    /**
     * 对应的考勤机，即打卡点(设备名称)
     */
    private String machine;

    /**
     * 距离考勤机的有效打卡范围，半径
     */
    private String scope;

    /**
     * 上班打卡时间设置
     */
    private String attendanceTime;

    /**
     * 下班打卡时间设置
     */
    private String leaveTime;

    /**
     * 该考勤信息是否启用
     */
    private String isUse;

    /**
     * 在该考勤组下的人员
     */
    private String peoples;

    /**
     * 选择上班时间，具体可选择：周一至周日
     */
    private String workeday;
}
