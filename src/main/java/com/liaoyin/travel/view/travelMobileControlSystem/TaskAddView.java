package com.liaoyin.travel.view.travelMobileControlSystem;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 生成任务计划
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-31 22:22
 */
@Data
@Accessors(chain = true)
public class TaskAddView {

    /**
     * 定时任务参数的id
     */
    @JSONField(serialize = false)
    private String id;

    /**
     * 业务id【可关联 用户 部门 工种 的id】
     */
    @JSONField(serialize = false)
    private String businessId;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务类型：可选择普通任务和巡更任务
     */
    private String type;

    /**
     * 任务优先级，可设置正常或者紧急。紧急则需优先完成后才能执行其他任务
     */
    private String level;

    /**
     * 任务类别
     */
    private String category;

    /**
     * 临时任务 定时发布的时间
     */
    private String pubTime;

    /**
     * 是否开启定时任务
     */
    private String isOpen;

    /**
     * 定时发布任务的执行时间
     */
    private String runTime;

    /**
     * 选择指定执行任务的类型，可选择人员、部门、工种
     */
    private String assignType;

    /**
     * 任务指定选择人员后，指定执行人员
     */
    private String assignPeople;

    /**
     * 任务指定选择部门后，指定执行部门
     */
    private String department;

    /**
     * 任务指定选择工种后，指定执行工种
     */
    private String workerType;

    /**
     * 执行任务的地理信息
     */
    private String location;

    /**
     * 任务详细信息说明
     */
    private String description;

    /**
     * 选择巡更任务后，指定执行人员的任务路线
     */
    private String taskPath;

    /**
     * 定时任务发布的星期
     */
    private String runWeek;
}
