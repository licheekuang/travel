package com.liaoyin.travel.view.travelMobileControlSystem;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 员工信息
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-31 16:08
 */
@Data
public class StaffInfoView {

    /**
     * 用户id
     */
    @JSONField(serialize = false)
    private String userId;

    /**
     * 用户类型
     */
    @JSONField(serialize = false)
    private String uDT;

    /**
     * 部门id
     */
    @JSONField(serialize = false)
    private String departId;

    /**
     * 用户类型分为员工端和管理端
     */
    private String userType;

    /**
     * 这是该账户是否投入使用的标识
     */
    private String isUse;

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 员工手机号，同事也是员工端的登录账号
     */
    private String phone;

    /**
     * 员工工号
     */
    private String empno;

    /**
     * 性别
     */
    private String sex;

    /**
     * 员工身份证
     */
    private String idCard;

    /**
     * 员工端为员工、管理端为部门经理
     */
    private String duty;

    /**
     * 选择该员工分属的景区的部门
     */
    private String department;

    /**
     * 该员工的工种选择
     */
    private String workerType;

    /**
     * 该员工的邮箱地址
     */
    private String email;

}
