package com.liaoyin.travel.view.ApiExternal;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author privatePanda777@163.com
 * @title: ApiExternalAttendance
 * @projectName travel
 * @description: TODO
 * @date 2019/8/1211:54
 */
@Data
@ApiModel("外部接口的考勤")
public class ApiExternalAttendance {

    @ApiModelProperty("打卡时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone="GMT+8")
    private Long detailedTime;

    @ApiModelProperty("用户昵称")
    private String usersNickName;

    @ApiModelProperty("类别")
    private String workType;

    @ApiModelProperty("考勤日期")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone="GMT+8")
    private Long checkTime;

}
