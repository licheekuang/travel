package com.liaoyin.travel.view.ApiExternal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author loong
 * @title: ApiExternalTaskData
 * @projectName travel
 * @description: TODO
 * @date 2019/8/1917:25
 */
@Data
@ApiModel("任务数据看板的View")
public class ApiExternalTaskData {

    @ApiModelProperty("月份")
    private String date;

    @ApiModelProperty("普通任务数")
    private String ordinaryTask;

    @ApiModelProperty("巡更任务数")
    private String patrolTask;

}
