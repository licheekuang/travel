package com.liaoyin.travel.view.ApiExternal;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author privatePanda777@163.com
 * @title: ApiExternalTask
 * @projectName travel
 * @description: TODO   外部接口的任务
 * @date 2019/8/1211:51
 */
@Data
@ApiModel("外部接口的任务")
public class ApiExternalTask {

    @ApiModelProperty("任务ID")
    private String taskId;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务类型 2.普通任务  3.巡更任务")
    private Short taskType;

    @ApiModelProperty("任务级别 1.正常 2.紧急")
    private Short taskLevel;

    @ApiModelProperty("任务类别 1.常规任务  2.临时任务")
    private Short taskCategories;

    @ApiModelProperty("任务说明")
    private String taskStatement;

    @ApiModelProperty("任务发布时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone="GMT+8")
    private Long creatTime;

    @ApiModelProperty("任务状态 1 未开始 2 进行中 3 已撤销 4 任务暂停 5已完成")
    private Short taskStatus;

    @ApiModelProperty("发布人昵称")
    private String nickName;
}
