package com.liaoyin.travel.view.ApiExternal;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author loong
 * @title: ApiExternalEventData
 * @projectName travel
 * @description: TODO    事件统计数据的vo
 * @date 2019/8/1910:24
 */
@Data
@ApiModel("事件统计数据")
public class ApiExternalEventData {

    @ApiModelProperty("时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone="GMT+8")
    private String date;

    @ApiModelProperty("事件总数")
    private String totalEventNum;

    @ApiModelProperty("不良事件数")
    private String badEvnetNum;

    @ApiModelProperty("不良事件数")
    private String alarmEventNum;
}
