package com.liaoyin.travel.view.ApiExternal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author privatePanda777@163.com
 * @title: ApiExternalUser
 * @projectName travel
 * @description: TODO  外部接口指派人的实体类
 * @date 2019/8/1216:06
 */
@Data
@ApiModel("指派人的实体")
public class ApiExternalUser {

    @ApiModelProperty("指派人id")
    private String id;

    @ApiModelProperty("指派人昵称")
    private String nickName;

    @ApiModelProperty("所属部门名称")
    private String teamName;

    @ApiModelProperty("所属工种名称")
    private String workerName;

    @ApiModelProperty("账户名")
    private String account;

    @ApiModelProperty("任务ID")
    private String taskId;

}
