package com.liaoyin.travel.view.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 部门员工显示类
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-13 17:27
 */
@Data
public class UserTeamView {

    @ApiModelProperty("部门id")
    private String teamId;

    @ApiModelProperty("部门名称")
    private String teamName;

    @ApiModelProperty("部门下的所有员工")
    private List<UserView> userViewList;

}
