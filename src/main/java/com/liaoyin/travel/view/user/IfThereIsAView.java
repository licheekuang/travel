package com.liaoyin.travel.view.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 判断人员,部门,工种是否存在
 *
 * @author Kuang.JiaZhuo
 * @date 2020-04-21 20:23
 */
@Data
@Accessors(chain = true)
public class IfThereIsAView {

    @ApiModelProperty("是否存在")
    private boolean isExist;

    @ApiModelProperty("不存在的原因(1:不存在;2:已禁用)")
    private String notExistCause;
}
