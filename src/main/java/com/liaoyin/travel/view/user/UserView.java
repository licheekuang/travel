package com.liaoyin.travel.view.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 员工信息显示类
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-13 17:29
 */
@Data
public class UserView {

    @ApiModelProperty("员工id")
    private String userId;

    @ApiModelProperty("员工昵称")
    private String nickName;

    @ApiModelProperty("员工头像地址")
    private String headPicUrl;

    @ApiModelProperty("担任职务")
    private String holdOffice;

    @ApiModelProperty("部门名称")
    private String teamName;
}
