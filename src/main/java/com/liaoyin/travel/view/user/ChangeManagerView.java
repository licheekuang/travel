package com.liaoyin.travel.view.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 换部门经理是否弹出弹框
 *
 * @author kuang.jiazhuo
 * @date 2019-12-03 14:18
 */
@Data
@Accessors(chain = true)
public class ChangeManagerView {

    @ApiModelProperty("是否弹框(是:true;否:false)")
    private boolean popout;

    @ApiModelProperty("弹框提示")
    private String popoutHint;

    @ApiModelProperty("原来的部门经理的id")
    private String originalDirectorId;
}
