package com.liaoyin.travel.view.user;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 分别返回各级的员工列表
 *
 * @author Kuang.JiaZhuo
 * @date 2020-04-08 17:30
 */
@Data
@Accessors(chain = true)
public class ScaleUserView {

    /**
     * 一级部门员工
     */
    private ClassificationInfo firstOrderUsers;

    /**
     * 二级部门员工
     */
    private List<ClassificationInfo> twoOrderUsers;

    /**
     * 三级部门员工
     */
    private List<ClassificationInfo> threeOrderUsers;
}
