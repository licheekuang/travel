package com.liaoyin.travel.view.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 部门员工显示类(后台管理系统特供)
 *
 * @author Kuang.JiaZhuo
 * @date 2019-11-13 17:27
 */
@Data
public class UserTeamBackView {

    @ApiModelProperty("部门id")
    private String teamId;

    @ApiModelProperty("部门名称")
    private String name;

    @ApiModelProperty("部门下的所有员工")
    private List<UserBackView> children;

}
