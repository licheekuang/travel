package com.liaoyin.travel.view.user;

import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.team.Team;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 员工部门分级信息
 *
 * @author Kuang.JiaZhuo
 * @date 2020-04-14 11:40
 */
@Data
@Accessors(chain = true)
public class ClassificationInfo {

    /**
     * 部门信息
     */
    private Team team;

    /**
     * 部门员工
     */
    private List<Users> departmentalStaffs;
}
