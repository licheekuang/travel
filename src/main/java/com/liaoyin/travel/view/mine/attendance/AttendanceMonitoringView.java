package com.liaoyin.travel.view.mine.attendance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 考勤监控数据
 *
 * @author Kuang.JiaZhuo
 * @date 2020-04-20 16:00
 */
@Data
@Accessors(chain = true)
public class AttendanceMonitoringView {

    @ApiModelProperty("考勤率")
    private double attendanceRate;

    @ApiModelProperty("考勤异常用户信息")
    private List<AbnormalUserView> abnormalUserList;
}
