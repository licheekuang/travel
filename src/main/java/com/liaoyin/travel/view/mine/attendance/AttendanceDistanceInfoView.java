package com.liaoyin.travel.view.mine.attendance;

import lombok.Data;

/**
 * 考勤管理的距离信息
 *
 * @author kuang.jiazhuo
 * @date 2019-11-19 19:50
 */
@Data
public class AttendanceDistanceInfoView {

    /**
     * 考勤管理id
     */
    private String attendanceId;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 经度
     */
    private String log;

    /**
     * 考勤范围(允许偏差的距离)
     */
    private Double attendanceRange;
}
