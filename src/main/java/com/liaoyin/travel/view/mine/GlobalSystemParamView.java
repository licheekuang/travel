package com.liaoyin.travel.view.mine;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.constant.CommonConstant;
import com.liaoyin.travel.entity.FileUpload;

@ApiModel("全局参数")
public class GlobalSystemParamView {

    @ApiModelProperty("id")
    @Getter
    @Setter
    private String id;
    @ApiModelProperty("参数名")
    @Getter
    @Setter
    private String paramName;
    @ApiModelProperty("参数值")
    @Getter
    @Setter
    private String paramValue;
    @ApiModelProperty("条件")
    @Getter
    @Setter
    private String conditionParam;
    
    @ApiModelProperty("创建时间")
    @Getter
    @Setter
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    
    @ApiModelProperty("参数描述")
    @Getter
    @Setter
    private String paramDesc;
    
    @ApiModelProperty("图片列表")
    @Setter
    private List<FileUpload> fileUploadList=new ArrayList<>();


    @ApiModelProperty("启动页图片")
    @Setter
    private FileUpload fileUpload;

    public List<FileUpload> getFileUploadList() {
        if(!fileUploadList.isEmpty()){
            for (FileUpload fileUpload :
                    this.fileUploadList) {
                fileUpload.setUrl(CommonConstant.FILE_SERVER + fileUpload.getFilePath());
            }
            return fileUploadList;
        }
        return null;
    }

    public FileUpload getFileUpload() {
        if(this.fileUpload!=null){
            fileUpload.setUrl(fileUpload.getFilePath());
        }
        return fileUpload;
    }
}

