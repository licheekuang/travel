package com.liaoyin.travel.view.mine.attendance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AttendanceStatisticsView {

	@ApiModelProperty("类目")
	private String displayName;
	
	@ApiModelProperty("次数")
	private Integer num;
	
	@ApiModelProperty("类别")
	private String workType;
}
