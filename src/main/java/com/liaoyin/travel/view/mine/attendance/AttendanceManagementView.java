package com.liaoyin.travel.view.mine.attendance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalTime;

/**
 * 考勤管理后台返回数据
 *
 * @author kuang.jiazhuo
 * @date 2019-11-18 17:47
 */
@Data
@ApiModel("考勤管理后台管理系统显示数据")
public class AttendanceManagementView {

    @ApiModelProperty("考勤管理表id")
    private String attendanceId;

    @ApiModelProperty("考勤名称")
    private String checkingInName;

    @ApiModelProperty("考勤点(设备名称)")
    private String equipmentName;

    @ApiModelProperty("考勤范围(允许偏差的距离)")
    private Double attendanceRange;

    @ApiModelProperty("考勤开始时间")
    private LocalTime attendanceStartTime;

    @ApiModelProperty("考勤结束时间")
    private LocalTime attendanceEndTime;

    @ApiModelProperty("是否启用(0:否;1:是)")
    private String isUsing;


}
