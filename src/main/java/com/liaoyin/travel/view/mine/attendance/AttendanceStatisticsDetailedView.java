package com.liaoyin.travel.view.mine.attendance;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.util.DictUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Transient;
import java.util.Date;

@ApiModel("考勤统计明细")
@Accessors(chain = true)
@Data
public class AttendanceStatisticsDetailedView {

	private String id;
	
	@ApiModelProperty("打卡时间")
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date detailedTime;
	
	@ApiModelProperty("星期")
	private String weekNum;
	
	@ApiModelProperty("次数")
	private Integer num;
	
	@ApiModelProperty("类别")
	private String workType;

	@ApiModelProperty("考勤日期")
	@JsonFormat(pattern="HH:mm:ss",timezone="GMT+8")
	private Date checkTime;
	
	private String workTypeDisplay;

	/*public String getWorkTypeDisplay() {
		return this.workType!=null?DictUtil.getDisplay("workType", this.workType+""):null;
	}*/

	@ApiModelProperty("用户Id")
	private String usersId;

	@ApiModelProperty("用户昵称")
	private String usersNickName;

	@Transient
	@ApiModelProperty("用户头像id")
	private String usersHeadPicId;

	@ApiModelProperty("工号")
	private String userWorkerNum;

	@Transient
	@ApiModelProperty("用户实体")
	private Users users;

	@ApiModelProperty("班次")
	private Short checkShifts;

	@ApiModelProperty("设备名称")
	private String equipmentName;

	public String getCheckShiftsDisplay() {
		return this.checkShifts!=null?DictUtil.getDisplay("checkShifts",this.checkShifts+""):null;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("AttendanceStatisticsDetailedView{");
		sb.append('}');
		return sb.toString();
	}
}
