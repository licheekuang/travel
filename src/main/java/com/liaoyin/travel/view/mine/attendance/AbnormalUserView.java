package com.liaoyin.travel.view.mine.attendance;

import com.liaoyin.travel.entity.Users;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 考勤异常用户信息
 *
 * @author Kuang.JiaZhuo
 * @date 2020-04-20 16:05
 */
@Data
@Accessors(chain = true)
public class AbnormalUserView {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("工号")
    private String workerNum;

    @ApiModelProperty("头像url")
    private String headPicUrl;

    @ApiModelProperty("部门id")
    private String teamId;

    @ApiModelProperty("部门名称")
    private String teamName;

    @ApiModelProperty("部门级别")
    private Short teamLevel;

    @ApiModelProperty("考勤异常状态(1.缺卡;2.旷工)")
    private String abnormalState;

    public String getAbnormalStateDisplay(){
        if(abnormalState==null){
            return null;
        }
        return abnormalState.equals("1")?"缺卡":"旷工";
    }
}
