package com.liaoyin.travel.view.scenic;

import com.alibaba.fastjson.annotation.JSONField;
import com.liaoyin.travel.util.DictUtil;
import com.liaoyin.travel.util.MD5Util;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * 景区管理显示数据
 *
 * @author Kuang.JiaZhuo
 * @date 2020-02-21 22:22
 */
@Data
@Accessors(chain = true)
public class ScenicInfoView {

    @ApiModelProperty("应急文旅资源ID")
    private String emergencyId;

    @ApiModelProperty("设备编号")
    private String equipmentNumber;

    @ApiModelProperty("是否为市文旅委:1.是")
    private Integer whlyw;

    @ApiModelProperty("是否为应急文旅资源:1.是")
    private Integer emergency;

    @ApiModelProperty("单位ID")
    private String scenicId;

    @ApiModelProperty("区域(暂定精确到区)")
    private String districtName;

    @ApiModelProperty("区域(暂定精确到区)id")
    private String districtId;

    @ApiModelProperty("市级区域id")
    private String cityId;

    @ApiModelProperty("省级区域id")
    private String provinceId;

    @ApiModelProperty("景区名字")
    private String scenicName;

    @ApiModelProperty("公司名字")
    private String corpName;

    @ApiModelProperty("管理员id")
    private String backUserId;

    @ApiModelProperty("审核状态【0:未审核；1:通过；2:拒绝】")
    private String auditStatus;

    @ApiModelProperty("审核状态Display")
    public String getAuditStatusDisplay(){
        if(auditStatus!=null){
            return auditStatus.equals("0")?"未审核":auditStatus.equals("1")?"通过":"拒绝";
        }
        return null;
    }

    @ApiModelProperty("申请时间")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applicationDateTime;

    @ApiModelProperty("景区管理员")
    private String adminName;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("身份证号")
    private String identityCard;

    @ApiModelProperty("性别【1:男;2:女】")
    private String gender;

    @ApiModelProperty("性别Display")
    public String getGenderDisplay(){
        if(gender!=null){
            return gender.equals("1")?"男":"女";
        }
        return null;
    }

    @ApiModelProperty("管理员账号")
    private String account;

    @ApiModelProperty("管理员密码")
    private String password;

    /**
     * 密码加密解密
     */
    public String getPassword(){
        if(password!=null){
            return MD5Util.Base64Decode(password);
        }
        return null;
    }

    @ApiModelProperty("当日最大承载量")
    private BigInteger maximumCapacityForTheDay;

    @ApiModelProperty("单位类型【关联数据字典unitType字段】")
    private String unitType;

    @ApiModelProperty("单位类型Display")
    public String getUnitTypeDisplay(){
        if(unitType!=null && !unitType.equals("")){
            return DictUtil.getDisplay("unitType",unitType);
        }
        return null;
    }

    @ApiModelProperty("机构类型【关联数据库organType字段】")
    private String organType;

    @ApiModelProperty("机构类型Display")
    public String getOrganTypeDisplay(){
        if(organType!=null && !organType.equals("")){
            return DictUtil.getDisplay("organType",organType);
        }
        return null;
    }

}
