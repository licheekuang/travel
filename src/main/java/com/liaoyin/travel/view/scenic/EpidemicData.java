package com.liaoyin.travel.view.scenic;

import lombok.Data;

import java.util.List;

/**
 * TODO
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-02 22:22
 */
@Data
public class EpidemicData {

    /**
     * 数据总量
     */
    private Integer total;

    /**
     * 查询到的数据
     */
    private List<TouristHealthCheckInfo> rows;

}
