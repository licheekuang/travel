package com.liaoyin.travel.view.scenic;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 景区员工信息
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-03 10:14
 */
@Data
public class StaffHealthCheckInfo {

    @ApiModelProperty("有无可疑症状")
    private String symptom;

    @ApiModelProperty("接触人员是否有确诊或可疑症状病例")
    private String contactHistory;

    @ApiModelProperty("体温℃")
    private String temp;

    @ApiModelProperty("健康状态")
    private String healthState;

    @ApiModelProperty("身份号码")
    private String idCard;

    @ApiModelProperty("姓名")
    private String staffName;

    @ApiModelProperty("联系电话")
    private String phoneNum;

}
