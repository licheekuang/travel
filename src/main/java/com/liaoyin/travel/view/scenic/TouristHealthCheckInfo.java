package com.liaoyin.travel.view.scenic;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 景区游客信息
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-02 17:44
 */
@Data
public class TouristHealthCheckInfo {

    @ApiModelProperty("游客姓名")
    private String touristName;

    @ApiModelProperty("联系电话")
    private String phoneNum;

    @ApiModelProperty("身份号码")
    private String idCard;

    @ApiModelProperty("游客来源地")
    private String location;

    @ApiModelProperty("有无可疑症状")
    private String symptom;

    @ApiModelProperty("接触人员是否有确诊或可疑症状病例")
    private String contactHistory;

    @ApiModelProperty("健康状态")
    private String healthState;

    @ApiModelProperty("体温℃")
    private String temp;

    @ApiModelProperty("审核时间")
    private String createTime;

}
