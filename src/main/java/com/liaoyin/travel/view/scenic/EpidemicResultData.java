package com.liaoyin.travel.view.scenic;

import lombok.Data;

/**
 * 疫情系统数据返回结果
 *
 * @author Kuang.JiaZhuo
 * @date 2020-03-02 20:19
 */
@Data
public class EpidemicResultData {

    /**
     * 状态码
     */
    private String status;

    /**
     * 消息
     */
    private String message;

    /**
     * 主体功能主键
     */
    private String key;

    /**
     * 返回数据
     */
    private String data;

}
