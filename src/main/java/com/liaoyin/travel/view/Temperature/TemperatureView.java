package com.liaoyin.travel.view.Temperature;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TemperatureView {

	
	@ApiModelProperty("状态")
	private String weatherType;
	
	@ApiModelProperty("数量")
	private Integer weatherTypeNum;
	
	@ApiModelProperty("星期")
	private String timeWeek;
}
