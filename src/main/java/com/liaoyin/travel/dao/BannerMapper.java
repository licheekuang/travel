package com.liaoyin.travel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.Banner;

import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface BannerMapper extends Mapper<Banner> {
	
	
	 /**
     * @方法名：selectBannerList
     * @描述： 获取banner列表
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
	List<Banner> selectBannerList (@Param("bannertype")String bannertype,@Param("bannerDesc")String bannerDesc);
	
	 /**
     * @方法名：deleteBannerByIds
     * @描述： 根据id删除banner
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
	void deleteBannerByIds(@Param("id")String[] id);
	
}