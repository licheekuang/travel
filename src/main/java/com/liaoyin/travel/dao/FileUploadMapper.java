package com.liaoyin.travel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.FileUpload;

import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
public interface FileUploadMapper extends Mapper<FileUpload> {
	
	 FileUpload selectByBusinessCode(String businessCode);

	 List<FileUpload> selectListByBusinessCode(String businessCode);

	 Integer deleteByBusinessCode(String businessCode);
	 
	 void inseretListFileUpload(@Param("list")List<FileUpload> list);
}