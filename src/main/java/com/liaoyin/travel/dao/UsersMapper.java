package com.liaoyin.travel.dao;

import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.view.travelMobileControlSystem.StaffInfoView;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface UsersMapper extends Mapper<Users> {

    //查询出用户条数
    Integer selectByAccountCount(@Param("account") String account);

    Integer selectByPhone(@Param("phone") String phone, @Param("userType") String userType);

    //查询出用户
    List<Users> selectByAccount(@Param("account") String account);

    /****
     * @方法名：selectUsersByPage
     * @描述：后台查询用户信息列表
     * @作者： rzy
     * @日期： Created in 2018/11/9 16:53****/
    List<Users> selectUserList(Users users);

    /**
     * @Description: 用户列表
     * @Author: rzy
     * @Date: 2018/11/12
     */
    List<Users> selectEnterpriseUserList(@Param("user_type") String userType, @Param("nickName") String nikeName, @Param("account") String account, @Param("commonArea") String commonArea, @Param("isUsing") String isUsing, @Param("isAuthentication") String isAuthentication, @Param("invitationCode") String invitationCode, @Param("isBounty") String isBounty);

    /****
     * @方法名：selectUsersByAccount
     * @描述：根据账号查询后台用户信息
     * @作者： lijing
     * @日期： Created in 2018/7/17 16:53****/
    UserInfo selectUsersByAccount(@Param("account") String account);

    /**
     * @Description: 批量删除用户
     * @Author: rzy
     * @Date: 2018/11/9
     */
    int deleteUsersById(@Param("ids") String[] ids);


    /***
     *
     * @方法名：selectUsersByPhone
     * @描述： 根据微信手机查询用户
     * @作者： lijing
     * @日期： 2018年11月19日
     */
    UserInfo selectUsersByPhone(@Param("phone") String phone);

    /**
     * 　* @description: TODO 根据UID查询用户
     * 　* @param [uid]
     * 　* @return com.liaoyin.travel.bean.UserInfo
     * 　* @throws
     * 　* @author privatePanda777@163.com
     * 　* @date 2019/7/29 12:35
     *
     */
    UserInfo selectUsersByFingerprint(@Param("uid") String uid);

    /**
     * @Description: 查询手机号码数量
     * @Author: rzy
     * @Date: 2018/11/20
     */
    Integer selectPhoneNum(@Param("phone") String phone);

    /**
     * @方法名：selectUsersListOnNickName
     * @描述： 查询所有用户的昵称等信息
     * @作者： lijing
     * @日期： 2018年11月20日
     */
    List<Users> selectUsersListOnNickName(@Param("list") List<String> userIdList);

    /**
     * @description: 用户统计
     * @author: RenZhiyong
     * @date: 2019/1/3 17:05
     */
    Integer selectUserStatistics(@Param("userType") String userType);


    /****
     *
     * @方法名：selectUsersList
     * @描述： 查询前端用户列表
     * @作者： lijing
     * @日期： 2019年7月16日
     */
    List<Users> selectUsersList(@Param("phone") String phone, @Param("userType") String userType,
                                @Param("nickName") String nickName, @Param("teamId") String teamId,
                                @Param("assumeOffice") String assumeOffice, @Param("workId") String workId,
                                @Param("isUsing") String isUsing,@Param("scenicId") String scenicId);

    /****
     *
     * @方法名：selectUsersListOnTeamById
     * @描述： 查询部门下的所有员工信息
     * @作者： lijing
     * @日期： 2019年7月15日
     */
    List<Users> selectUsersListOnTeamById(@Param("teamId") String teamId);

    /**
     * @author 王海洋
     * @methodName: 查询部门下员工数量
     * @methodDesc:
     * @description:
     * @param:
     * @return
     * @create 2019-12-04 11:47
     **/
    int selectUsersCnt(@Param("teamId") String teamId);


    /**
     * 　* @description: TODO 根据ID查询用户的详细信息
     * 　* @param [uid]
     * 　* @return com.liaoyin.travel.entity.Users
     * 　* @throws
     * 　* @author privatePanda777@163.com
     * 　* @date 2019/7/24 15:56
     *
     */
    Users selectUsersById(String uid);


    /****
     *
     * @方法名：selectselectUsersListBybusinessIds
     * @描述： 查询多个部门/工种下的人员
     * @作者： lijing
     * @日期： 2019年7月25日
     * @param type  :1： 查询部门 2：查询工种
     */
    public List<Users> selectselectUsersListBybusinessIds(@Param("type") String type,
                                                          @Param("id") String[] businessId);


    List<UserInfo> selectUserInfoList();

    /**
     * @方法名：selectUsersListByTeamId
     * @描述： 根据部门id查询用户列表
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/13 18:03
     */
    List<Users> selectUsersListByTeamId(@Param("teamId") String teamId,@Param("scenicId") String scenicId);

    List<Users> selectUsersByIds(@Param("userIdList") List<String> userIdList);

    Users selectUsersByCardReissueProposer(String id);

    Users selectUsersByAskForLeaveProposer(String id);

    Users selectUsersByGoOutProposer(String id);

    List<String> selectTeamNameByUserId(String userId);

    String selectNiceNameById(String userId);

    Users selectUsersByCardReissuePacketId(String packetId);

    Users selectUsersByAskForLeavePacketId(String packetId);

    Users selectUsersByGoOutPacketId(String packetId);

    /**
     * @方法名：selectUserListByPhone
     * @描述： 根据手机号查询用户信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/23 10:55
     */
    List<Users> selectUserListByPhone(String phone);

    /**
     * @方法名：selectScenicIdByUserId
     * @描述： 根据用户id查询用户的景点id
     * @作者： kjz
     * @日期： Created in 2020/2/24 1:49
     */
    String selectScenicIdByUserId(String userId);

    /**
     * @方法名：selectUserByIdentityCard
     * @描述： 通过身份证查询用户信息
     * @作者： kjz
     * @日期： Created in 2020/2/24 16:37
     */
    List<Users> selectUserByIdentityCard(String identityCard);

    /**
     * @方法名：selectUserByIdentityCardAndUserId
     * @描述： 通过身份证号和排除用户id查询用户信息
     * @作者： kjz
     * @日期： Created in 2020/2/24 16:43
     */
    List<Users> selectUserByIdentityCardAndUserId(@Param("identityCard") String identityCard, @Param("userId") String userId);

    /**
     * @方法名：selectUsersByWorkNum
     * @描述： 根据工号等条件查询用户信息
     * @作者： kjz
     * @日期： Created in 2020/2/27 17:52
     */
    List<Users> selectUsersByWorkNum(@Param("workerNum") String workerNum,
                               @Param("scenicId") String scenicId,
                               @Param("userId") String userId);

    /**
     * @方法名：physicsDeleteUsersById
     * @描述： 根据用户id物理批量删除用户数据
     * @作者： kjz
     * @日期： Created in 2020/2/29 16:06
     */
    int physicsDeleteUsersById(@Param("ids") String[] ids);

    /**
     * @方法名：selectUsersByPhoneAndId
     * @描述： 根据手机号查询用户
     * @作者： kjz
     * @日期： Created in 2020/3/5 12:00
     */
    List<Users> selectUsersByPhoneAndId(@Param("phone") String phone, @Param("id") String id);

    List<String> judgeUserIsCorrelationWorker(@Param("workerIds")List<String> workerIds);

    Integer countUsers();

    List<StaffInfoView> getStaffInfoViewList();

    String selectNiceNamesByIds(@Param("businessIds") List<String> businessIds);

    List<Users> selectUsersListOnTeamByParentId(String teamId);

    List<Users> selectUsersListByWorkId(@Param("workId") String workId,
                                        @Param("scenicId") String scenicId);

    List<Users> selectUsersListByUserIds(@Param("userList") List<String> userList);
}
