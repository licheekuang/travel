package com.liaoyin.travel.dao;

import com.liaoyin.travel.entity.DictValue;

import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
public interface DictValueMapper extends Mapper<DictValue> {
}