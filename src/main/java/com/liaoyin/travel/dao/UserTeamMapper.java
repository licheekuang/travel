package com.liaoyin.travel.dao;

import com.liaoyin.travel.entity.UserTeam;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface UserTeamMapper extends Mapper<UserTeam> {


	/***
	 *
	 * @方法名：deleteUserTeamByIds
	 * @描述： 根据用户id删除
	 * @作者： lijing
	 * @日期： 2019年8月19日
	 */
	void deleteUserTeamByIds(@Param("userId") String userId);


	/***
	 *
	 * @方法名：selectUserTeam
	 * @描述：
	 * @作者： lijing
	 * @日期： 2019年8月19日
	 */
	List<UserTeam> selectUserTeam(@Param("userId") String userId, @Param("teamId") String teamId);

    String selectBranchManagerIdByStaffId(String userId);

	List<String> selectDirectorIdByTeamId(String teamId);

	int updateAssumeOfficeActAsTwoByTeamIdAndUserId(@Param("teamId") String teamId, @Param("originalDirectorId") String originalDirectorId);

	int deleteUserTeamByTeamIds(@Param("teamIds")List<String> teamIds);

	List<String> judgeTeamIsCorrelationStaff(@Param("teamIds")List<String> teamIds);

	int deleteUserTeamByUserIds(@Param("ids") String[] ids);
}