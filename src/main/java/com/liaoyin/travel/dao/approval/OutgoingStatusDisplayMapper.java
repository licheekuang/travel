package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.OutgoingStatusDisplay;
import tk.mybatis.mapper.common.Mapper;

import java.time.LocalDateTime;
import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface OutgoingStatusDisplayMapper extends Mapper<OutgoingStatusDisplay> {

    List<OutgoingStatusDisplay> selectDisplayByDate(LocalDateTime now);
}