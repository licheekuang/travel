package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.vo.approval.ApprovalPacketRecordVo;
import com.liaoyin.travel.entity.approval.AskForLeavePacketRecord;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AskForLeavePacketRecordMapper extends Mapper<AskForLeavePacketRecord> {

    List<AskForLeavePacketRecord> selectAskForLeavePacketRecordByCondition(ApprovalPacketRecordVo approvalPacketRecordVo);

    int deleteAskForLeavePacketRecordByIds(List<String> list);

    AskForLeavePacketRecord selectAskForLeavePacketById(String id);

    int updateById(AskForLeavePacketRecord askForLeavePacketRecord);

    int deleteAskForLeavePacketRecordById(String packId);

    int selectRecordCountByPacketName(@Param("packetName") String packetName,
                                      @Param("id") String id);
}