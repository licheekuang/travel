package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.GoOutPacketApplicant;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface GoOutPacketApplicantMapper extends Mapper<GoOutPacketApplicant> {

    List<GoOutPacketApplicant> selectApplicantByPacketId(String packetId);

    String selectPacketIdByApplicantId(String userId);

    List<GoOutPacketApplicant> selectApplicantByUserId(String userId);
}