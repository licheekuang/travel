package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.AskForLeaveVisibleUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ApprovalAskForLeaveVisibleUserMapper extends Mapper<AskForLeaveVisibleUser> {

    int deleteApprovalAskForLeaveVisibleUserAll();

    List<AskForLeaveVisibleUser> selectAskForLeaveVisibleUserByScenicId(String scenicId);

    int deleteAskForLeaveVisibleUserByUserIds(@Param("ids") String[] ids);
}