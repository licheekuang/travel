package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.AskForLeaveRecord;
import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
public interface AskForLeaveRecordMapper extends Mapper<AskForLeaveRecord> {

    AskForLeaveRecord selectAskForLeaveRecordByApprovalRecordId(String approvalRecordId);
}