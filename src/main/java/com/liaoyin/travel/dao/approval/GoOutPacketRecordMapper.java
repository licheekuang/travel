package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.vo.approval.ApprovalPacketRecordVo;
import com.liaoyin.travel.entity.approval.GoOutPacketRecord;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
@org.apache.ibatis.annotations.Mapper
public interface GoOutPacketRecordMapper extends Mapper<GoOutPacketRecord> {

    List<GoOutPacketRecord> selectGoOutPacketRecordByCondition(ApprovalPacketRecordVo approvalPacketRecordVo);

    int deleteGoOutPacketRecordByIds(List<String> list);

    GoOutPacketRecord selectGoOutPacketById(String id);

    int deleteGoOutPacketRecordById(String packId);

    int selectRecordCountByPacketName(@Param("packetName") String packetName,
                                      @Param("id") String id);
}