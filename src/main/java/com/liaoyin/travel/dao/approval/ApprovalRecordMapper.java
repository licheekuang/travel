package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.vo.approval.ApprovalRecordBackVo;
import com.liaoyin.travel.entity.approval.ApprovalRecord;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.time.LocalDateTime;
import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ApprovalRecordMapper extends Mapper<ApprovalRecord> {


    List<ApprovalRecord> selectApprovalRecordByApproverId(String approverId);

    List<ApprovalRecord> selectRequestRecordByInitiatorId(@Param("initiatorId") String initiatorId, @Param("approvalType") String approvalType);

    ApprovalRecord selectApprovalDetailsByApprovalId(String approvalId);

    String selectApprovalStatusByApproverId(String approvalRecordId);

    int updateApprovalStatusSetThree(@Param("refusalCause") String refusalCause,
                                     @Param("approvalRecordId") String approvalRecordId,
                                     @Param("finishTime") LocalDateTime finishTime,
                                     @Param("approvalConsuming") String approvalConsuming);

    int updateApprovalStatusSetFour(String approvalRecordId);

    int updateApprovalStatusSetTwo( @Param("approvalRecordId") String approvalRecordId,
                                    @Param("finishTime") LocalDateTime finishTime,
                                    @Param("approvalConsuming") String approvalConsuming);

    List<ApprovalRecord> selectApprovalRecordByCondition(ApprovalRecordBackVo approvalRecordBackVo);
}