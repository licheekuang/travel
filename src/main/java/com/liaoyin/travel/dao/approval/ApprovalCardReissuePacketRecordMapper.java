package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.vo.approval.ApprovalPacketRecordVo;
import com.liaoyin.travel.entity.approval.CardReissuePacketRecord;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ApprovalCardReissuePacketRecordMapper extends Mapper<CardReissuePacketRecord> {


    List<CardReissuePacketRecord> selectCardReissuePacketRecordByCondition(ApprovalPacketRecordVo approvalPacketRecordVo);

    int deleteCardReissuePacketRecordByIds(List<String> list);

    CardReissuePacketRecord selectCardReissuePacketById(@Param("id") String id);

    int updateById(CardReissuePacketRecord cardReissuePacketRecord);

    int deleteCardReissuePacketRecordById(String packId);

    int selectRecordCountByPacketName(@Param("packetName") String packetName,
                                      @Param("id") String id);
}