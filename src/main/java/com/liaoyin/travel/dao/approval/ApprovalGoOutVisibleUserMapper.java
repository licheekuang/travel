package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.GoOutVisibleUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ApprovalGoOutVisibleUserMapper extends Mapper<GoOutVisibleUser> {

    int deleteApprovalGoOutVisibleUserAll();

    List<GoOutVisibleUser> selectGoOutVisibleUserByScenicId(String scenicId);

    int deleteGoOutVisibleUserByUserIds(@Param("ids") String[] ids);
}