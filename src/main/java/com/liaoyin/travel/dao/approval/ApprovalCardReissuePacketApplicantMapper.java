package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.CardReissuePacketApplicant;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ApprovalCardReissuePacketApplicantMapper extends Mapper<CardReissuePacketApplicant> {

    List<CardReissuePacketApplicant> selectApplicantByPacketId(String packetId);

    String selectPacketIdByApplicantId(String userId);

    List<CardReissuePacketApplicant> selectApplicantByUserId(String userId);
}