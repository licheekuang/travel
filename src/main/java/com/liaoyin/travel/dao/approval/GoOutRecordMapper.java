package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.GoOutRecord;
import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
public interface GoOutRecordMapper extends Mapper<GoOutRecord> {

    GoOutRecord selectGoOutRecordByApprovalRecordId(String approvalRecordId);
}