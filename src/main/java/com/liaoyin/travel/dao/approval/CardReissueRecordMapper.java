package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.CardReissueRecord;
import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
public interface CardReissueRecordMapper extends Mapper<CardReissueRecord> {

    CardReissueRecord selectCardReissueRecordByApprovalRecordId(String approvalRecordId);
}