package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.AskForLeavePacketApplicant;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AskForLeavePacketApplicantMapper extends Mapper<AskForLeavePacketApplicant> {

    List<AskForLeavePacketApplicant> selectApplicantByPacketId(String packetId);

    String selectPacketIdByApplicantId(String userId);

    List<AskForLeavePacketApplicant> selectApplicantByUserId(String userId);
}