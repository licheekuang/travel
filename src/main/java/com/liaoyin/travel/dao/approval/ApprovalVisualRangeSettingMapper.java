package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.ApprovalVisualRangeSetting;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ApprovalVisualRangeSettingMapper extends Mapper<ApprovalVisualRangeSetting> {

    ApprovalVisualRangeSetting selectApprovalVisualRangeSettingByApprovalType(String approvalType);

    List<ApprovalVisualRangeSetting> selectApprovalVisualRangeSettingByAll(String scenicId);

    int usingOrDisableApprovalType(@Param("onOff") String onOff,
                                   @Param("approvalType") String approvalType,
                                   @Param("scenicId") String scenicId);

    int updateById(ApprovalVisualRangeSetting approvalVisualRangeSetting);


    int deleteApprovalVisualRangeSettingById(String id);
}