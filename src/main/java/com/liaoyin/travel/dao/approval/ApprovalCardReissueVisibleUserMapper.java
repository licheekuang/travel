package com.liaoyin.travel.dao.approval;

import com.liaoyin.travel.entity.approval.CardReissueVisibleUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ApprovalCardReissueVisibleUserMapper extends Mapper<CardReissueVisibleUser> {

    int deleteApprovalCardReissueVisibleUserAll();

    List<CardReissueVisibleUser> selectCardReissueVisibleUserByScenicId(String scenicId);

    int deleteCardReissueVisibleUserByUserIds(@Param("ids") String[] ids);
}