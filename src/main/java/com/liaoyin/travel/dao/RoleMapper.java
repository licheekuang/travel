package com.liaoyin.travel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.Role;

import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface RoleMapper extends Mapper<Role> {
	
	
	 /**
     * 
     * 方 法 名：selectRoleListByUserId
     * 创建日期：2017/11/25 15:00
     * 创 建 者：li.jing
     * 描    述：根据createUserId查询角色列表
     * @Param 
     * @return 
     */
	public List<Role> selectRoleList(@Param("createUserId")String createUserId,@Param("roleName")String roleName,@Param("roleAffiliate")String roleAffiliate,@Param("attr1")String attr1);
	
	 /**
     * 
     * 方 法 名：selectAllRoleList
     * 创建日期：2017/11/25 15:00
     * 创 建 者：li.jing
     * 描    述：根据查询所有角色列表
     * @Param 
     * @return 
     */
	public List<Role> selectAllRoleList(@Param("id")String id);

	/**
	 * @Description 当前角色下是否绑定有用户
	 * @Author  rzy
	 * @date 2018/12/24 15:09
	 */
	Integer selectIsExistBackUser(String id);
}