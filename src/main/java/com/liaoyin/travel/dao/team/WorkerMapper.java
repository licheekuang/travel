package com.liaoyin.travel.dao.team;

import com.liaoyin.travel.entity.team.Worker;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface WorkerMapper extends Mapper<Worker> {
	
	/****
	 * 
	     * @方法名：selectWorkerList
	     * @描述： 查询工种列表
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	List<Worker> selectWorkerList(@Param("workerName")String workerName, @Param("workerCode")String workerCode,
	                              @Param("isUsing")String isUsing, @Param("scenicId")String scenicId                     );


	/**
	　* @description: TODO  根据工种名称查询工种ID
	　* @param [name]
	　* @return com.liaoyin.travel.entity.team.Worker
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/7/25 17:50
	　*/
	Worker selectWorkerByName(@Param("name") String name, @Param("scenicId") String scenicId);

	/**
	 * @方法名：selectWorkerListByName
	 * @描述： 根据工种名字查询工种信息
	 * @作者： kjz
	 * @日期： Created in 2020/3/7 13:04
	 */
    List<Worker> selectWorkerListByName(@Param("workerName") String workerName,
                                        @Param("scenicId") String scenicId,
                                        @Param("workId") String workId);

    /**
     * @方法名：selectWorkerListByCode
     * @描述： 根据工种编码查询工种信息
     * @作者： kjz
     * @日期： Created in 2020/3/7 13:09
     */
	List<Worker> selectWorkerListByCode(@Param("workerCode") String workerCode,
	                                    @Param("scenicId") String scenicId,
	                                    @Param("workId") String workId);

    int deleteByidsBatch(@Param("ids")List<String> ids);

    String selectWorkerNameByTeamId(String departId);

    String selectWorkerNameByIds(@Param("businessIds") List<String> businessIds);

    List<Worker> selectWorkerByIds(@Param("workIdList")List<String> workIdList);
}