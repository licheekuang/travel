package com.liaoyin.travel.dao.team;

import com.liaoyin.travel.entity.team.Team;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TeamMapper extends Mapper<Team> {
	
	/*****
	 * 
	     * @方法名：selectTeamList
	     * @描述： 查询部门列表
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	 List<Team> selectTeamList(@Param("teamName")String teamName,@Param("teamLevel")String teamLevel, @Param("parentId")String parentId,
	                           @Param("teamCode")String teamCode,@Param("isUsing")String isUsing,@Param("scenicId")String scenicId);

	 /**
	 　* @description: TODO 根据部门名称查询部门ID
	 　* @param [name]
	 　* @return com.liaoyin.travel.entity.team.Team
	 　* @throws
	 　* @author privatePanda777@163.com
	 　* @date 2019/7/25 17:51
	 　*/
	 Team selectTemaByName(@Param("name") String name, @Param("scenicId") String scenicId);

	 /**
	  * @方法名：selectTeamByAll
	  * @描述： 查询所有的部门列表
	  * @作者： Kuang.JiaZhuo
	  * @日期： Created in 2019/11/13 17:52
	  */
	 List<Team> selectTeamByAll(String scenicId);

	 /**
	  * @方法名：selectTeamListByName
	  * @描述： 根据部门名称查询部门信息
	  * @作者： kjz
	  * @日期： Created in 2020/3/7 12:30
	  */
    List<Team> selectTeamListByName(@Param("teamName") String teamName,
                                    @Param("scenicId") String scenicId,
                                    @Param("teamId") String teamId);

    /**
     * @方法名：selectTeamListByCode
     * @描述： 根据部门编码查询部门信息
     * @作者： kjz
     * @日期： Created in 2020/3/7 12:48
     */
	List<Team> selectTeamListByCode(@Param("teamCode") String teamCode,
	                                @Param("scenicId") String scenicId,
	                                @Param("teamId") String teamId);

	/**
	 * 批量删除部门
	 * @param ids
	 * @return
	 */
	int deleteByidsBatch(@Param("ids")List<String> ids);
	/**
	 * 批量删除部门-工种关系
	 * @param ids
	 * @return
	 */
    int deleteTeamWorkerByTeamIds(@Param("ids")List<String> ids);

    String selectTeamNamesByIds(@Param("businessIds") List<String> businessIds);

    List<Team> selectTeamByParentId(String teamId);

    List<Team> selectTeamByIds(@Param("teamIds")List<String> teamIds);
}