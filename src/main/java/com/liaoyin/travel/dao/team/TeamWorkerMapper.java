package com.liaoyin.travel.dao.team;

import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.team.TeamWorker;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TeamWorkerMapper extends Mapper<TeamWorker> {
	
	/****
	 * 
	     * @方法名：selectTeamWorkerList
	     * @描述： 查询部门下的所有工种
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	List<TeamWorker> selectTeamWorkerList(@Param("teamId")String teamId);
	
	
	/***
	 * 
	     * @方法名：deleteTeamWorkerByTeamId
	     * @描述： 通过部门id删除
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	 void deleteTeamWorkerByTeamId(@Param("teamId")String teamId);

	/**
	 * 查询部门下所有员工
	 * @param teamId
	 * @return
	 */
	List<Users> selectUsersListByTeamId(@Param("teamId") String teamId);

	/**
	 * 根据部门id和工种id查询部门和工种的关联信息
	 * @param teamId 部门id
	 * @param workId 工种id
	 * @return
	 */
    TeamWorker selectTeamWorkerByTeamIdAndWorkerId(@Param("teamId") String teamId, @Param("workId") String workId);

	/**
	 * 判断部门是否已经关联工种
	 * @param workerIds
	 * @return
	 */
	List<String> judgeWorkerIsCorrelationTeam(@Param("workerIds")List<String> workerIds);

	/**
	 * 批量删除工种-部门关系表
	 * @param workerIds
	 * @return
	 */
	int deleteWorkerTeamByWorkerIds(@Param("workerIds")List<String> workerIds);
}