package com.liaoyin.travel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.view.mine.GlobalSystemParamView;
import com.liaoyin.travel.vo.GlobalSystemParamVo;
import com.liaoyin.travel.entity.GlobalSystemParam;

import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface GlobalSystemParamMapper extends Mapper<GlobalSystemParam> {
    GlobalSystemParamView selectGlobalSystemParamByParamCode(@Param("paramCode") String paramCode);
    List<GlobalSystemParam> selectGlobalSystemParamList(GlobalSystemParamVo globalSystemParamVo);

    /**
     * @Description 修改悬赏金充值手续费
     * @Author  rzy
     * @date 2018/12/18 18:05
     */
    void updateRewardServiceCharge(@Param("serviceCharge") Integer serviceCharge);
    GlobalSystemParam selectRewardGold();
}