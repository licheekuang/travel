package com.liaoyin.travel.dao;

import com.liaoyin.travel.entity.UserRong;

import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface UserRongMapper extends Mapper<UserRong> {

    int selectCountAll();

    int deleteRongByAll();
}