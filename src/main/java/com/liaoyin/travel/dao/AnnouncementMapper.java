package com.liaoyin.travel.dao;

import com.liaoyin.travel.vo.announce.AnnounceUserVO;
import com.liaoyin.travel.vo.request.AnnounceSearchRequestVO;
import com.liaoyin.travel.entity.Announcement;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AnnouncementMapper extends Mapper<Announcement> {


    /**
    　* @description: TODO  条件查询公告、通知列表
    　* @param [title, content, time, userName]
    　* @return java.util.List<com.liaoyin.travel.entity.Announcement>
    　* @throws
    　* @author privatePanda777@163.com
    　* @date 2019/9/24 16:42
    　*/
    List<Announcement> selectAnnouncementList(@Param("title") String title,
                                              @Param("content")String content,
                                              @Param("time")String time,
                                              @Param("userId")String userId,
                                              @Param("scenicId")String scenicId);


    Announcement selectAnnouncementByPrimaryKey(String announcementId);

    /**
     * @author 王海洋
     * @methodName: getReceiveUserByAnnounceId
     * @methodDesc: 根据公告id获取接收公告的用户列表
     * @description:
     * @param:
     * @return
     * @create 2019-11-29 16:08
     **/
    List<AnnounceUserVO> getReceiveUserByAnnounceId(@Param("id") String id);

    /**
     * @author 王海洋
     * @methodName: 搜索公告列表
     * @methodDesc:
     * @description:
     * @param:
     * @return
     * @create 2019-11-29 17:29
     **/
    List<Announcement> getAnnounceList(AnnounceSearchRequestVO announceSearchRequestVO);

    /**
     * @author 王海洋
     * @methodName: 删除
     * @methodDesc:
     * @description:
     * @param:
     * @return
     * @create 2019-12-02 9:29
     **/
    int delateById(@Param("ids") String ids);

    /**
     * @author 王海洋
     * @methodName: 修改公告
     * @methodDesc:
     * @description:
     * @param: 
     * @return 
     * @create 2019-12-04 17:53
     **/
    int updateById(Announcement announcement);

    int delateByIds(@Param("ids") String ids);
}