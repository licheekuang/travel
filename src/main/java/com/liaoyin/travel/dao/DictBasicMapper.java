package com.liaoyin.travel.dao;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.DictBasic;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
public interface DictBasicMapper extends Mapper<DictBasic> {

    List<DictBasic> selectDictBasics(@Param("code") String[] code);

    List<DictBasic> selectDictBasicsWithGroup(@Param("dictBasic") List<DictBasic> dictBasic);

    List<Map<String,Object>> selectAllDiceByCodeBuildTree(String code);

    List<Map<String,Object>> selectByCodeAndGroupcodeBuildTree(@Param("dictCode") String dictCode, @Param("groupCode") String groupCode);

    List<DictBasic> selectDictByCode(@Param("code") String code);
    
    /**
	 * 
	 * 方   法  名：selectDictBasicList
	 * 创建日期：2018年08月1日 下午9:18:12
	 * 创   建  者：lijing
	 * 描          述：查询字典列表
	 * @param 
	 * @return
	 */
    List<DictBasic> selectDictBasicList(@Param("code")String code,@Param("isUsing")String isUsing);
    
    /**
	 * 
	 * 方   法  名：selectDictBasicById
	* 创建日期：2018年08月1日 下午9:18:12
	 * 创   建  者：li.jing
	 * 描          述：根据id查询
	 * @param dictBasic
	 * @return
	 */
	DictBasic selectDictBasicById(@Param("id")String id);
	
	/**
	 * 
	 * 方   法  名：selectMenuMAXfuncCode
	 * 创建日期：2018年08月1日 下午9:18:12
	 * 创   建  者：li.jing
	 * 描          述：查询功能当前最大字典值
	 * @param 
	 * @return
	 */
	  List<DictBasic> selectDirectLevelOrgsNoSearchNotOnlyManager(DictBasic d);
	  
	 /**
	 * 
	 * 方   法  名：selectDictBasicMax
	* 创建日期：2017年12月13日 下午3:00:05
	 * 创   建  者：li.jing
	 * 描          述：根据id查询
	 * @param dictBasic
	 * @return
	 */
	List<String> selectDictBasicMax(@Param("code")String code);  


	List<DictBasic> selectDictBasicByDisplay(String display);


	List<DictBasic> selectDictBasicListByCodeAndValue(@Param("code") String code, @Param("value") String value);
	
}