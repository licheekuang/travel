package  com.liaoyin.travel.dao;
import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.view.moble.back.MenuView;
import com.liaoyin.travel.entity.Menu;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;
@org.apache.ibatis.annotations.Mapper
public interface MenuMapper extends Mapper<Menu> {
	
	public List<Menu> selectMenu(String partymemberId);
	

	public List<MenuView> selectMenuByRoleIdIsAdd(@Param("roleId") String roleId);
	public List<MenuView> selectMenuAll();
	
	public List<Menu> selectMenuByRoleId(@Param("roleId") String roleId);
	/**
	 * 作者：cy
	 * 描述：根据人员和组织获取角色菜单
	 * 日期：2018/1/17 19:46
	 */
	List<Menu> selectMenuByUserAndOrg(@Param("userId")String userId,@Param("orgId")String orgId);

    /**
     * 根据用户和组的权限关系查找用户可访问菜单
     * @param userId
     * @return
     */
    public List<MenuView> selectMenuByUserId(@Param("userId") String userId);

    /**
     * 根据组织查找该组织可访问菜单
     * @param orgId
     * @return
     */
    public List<Menu> selectMenuByOrgId(@Param("orgId") String orgId);
    /**
     * 作者：cy
     * 描述：根据权限ids获取菜单
     * 日期：2017/12/4 22:06
     */
    List<Menu> selectMenuByRoleIds(@Param("roleIds") List<String> roleIds);
    
    
    /**
	 * 
	 * 方   法  名：selectMenuMAXfuncCode
	 * 创建日期：2017年10月17日 11:03:06
	 * 创   建  者：li.jing
	 * 描          述：查询功能当前最大code
	 * @param 
	 * @return
	 */
    List<Menu> selectDirectLevelOrgsNoSearchNotOnlyManager(Menu menu);
    
    /**
	 * 
	 * 方   法  名：selectMenuListByPage
	 * 创建日期：2017年10月17日 11:03:06
	 * 创   建  者：li.jing
	 * 描          述：后台查询菜单(带分页)
	 * @param 
	 * @return
	 */
    List<Menu> selectMenuListByPage(@Param("id")String id);
    
    /**
   	 * 
   	 * 方   法  名：selectMenuById
   	 * 创建日期：2017年12月8日 下午7:59:06
   	 * 创   建  者：li.jing
   	 * 描          述：菜单id查询
   	 * @param id
   	 * @return Menu
   	 */
    Menu selectMenuById(@Param("id")String id);
    
    /**
     * 根获取菜单所有--带返回下级菜单数量
     * @param id
     * @return
     */
    public List<Menu> selectMenuAllThis(@Param("id")String id);
    
    /**
     * 获取菜单所有---带返回下级菜单数量--不传id
     * @param
     * @return
     */
    public List<Menu> selectMenuAllAllThisMenu();
   
    /**
     * 获取菜单所有
     * @param id
     * @return
     */
    public List<MenuView> selectMenuAllById(@Param("id")String id);



    /**
     * 获取最大
     * @param id
     * @return
     */
    public List<String> selectMenuMaxById(@Param("id")String id);
    
    /**
   	 * 
   	 * 方   法  名：selectMenuListByPage
   	 * 创建日期：2017年10月17日 11:03:06
   	 * 创   建  者：li.jing
   	 * 描          述：后台查询菜单(带分页)
   	 * @param 
   	 * @return
   	 */
     List<Menu> selectMenuListByPageByThis(@Param("id")String id,@Param("dataType")String dataType,@Param("search")String search);
	/**
	 * 作者：cy
	 * 描述：获取移动端免登陆菜单
	 * 日期：2018/3/26 19:45
	 */
	List<Menu> getLogoutAppMenu();

	/**
	 * @方法名：selectMenuListByRoleId
	 * @描述： 根据角色查询角色权限
	 * @作者： lijing
	 * @日期：2018年8月2日
	 */
	 List<Menu> selectMenuListByRoleId(@Param("roleId")String roleId);

	 /**
	  * @方法名：updateScenicRoleNoSubordinateByAll
	  * @描述： 把所有的菜单都设置为景区管理员不可选择
	  * @作者： kjz
	  * @日期： Created in 2020/5/14 16:12
	  */
     int updateScenicRoleNoSubordinateByAll();

     /**
      * @方法名：updateScenicRoleThisMenu
      * @描述： 根据菜单id集合把菜单设置为单位子管理员可选择
      * @作者： kjz
      * @日期： Created in 2020/5/14 16:19
      */
	 int updateScenicRoleThisMenu(@Param("menuIdList") List<String> menuIdList);

	 /**
	  * @方法名：selectMenuBySubordinateEqualOne
	  * @描述： 查询景区子管理员可以进行分配的菜单
	  * @作者： kjz
	  * @日期： Created in 2020/5/14 16:44
	  */
	 List<Menu> selectMenuBySubordinateEqualOne();

	 /**
	  * @方法名：selectMenuAllThisScenicLowerLevelAdminMenu
	  * @描述： 单位子管理员的所有菜单---带返回下级菜单数量
	  * @作者： kjz
	  * @日期： Created in 2020/5/14 18:00
	  */
	 List<Menu> selectMenuAllThisScenicLowerLevelAdminMenu();

	 /**
	  * @方法名：selectScenicLowerLevelAdminMenuByUserId
	  * @描述： 根据后台用户id查询单位子管理员的菜单
	  * @作者： kjz
	  * @日期： Created in 2020/5/15 10:19
	  */
	List<MenuView> selectScenicLowerLevelAdminMenuByUserId(String backUserId);
}