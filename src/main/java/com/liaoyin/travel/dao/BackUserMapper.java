package com.liaoyin.travel.dao;

import com.liaoyin.travel.bean.UserInfo;
import com.liaoyin.travel.entity.BackUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
@org.apache.ibatis.annotations.Mapper
public interface BackUserMapper extends Mapper<BackUser> {
   
	
	/**
     * @方法名：selectBackUserList
     * @描述： 查询后台用户列表
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
	List<BackUser> selectBackUserList(
			@Param("userType")String userType,
			@Param("nickName")String nickName,
			@Param("isUsing")String isUsing,
			@Param("scenicId")String scenicId);

	 /**
     * @方法名：deleteBackUserById
     * @描述： 删除后台用户
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
	void deleteBackUserById(@Param("id")String[] id);
	
	 /**
     * @方法名：selectByAccountCount
     * @描述： 根据账户查询是否已经存在
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
	Integer selectByAccountCount(@Param("account")String account);
	
	/**
     * @方法名：selectThisByBusniId
     * @描述： 根据商家id查询后台用户
     * @作者： lijing
     * @日期： Created in 2018/6/20 10:27
     */
	BackUser selectThisByBusniId(@Param("id")String id);

	/**
	* @Description: 通过账号查询用户
	* @Author: rzy
	* @Date: 2018/11/28
	*/
	UserInfo selectBackUserAccount(@Param("account") String account);

	/** 
	* @Description: 通过id查询后台用户
	* @Author: rzy 
	* @Date: 2018/11/30 
	*/
	BackUser selectBackUserById(@Param("id") String id);

	/**
	 * @方法名：selectBackUserByPhone
	 * @描述： 通过电话查询账号
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2020/2/21 15:27
	 */
    List<BackUser> selectBackUserByPhone(@Param("phone") String phone);

    /**
     * @方法名：selectBackUserByAccount
     * @描述： 通过账号查询后台用户
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 16:20
     */
	List<BackUser> selectBackUserByAccount(@Param("account") String account);

	/**
	 * @方法名：selectBackUserByAccountOrPhone
	 * @描述： 根据账号或联系电话查询后台用户
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2020/2/21 18:47
	 */
	List<BackUser> selectBackUserByAccountOrPhone(@Param("account") String account, @Param("phone") String phone);

	/**
	 * @方法名：selectBackUserByIdentityCard
	 * @描述： 根据身份证号查询后台用户信息
	 * @作者： kjz
	 * @日期： Created in 2020/2/24 17:42
	 */
    List<BackUser> selectBackUserByIdentityCard(@Param("identityCard") String identityCard, @Param("scenicId") String scenicId);

    /**
     * @方法名：selectBackUserByAccountAndId
     * @描述： 根据账户和(不等于)id查询后台用户
     * @作者： kjz
     * @日期： Created in 2020/4/28 15:11
     */
    List<BackUser> selectBackUserByAccountAndId(@Param("account") String account, @Param("id") String id);

    /**
     * @方法名：selectNickNameById
     * @描述： 根据后台用户id查询后台用户昵称
     * @作者： kjz
     * @日期： Created in 2020/8/10 19:22
     */
    String selectNickNameById(String userId);
}