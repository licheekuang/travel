package com.liaoyin.travel.dao;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.Function;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;
@org.apache.ibatis.annotations.Mapper
public interface FunctionMapper extends Mapper<Function> {

	/**
	 * @方法名：selectFunctionsByUserId
	 * @描述： 通过用户ID获取用户功能权限
	 * @作者： li.jing
	 * @日期： Created in 2017/11/6 21:27
	 */
	List<Function> selectFunctionsByUserId(@Param("userId") String userId);

	/**
	 * @方法名：selectFunctionsByOrgId
	 * @描述： 通过组织ID获取用户功能权限
	 * @作者： li.jing
	 * @日期： Created in 2017/12/4 22:06
	 */
	List<Function> selectFunctionsByOrgId(@Param("orgId") String orgId);

	/**
	 * 作者：cy
	 * 描述：根据权限ids获取菜单
	 * 日期：2017/12/4 22:06
	 */
	List<Function> selectMenuByRoleIds(@Param("roleIds") List<String> roleIds);

	/**
	 * 作者：cy
	 * 描述：根据菜单ID获取静态页面权限
	 * 日期：2018/3/13 11:20
	 */
	List<Function> getHtmlListByMenu(@Param("menuIds") List<String> menuIds);


	/**
	 * @方法名：selectFunctionsByUserIdAndMenuId
	 * @描述： 通过用户ID跟菜单ID，获取用户该菜单下的功能权限
	 * @作者： cy
	 * @日期： Created in 2018/3/13 11:20
	 */
	List<Function> selectFunctionsByUserIdAndMenuId(@Param("userId")String userId,@Param("menuId") String menuId);
	
	

	/**
	 * @方法名：selectFunctionListByPage
	 * @描述： 查询功能列表
	 * @作者： li.jing
	 * @日期： Created in 2017/12/05 15:00
	 */
	List<Function> selectFunctionListByPage(@Param("menuId")String menuId,
											@Param("dataType")String dataType,
											@Param("memtype")String memtype,
											@Param("method")String method,
											@Param("search")String search);

	/**
	 * @方法名：selectFunctionListByPageInMenu
	 * @描述： 查询功能列表
	 * @作者： i.jing
	 * @日期： Created in 2017/12/05 15:00
	 */
	List<Function> selectFunctionListByPageInMenu(@Param("menuId")String menuId);
	

	/**
	 * @方法名：selectFunctionMAXfuncCode
	 * @描述： 查询功能当前最大code
	 * @作者： li.jing
	 * @日期： Created in 2017年10月17日 11:03:06
	 */
	List<String> selectFunctionMAXfuncCode();

	/**
	 * @方法名：selectFunctionListByMenuId
	 * @描述： 根据菜单ID查询功能列表
	 * @作者： li.jing
	 * @日期： Created in 2017/12/09 15:00
	 */
	List<Function> selectFunctionListByMenuId(@Param("menuId")String menuId);

	/**
	 * @方法名：updateMenuAndFun
	 * @描述： 根据菜单ID查询功能列
	 * @作者： li.jing
	 * @日期： Created in 2017/12/09 15:00
	 */
    void updateMenuAndFun(@Param("id")String id,@Param("funId")String funId);
	
}