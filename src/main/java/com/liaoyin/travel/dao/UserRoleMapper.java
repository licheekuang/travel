package com.liaoyin.travel.dao;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;

import com.liaoyin.travel.entity.UserRole;

@org.apache.ibatis.annotations.Mapper
public interface UserRoleMapper extends Mapper<UserRole> {
	/**
	 * 作者：cy
	 * 描述：根据组织ID查询是否有对应角色
	 * 日期：2017/12/4 21:49
	 */
	List<UserRole> selectRoleByOrgId(String orgId);
	/**
	 * 作者：cy
	 * 描述：根据人员ID查询是否有对应角色
	 * 日期：2017/12/21 19:07
	 */
	List<UserRole> selectRoleByUserId(String orgId);
}