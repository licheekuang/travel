package com.liaoyin.travel.dao.report;

import com.liaoyin.travel.entity.FileUpload;
import com.liaoyin.travel.entity.report.UserBriefing;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface UserBriefingMapper extends Mapper<UserBriefing> {
	
	
	
	/****
	 * 
	     * @方法名：selectMyUserBriefingList
	     * @描述： 查询简报--前后端
	     * @作者： lijing
	     * @日期： 2019年7月24日
	 */
	List<UserBriefing> selectMyUserBriefingList(@Param("userId")String userId,
	                                            @Param("qurtyDate")String qurtyDate,
	                                            @Param("teamId")String teamId,
	                                            @Param("scenicId")String scenicId);


	/****
	 * 
	     * @方法名：deleteUserBriefingByIds
	     * @描述： 批量删除
	     * @作者： lijing
	     * @日期： 2019年7月24日
	 */
   void deleteUserBriefingByIds(@Param("id")String[] id);

   /**
   　* @description: TODO  根据简报ID查询简报详情
   　* @param [id]
   　* @return com.liaoyin.travel.entity.report.UserBriefing
   　* @throws
   　* @author privatePanda777@163.com
   　* @date 2019/7/31 09:56
   　*/
   UserBriefing selectUserBriefingDetailsById(@Param("id") String id);
   List<FileUpload> selectFileById(@Param("id") String id);
}