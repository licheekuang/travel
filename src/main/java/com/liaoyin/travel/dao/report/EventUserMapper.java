package com.liaoyin.travel.dao.report;

import com.liaoyin.travel.entity.report.EventUser;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface EventUserMapper extends Mapper<EventUser> {
}