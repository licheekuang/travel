package com.liaoyin.travel.dao.report;

import com.liaoyin.travel.entity.report.EventReport;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface EventReportMapper extends Mapper<EventReport> {


    List<EventReport> selectEventList(@Param("eventType") Integer eventType, @Param("queryType") Integer queryType,
                                      @Param("userId") String userId, @Param("eventTime") String eventTime);

    List<EventReport> selectEventListByCondition(@Param("eventType") Integer eventType, @Param("queryType") Integer queryType,
                                                 @Param("userId") String userId, @Param("eventTime") String eventTime,
                                                 @Param("startTime") String startTime, @Param("endTime") String endTime,
                                                 @Param("reportPersonList") List<String> reportPersonList,
                                                 @Param("eventProgress") Integer eventProgress,@Param("scenicId") String scenicId);

    /**
     * 　* @description: TODO  根据时间查询事件总数 1.yyyy   2.MM   3.dd
     * 　* @param [time, queryType]
     * 　* @return java.lang.Integer
     * 　* @throws
     * 　* @author privatePanda777@163.com
     * 　* @date 2019/8/17 20:14
     */
    Integer selectTotalEventByTime(@Param("time") String time, @Param("queryType") String queryType);

    Integer selectBadEventByTime(@Param("time") String time, @Param("queryType") String queryType);

    Integer selectAlarmEventByTime(@Param("time") String time, @Param("queryType") String queryType);

    List<String> selectDataYear();

    List<String> selectDataMonth();

    /**
     * @方法名：selectEventListByReportPerson
     * @描述： 根据上报人id、事件类型、上报事件查询事件列表
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2019/11/5 10:44
     */
    List<EventReport> selectEventListByReportPerson(@Param("eventType") Integer eventType,
                                                    @Param("userId") String userId,
                                                    @Param("eventTime") String eventTime,
                                                    @Param("scenicId") String scenicId);


}