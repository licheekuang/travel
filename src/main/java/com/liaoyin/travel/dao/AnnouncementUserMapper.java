package com.liaoyin.travel.dao;

import com.liaoyin.travel.entity.AnnouncementUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface AnnouncementUserMapper extends Mapper<AnnouncementUser> {

    int deleteByAnnounceId(@Param("announceId") String announceId);

    int deleteByAnnounceIds(@Param("ids") String ids);
}