package com.liaoyin.travel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.RoleMenu;

import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface RoleMenuMapper extends Mapper<RoleMenu> {

	/**
	 * @方法名：deleteRoleMenuByMenuId
	 * @描述： 根据菜单id删除
	 * @作者： li.jing
	 * @日期： 2017年12月08日 17:28:40
	 */
	void deleteRoleMenuByMenuId(@Param("menuId")String menuId);

	/**
	 * @方法名：deleteRoleMenuByMenuIdS
	 * @描述： 根据菜单id和角色id批量删除
	 * @作者： li.jing
	 * @日期： 2017年12月08日 17:28:40
	 */
	void deleteRoleMenuByMenuIdS(@Param("menuIdS")String[] menuIdS,@Param("roleId")String roleId);

	/**
	 * @方法名：selectMenuByMenuId
	 * @描述： 根据角色id查询列表
	 * @作者： li.jing
	 * @日期： 2017年12月09日 17:28:40
	 */
	List<RoleMenu> selectMenuByMenuId(@Param("roleId")String roleId);

	/**
	 * @方法名：insertRoleMenuByroleId
	 * @描述： 根据角色id查询列表
	 * @作者： li.jing
	 * @日期： 2017年12月09日 17:28:40
	 */
	void insertRoleMenuByroleId(@Param("list")List<RoleMenu> insertMenuList);
	

	/**
	 * @方法名：deleteRoleMenuByroleId
	 * @描述： 根据角色id删除权限
	 * @作者： li.jing
	 * @日期： 2017年12月09日 17:28:40
	 */
	void deleteRoleMenuByroleId(@Param("roleId")String roleId);


	
}