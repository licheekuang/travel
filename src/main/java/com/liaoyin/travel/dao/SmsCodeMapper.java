package com.liaoyin.travel.dao;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.SmsCode;

import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
public interface SmsCodeMapper extends Mapper<SmsCode> {
	
    SmsCode selectByPhoneAndFunction(@Param("phone") String phone, @Param("functionCode") String functionCode);
    
    
}