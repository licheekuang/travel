package com.liaoyin.travel.dao.scenicInfo;

import com.liaoyin.travel.entity.scenicInfo.ScenicLowerLevelAdminMenu;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface LowerLevelAdminMenuMapper extends Mapper<ScenicLowerLevelAdminMenu> {

    int deleteLowerLevelAdminMenuByBackUserId(String backUserId);

    List<ScenicLowerLevelAdminMenu> selectBackUserThisMenuByUserId(String backUserId);
}