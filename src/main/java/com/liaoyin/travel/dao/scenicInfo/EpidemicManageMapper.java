package com.liaoyin.travel.dao.scenicInfo;

import com.liaoyin.travel.entity.scenicInfo.EpidemicManage;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface EpidemicManageMapper extends Mapper<EpidemicManage> {

    /**
     * @方法名：selectEpidemicInfo
     * @描述： 查询疫情管理信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 23:08
     */
    List<EpidemicManage> selectEpidemicInfo(String scenicId);

    /**
     * @方法名：updateEpidemicManage
     * @描述： 启用/禁用 疫情管理
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 23:24
     */
    int updateEpidemicManage(@Param("epidemicId") String epidemicId, @Param("isUsing") String isUsing);

    /**
     * @方法名：selectEpidemicManageById
     * @描述： 根据id查询疫情管理详情
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/23 12:25
     */
    EpidemicManage selectEpidemicManageById(String id);

    /**
     * @方法名：deleteEpidemicManageByIds
     * @描述： 根据ids批量删除疫情管理
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/23 12:32
     */
    int deleteEpidemicManageByIds(@Param("ids") String[] ids);

    /**
     * @方法名：updateEpidemicManageSelective
     * @描述： 更新疫情管理信息
     * @作者： kjz
     * @日期： Created in 2020/2/24 11:03
     */
    int updateEpidemicManageSelective(EpidemicManage epidemicManage);
}