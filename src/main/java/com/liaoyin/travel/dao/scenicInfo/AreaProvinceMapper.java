package com.liaoyin.travel.dao.scenicInfo;

import com.liaoyin.travel.entity.scenicInfo.AreaProvince;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AreaProvinceMapper extends Mapper<AreaProvince> {

    /**
     * @方法名：selectAreaProvince
     * @描述： 查询省级区域
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 14:14
     */
    List<AreaProvince> selectAreaProvince();
}