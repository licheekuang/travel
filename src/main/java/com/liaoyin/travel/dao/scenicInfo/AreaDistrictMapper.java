package com.liaoyin.travel.dao.scenicInfo;

import com.liaoyin.travel.entity.scenicInfo.AreaDistrict;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AreaDistrictMapper extends Mapper<AreaDistrict> {

    /**
     * @方法名：selectAreaDistrictByCityId
     * @描述： 根据市级区域id查询区县区域信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 14:37
     */
    List<AreaDistrict> selectAreaDistrictByCityId(String cityId);
}