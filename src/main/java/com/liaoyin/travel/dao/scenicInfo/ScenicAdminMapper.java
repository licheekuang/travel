package com.liaoyin.travel.dao.scenicInfo;

import com.liaoyin.travel.entity.scenicInfo.ScenicAdmin;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ScenicAdminMapper extends Mapper<ScenicAdmin> {

    /**
     * @方法名：updateScenicAdminByScenicId
     * @描述： 根据景区id更新景区管理员信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 20:05
     */
    int updateScenicAdminByScenicId(ScenicAdmin scenicAdmin);

    /**
     * @方法名：selectScenicAdminByAccount
     * @描述： 根据账户查询未审核的管理员信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 20:37
     */
    List<ScenicAdmin> selectScenicAdminByAccount(String account);

    /**
     * @方法名：selectScenicAdminByPhone
     * @描述： 根据联系电话查询未审核的管理员信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 20:44
     */
    List<ScenicAdmin> selectScenicAdminByPhone(String phone);

    /**
     * @方法名：selectScenicAdminByIdentityCard
     * @描述： 根据身份证号查询景区管理员信息
     * @作者： kjz
     * @日期： Created in 2020/2/24 17:34
     */
    List<ScenicAdmin> selectScenicAdminByIdentityCard(@Param("identityCard") String identityCard, @Param("scenicId") String scenicId);

    Integer countScenicAdminByCount(@Param("account")String account,@Param("scenicId")String scenicId);
}