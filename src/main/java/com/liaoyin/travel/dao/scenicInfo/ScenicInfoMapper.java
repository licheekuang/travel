package com.liaoyin.travel.dao.scenicInfo;

import com.liaoyin.travel.view.scenic.ScenicInfoView;
import com.liaoyin.travel.entity.scenicInfo.ScenicInfo;
import com.liaoyin.travel.vo.scenic.SelectScenicVo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface ScenicInfoMapper extends Mapper<ScenicInfo> {

    /**
     * @方法名：selectScenicInfoByTermAndOne
     * @描述： 按条件查询已经通过审核的景区管理信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/21 23:29
     */
    List<ScenicInfoView> selectScenicInfoByTermAndOne(SelectScenicVo selectScenicVo);

    /**
     * @方法名：selectScenicInfoByTermAndzeroOrTwo
     * @描述： 按条件查询未审核或者拒绝通过的景区管理信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 0:16
     */
    List<ScenicInfoView> selectScenicInfoByTermAndzeroOrTwo(SelectScenicVo selectScenicVo);

    /**
     * @方法名：selectScenicInfoViewByIdAndStatusOne
     * @描述： 根据id查询已通过审核的景区详情
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 1:06
     */
    ScenicInfoView selectScenicInfoViewByIdAndStatusOne(String scenicId);

    /**
     * @方法名：selectScenicInfoViewByIdAndStatusZeroOrTwo
     * @描述： 根据id查询未审核或被拒绝通过的景区详情
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 1:07
     */
    ScenicInfoView selectScenicInfoViewByIdAndStatusZeroOrTwo(String scenicId);

    /**
     * @方法名：updateAuditStatusToTwoById
     * @描述： 拒绝通过审核
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 10:54
     */
    int updateAuditStatusToTwoById(String scenicId);

    /**
     * @方法名：deleteScenicInfoByIds
     * @描述： 根据ids删除景区信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 12:08
     */
    int deleteScenicInfoByIds(@Param("ids") String[] ids);

    /**
     * @方法名：deleteScenicAdminByIds
     * @描述： 根据景区id删除景区管理员信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 12:09
     */
    int deleteScenicAdminByIds(@Param("ids") String[] ids);

    /**
     * @方法名：deleteBackUserByScenicId
     * @描述： 根据景区id删除后台用户信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 12:11
     */
    int deleteBackUserByScenicId(@Param("ids") String[] ids);

    /**
     * @方法名：updateAuditStatusToOneById
     * @描述： 通过审核
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 12:29
     */
    int updateAuditStatusToOneById(@Param("scenicId") String scenicId, @Param("backUserId") String backUserId);

    /**
     * @方法名：deleteUsersByScenicId
     * @描述： 根据景区id删除移动端用户信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/23 11:07
     */
    int deleteUsersByScenicId(@Param("ids") String[] ids);

    /**
     * @方法名：selectAuditStatusByScenicId
     * @描述： 通过景区id查询景区审核状态
     * @作者： kjz
     * @日期： Created in 2020/2/24 13:32
     */
    String selectAuditStatusByScenicId(String scenicId);

    /**
     * @方法名：selectBackUserIdByScenicId
     * @描述： 通过景区id查询后台用户的id
     * @作者： kjz
     * @日期： Created in 2020/2/24 13:42
     */
    String selectBackUserIdByScenicId(String scenicId);

    /**
     * @方法名：selectScenicInfoById
     * @描述： 通过景区id查询景区信息
     * @作者： kjz
     * @日期： Created in 2020/2/24 17:38
     */
    ScenicInfo selectScenicInfoById(String scenicId);

    /**
     * @方法名：updateScenicInfoSelective
     * @描述： 更新景区信息
     * @作者： kjz
     * @日期： Created in 2020/2/25 0:27
     */
    int updateScenicInfoSelective(ScenicInfo scenicInfo);

    /**
     * @方法名：updateAuditStatusToZeroById
     * @描述： 重置景区账号信息未审核状态
     * @作者： kjz
     * @日期： Created in 2020/2/25 0:39
     */
    int updateAuditStatusToZeroById(String scenicId);

    /**
     * @方法名：selectIsPassedByScenicId
     * @描述： 查询是否已经通过了一次，并重新申请
     * @作者： kjz
     * @日期： Created in 2020/2/25 1:16
     */
    String selectIsPassedByScenicId(String scenicId);

    /**
     * @方法名：deleteVisualRangeSettingByScenicInfoId
     * @描述： 根据景区id删除审批范围设置
     * @作者： kjz
     * @日期： Created in 2020/2/27 23:28
     */
    int deleteVisualRangeSettingByScenicInfoId(@Param("ids") String[] ids);

    /**
     * @方法名：selectScenicNameByScenicId
     * @描述： 通过单位id查询单位名字
     * @作者： kjz
     * @日期： Created in 2020/3/3 16:12
     */
    String selectScenicNameByScenicId(String scenicId);

    Integer selectScenicInfoByEmergencyId(@Param("emergencyId")String emergencyId,@Param("id")String id);

    Integer countScenicInfoByScenicName(@Param("scenicName")String scenicName,@Param("id")String id);
}