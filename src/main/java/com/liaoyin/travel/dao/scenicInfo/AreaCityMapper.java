package com.liaoyin.travel.dao.scenicInfo;

import com.liaoyin.travel.entity.scenicInfo.AreaCity;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AreaCityMapper extends Mapper<AreaCity> {

    /**
     * @方法名：selectAreaCityByProvinceId
     * @描述： 根据省级编码查询市级区域
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/22 14:30
     */
    List<AreaCity> selectAreaCityByProvinceId(String provinceId);

    /**
     * @方法名：selectAreaCityByDistrictId
     * @描述： 通过区域id查询市级区域信息
     * @作者： Kuang.JiaZhuo
     * @日期： Created in 2020/2/23 13:00
     */
    AreaCity selectAreaCityByDistrictId(String districtId);
}