package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.entity.task.TaskExceptionDescription;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TaskExceptionDescriptionMapper extends Mapper<TaskExceptionDescription> {


    List<TaskExceptionDescription> selectExceptionTaskList(@Param("exceptionType") Integer exceptionType,
                                                           @Param("teamId")String teamId,
                                                           @Param("scenicId")String scenicId);

    int deleteTaskExceptionDescription(@Param("ids") String[] ids);

    List<TaskExceptionDescription> selectNewExceptionTaskList(@Param("exceptionType") Integer exceptionType,
                                                              @Param("userId") String userId,
                                                              @Param("scenicId") String scenicId,
                                                              @Param("taskName") String taskName);
}