package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.entity.task.TaskTiming;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
public interface TaskTimingMapper extends Mapper<TaskTiming> {

    @MapKey("pubWeek")
    Map<String, TaskTiming> selectTaskTimingbyTimingTaskParameterId(String timingTaskParameterId);

    List<TaskTiming> selectTaskTimingByTaskId(@Param("timedTaskId") String timedTaskId, @Param("scenicId") String scenicId);

    int deleteTaskTimingByTaskIds(@Param("taskIds")List<String> taskIds);
}