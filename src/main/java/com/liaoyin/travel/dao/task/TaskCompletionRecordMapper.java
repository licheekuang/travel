package com.liaoyin.travel.dao.task;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.task.TaskCompletionRecord;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface TaskCompletionRecordMapper extends Mapper<TaskCompletionRecord> {
	
	/***
	 * 
	     * @方法名：selectTaskCompletionRecordByTaskIdAndDetailsIdAndUserId
	     * @描述： 根据任务id，详情id，用户id 查询任务完成情况
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	TaskCompletionRecord selectTaskCompletionRecordByTaskIdAndDetailsIdAndUserId(
			@Param("taskId")String taskId,@Param("detailsId")String detailsId,@Param("userId")String userId);
}