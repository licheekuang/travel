package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.vo.task.SelectTaskListVo;
import com.liaoyin.travel.entity.task.Task;
import com.liaoyin.travel.entity.task.TaskReceivingRecords;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TaskReceivingRecordsMapper extends Mapper<TaskReceivingRecords> {


	/****
	 *
	 * @方法名：updateTaskReceivingRecords
	 * @描述： 修改任务领取
	 * @作者： lijing
	 * @日期： 2019年7月15日
	 */
	int updateTaskReceivingRecords(TaskReceivingRecords taskReceivingRecords);

	/****
	 *
	 * @方法名：selectTaskReceivingRecordsByTaskId
	 * @描述： 根据任务id查询 任务领取记录
	 * @作者： lijing
	 * @日期： 2019年7月16日
	 */
	List<TaskReceivingRecords> selectTaskReceivingRecordsByTaskId(@Param("taskId") String taskId,
																  @Param("userId") String userId,
																  @Param("tastStatus") String tastStatus);


	List<Task> selectTaskByUserNow(@Param("userId") String userId);


	/***
	 *
	 * @方法名：selectTaskReceivingRecordsByTaskId
	 * @描述： 查询任务领取列表
	 * @作者： lijing
	 * @日期： 2019年8月26日
	 */
	List<TaskReceivingRecords> selectTaskReceivingRecordsByTaskIdBack(SelectTaskListVo selectTaskListVo);

	/**
	 * 　* @description: TODO  查询是否有未执行的紧急任务
	 * 　* @param [userId]
	 * 　* @return java.util.List<com.liaoyin.travel.entity.task.TaskReceivingRecords>
	 * 　* @throws
	 * 　* @author privatePanda777@163.com
	 * 　* @date 2019/8/29 16:10
	 */
	List<TaskReceivingRecords> selectTaskUrgentByUserId(@Param("userId") String userId);

	/**
	 * @方法名：selectTaskReceivingRecordsByTastStatus
	 * @描述： 按用户id(多个)和任务的状态查询任务领取记录
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/10/28 17:42
	 */
	List<TaskReceivingRecords> selectTaskReceivingRecordsByTaskStatus(@Param("userIdList") List<String> userIdList, @Param("taskStatus") int taskStatus);

	/**
	 * @方法名：selectTaskReceivingRecordsByWorkId
	 * @描述： 返回指定的工种中人员按不同状态查询任务的领取记录
	 * @作者： Kuang.JiaZhuo
	 * @日期： Created in 2019/10/28 19:19
	 */
    List<TaskReceivingRecords> selectTaskReceivingRecordsByWorkId(@Param("workIdList") List<String> workIdList, @Param("taskStatus") int taskStatus);

    int countTaskReceivingRecordsByTaskId(String taskId);

}