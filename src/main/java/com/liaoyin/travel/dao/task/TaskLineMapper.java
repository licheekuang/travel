package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.entity.task.TaskLine;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TaskLineMapper extends Mapper<TaskLine> {
	
	
	/***
	 * 
	     * @方法名：selectTaskLineBackList
	     * @描述： 查询任务线路列表
	     * @作者： lijing
	     * @日期： 2019年7月26日
	 */
	 List<TaskLine> selectTaskLineBackList(@Param("lineName")String linekName,@Param("scenicId")String scenicId);
	 
	 /***
		 * 
		     * @方法名：deleteTaskLineByIdOnBack
		     * @描述： 根据id删除任务线路-后台
		     * @作者： lijing
		     * @日期： 2019年7月26日
		 */
	 int deleteTaskLineByIdOnBack(@Param("id")String[] id);

    Integer selectCountByLineName(@Param("lineName") String lineName, @Param("id") String id);
}