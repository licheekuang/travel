package com.liaoyin.travel.dao.task;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.task.TaskBusiness;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface TaskBusinessMapper extends Mapper<TaskBusiness> {
	
	
	/***
	 * 
	     * @方法名：selectTaskBusinessList
	     * @描述： 根据任务id查询指定的领取人
	     * @作者： lijing
	     * @日期： 2019年7月25日
	 */
	List<TaskBusiness> selectTaskBusinessList(@Param("taskId")String taskId);
}