package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.view.moble.task.TimedTaskBackView;
import com.liaoyin.travel.view.moble.task.TimedTaskView;
import com.liaoyin.travel.view.travelMobileControlSystem.TaskAddView;
import com.liaoyin.travel.vo.task.SelectTimedTaskVo;
import com.liaoyin.travel.vo.task.TimingTaskParameterVo;
import com.liaoyin.travel.entity.task.TimingTaskParameter;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TimingTaskParameterMapper extends Mapper<TimingTaskParameter> {

    List<TimingTaskParameter> selectTimingTaskParameterByCondition(TimingTaskParameterVo timingTaskParameterVo);


    List<TimedTaskView> selectTimingTaskViewByWeekDay(String weekDay);

    List<TimedTaskView> selectTimingTaskViewNow(String time);

    List<TimedTaskBackView> selectTimedTaskBackList(SelectTimedTaskVo selectTimedTaskVo);

    TimedTaskBackView selectTimedTaskById(String timedTaskId);

    int delTaskParameter(String id);

    int delTaskTiming(String id);

    int delTaskReceiver(String id);

    Integer countTimingTaskParameter();

    List<TaskAddView> getTaskAddViewList();

    int disabledTimingTaskByUserId(String userId);

    int deleteTaskParameterByIds(@Param("ids")List<String> ids);
}