package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.view.moble.task.TemporaryTaskManageView;
import com.liaoyin.travel.vo.task.SelectTaskListVo;
import com.liaoyin.travel.vo.task.TemporaryTaskManageVo;
import com.liaoyin.travel.entity.BackUser;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.task.Task;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
@org.apache.ibatis.annotations.Mapper
public interface TaskMapper extends Mapper<Task> {
	
	
	/****
	 * 
	     * @方法名：selectTaskListByEmployee
	     * @描述： 查询任务列表--员工端
	     * @作者： lijing
	     * @日期： 2019年7月12日
	 */
	List<Task> selectTaskListByEmployee(SelectTaskListVo selectTaskListVo);
	/****
	 * 
	 * @方法名：selectTaskListByEmployee
	 * @描述： 查询任务列表--管理端
	 * @作者： lijing
	 * @日期： 2019年7月12日
	 */
	List<Task> selectTaskListByManagement(SelectTaskListVo selectTaskListVo);
	/****
	 * 
	 * @方法名：selectTaskListOnBack
	 * @描述： 后台查询任务列表
	 * @作者： lijing
	 * @日期： 2019年7月12日
	 */
	List<Task> selectTaskListOnBack(SelectTaskListVo selectTaskListVo);
	
	/****
	 * 
	     * @方法名：selectTaskByUserId
	     * @描述： 根据任务id和用户id查询用户任务
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	List<Task> selectTaskByUserId(@Param("id")String id,@Param("userId")String userId);
	
	
	/****
	 * 
	     * @方法名：deleteTaskByIdOnBack
	     * @描述： 根据id删除任务-后台
	     * @作者： lijing
	     * @日期： 2019年7月16日
	 */
	void deleteTaskByIdOnBack(@Param("id")String[] id);
	
	
	/******
	 * 
	     * @方法名：selectTaskUserList
	     * @描述： 查询用户已经领取的任务
	     * @作者： lijing
	     * @日期： 2019年7月23日
	     * @param staus:【字典tastStatus】
	 */
	List<Task> selectTaskUserList(@Param("userId")String userId,@Param("status")String status,@Param("taskType")String taskType,
					@Param("taskLevel")String taskLevel,@Param("taskCategories")String taskCategories,@Param("taskName")String taskName);

	/**
	　* @description: TODO   查询所有空闲员工
	　* @param []
	　* @return java.lang.String
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/8/12 14:23
	　*/
	List<String> selectIdleEmployeeId();

	String selectNickNameByUsers(String uid);
	String selectNickNameByBackUser(String uid);


	Integer selectTaskDataByMonth(@Param("time") String time,@Param("taskType") Integer taskType,@Param("userId") String userId);

	List<String> getDataTaskYear();

	BackUser selectBackUserById(String userId);

	Users selectUsersById(String userId);

	List<TemporaryTaskManageView> selectTemporaryTaskManageByCondition(TemporaryTaskManageVo temporaryTaskManageVo);
}