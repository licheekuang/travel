package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.entity.task.TimingTaskSpecificReleaseTime;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TimingTaskSpecificReleaseTimeMapper extends Mapper<TimingTaskSpecificReleaseTime> {

    List<TimingTaskSpecificReleaseTime> selectTimingTaskSpecificReleaseTimeByTimedTaskId(@Param("timedTaskId") String timedTaskId, @Param("scenicId") String scenicId);

    int deleteTaskSpecificReleaseTimeByTaskIds(@Param("taskIds")List<String> taskIds);
}