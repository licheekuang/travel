package com.liaoyin.travel.dao.task;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.task.TaskDetails;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface TaskDetailsMapper extends Mapper<TaskDetails> {
	
	/*****
	 * 
	     * @方法名：selectTaskDetailsListByTaskId
	     * @描述： 根据任务id查询任务详情
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	List<TaskDetails> selectTaskDetailsListByTaskId(@Param("taskId")String taskId,@Param("queryType")Integer queryType);
	
	/*****
	 * 
	     * @方法名：selectMaxTaskDetailsTaskFlow
	     * @描述： 查询任务最大的流程
	     * @作者： lijing
	     * @日期： 2019年7月15日
	 */
	Integer selectMaxTaskDetailsTaskFlow(@Param("taskId")String taskId);

	/**
	　* @description: TODO    根据任务ID查询任务详情及任务路线
	　* @param [taskId]
	　* @return com.liaoyin.travel.entity.task.TaskDetails
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/7/26 13:00
	　*/
	TaskDetails selectTaskDetailByTid(@Param("taskId")String taskId);
}