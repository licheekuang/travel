package com.liaoyin.travel.dao.task;

import com.liaoyin.travel.entity.task.TimingTaskDesignatedReceiver;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface TimingTaskDesignatedReceiverMapper extends Mapper<TimingTaskDesignatedReceiver> {

    int deleteTaskDesignatedReceiverByTaskIds(@Param("taskIds")List<String> taskIds);
}