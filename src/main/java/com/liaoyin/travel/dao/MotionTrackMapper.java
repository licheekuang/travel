package com.liaoyin.travel.dao;

import com.liaoyin.travel.entity.MotionTrack;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface MotionTrackMapper extends Mapper<MotionTrack> {


   List<MotionTrack> selectMotionTrackByUserId(@Param("userId") String userId);

   List<MotionTrack> selectMotionTrackByUserIdAndTime(@Param("userId") String userId,
                                                      @Param("startTime") String startTime,
                                                      @Param("endTime") String endTime);

    int deleteMotionTrackByUserIds(@Param("userIds") String[] userIds);

    MotionTrack selectMotionTrackLastTimeByUserID(String userId);
}