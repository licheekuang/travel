package com.liaoyin.travel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.RoleFunc;

import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface RoleFuncMapper extends Mapper<RoleFunc> {

	/**
	 * @方法名：selectFunctionByroleId
	 * @描述： 根据角色id查询列表
	 * @作者： li.jing
	 * @日期： 2017年12月09日 17:28:40
	 */
	List<RoleFunc> selectFunctionByroleId(@Param("roleId")String roleId);

	/**
	 * @方法名：insertFunctionByroleId
	 * @描述： 新增角色--功能  关联数据
	 * @作者： li.jing
	 * @日期： 2017年12月09日 17:28:40
	 */
	void insertFunctionByroleId(@Param("list")List<RoleFunc> insertfunList);

	/**
	 * @方法名：deleteFunctionByroleId
	 * @描述： 根据角色id删除此角色已有的功能
	 * @作者： li.jing
	 * @日期： 2017年12月09日 17:28:40
	 */
	void deleteFunctionByroleId(@Param("roleId")String roleId);
	
}