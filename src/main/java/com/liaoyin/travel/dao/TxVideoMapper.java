package com.liaoyin.travel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.TxVideo;

import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
public interface TxVideoMapper extends Mapper<TxVideo> {
	
	/**
     * @method: selectTxVideoListByVideoId
     * @Description: 根据videoId获取视频列表
     * @param: []
     * @return: java.util.List<com.ly.prizeclaw.entity.TxVideo>
     * @author: zhang.zhipeng
     * @Date: 2018/5/29 10:35
     **/
	List<TxVideo> selectTxVideoListByVideoId(@Param("videoId")String videoId);
}