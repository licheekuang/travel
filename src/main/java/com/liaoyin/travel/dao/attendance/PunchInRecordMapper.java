package com.liaoyin.travel.dao.attendance;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.liaoyin.travel.entity.attendance.PunchInRecord;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface PunchInRecordMapper extends Mapper<PunchInRecord> {
	
	
	/***
	 * 
	     * @方法名：selectPunchInRecordListToday
	     * @描述： 获取用户当日打卡的记录
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	List<PunchInRecord> selectPunchInRecordListToday(@Param("userId")String userId,@Param("time") String time);
	
	
	
	/****
	 * 
	     * @方法名：selectPunchInRecordListOnMoble
	     * @描述： 我的打卡记录
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	List<PunchInRecord> selectPunchInRecordListOnMoble(@Param("checkTime")String checkTime,@Param("userId")String userId);

	/**
	　* @description: TODO  查询指定用户的考勤记录
	　* @param [startTime, endTime, userId]
	　* @return java.util.List<com.liaoyin.travel.entity.attendance.PunchInRecord>
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/7/31 11:33
	　*/
	List<PunchInRecord> selectPunchInRecordListOnBack(@Param("startTime") String startTime,
													  @Param("endTime")String endTime,@Param("userId")String userId);

	/**
	　* @description: TODO  获取部门所以人的打卡记录
	　* @param [teamIds, checkTime]
	　* @return java.util.List<com.liaoyin.travel.entity.attendance.PunchInRecord> 
	　* @throws
	　* @author privatePanda777@163.com
	　* @date 2019/8/30 09:40 
	　*/
	List<PunchInRecord> selectTeamPunchInRecordListOnMoble(@Param("teamIds") String teamIds,@Param("checkTime") String checkTime);

	List<PunchInRecord> selectTeamPunchInRecordList(@Param("teamIds") String teamIds,@Param("checkTime") String checkTime);

	List<PunchInRecord> selectTeamPunchInRecordListByAtt(@Param("startTime") String startTime,
														 @Param("endTime")String endTime,@Param("userId")String userId);
}