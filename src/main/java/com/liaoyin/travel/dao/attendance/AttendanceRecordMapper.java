package com.liaoyin.travel.dao.attendance;

import com.liaoyin.travel.vo.attendance.*;
import com.liaoyin.travel.vo.request.AttendanceRecordRequestVO;
import com.liaoyin.travel.vo.request.MobileAttendanceStatRequestVO;
import com.liaoyin.travel.entity.Users;
import com.liaoyin.travel.entity.attendance.AttendanceRecord;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
public interface AttendanceRecordMapper extends Mapper<AttendanceRecord> {

    AttendanceRecord selectAttendanceRecordByUserIdAndDateOfAttendance(@Param("userId") String userId, @Param("dateOfAttendance") LocalDate dateOfAttendance);

    List<AttendanceRecord> selectLackOfCardRecordByUserId(String userId);

    List<AttendanceRecord> getLackRecord(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO);

    int onTheMorningOfCard(String attendanceRecordId);

    int onTheAfternoonOfCard(String attendanceRecordId);

    AttendanceRecord selectAttendanceRecordById(String id);

    int updateFinalAttendanceStatusSetOne(String attendanceRecordId);

    int updateFinalAttendanceStatusSetSix(String attendanceRecordId);

    int updateFinalAttendanceStatusSetThree(String attendanceRecordId);

    @Override
    int updateByPrimaryKeySelective(AttendanceRecord attendanceRecord);

    int getLeaveCntByDate(@Param("date") String date,@Param("userId") String userId);

    int getOutCntByDate(@Param("date") String date,@Param("userId") String userId);

    List<AttanceRecordVO> getAttanceRecordList(AttendanceRecordRequestVO attendanceRecordRequestVO);

    List<AttendanceStatVO> getAttendanceStatList(AttendanceRecordRequestVO attendanceRecordRequestVO);

    MobileAttendanceStatVO getMobileAttendanceStat(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO);

    /**
     * @author 王海洋
     * @methodName: 正常考勤次数，最终状态为正常范围内
     * @create 2019-12-02 17:01
     **/
    List<String> getNormalAttendanceCnt(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO);

    /**
     * @author 王海洋
     * @methodName: 异常考勤次数，最终状态为异常范围内
     * @create 2019-12-02 17:03
     **/
    int getAbnormalAttendanceCnt(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO);

    List<MobileAttendanceRecordVO> getRecordList(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO);

    List<Map<String,String>> getAttendanceRecordByType(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO);

    int updateFinalAttendanceStatusSetTwo(String attendanceRecordId);

    int updateAttendanceRecordSelective(AttendanceRecord ordAttendanceRecord);

    UserAttendanceRecordVO getUserAttendanceRecordVO(@Param("userId") String userId,@Param("time") String time);

    UserAttendanceRecordVO getUserAttendanceTime(@Param("userId") String userId);

    int getAttenceCnt(@Param("users") List<Users>users,@Param("time") String time);

    int updateFinalAttendanceStatusSetEight(String attendanceRecordId);

    List<AttanceRecordVO> selectAttendanceRecordListById(@Param("startTime") String startTime,
                                                         @Param("endTime") String endTime,
                                                         @Param("userId") String userId,
                                                         @Param("scenicId") String scenicId);


    String getAttendanceCntByMobileAttendanceStatRequestVO(MobileAttendanceStatRequestVO mobileAttendanceStatRequestVO);

    int deleteAttendanceRecordByUserId(@Param("userIds") String[] userIds);
}