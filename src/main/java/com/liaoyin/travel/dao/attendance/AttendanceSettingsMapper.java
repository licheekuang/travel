package com.liaoyin.travel.dao.attendance;

import com.liaoyin.travel.vo.attendance.AttendanceSettingVO;
import com.liaoyin.travel.entity.attendance.AttendanceSettings;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AttendanceSettingsMapper extends Mapper<AttendanceSettings> {
	
	/****
	 * 
	     * @方法名：selectAttendanceSettingsListOnMoble
	     * @描述： 前端查询考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	List<AttendanceSettings> selectAttendanceSettingsList(@Param("checkName")String checkName,@Param("isUsing")String isUsing,@Param("teamId")String teamId);


	/****
	 * 
	     * @方法名：deleteAttendanceSettingsById
	     * @描述： 删除考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	void deleteAttendanceSettingsById(@Param("id")String[] id,@Param("userId")String userId);
	
	/****
	 * 
	     * @方法名：selectAttendanceSettingsByEquipmentId
	     * @描述： 根据设备id查询考勤设置
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	List<AttendanceSettings> selectAttendanceSettingsByEquipmentId(@Param("id")String[] id);
	
	
	/*****
	 * 
	     * @方法名：selectAttendanceSettingsByCheckShiftsAndTeamId
	     * @描述： 根据班次查询打卡设置
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	AttendanceSettings selectAttendanceSettingsByCheckShiftsAndTeamId(@Param("checkShifts")String checkShifts,@Param("teamId")String teamId);

	/**
	 * @author 王海洋
	 * @methodName: 获取当前最近的一次打卡信息
	 * @methodDesc:
	 * @description:
	 * @param: 
	 * @return 
	 * @create 2019-10-25 17:20
	 **/
	List<AttendanceSettingVO> getCurrentAttendanceSetting(@Param("userId") String userId);

	/****
	 * 
	     * @方法名：selectMAXcheckShifts
	     * @描述： 查询最大的班次
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	AttendanceSettings selectMAXcheckShifts(@Param("teamId")String teamId);
}