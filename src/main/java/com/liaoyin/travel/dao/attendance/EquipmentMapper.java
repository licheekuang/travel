package com.liaoyin.travel.dao.attendance;

import com.liaoyin.travel.entity.attendance.Equipment;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface EquipmentMapper extends Mapper<Equipment> {
	
	
	 /****
	  * 
	      * @方法名：selectEquipmentList
	      * @描述： 查询设备列表【带分页】
	      * @作者： lijing
	      * @日期： 2019年7月10日
	  */
	List<Equipment> selectEquipmentList(@Param("equipmentType")String equipmentType, @Param("equipmentName")String equipmentName,
	                                    @Param("isUsing")String isUsing,@Param("scenicId")String scenicId);
	
	
	/****
	 * 
	     * @方法名：deleteEquipmentById
	     * @描述： 删除设备
	     * @作者： lijing
	     * @日期： 2019年7月10日
	 */
	void deleteEquipmentById(@Param("id")String[] id);
}