package com.liaoyin.travel.dao.attendance;

import com.liaoyin.travel.view.mine.attendance.AttendanceStatisticsDetailedView;
import com.liaoyin.travel.view.mine.attendance.AttendanceStatisticsView;
import com.liaoyin.travel.entity.attendance.AttendanceStatistics;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AttendanceStatisticsMapper extends Mapper<AttendanceStatistics> {
	
	
	/****
	 * 
	     * @方法名：selectAttendanceStatisticsByUserToday
	     * @描述： 判断当日的打卡分类统计
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	List<AttendanceStatistics> selectAttendanceStatisticsByUserToday(@Param("userId")String userId,@Param("workType")String workType);

	/*****
	 * 
	     * @方法名：selectAttendanceStatisticsMap
	     * @描述： 我的考勤统计
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
	List<AttendanceStatisticsView> selectAttendanceStatisticsMap(@Param("userId")String userId,@Param("checkendTime")String checkendTime,@Param("checkStartTime")String checkStartTime);


	List<AttendanceStatisticsView> selectAttendanceStatisticsForAttendance(@Param("userId")String userId,@Param("checkendTime")String checkendTime,@Param("checkStartTime")String checkStartTime);


	/*****
	 * 
	     * @方法名：selectAttendanceStatisticsDetailedList
	     * @描述： 我的考勤统计明细
	     * @作者： lijing
	     * @日期： 2019年7月11日
	 */
    List<AttendanceStatisticsDetailedView> selectAttendanceStatisticsDetailedList(
			@Param("userId")String userId,@Param("checkendTime")String checkendTime,@Param("checkStartTime")String checkStartTime,@Param("workType")String workType);


    /**
     * @方法名：selectAttendanceStatisticsDetailedListByTime
     * @描述： 查询部门下所有员工的考勤记录根据日期
     * @作者： 周明智
     * @日期： Created in 2019/8/6 14:30
     */
	List<AttendanceStatisticsDetailedView> selectAttendanceStatisticsDetailedListByTime(
			@Param("temaId")String  temaId,
			@Param("time")String  time
	);
}