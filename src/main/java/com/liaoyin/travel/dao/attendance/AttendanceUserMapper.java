package com.liaoyin.travel.dao.attendance;

import com.liaoyin.travel.view.mine.attendance.AttendanceDistanceInfoView;
import com.liaoyin.travel.vo.attendance.AttendanceOldUser;
import com.liaoyin.travel.entity.attendance.AttendanceUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.time.LocalTime;
import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AttendanceUserMapper extends Mapper<AttendanceUser> {


    List<AttendanceOldUser> selectUserIdsByAttendanceTime(LocalTime attendanceStartTime, LocalTime attendanceEndTime);

    List<AttendanceOldUser> selectAttendanceOldUserByUserIds(@Param("list") List<String> list);

    AttendanceDistanceInfoView selectAttendanceDistanceInfoViewByUserId(@Param("userId") String userId, @Param("scenicId") String scenicId);

    int deleteAttendanceUsersByAttendanceId(String attendanceId);

    List<AttendanceUser> selectAttendanceUserByUserId(String userId);

    /**
     * @author 王海洋
     * @methodName: 删除参与考勤的用户
     * @methodDesc:
     * @description:
     * @param:
     * @return
     * @create 2019-12-04 10:07
     **/
    int deleteByParticipant(@Param("participantIds") String participantIds);


    AttendanceOldUser selectAttendanceUserByUserIdAndAttendanceId(@Param("userId") String userId,
                                                                  @Param("attendanceId") String attendanceId);

    int deleteAttendanceUsersByUserIds(@Param("ids") String[] ids);
}