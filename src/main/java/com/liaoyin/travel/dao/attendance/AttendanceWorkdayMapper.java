package com.liaoyin.travel.dao.attendance;

import com.liaoyin.travel.entity.attendance.AttendanceWorkday;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AttendanceWorkdayMapper extends Mapper<AttendanceWorkday> {

    AttendanceWorkday selectAttendanceWorkdayByAttendanceIdAndWhatDay(@Param("attendanceId") String attendanceId, @Param("whatDay") Integer whatDay);

    List<AttendanceWorkday> selectAttendanceWorkdayByUserId(String userId);
}