package com.liaoyin.travel.dao.attendance;

import com.liaoyin.travel.view.mine.attendance.AttendanceManagementView;
import com.liaoyin.travel.view.moble.attendance.ClockSettingsView;
import com.liaoyin.travel.view.travelMobileControlSystem.AttendanceView;
import com.liaoyin.travel.vo.attendance.AttendanceManageVo;
import com.liaoyin.travel.vo.attendance.AttendanceManagementVo;
import com.liaoyin.travel.entity.attendance.AttendanceManagement;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface AttendanceManagementMapper extends Mapper<AttendanceManagement> {


    List<AttendanceManagementView> selectAttendanceManagementViewByCondition(AttendanceManagementVo attendanceManagementVo);

    AttendanceManageVo selectAttendanceManageVoByUserId(String userId);

    ClockSettingsView selectClockSettingsViewByUserID(String userId);

    AttendanceManagement selectAttendanceManagementById(String id);

    AttendanceManagement selectAttendanceManagementByUserId(String userId);

    String selectManagementIdByUserId(String userId);

    int deleteAttendanceManagementById(String id);

    int updateById(AttendanceManagement attendanceManagement);

    List<AttendanceView> getAttendanceViewList();

    Integer countAttendanceManagement();

    String selectUserNamesByAttendanceId(String attendanceId);

    Integer selectCountByCheckingInName(@Param("checkingInName") String checkingInName,
                                        @Param("id") String id,
                                        @Param("scenicId") String scenicId);
}