package com.liaoyin.travel.common.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;

@Getter
@Setter
public class Page {
    /**
     * 第几页
     */
    @ApiModelProperty(value = "第几页")
    @Transient
    Integer num;
    /**
     * 每页显示多少行
     */
    @ApiModelProperty(value = "每页显示多少行")
    @Transient
    Integer size;
    /**
     * 排序字段 例如：orgType desc/asc
     */
    @ApiModelProperty(value = "排序字段 例如：orgType desc/asc")
    @Transient
    String orderby;
    /**
     * 是否是传参，作为传参时，不应该设置display
     */
    @Transient
    Boolean isParam = false;
}
