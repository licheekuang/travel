package com.liaoyin.travel.common.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import com.liaoyin.travel.base.entity.Page;

import javax.persistence.Transient;

/**
 * @项目名称：
 * @类名称：${type_name}
 * @类描述：${todo}
 * @author ：zhang.zhipeng
 * @时间：${date} ${time}
 * @版本信息：1.0
 */
@Data
public class Pages extends Page{

    @ApiModelProperty(value = "排序方式 desc asc")
    @Transient
    String orderWay;


    @Override
    public String getOrderby() {
        if(StringUtils.isEmpty(super.getOrderby())){
            return null;
        }
        if(StringUtils.isEmpty(orderWay)){
            //拼接排序字符串
            return super.getOrderby()+" DESC";
        }
        return super.getOrderby()+" "+this.getOrderWay();
    }
}
