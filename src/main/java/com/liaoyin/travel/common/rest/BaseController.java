package com.liaoyin.travel.common.rest;

import org.springframework.beans.factory.annotation.Autowired;

import com.liaoyin.travel.base.service.BaseService;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class BaseController<T extends BaseService,Entity> {
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected T baseService;
}
