package tk.mybatis.mapper.common;

import tk.mybatis.mapper.common.special.InsertListMapper;

public interface Mapper<T> extends
        BaseMapper<T>,
        ExampleMapper<T>,
        RowBoundsMapper<T>,
        InsertListMapper<T>,
        Marker {
}